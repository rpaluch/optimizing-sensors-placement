package graphs;

import java.util.ArrayList;

/**
 * Basic class for node for undirected network. 
 * @author Robert Paluch
 */

public class Node {
	
	/**
	 * Data fields
	 */
	
	int id;
	int index;
	int degree;
	int membership;
	double betweenness;
	double closeness;
	
	// Fields for nodal states
	int state; 
	double value;
	boolean flag;
	
	// Neighbours of given node
	ArrayList<Node> neighbours;
	
	// Neighbours of the node in other layers
	ArrayList<Node> interlayerNeighbours;
	int layerId;
	int indexM; // index of node in MultiLayerGraph
		
	/**
	 * Constructors
	 */
	
	public Node(int newId) {
		id = newId;
		index = 0;
		degree = 0;
		membership = 0;
		state = 0;
		flag = false;
		neighbours = new ArrayList<Node>();
	}
	
	public Node(int newId, int initialState) {
		id = newId;
		index = 0;
		degree = 0;
		membership = 0;
		state = initialState;
		flag = false;
		neighbours = new ArrayList<Node>();
	}
	
	public Node(int newId, ArrayList<Node> initialNeighbours) {
		id = newId;
		index = 0;
		degree = initialNeighbours.size();
		membership = 0;
		state = 0;
		flag = false;
		neighbours = new ArrayList<Node>();
		for(Node n : initialNeighbours) {
			neighbours.add(n);
		}
	}
	
	public Node(int newId, ArrayList<Node> initialNeighbours, int initialState) {
		id = newId;
		index = 0;
		degree = initialNeighbours.size();
		membership = 0;
		state = initialState;
		flag = false;
		neighbours = new ArrayList<Node>();
		for(Node n : initialNeighbours) {
			neighbours.add(n);
		}
	}
	
	/***
	 * Public methods
	 */
	
	// Return true if a neighbour is on the neighbour list
	public boolean isNeighbour(Node neighbour) {
		return neighbours.contains(neighbour);
	}
	
	// Return true if a neighbour is on the neighbour list
	public boolean isNeighbour(int neighbourId) {
		for(Node n : neighbours)
			if(n.getId() == neighbourId)
				return true;
		return false;
	}
	
	// Return true if a neighbour was added and false if the neighbour has been already added before
	public boolean addNeighbour(Node neighbour) {
		if(neighbour!=this && !neighbours.contains(neighbour)) {
			neighbours.add(neighbour);
			degree = degree + 1;
			return true;
		}
		else {
			return false;
		}
	}
	
	// Return true if a neighbour was removed and false if the neighbour haven't been on the list
	public boolean removeNeighbour(Node neighbour) {
		if(neighbours.remove(neighbour)) {
			degree = degree - 1;
			return true;
		}
		else {
			return false;
		}
	}
	
	public void removeAllNeighbours() {
		neighbours = new ArrayList<Node>();
		degree = 0;
	}
	
	public boolean addInterlayerNeighbour(Node neighbour) {
		if(interlayerNeighbours==null)
			interlayerNeighbours = new ArrayList<Node>();
		if(neighbour!=this && !interlayerNeighbours.contains(neighbour)) {
			interlayerNeighbours.add(neighbour);
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean removeInterlayerNeighbour(Node neighbour) {
		if(interlayerNeighbours.remove(neighbour)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public void removeAllInterlayerNeighbours() {
		interlayerNeighbours = null;
	}
		
	public void print() {
		System.out.printf("hashcode:%h, id:%d, membership:%d, degree=%d\n", this, id, membership, degree);
	}
	
	public void printWithLayerId() {
		System.out.printf("hashcode:%h, id:%d, layer:%d, degree=%d\n", this, id, layerId, degree);
	}
	
	public void printNeighbours() {
		for(Node n : neighbours) System.out.printf("%d ", n.getId());
		System.out.println();
	}
	
	/***
	 * Getters and setters
	 */
	
	public int getId() {
		return id;
	}
	
	public int getIndex() {
		return index;
	}
	
	public int getDegree() {
		return degree;
	}
	
	public int getMembership() {
		return membership;
	}
	
	public double getBetweenness() {
		return betweenness;
	}
	
	public double getCloseness() {
		return closeness;
	}
	
	public int getState() {
		return state;
	}
	
	public double getValue() {
		return value;
	}
	
	public boolean getFlag() {
		return flag;
	}
	
	public int getLayerId() {
		return layerId;
	}
	
	public int getIndexM() {
		return indexM;
	}
	
	public ArrayList<Node> getNeighbours() {
		return neighbours;
	}
	
	public ArrayList<Node> getInterlayerNeighbours() {
		return interlayerNeighbours;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setIndex(int index) {
		this.index = index;
	}
	
	public void setMembership(int membership) {
		this.membership = membership;
	}
	
	public void setBetweenness(double betweenness) {
		this.betweenness = betweenness;
	}
	
	public void setCloseness(double closeness) {
		this.closeness = closeness;
	}
	
	public void setState(int state) {
		this.state = state;
	}
	
	public void setValue(double value) {
		this.value = value;
	}
	
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	
	public void setLayerId(int layerId) {
		this.layerId = layerId;
	}
	
	public void setIndexM(int indexM) {
		this.indexM = indexM;
	}
}
