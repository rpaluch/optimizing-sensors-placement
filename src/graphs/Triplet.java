package graphs;

/**
 * A triplet is three nodes that are connected by either two (open triplet) or three (closed triplet) undirected ties. 
 * @author Robert Paluch
 */

public class Triplet {

	/**
	 * Data fields
	 */
	
	Node center;
	Node neighbourOne;
	Node neighbourTwo;
	boolean closed;
	
	public Triplet(Node center, Node neighbourOne, Node neighbourTwo){
		this.center = center;
		this.neighbourOne = neighbourOne;
		this.neighbourTwo = neighbourTwo;
		this.closed = checkIfClosed();
	}
	
	public Triplet(Node center, Node neighbourOne, Node neighbourTwo, boolean closed){
		this.center = center;
		this.neighbourOne = neighbourOne;
		this.neighbourTwo = neighbourTwo;
		this.closed = closed;
	}
	
	/***
	 * Private methods
	 */
	
	private boolean checkIfClosed() {
		return neighbourOne.getNeighbours().contains(neighbourTwo);
	}
	
	/***
	 * Getters and setters
	 */

	public boolean isClosed() {
		return closed;
	}

	public void setClosed(boolean closed) {
		this.closed = closed;
	}

	public Node getCenter() {
		return center;
	}

	public Node getNeighbourOne() {
		return neighbourOne;
	}

	public Node getNeighbourTwo() {
		return neighbourTwo;
	}
		
}
