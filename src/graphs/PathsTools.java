package graphs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Queue;

public class PathsTools {

	/***
	 * Returns the length of the shortest path betwen source and target. If the path does not exists, returns -1.
	 */
	public static int shortestPathLength(Graph graph, int sourceID, int targetID) {
		return PathsTools.shortestPath(graph, sourceID, targetID).size()-1;
	}
	
	/***
	 * Returns the average length of the shortest path between source and a group of other nodes.
	 */
	public static double shortestPathLength(Graph graph, int sourceID, ArrayList<Integer> targetID) {
		double spl = 0.0;
		ArrayList<LinkedList<Node>> paths = PathsTools.shortestPaths(graph, sourceID, targetID);
		for(LinkedList<Node> p : paths) spl = spl + p.size()-1;
		return spl/targetID.size();
	}
	
	/***
	 * Returns the average length of the shortest path between source and a group of other nodes in weighted graph.
	 */
	public static double shortestWeightedPathLength(Graph graph, int sourceID, ArrayList<Integer> targetID) {
		double swpl = 0.0;
		ArrayList<Double> pathsLengths = PathsTools.shortestWeightedPathsLengths(graph, sourceID, targetID);
		for(Double pl : pathsLengths) swpl = swpl + pl;
		return swpl/targetID.size();
	}
	
	/***
	 * Return integer array with the shortest paths lengths between the source and the nodes from the list targetsID.
	 * If the path between the source and a given target does not exist, the path length is set to N+1, where N is the network size
	 */
	public static ArrayList<Integer> shortestPathsLengths(Graph graph, int sourceID, ArrayList<Integer> targetsID) {
		
		Node source = graph.getNode(sourceID);
		ArrayList<Node> targets = new ArrayList<Node>();
		for(int i=0; i<targetsID.size(); i++) targets.add(graph.getNode(targetsID.get(i)));
		Queue<Node> queue = new LinkedList<Node>();
		Node currentNode = null;
		
		int dist = 0;
		int size = graph.getNumberOfNodes();
		int[] pathLength = new int[size];
		Node[] previous = new Node[size];
		
		for(int i=0; i<size; i++) {
			pathLength[i] = size + 1;
		}
		pathLength[source.getIndex()] = 0;
		queue.add(source);
		
		while(!queue.isEmpty()) {
			currentNode = queue.poll();
			if(targets.size()==0) break;
			else if(targets.contains(currentNode)) targets.remove(currentNode);
			for(Node n : currentNode.getNeighbours()) {
				dist = pathLength[currentNode.getIndex()]+1;
				if(pathLength[n.getIndex()]>dist) {
					pathLength[n.getIndex()] = dist;
					queue.add(n);
					previous[n.getIndex()] = currentNode;
				}
			}
		}
		
		// Does reverse iteration if the path between source and target exists.
		ArrayList<Integer> lengths = new ArrayList<Integer>();
		for(int i=0; i<targetsID.size(); i++) 
			lengths.add(pathLength[graph.getNode(targetsID.get(i)).getIndex()]);
		
		return lengths;
	}
	
	/***
	 * Return integer array with the number of shortest paths between the source and the nodes from the list targetsID.
	 * If the path between the source and a given target does not exist, the number of shortest paths is zero
	 */
	public static ArrayList<Integer> shortestPathsNumber(Graph graph, int sourceID, ArrayList<Integer> targetsID) {
		
		Node source = graph.getNode(sourceID);
		ArrayList<Node> targets = new ArrayList<Node>();
		for(int i=0; i<targetsID.size(); i++) targets.add(graph.getNode(targetsID.get(i)));
		Queue<Node> queue = new LinkedList<Node>();
		Node currentNode = null;
		
		int dist = 0;
		int size = graph.getNumberOfNodes();
		int[] pathLength = new int[size];
		int[] pathCount = new int[size];
		
		for(int i=0; i<size; i++) {
			pathLength[i] = size + 1;
			pathCount[i] = 0;
		}
		pathLength[source.getIndex()] = 0;
		pathCount[source.getIndex()] = 1;
		queue.add(source);
		
		while(!queue.isEmpty()) {
			currentNode = queue.poll();
			//if(targets.size()==0) break;
			//else if(targets.contains(currentNode)) targets.remove(currentNode);
			for(Node n : currentNode.getNeighbours()) {
				dist = pathLength[currentNode.getIndex()]+1;
				if(pathLength[n.getIndex()]>dist) {
					pathLength[n.getIndex()] = dist;
					pathCount[n.getIndex()] = pathCount[currentNode.getIndex()];
					queue.add(n);
				}
				else if(pathLength[n.getIndex()]==dist) {
					pathCount[n.getIndex()] = pathCount[n.getIndex()] + pathCount[currentNode.getIndex()];
				}
			}
		}
		
		ArrayList<Integer> counts = new ArrayList<Integer>();
		for(int i=0; i<targetsID.size(); i++) 
			counts.add(pathCount[graph.getNode(targetsID.get(i)).getIndex()]);
		
		return counts;
	}
	
	/***
	 * Return the list of all shortest paths from the given source.
	 * The path contains also source and target, therefore the length of the path is the size of the list minus 1.
	 * The size of ArrayList<Path> for a given pair of nodes is equal to the number of the shortest paths between them.
	 * If the path does not exists, the ArrayList<Path> is empty and has size 0.
	 */
	public static ArrayList<ArrayList<Path>> allShortestPaths(Graph graph, int sourceID) {
		
		Node currentNode = null;
		double dist = 0.0;
		int size = graph.getNumberOfNodes();
		int[] pathCount = new int[size];
		double[] distances = new double[size];
		Queue<Node> queue = new LinkedList<Node>();
		ArrayList<ArrayList<Node>> previous = new ArrayList<ArrayList<Node>>();
		ArrayList<Node> sortedNodes = new ArrayList<Node>();
		ArrayList<ArrayList<Path>> allPaths = new ArrayList<ArrayList<Path>>();
		
		// Initialize lists and distances
		for(int i=0; i<size; i++) {
			previous.add(new ArrayList<Node>());
			allPaths.add(new ArrayList<Path>());
			distances[i] = Double.POSITIVE_INFINITY;
			sortedNodes.add(graph.getNodesList().get(i));
		}
		// Set the source
		Node source = graph.getNode(sourceID);
		distances[source.getIndex()] = 0.0;
		pathCount[source.getIndex()] = 1;
		allPaths.get(source.getIndex()).add(new Path(source));
		queue.add(source);
		
		// Go forward
		while(!queue.isEmpty()) {
			currentNode = queue.poll();
			for(Node n : currentNode.getNeighbours()) {
				dist = distances[currentNode.getIndex()]+1;
				if(distances[n.getIndex()]>dist) {
					distances[n.getIndex()] = dist;
					pathCount[n.getIndex()] = pathCount[currentNode.getIndex()];
					queue.add(n);
					previous.get(n.getIndex()).clear();
					previous.get(n.getIndex()).add(currentNode);
				}
				else if(distances[n.getIndex()]==dist) {
					pathCount[n.getIndex()] = pathCount[n.getIndex()] + pathCount[currentNode.getIndex()];
					previous.get(n.getIndex()).add(currentNode);
				}
			}
		}
		
		// Sort the nodes ascending according to the distance from the source
		Collections.sort(sortedNodes, Comparator.comparingDouble(s -> distances[((Node)s).getIndex()]));
		// Find the paths
		for(int i=1; i<sortedNodes.size(); i++) {
			currentNode = sortedNodes.get(i);
			if(distances[currentNode.getIndex()]<Double.POSITIVE_INFINITY) {
				for(Node prev : previous.get(currentNode.getIndex())) {
					for(Path pathOfPrevious : allPaths.get(prev.getIndex())) {
						Path newPath = new Path(pathOfPrevious);
						newPath.add(currentNode);
						allPaths.get(currentNode.getIndex()).add(newPath);
					}
				}
			}
		}
		return allPaths;
	}
	
	/***
	 * Return the list of all shortest paths from the given source to the given nodes.
	 * The path contains also source and target, therefore the length of the path is the size of the list minus 1.
	 * The size of ArrayList<Path> for a given pair of nodes is equal to the number of the shortest paths between them.
	 * If the path does not exists, the ArrayList<Path> is empty and has size 0.
	 */
	public static ArrayList<ArrayList<Path>> allShortestPaths(Graph graph, int sourceID, ArrayList<Integer> targetsID) {
		
		Node currentNode = null;
		double dist = 0.0;
		int size = graph.getNumberOfNodes();
		double[] distances = new double[size];
		int[] pathCount = new int[size];
		Queue<Node> queue = new LinkedList<Node>();
		ArrayList<ArrayList<Path>> allPaths = new ArrayList<ArrayList<Path>>();
		ArrayList<ArrayList<Node>> previous = new ArrayList<ArrayList<Node>>();
		ArrayList<Node> sortedNodes = new ArrayList<Node>();
		ArrayList<Node> targets = new ArrayList<Node>();
			
		// Initialize lists and distances
		for(int i=0; i<size; i++) {
			previous.add(new ArrayList<Node>());
			allPaths.add(new ArrayList<Path>());
			//graph.getNodesList().get(i).setDistance(Double.POSITIVE_INFINITY);
			distances[i] = Double.POSITIVE_INFINITY;
			sortedNodes.add(graph.getNodesList().get(i));
		}
		for(int i=0; i<targetsID.size(); i++) targets.add(graph.getNode(targetsID.get(i)));

		// Set the source
		Node source = graph.getNode(sourceID);
		//source.setDistance(0.0);
		distances[source.getIndex()] = 0.0;
		pathCount[source.getIndex()] = 1;
		allPaths.get(source.getIndex()).add(new Path(source));
		queue.add(source);
		
		// Go forward
		while(!queue.isEmpty()) {
			currentNode = queue.poll();
			if(targets.size()==0) break;
			else if(targets.contains(currentNode)) targets.remove(currentNode);
			for(Node n : currentNode.getNeighbours()) {
				dist = distances[currentNode.getIndex()]+1;
				if(distances[n.getIndex()]>dist) {
					distances[n.getIndex()] = dist;
					pathCount[n.getIndex()] = pathCount[currentNode.getIndex()];
					queue.add(n);
					previous.get(n.getIndex()).clear();
					previous.get(n.getIndex()).add(currentNode);
				}
				else if(distances[n.getIndex()]==dist) {
					pathCount[n.getIndex()] = pathCount[n.getIndex()] + pathCount[currentNode.getIndex()];
					previous.get(n.getIndex()).add(currentNode);
				}
			}
		}		
		// Sort the nodes ascending according to the distance from the source
		Collections.sort(sortedNodes, Comparator.comparingDouble(s -> distances[((Node)s).getIndex()]));
		// Find all paths
		for(int i=1; i<sortedNodes.size(); i++) {
			currentNode = sortedNodes.get(i);
			if(distances[currentNode.getIndex()]<Double.POSITIVE_INFINITY) {
				for(Node prev : previous.get(currentNode.getIndex())) {
					for(Path pathOfPrevious : allPaths.get(prev.getIndex())) {
						Path newPath = new Path(pathOfPrevious);
						newPath.add(currentNode);
						allPaths.get(currentNode.getIndex()).add(newPath);
					}
				}
			}
		}
		// Select paths to the targets
		ArrayList<ArrayList<Path>> pathsToTargets = new ArrayList<ArrayList<Path>>();
		Node target;
		for(int i=0; i<targetsID.size(); i++) {
			target = graph.getNode(targetsID.get(i));
			pathsToTargets.add(allPaths.get(target.index));
		}	
		return pathsToTargets;
	}
	
	/***
	 * Return the list of all shortest paths from the given source which length is not greater than parameter "maxLength".
	 * The path contains also source and target, therefore the length of the path is the size of the list minus 1.
	 * The size of ArrayList<Path> for a given pair of nodes is equal to the number of the shortest paths between them.
	 * If the path does not exists, the ArrayList<Path> is empty and has size 0.
	 */
	public static ArrayList<ArrayList<Path>> allLimitedShortestPaths(Graph graph, int sourceID, int maxLength) {
		
		Node currentNode = null;
		double dist = 0.0;
		int size = graph.getNumberOfNodes();
		int[] pathCount = new int[size];
		double[] distances = new double[size];
		Queue<Node> queue = new LinkedList<Node>();
		ArrayList<ArrayList<Node>> previous = new ArrayList<ArrayList<Node>>();
		ArrayList<Node> sortedNodes = new ArrayList<Node>();
		ArrayList<ArrayList<Path>> allPaths = new ArrayList<ArrayList<Path>>();
		
		// Initialize lists and distances
		for(int i=0; i<size; i++) {
			previous.add(new ArrayList<Node>());
			allPaths.add(new ArrayList<Path>());
			distances[i] = Double.POSITIVE_INFINITY;
			sortedNodes.add(graph.getNodesList().get(i));
		}
		// Set the source
		Node source = graph.getNode(sourceID);
		distances[source.getIndex()] = 0.0;
		pathCount[source.getIndex()] = 1;
		Path sourcePath = new Path(source);
		allPaths.get(source.getIndex()).add(sourcePath);
		queue.add(source);
		
		// Go forward
		while(!queue.isEmpty()) {
			currentNode = queue.poll();
			for(Node n : currentNode.getNeighbours()) {
				dist = distances[currentNode.getIndex()]+1;
				if(dist<=maxLength) {
					if(distances[n.getIndex()]>dist) {
						distances[n.getIndex()] = dist;
						pathCount[n.getIndex()] = pathCount[currentNode.getIndex()];
						queue.add(n);
						previous.get(n.getIndex()).clear();
						previous.get(n.getIndex()).add(currentNode);
					}
					else if(distances[n.getIndex()]==dist) {
						pathCount[n.getIndex()] = pathCount[n.getIndex()] + pathCount[currentNode.getIndex()];
						previous.get(n.getIndex()).add(currentNode);
					}
				}
			}
		}
		
		// Sort the nodes ascending according to the distance from the source
		Collections.sort(sortedNodes, Comparator.comparingDouble(s -> distances[((Node)s).getIndex()]));
		// Find the paths
		for(int i=1; i<sortedNodes.size(); i++) {
			currentNode = sortedNodes.get(i);
			if(distances[currentNode.getIndex()]<=maxLength) {
				for(Node prev : previous.get(currentNode.getIndex())) {
					for(Path pathOfPrevious : allPaths.get(prev.getIndex())) {
						Path newPath = new Path(pathOfPrevious);
						newPath.add(currentNode);
						allPaths.get(currentNode.getIndex()).add(newPath);
					}
				}
			}
		}
		return allPaths;
	}
	
	/***
	 * Returns the register of all shortest paths between all nodes in the graph.
	 * The path contains also source and target, therefore the length of the path is the size of the list minus 1.
	 */
	public static PathsRegister allShortestPathsInGraph(Graph graph) {
		PathsRegister register = new PathsRegister();
		for(Node source : graph.getNodesList())
			register.addAllAll(allShortestPaths(graph, source.getId()));
		return register;
	}
	
	/***
	 * Returns the register of the shortest paths between all nodes in the graph.
	 * For any pair of nodes it returns only one, arbitrary shortest path.
	 * The path contains also source and target, therefore the length of the path is the size of the list minus 1.
	 */
	public static PathsRegister singleShortestPathsInGraph(Graph graph) {
		PathsRegister register = new PathsRegister();
		for(Node source : graph.getNodesList())
			register.addAll(singleShortestPaths(graph, source.getId()));
		return register;
	}
	
	/***
	 * Returns the register of all limited shortest paths in the graph which have length not greater than parameter maxLength.
	 * The path contains also source and target, therefore the length of the path is the size of the list minus 1.
	 */
	public static PathsRegister allLimitedShortestPathsInGraph(Graph graph, int maxLength) {
		PathsRegister register = new PathsRegister();
		for(Node source : graph.getNodesList())
			register.addAllAll(PathsTools.allLimitedShortestPaths(graph, source.getId(), maxLength));
		return register;
	}
	
	/***
	 * Returns the register of the limited shortest paths in the graph which have length not greater than parameter maxLength.
	 * For any pair of nodes it returns only one, arbitrary shortest path (if is less or equal to maxLength)
	 * The path contains also source and target, therefore the length of the path is the size of the list minus 1.
	 */
	public static PathsRegister singleLimitedShortestPathsInGraph(Graph graph, int maxLength) {
		PathsRegister register = new PathsRegister();
		for(Node source : graph.getNodesList())
			register.addAll(PathsTools.singleLimitedShortestPaths(graph, source.getId(), maxLength));
		return register;
	}
	
	/***
	 * Transforms PathsRegister (which contains all shortest paths from a given graph into 2D array with distances between all nodes in the network. 
	 */
	public static int[][] allDistancesInGraph(Graph graph, PathsRegister register) {
		ArrayList<Node> nodes = graph.getNodesList();
		int size = nodes.size();
		int[][] distances = new int[size][size];
		int dist = -1;
		
		for(int i=0; i<size-1; i++)
			for(int j=i+1; j<size; j++) {
				dist = register.getPathLength(nodes.get(i).getId(), nodes.get(j).getId());
				distances[i][j] = dist;
				distances[j][i] = dist;
			}
		
		return distances;
	}
	public static int[][] allDistancesInGraph(Graph graph) {
		ArrayList<Integer> nodesId = GraphTools.nodesToIDs(graph.getNodesList());
		int size = nodesId.size();
		ArrayList<Integer> distancesFromGivenSource;
		int[][] distances = new int[size][size];
		int sourceId = -1;
		int sourceIndex = -1;
		int nodeIndex = -1;
		
		for(int i=0; i<size-1; i++) {
			sourceId = nodesId.remove(nodesId.size()-1);
			distancesFromGivenSource = PathsTools.shortestPathsLengths(graph, sourceId, nodesId);
			for(int j=0; j<distancesFromGivenSource.size(); j++) {
				sourceIndex = graph.getNode(sourceId).getIndex();
				nodeIndex = graph.getNode(nodesId.get(j)).getIndex();
				distances[sourceIndex][nodeIndex] = distancesFromGivenSource.get(j);
				distances[nodeIndex][sourceIndex] = distancesFromGivenSource.get(j);
			}
		}
		return distances;
	}
	
	/***
	 * Return double array with the shortest paths lengths between the source and the nodes from the list targetsID.
	 * The weights of links are taken into account.
	 * If the path between the source and a given target does not exist, the path length is set to Double.POSITIVE_INFINITY.
	 */
	public static ArrayList<Double> shortestWeightedPathsLengths(Graph graph, int sourceID, ArrayList<Integer> targetsID) {
		
		int size = graph.getNumberOfNodes();
		ArrayList<Node> targets = new ArrayList<Node>();
		for(int i=0; i<targetsID.size(); i++) targets.add(graph.getNode(targetsID.get(i)));
		double[] distances = new double[size];
		Node[] previous = new Node[size];
		LinkedList<Node> queue = new LinkedList<Node>();
		for(Node node : graph.getNodesList()) {
			if(node.getId()==sourceID) distances[node.getIndex()] = 0.0;
			else distances[node.getIndex()] = Double.POSITIVE_INFINITY;
			queue.add(node);
		}
		Node currentNode = null;
		double dist = 0;
		
		while(!queue.isEmpty()) {
			currentNode = Collections.min(queue, Comparator.comparingDouble(s -> distances[((Node)s).getIndex()]));
			queue.remove(currentNode);
			if(targets.size()==0) break;
			else if(targets.contains(currentNode)) targets.remove(currentNode);
			for(Node n : currentNode.getNeighbours()) {
				dist = distances[currentNode.getIndex()] + graph.getLink(currentNode.getId(), n.getId()).getWeight();
				if(distances[n.getIndex()]>dist) {
					distances[n.getIndex()] = dist;
					previous[n.getIndex()] = currentNode;
				}
			}
		}
		
		ArrayList<Double> lengths = new ArrayList<Double>();
		for(int i=0; i<targetsID.size(); i++) 
			lengths.add(distances[graph.getNode(targetsID.get(i)).getIndex()]);
			
		return lengths;
	}
	
	/***
	 * Returns the list of nodes placed on the shortest path between a source and a target.
	 * The list contains also a source and a target, therefore the length of the path is the size of the list minus 1.
	 */
	public static LinkedList<Node> shortestPath(Graph graph, int sourceID, int targetID) {
		
		Node source = graph.getNode(sourceID);
		Node target = graph.getNode(targetID);
		LinkedList<Node> path = new LinkedList<Node>();
		Queue<Node> queue = new LinkedList<Node>();
		Node currentNode = null;
		
		int dist = 0;
		int size = graph.getNumberOfNodes();
		int[] pathLength = new int[size];
		Node[] previous = new Node[size];
		
		for(int i=0; i<size; i++) {
			pathLength[i] = size + 1;
		}
		pathLength[source.getIndex()] = 0;
		queue.add(source);
		
		while(!queue.isEmpty()) {
			currentNode = queue.poll();
			if(currentNode==target) break;
			for(Node n : currentNode.getNeighbours()) {
				dist = pathLength[currentNode.getIndex()]+1;
				if(pathLength[n.getIndex()]>dist) {
					pathLength[n.getIndex()] = dist;
					queue.add(n);
					previous[n.getIndex()] = currentNode;
				}
			}
		}
		
		// Does reverse iteration if the path between source and target exists.
		if(pathLength[target.getIndex()]<size) {
			while(currentNode!=source) {
				path.push(currentNode);
				currentNode = previous[currentNode.getIndex()];
			}
			path.push(source);
		}
		
		return path;
	}
		
	/***
	 * Return the list of shortest paths between the source and any other node in the network.
	 * The path contains also source and target, therefore the length of the path is the size of the list minus 1.
	 */
	public static ArrayList<LinkedList<Node>> shortestPaths(Graph graph, int sourceID) {
		
		Node source = graph.getNode(sourceID);
		ArrayList<LinkedList<Node>> paths = new ArrayList<LinkedList<Node>>();
		Queue<Node> queue = new LinkedList<Node>();
		Node currentNode = null;
		
		int dist = 0;
		int size = graph.getNumberOfNodes();
		int[] pathLength = new int[size];
		Node[] previous = new Node[size];
		
		for(int i=0; i<size; i++) {
			pathLength[i] = size + 1;
		}
		pathLength[source.getIndex()] = 0;
		queue.add(source);
		
		while(!queue.isEmpty()) {
			currentNode = queue.poll();
			for(Node n : currentNode.getNeighbours()) {
				dist = pathLength[currentNode.getIndex()]+1;
				if(pathLength[n.getIndex()]>dist) {
					pathLength[n.getIndex()] = dist;
					queue.add(n);
					previous[n.getIndex()] = currentNode;
				}
			}
		}
		
		// Does reverse iteration if the path between source and target exists.
		for(int j=0; j<size; j++){
			currentNode = graph.getNodesList().get(j);
			paths.add(new LinkedList<Node>());
			if(pathLength[j]<size) {
				while(currentNode!=source) {
					paths.get(j).push(currentNode);
					currentNode = previous[currentNode.getIndex()];
				}
				paths.get(j).push(source);
			}
		}
		
		return paths;
	}
	
	/***
	 * Return the list of shortest paths between the source and any other node in the network.
	 * The path contains also source and target, therefore the length of the path is the size of the list minus 1.
	 */
	public static ArrayList<Path> singleShortestPaths(Graph graph, int sourceID) {
		
		Node source = graph.getNode(sourceID);
		ArrayList<Path> paths = new ArrayList<Path>();
		Queue<Node> queue = new LinkedList<Node>();
		Node currentNode = null;
		
		int dist = 0;
		int size = graph.getNumberOfNodes();
		int[] pathLength = new int[size];
		Node[] previous = new Node[size];
		
		for(int i=0; i<size; i++) {
			pathLength[i] = size + 1;
		}
		pathLength[source.getIndex()] = 0;
		queue.add(source);
		
		while(!queue.isEmpty()) {
			currentNode = queue.poll();
			for(Node n : currentNode.getNeighbours()) {
				dist = pathLength[currentNode.getIndex()]+1;
				if(pathLength[n.getIndex()]>dist) {
					pathLength[n.getIndex()] = dist;
					queue.add(n);
					previous[n.getIndex()] = currentNode;
				}
			}
		}
		
		// Does reverse iteration if the path between source and target exists.
		for(int j=0; j<size; j++){
			currentNode = graph.getNodesList().get(j);
			paths.add(new Path());
			if(pathLength[j]<size) {
				while(currentNode!=source) {
					paths.get(j).push(currentNode);
					currentNode = previous[currentNode.getIndex()];
				}
				paths.get(j).push(source);
			}
		}
		
		return paths;
	}
	
	/***
	 * Return the list of shortest paths between the source and any other node in the network.
	 * The path contains also source and target, therefore the length of the path is the size of the list minus 1.
	 */
	public static ArrayList<Path> singleLimitedShortestPaths(Graph graph, int sourceID, int maxLength) {
		
		Node source = graph.getNode(sourceID);
		ArrayList<Path> paths = new ArrayList<Path>();
		Queue<Node> queue = new LinkedList<Node>();
		Node currentNode = null;
		
		int dist = 0;
		int size = graph.getNumberOfNodes();
		int[] pathLength = new int[size];
		Node[] previous = new Node[size];
		
		for(int i=0; i<size; i++) {
			pathLength[i] = size + 1;
		}
		pathLength[source.getIndex()] = 0;
		queue.add(source);
		
		while(!queue.isEmpty()) {
			currentNode = queue.poll();
			for(Node n : currentNode.getNeighbours()) {
				dist = pathLength[currentNode.getIndex()]+1;
				if(dist<=maxLength && pathLength[n.getIndex()]>dist) {
					pathLength[n.getIndex()] = dist;
					queue.add(n);
					previous[n.getIndex()] = currentNode;
				}
			}
		}
		
		// Does reverse iteration if the path between source and target is less or equal maxLength
		for(int j=0; j<size; j++){
			currentNode = graph.getNodesList().get(j);
			paths.add(new Path());
			if(pathLength[j]<=maxLength) {
				while(currentNode!=source) {
					paths.get(j).push(currentNode);
					currentNode = previous[currentNode.getIndex()];
				}
				paths.get(j).push(source);
			}
		}
		
		return paths;
	}
	
	/***
	 * Return the list of shortest paths between the source and the nodes from the list targetsID.
	 * The path contains also source and target, therefore the length of the path is the size of the list minus 1.
	 */
	public static ArrayList<LinkedList<Node>> shortestPaths(Graph graph, int sourceID, ArrayList<Integer> targetsID) {
		
		Node source = graph.getNode(sourceID);
		ArrayList<Node> targets = new ArrayList<Node>();
		for(int i=0; i<targetsID.size(); i++) targets.add(graph.getNode(targetsID.get(i)));
		ArrayList<LinkedList<Node>> paths = new ArrayList<LinkedList<Node>>();
		Queue<Node> queue = new LinkedList<Node>();
		Node currentNode = null;
		
		int dist = 0;
		int size = graph.getNumberOfNodes();
		int[] pathLength = new int[size];
		Node[] previous = new Node[size];
		
		for(int i=0; i<size; i++) {
			pathLength[i] = size + 1;
		}
		pathLength[source.getIndex()] = 0;
		queue.add(source);
		
		while(!queue.isEmpty()) {
			currentNode = queue.poll();
			if(targets.size()==0) break;
			else if(targets.contains(currentNode)) targets.remove(currentNode);
			for(Node n : currentNode.getNeighbours()) {
				dist = pathLength[currentNode.getIndex()]+1;
				if(pathLength[n.getIndex()]>dist) {
					pathLength[n.getIndex()] = dist;
					queue.add(n);
					previous[n.getIndex()] = currentNode;
				}
			}
		}
		
		// Does reverse iteration if the path between source and target exists.
		targets = new ArrayList<Node>();
		for(int i=0; i<targetsID.size(); i++) targets.add(graph.getNode(targetsID.get(i)));
		for(int j=0; j<targets.size(); j++){
			currentNode = targets.get(j);
			paths.add(new LinkedList<Node>());
			if(pathLength[currentNode.getIndex()]<size) {
				while(currentNode!=source) {
					paths.get(j).push(currentNode);
					currentNode = previous[currentNode.getIndex()];
				}
				paths.get(j).push(source);
			}
		}
		
		return paths;
	}
	
	/***
	 * Return the list of shortest paths between the source and the nodes from the list targets.
	 * The path contains also source and target, therefore the length of the path is the size of the list minus 1.
	 */
	public static ArrayList<Path> shortestPaths(Graph graph, Node source, ArrayList<Node> targets) {
		
		ArrayList<Node> targetsList = new ArrayList<Node>();
		for(int i=0; i<targets.size(); i++) targetsList.add(targets.get(i));
		ArrayList<Path> paths = new ArrayList<Path>();
		Queue<Node> queue = new LinkedList<Node>();
		Node currentNode = null;
		
		int dist = 0;
		int size = graph.getNumberOfNodes();
		int[] pathLength = new int[size];
		Node[] previous = new Node[size];
		
		for(int i=0; i<size; i++) {
			pathLength[i] = size + 1;
		}
		pathLength[source.getIndex()] = 0;
		queue.add(source);
		
		while(!queue.isEmpty()) {
			currentNode = queue.poll();
			if(targetsList.size()==0) break;
			else if(targetsList.contains(currentNode)) targetsList.remove(currentNode);
			for(Node n : currentNode.getNeighbours()) {
				dist = pathLength[currentNode.getIndex()]+1;
				if(pathLength[n.getIndex()]>dist) {
					pathLength[n.getIndex()] = dist;
					queue.add(n);
					previous[n.getIndex()] = currentNode;
				}
			}
		}
		
		// Does reverse iteration if the path between source and target exists.
		for(int j=0; j<targets.size(); j++){
			currentNode = targets.get(j);
			paths.add(new Path());
			if(pathLength[currentNode.getIndex()]<size) {
				while(currentNode!=source) {
					paths.get(j).push(currentNode);
					currentNode = previous[currentNode.getIndex()];
				}
				paths.get(j).push(source);
			}
		}
		
		return paths;
	}
	
	/***
	 * Return the list of shortest paths between the source and any other node in the network.
	 * The weights of links are taken into account.
	 * The path contains also source and target, therefore the length of the path is the size of the list minus 1.
	 */
	public static ArrayList<LinkedList<Node>> shortestWeightedPaths(Graph graph, int sourceID) {
		
		int size = graph.getNumberOfNodes();
		Node source = graph.getNode(sourceID);
		ArrayList<LinkedList<Node>> paths = new ArrayList<LinkedList<Node>>();
		double[] distances = new double[size];
		Node[] previous = new Node[size];
		LinkedList<Node> queue = new LinkedList<Node>();
		for(Node node : graph.getNodesList()) {
			if(node.getId()==sourceID) distances[node.getIndex()] = 0.0;
			else distances[node.getIndex()] = Double.POSITIVE_INFINITY;
			queue.add(node);
		}
		Node currentNode = null;
		double dist = 0.0;
		
		while(!queue.isEmpty()) {
			currentNode = Collections.min(queue, Comparator.comparingDouble(s -> distances[((Node)s).getIndex()]));
			queue.remove(currentNode);
			for(Node n : currentNode.getNeighbours()) {
				dist = distances[currentNode.getIndex()] + graph.getLink(currentNode.getId(), n.getId()).getWeight();
				if(distances[n.getIndex()]>dist) {
					distances[n.getIndex()] = dist;
					previous[n.getIndex()] = currentNode;
				}
			}
		}
		
		// Does reverse iteration if the path between source and target exists.
		for(int j=0; j<graph.getNumberOfNodes(); j++){
			currentNode = graph.getNodesList().get(j);
			paths.add(new LinkedList<Node>());
			if(distances[currentNode.getIndex()]<Double.POSITIVE_INFINITY) {
				while(currentNode!=source) {
					paths.get(j).push(currentNode);
					currentNode = previous[currentNode.getIndex()];
				}
				paths.get(j).push(source);
			}
		}
		
		return paths;
	}
	
	/***
	 * Returns the list of nodes placed on the shortest path between a source and a target.
	 * The weights of links are taken into account.
	 * The list contains also a source and a target, therefore the length of the path is the size of the list minus 1.
	 */
	public static LinkedList<Node> shortestWeightedPath(Graph graph, int sourceID, int targetID) {
		
		int size = graph.getNumberOfNodes();
		Node source = graph.getNode(sourceID);
		Node target = graph.getNode(targetID);
		LinkedList<Node> path = new LinkedList<Node>();
		double[] distances = new double[size];
		Node[] previous = new Node[size];
		LinkedList<Node> queue = new LinkedList<Node>();
		for(Node node : graph.getNodesList()) {
			if(node.getId()==sourceID) distances[node.getIndex()] = 0.0;
			else distances[node.getIndex()] = Double.POSITIVE_INFINITY;
			queue.add(node);
		}
		Node currentNode = null;
		double dist = 0;
		
		
		while(!queue.isEmpty()) {
			currentNode = Collections.min(queue, Comparator.comparingDouble(s -> distances[((Node)s).getIndex()]));
			queue.remove(currentNode);
			if(currentNode==target) break;
			for(Node n : currentNode.getNeighbours()) {
				dist = distances[currentNode.getIndex()] + graph.getLink(currentNode.getId(), n.getId()).getWeight();
				if(distances[n.getIndex()]>dist) {
					distances[n.getIndex()] = dist;
					previous[n.getIndex()] = currentNode;
				}
			}
		}
		
		// Does reverse iteration if the path between source and target exists.
		if(distances[target.getIndex()]<Double.POSITIVE_INFINITY) {
			while(currentNode!=source) {
				path.push(currentNode);
				currentNode = previous[currentNode.getIndex()];
			}
			path.push(source);
		}
		
		return path;
	}
	
	/***
	 * Return the list of shortest paths between the source and the nodes from the list targetsID.
	 * The weights of links are taken into account.
	 * The path contains also source and target, therefore the length of the path is the size of the list minus 1.
	 */
	public static ArrayList<LinkedList<Integer>> shortestWeightedPaths(Graph graph, int sourceID, ArrayList<Integer> targetsID) {
		
		int size = graph.getNumberOfNodes();
		Node source = graph.getNode(sourceID);
		ArrayList<Node> targets = new ArrayList<Node>();
		for(int i=0; i<targetsID.size(); i++) targets.add(graph.getNode(targetsID.get(i)));
		ArrayList<LinkedList<Integer>> paths = new ArrayList<LinkedList<Integer>>();
		double[] distances = new double[size];
		Node[] previous = new Node[size];
		LinkedList<Node> queue = new LinkedList<Node>();
		for(Node node : graph.getNodesList()) {
			if(node.getId()==sourceID) distances[node.getIndex()] = 0.0;
			else distances[node.getIndex()] = Double.POSITIVE_INFINITY;
			queue.add(node);
		}
		Node currentNode = null;
		double dist = 0;
		
		while(!queue.isEmpty()) {
			currentNode = Collections.min(queue, Comparator.comparingDouble(s -> distances[((Node)s).getIndex()]));
			queue.remove(currentNode);
			if(targets.size()==0) break;
			else if(targets.contains(currentNode)) targets.remove(currentNode);
			for(Node n : currentNode.getNeighbours()) {
				dist = distances[currentNode.getIndex()] + graph.getLink(currentNode.getId(), n.getId()).getWeight();
				if(distances[n.getIndex()]>dist) {
					distances[n.getIndex()] = dist;
					previous[n.getIndex()] = currentNode;
				}
			}
		}
		
		// Does reverse iteration if the path between source and target exists.
		targets = new ArrayList<Node>();
		for(int i=0; i<targetsID.size(); i++) targets.add(graph.getNode(targetsID.get(i)));
		for(int j=0; j<targets.size(); j++){
			currentNode = targets.get(j);
			paths.add(new LinkedList<Integer>());
			if(distances[currentNode.getIndex()]<Double.POSITIVE_INFINITY) {
				while(currentNode!=source) {
					paths.get(j).push(currentNode.getId());
					currentNode = previous[currentNode.getIndex()];
				}
				paths.get(j).push(source.getId());
			}
		}
		
		return paths;
	}
	
	/***
	 * Return the list of shortest paths (which preserve constrains) between the source and any other node in the network.
	 * The ties are the links between given nodes and their predecessors which have to be preserved.
	 * The path contains also source and target, therefore the length of the path is the size of the list minus 1.
	 */
	public static ArrayList<LinkedList<Node>> shortestConstrainedPaths(Graph graph, int sourceID, ArrayList<Link> constrained) {
		
		Node source = graph.getNode(sourceID);
		ArrayList<LinkedList<Node>> paths = new ArrayList<LinkedList<Node>>();
		Queue<Node> queue = new LinkedList<Node>();
		Node currentNode = null;
		
		int dist = 0;
		int size = graph.getNumberOfNodes();
		int[] pathLength = new int[size];
		boolean allowedLink;
		Node[] previous = new Node[size];
		for(Link l : constrained)
			previous[l.getEnd().getIndex()] = graph.getNode(l.getStart().getId());
		
		for(int i=0; i<size; i++) {
			pathLength[i] = size + 1;
		}
		pathLength[source.getIndex()] = 0;
		queue.add(source);
		
		while(!queue.isEmpty()) {
			currentNode = queue.poll();
			for(Node n : currentNode.getNeighbours()) {
				dist = pathLength[currentNode.getIndex()]+1;
				allowedLink = true;
				if(previous[n.getIndex()]!=null)
					if(previous[n.getIndex()].getId()!=currentNode.getId())
						allowedLink = false;
				if(pathLength[n.getIndex()]>dist && allowedLink) {
					pathLength[n.getIndex()] = dist;
					queue.add(n);
					previous[n.getIndex()] = currentNode;
				}
			}
		}
		
		// Does reverse iteration if the path between source and target exists.
		for(int j=0; j<size; j++){
			currentNode = graph.getNodesList().get(j);
			paths.add(new LinkedList<Node>());
			if(pathLength[j]<size) {
				while(currentNode!=source) {
					paths.get(j).push(currentNode);
					currentNode = previous[currentNode.getIndex()];
				}
				paths.get(j).push(source);
			}
		}
		
		return paths;
	}
	
	/***
	 * Return the list of shortest paths (which preserve constrains) between the source and the nodes from the list targetsID.
	 * The ties are the links between given nodes and their predecessors which have to be preserved.
	 * The path contains also source and target, therefore the length of the path is the size of the list minus 1.
	 */
	public static ArrayList<LinkedList<Node>> shortestConstrainedPaths(Graph graph, int sourceID, ArrayList<Integer> targetsID, ArrayList<Link> constrained) {
		
		Node source = graph.getNode(sourceID);
		ArrayList<Node> targets = new ArrayList<Node>();
		for(int i=0; i<targetsID.size(); i++) targets.add(graph.getNode(targetsID.get(i)));
		ArrayList<LinkedList<Node>> paths = new ArrayList<LinkedList<Node>>();
		Queue<Node> queue = new LinkedList<Node>();
		Node currentNode = null;
		
		int dist = 0;
		int size = graph.getNumberOfNodes();
		int[] pathLength = new int[size];
		boolean allowedLink;
		Node[] previous = new Node[size];
		for(Link l : constrained)
			previous[l.getEnd().getIndex()] = graph.getNode(l.getStart().getId());
		
		for(int i=0; i<size; i++) {
			pathLength[i] = size + 1;
		}
		pathLength[source.getIndex()] = 0;
		queue.add(source);
		
		while(!queue.isEmpty()) {
			currentNode = queue.poll();
			if(targets.size()==0) break;
			else if(targets.contains(currentNode)) targets.remove(currentNode);
			for(Node n : currentNode.getNeighbours()) {
				dist = pathLength[currentNode.getIndex()]+1;
				allowedLink = true;
				if(previous[n.getIndex()]!=null)
					if(previous[n.getIndex()].getId()!=currentNode.getId())
						allowedLink = false;
				if(pathLength[n.getIndex()]>dist && allowedLink) {
					pathLength[n.getIndex()] = dist;
					queue.add(n);
					previous[n.getIndex()] = currentNode;
				}
			}
		}
		
		// Does reverse iteration if the path between source and target exists.
		for(int i=0; i<targetsID.size(); i++) targets.add(graph.getNode(targetsID.get(i)));
		for(int j=0; j<targets.size(); j++){
			currentNode = targets.get(j);
			paths.add(new LinkedList<Node>());
			if(pathLength[currentNode.getIndex()]<size) {
				while(currentNode!=source) {
					paths.get(j).push(currentNode);
					currentNode = previous[currentNode.getIndex()];
				}
				paths.get(j).push(source);
			}
		}
		
		return paths;
	}
	
	/***
	 * Returns the average length of the shortest path between any two nodes in the network
	 */
	public static double averagePathLength(Graph graph) {	
		double apl = 0.0;
		int number_of_paths = graph.getNumberOfNodes()*(graph.getNumberOfNodes()-1);
		ArrayList<LinkedList<Node>> paths;
		for(Node n : graph.getNodesList()) {
			paths = PathsTools.shortestPaths(graph, n.getId());
			for(LinkedList<Node> l : paths) apl = apl + l.size() - 1;
		}
		apl = apl/number_of_paths;
		return apl;
	}
	
	public static Path reverse(Path path) {
		Path reversePath = new Path();
		for(int i=path.getSequence().size()-1; i>=0; i--)
			reversePath.add(path.getSequence().get(i));
		return reversePath;
	}
	
	public static WeightedPath reverse(WeightedPath path) {
		WeightedPath reversePath = new WeightedPath();
		for(int i=path.getSequence().size()-1; i>=0; i--)
			reversePath.add(path.getNode(i), path.getWeight(i));
		return reversePath;
	}
	
	/***
	 * Simple add nodes from path2 to nodes in path1
	 * @param path1
	 * @param path2
	 * @return
	 */
	public static Path merge(Path path1, Path path2) {
		Path path = new Path(path1);
		path.add(path2);
		return path;
	}
	
	/***
	 * Simple add nodes and weights from path1 to path2.
	 * @param path1
	 * @param path2
	 * @param weight The weight between last node in path1 and first node in path2
	 * @return
	 */
	public static WeightedPath merge(WeightedPath path1, WeightedPath path2, double weight) {
		WeightedPath path = new WeightedPath(path1);
		path.add(path2, weight);
		return path;
	}
	
	/***
	 * This method requires that last node in path1 and first node in path2 is the same node.
	 * @param path1
	 * @param path2
	 * @return
	 */
	public static Path mergeWithoutOverlap(Path path1, Path path2) {
		int counter = 0;
		int max = Math.min(path1.getNumberOfNodes(), path2.getNumberOfNodes());
		while(path1.getNode(path1.getNumberOfLinks()-counter).equals(path2.getNode(counter))) {
			counter = counter + 1;
			if(counter==max) break;
		}
		Path path = new Path(path1.subPath(0, path1.getNumberOfNodes()-counter));
		path.add(path2.subPath(Math.max(0, counter-1), path2.getNumberOfNodes()));
		return path;
	}
	
	/***
	 * This method requires that last node in path1 and first node in path2 is the same node.
	 * @param path1
	 * @param path2
	 * @return
	 */
	public static WeightedPath mergeWithoutOverlap(WeightedPath path1, WeightedPath path2) {
		int counter = 0;
		int max = Math.min(path1.getNumberOfNodes(), path2.getNumberOfNodes());
		while(path1.getNode(path1.getNumberOfLinks()-counter).equals(path2.getNode(counter))) {
			counter = counter + 1;
			if(counter==max) break;
		}
		WeightedPath path = new WeightedPath(path1.subPath(0, path1.getNumberOfNodes()-counter));
		path.add(path2.subPath(Math.max(0, counter-1), path2.getNumberOfNodes()), path1.getWeight(path1.getNumberOfNodes()-counter));
		return path;
	}

	public static ArrayList<Link> union(ArrayList<Path> paths) {
		ArrayList<Link> union = new ArrayList<Link>(paths.get(0).toLinksList());
		LinkedList<Link> links;
		boolean match;
		for(int i=1; i<paths.size(); i++) {
			links = paths.get(i).toLinksList();
			for(Link l : links) {
				match = false;
				for(Link u : union)
					if(u.equals(l)) {
						match = true;
						break;
					}
				if(!match) union.add(l);
			}
		}
		return union;
	}

	public static ArrayList<Link> overlappingLinks(ArrayList<Link> list1, ArrayList<Link> list2) {
		ArrayList<Link> overlapped = new ArrayList<Link>();
		for(Link l1 : list1)
			for(Link l2: list2)
				if(l2.equals(l1))
					overlapped.add(l2);
		return overlapped;
	}
	
	public static double pathsOverlapEPP(ArrayList<Path> paths1, ArrayList<Path> paths2) {
		int counter = 0;
		for(Path path1 : paths1)
			for(Path path2 : paths2)
				counter = counter + path1.overlappingLinks(path2).size();
		return 1.0*counter/(paths1.size()*paths2.size());
	}

	public static double pathsOverlapEPL(ArrayList<Path> paths1, ArrayList<Path> paths2) {
		ArrayList<Link> union1 = union(paths1);
		ArrayList<Link> union2 = union(paths2);
		ArrayList<Path> allPaths = new ArrayList<Path>(paths1);
		allPaths.addAll(paths2);
		ArrayList<Link> unionAll = union(allPaths);
		ArrayList<Link> overlapped = overlappingLinks(union1,union2);
		//System.out.println(1.0*overlapped.size()/unionAll.size());
		return 1.0*overlapped.size()/unionAll.size();
	}
	
	public static double twoPathsCorrelation(Path path1, Path path2) {
		ArrayList<Link> overlapping = path1.overlappingLinks(path2);
		return 2.0*overlapping.size()/(path1.getLength()+path2.getLength());
	}
	
	public static String pathsToString(ArrayList<Path> paths) {
		String string = "";
		for(int i=0; i<paths.size()-1; i++)
			string = string + paths.get(i).toString() + "\n";
		string = string + paths.get(paths.size()-1).toString();
		return string;
	}
}
