package graphs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Basic class for a path, which is sequence of nodes. It's a wrapper for LinkedList<Node>
 * @author Robert Paluch
 */

public class Path {
	
	/**
	 * Data fields
	 */
	
	LinkedList<Node> sequence;
	
	/**
	 * Constructors
	 */
	
	public Path(){
		this.sequence = new LinkedList<Node>();
	}
	
	public Path(Node node){
		this.sequence = new LinkedList<Node>();
		this.sequence.add(node);
	}
		
	public Path(List<Node> sequence) {
		this.sequence = new LinkedList<Node>();
		for(Node n : sequence)
			this.sequence.add(n);
	}
	
	public Path(Path path) {
		this.sequence = new LinkedList<Node>();
		for(int i=0; i<path.getNumberOfNodes(); i++)
			sequence.add(path.getNode(i));
	}

	/**
	 * Getters and setters
	 */
	
	public LinkedList<Node> getSequence() {
		return sequence;
	}

	public void setSequence(LinkedList<Node> sequence) {
		this.sequence = sequence;
	}
	
	public int getLength() {
		if(sequence.size()>0 )
			return sequence.size()-1;
		else return 0;
	}
	
	public int getNumberOfLinks() {
		if(sequence.size()>0 )
			return sequence.size()-1;
		else return 0;
	}
	
	public int getNumberOfNodes() {
		return sequence.size();
	}
	
	public Node getNode(int index) {
		return sequence.get(index);
	}
	
	public Node getFirst() {
		return sequence.getFirst();
	}
	
	public Node getLast() {
		return sequence.getLast();
	}
	
	/**
	 * Public methods
	 */
	
	public boolean isEmpty() {
		return sequence.isEmpty();
	}
	
	public void add(Node node) {
		sequence.add(node);
	}
	
	public void add(Path path) {
		for(Node node : path.getSequence())
			sequence.add(node);
	}
	
	public void push(Node node) {
		sequence.push(node);
	}
		
	public void remove(Node node) {
		sequence.remove(node);
	}
	
	public Path subPath(int fromIndex, int toIndex) {
		return new Path(sequence.subList(fromIndex, toIndex));
	}
	
	public boolean contains(Node node) {
		return sequence.contains(node);
	}
	
	public ArrayList<Link> union(Path path) {
		ArrayList<Link> union = new ArrayList<Link>(this.toLinksList());
		ArrayList<Link> complementery = this.complementaryLinks(path);
		union.addAll(complementery);
		return union;
	}
	
	// Returns the links which occur in both paths
	public ArrayList<Link> overlappingLinks(Path path) {
		ArrayList<Link> overlapped = new ArrayList<Link>();
		LinkedList<Link> list1 = this.toLinksList();
		LinkedList<Link> list2 = path.toLinksList();
		for(Link l1 : list1)
			for(Link l2: list2)
				if(l2.equals(l1))
					overlapped.add(l2);
		return overlapped;
	}
	
	// Returns the links which occur in the argument but don't occur in this path
	public ArrayList<Link> complementaryLinks(Path path) {
		ArrayList<Link> complementary = new ArrayList<Link>();
		LinkedList<Link> list1 = this.toLinksList();
		LinkedList<Link> list2 = path.toLinksList();
		boolean match;
		for(Link l2 : list2) {
			match = false;
			for(Link l1: list1)
				if(l2.equals(l1)) {
					match = true;
					break;
				}
			if(!match) complementary.add(l2);
		}		
		return complementary;
	}
	
	public int lengthOfOverlappingLinksFromStart(Path path) {
		int shared = 0;
		int i = 0;
		int max = Math.min(this.getLength(), path.getLength());
		if(this.getFirst().getId()==path.getFirst().getId()) {
			while(i<max) {
				if(this.getNode(i+1).getId()==path.getNode(i+1).getId()) {
					shared = shared + 1;
					i = i + 1;
				}
				else i = this.getLength();
			}
		}
		return shared;
	}
	
	public LinkedList<Link> toLinksList() {
		LinkedList<Link> list = new LinkedList<Link>();
		for(int i=0; i<sequence.size()-1; i++)
			list.add(new Link(sequence.get(i), sequence.get(i+1)));
		return list;
	}
	
	public String toString() {
		String string = Integer.toString(sequence.getFirst().getId());
		for(int i=1; i<sequence.size(); i++)
			string = string + "->" + Integer.toString(sequence.get(i).getId());
		return string;
	}
}
