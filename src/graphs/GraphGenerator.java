package graphs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

import jdistlib.generic.GenericDistribution;

/***
 * This class is used to generate some well-known graphs, i.e. Erdos-Renyi or Barabasi-Albert
 * @author rpaluch
 */

public class GraphGenerator {
	
	public static Graph lineGraph(int size) {
		Graph lineGraph = new Graph(size);
		for(int i=1; i<size; i++)
			lineGraph.addLink(i, i+1);
		return lineGraph;
	}
		
	public static Graph erdosRenyi(int size, double prob) {
		
		Graph er = new Graph(size);
		Random rand = new Random();
		
		for(int i=1; i<=size-1; i++)
			for(int j=i+1; j<=size; j++)
				if(rand.nextDouble()<prob)
					er.addLink(i, j);
		
		return er;
	}
	
	
	/***
	 * Generates normal Erdos-Renyi graph and then add few extra edges to connect all components.
	 * Resulting probability per edge in graph might be larger than the probability given as parameter for method.
	 */
	public static Graph contiguousErdosRenyi(int size, double prob) {
		
		Graph er = erdosRenyi(size, prob);
		ArrayList<Integer> csizes = GraphTools.findComponents(er);
		if(csizes.size()>1) GraphTools.connectAllComponents(er);
		return er;
	}
	
	public static Graph erdosRenyi(int size, int edges) {
		
		Graph er = new Graph(size);
		int i,j, counter=0;
		
		// Proceed until all edges are added.
		while(counter<edges) {
			// Draw nodes to be connected
			i = er.getRandomNode().getId();
			j = er.getRandomNode().getId();
			// If adding a link is successful it returns true, false otherwise 
			if(er.addLink(i, j)) counter = counter + 1;
		}
		
		return er;
	}
	
	/***
	 * Generates normal Erdos-Renyi graph and then add few extra edges to connect all components.
	 * Resulting number of edges in graph might be larger than the number given as parameter for method.
	 */
	public static Graph contiguousErdosRenyi(int size, int edges) {
		
		Graph er = erdosRenyi(size, edges);
		ArrayList<Integer> csizes = GraphTools.findComponents(er);
		if(csizes.size()>1) GraphTools.connectAllComponents(er);
		return er;
	}
	
	public static Graph erdosRenyi(int size, int edgesOrDegree, String mode) {
		
		Graph er = new Graph(size);
		int i,j,counter=0;
		int edges;
		
		if(mode.equals("degree"))
			edges = edgesOrDegree*size/2;
		else
			edges = edgesOrDegree;
		
		// Proceed until all edges are added.
		while(counter<edges) {
			// Draw nodes to be connected
			i = er.getRandomNode().getId();
			j = er.getRandomNode().getId();
			// If adding a link is successful it returns true, false otherwise 
			if(er.addLink(i, j)) counter = counter + 1;
		}
		
		return er;
	}
	
	public static Graph contiguousErdosRenyi(int size, int edgesOrDegree, String mode) {
		
		Graph er = erdosRenyi(size, edgesOrDegree, mode);
		ArrayList<Integer> csizes = GraphTools.findComponents(er);
		if(csizes.size()>1) GraphTools.connectAllComponents(er);
		return er;
	}
	
	/***
	 * This class should be re-designed
	 * @param size
	 * @param m
	 * @return
	 */
	public static Graph randomTree(int size, int m) {
		
		Graph tree = GraphGenerator.kStar(m);
		int id = tree.getRandomNode().getId();
		
		// Proceed until all nodes are added.
		for(int i=m+2; i<=size; i++) {
			if(i % m == 0) id = tree.getRandomNode().getId();
			tree.addLink(id, i);
		}
				
		return tree;
	}
	
	public static Graph fullyConnected(int size) {
		
		Graph graph = new Graph();
		graph.addNode(1);
		for(int i=2; i<=size; i++)
			for(int j=1; j<i; j++)
				graph.addLink(i, j);
		
		return graph;
	}
	
	public static Graph kStar(int k) {
		
		Graph graph = new Graph();
		graph.addNode(1);
		for(int i=0; i<k; i++)
			graph.addLink(1, i+2);
		return graph;
	}
	
	public static Graph multiStar(ArrayList<Integer> kdegree) {
		Graph graph = new Graph();
		graph.addNode(1);
		kMultiStar(graph, kdegree, graph.getNode(1));
		return graph;
	}
	
	private static Node kMultiStar(Graph g, ArrayList<Integer> kdegree, Node center) {
		
		ArrayList<Integer> sub = new ArrayList<Integer>(kdegree.subList(1, kdegree.size()));
		if(kdegree.size()==1)
			for(int i=0; i<kdegree.get(0); i++)
				g.addLink(center, new Node(kdegree.get(0)*center.getId()+i));
		else
			for(int i=0; i<kdegree.get(0); i++)
				g.addLink(center, kMultiStar(g, sub,new Node(kdegree.get(0)*center.getId()+i)));
		return center;
	}
	
	public static Graph barabasiAlbert(int m0, int m, int size) {
		
		Graph ba = fullyConnected(m0);
		ArrayList<Node> randomNodes;
		
		for(int i=m0+1; i<=size; i++) {
			randomNodes = ba.getRandomNodesByDegree(m);
			for(Node chosen : randomNodes) ba.addLink(i, chosen.getId());
		}
		
		return ba;
	}
	
	public static Graph evolvingExponentialRandomGraph(int m0, int m, int size) {
		
		Graph erg = fullyConnected(m0);
		ArrayList<Node> randomNodes;
		
		for(int i=m0+1; i<=size; i++) {
			randomNodes = erg.getRandomNodes(m);
			for(Node chosen : randomNodes) erg.addLink(i, chosen.getId());
		}
		
		return erg;
	}

	public static Graph evolvingExponentialRandomGraph(int m0, int m, int size, double c) {
		
		Graph erg = fullyConnected(m0);
		Random rand = new Random();
		Node first;
		LinkedList<Node> neighbours;
		
		for(int i=m0+1; i<=size; i++) {
			first = erg.getRandomNode();
			neighbours = new LinkedList<Node>();
			for(Node n : first.getNeighbours()) neighbours.add(n);
			Collections.shuffle(neighbours);
			erg.addLink(i, first.getId());
			for(int j=0; j<m-1; j++)
				if(rand.nextDouble()<c){
					while(!erg.addLink(i, neighbours.pollLast().getId()));
				}
				else while(!erg.addLink(i, erg.getRandomNode().getId()));
		}
		
		return erg;
	}
	
	public static Graph exponentialRandomGraph(int size, int minDegree, int maxDegree, double lambda) {
		
		Random rand = new Random();
		int kmax = maxDegree;
		if(maxDegree>size-1) kmax = size-1;
		int kmin = minDegree;
		if(minDegree<0) kmin = 0;
		double[] prob = new double[size];
		double[] cdf = new double[size];
		double norm = 0;
		double x = 0;
		LinkedList<Integer> stubs = new LinkedList<Integer>();
		int start = 0;
		
		for(int i=kmin; i<=kmax; i++) {
			prob[i] = lambda*Math.exp(-lambda*i);
			norm = norm + prob[i];
		}

		for(int i=1; i<size; i++) {
			prob[i] = prob[i]/norm;
			cdf[i] = cdf[i-1] + prob[i];
		}
		
		for(int i=1; i<=size; i++){
			x = rand.nextDouble();
			for(int k=kmin; k<=kmax; k++)
				if(x<=cdf[k]){
					for(int j=0; j<k; j++) stubs.add(i);
					break;
				}
		}
		Collections.shuffle(stubs);
		
		Graph sfn = new Graph(size);
		while(!stubs.isEmpty()){
			start = stubs.pollLast();
			for(int i=stubs.size()-1; i>=0; i--)
				if(stubs.get(i)!=start && !sfn.containsLink(start, stubs.get(i))){
					sfn.addLink(start, stubs.remove(i));
					break;
				}				
		}
		return sfn;
	}
			
	public static Graph scaleFreeNetwork(int size, int minDegree, int maxDegree, double gamma) {
		
		Random rand = new Random();
		int kmax = maxDegree;
		if(maxDegree>size-1) kmax = size-1;
		int kmin = minDegree;
		if(minDegree<1) kmin = 1;
		double[] prob = new double[size];
		double[] cdf = new double[size];
		double norm = 0;
		double x = 0;
		LinkedList<Integer> stubs = new LinkedList<Integer>();
		int start = 0;
		
		for(int i=kmin; i<=kmax; i++) {
			prob[i] = Math.pow(i, -gamma);
			norm = norm + prob[i];
		}

		for(int i=1; i<size; i++) {
			prob[i] = prob[i]/norm;
			cdf[i] = cdf[i-1] + prob[i];
		}
		
		for(int i=1; i<=size; i++){
			x = rand.nextDouble();
			for(int k=kmin; k<=kmax; k++)
				if(x<=cdf[k]){
					for(int j=0; j<k; j++) stubs.add(i);
					break;
				}
		}
		Collections.shuffle(stubs);
		
		Graph sfn = new Graph(size);
		while(!stubs.isEmpty()){
			start = stubs.pollLast();
			for(int i=stubs.size()-1; i>=0; i--)
				if(stubs.get(i)!=start && !sfn.containsLink(start, stubs.get(i))){
					sfn.addLink(start, stubs.remove(i));
					break;
				}				
		}
		return sfn;
	}
	
	public static Graph configurationModelWithClustering(int[] degrees, double clustering) {
		Graph graph = new Graph(degrees.length);
		ArrayList<Node> nodes = graph.getNodesList();
		int numberOfTriangles = 0;
		int numberOfEdges = 0;
		int counter = 0;
		int[] residual = new int[degrees.length];
		ArrayList<Integer> stubs = new ArrayList<Integer>();
		//Compute the number of edges and triangles, prepare the initial list of stubs
		for(int i=0; i<degrees.length; i++) {
			residual[i] = degrees[i];
			numberOfEdges = numberOfEdges + degrees[i];
			numberOfTriangles = numberOfTriangles + degrees[i]*(degrees[i]-1)/2;
			if(residual[i]>=2) {
				counter = counter + 1;
				for(int d=0; d<residual[i]; d++)
					stubs.add(i);
			}
			
		}
		numberOfEdges = numberOfEdges/2;
		numberOfTriangles = (int) (numberOfTriangles*clustering/3);
		//Add triangles
		Random rand = new Random();
		int a,b,c;
		while(numberOfTriangles>0 && counter>=3) {
			//Choose first node
			a = stubs.get(rand.nextInt(stubs.size()));
			//Choose second node
			b = stubs.get(rand.nextInt(stubs.size()));
			while(b==a) b = stubs.get(rand.nextInt(stubs.size()));
			//Choose third node
			c = stubs.get(rand.nextInt(stubs.size()));
			while(c==a || c==b) c = stubs.get(rand.nextInt(stubs.size()));
			//Create (or not) first edge
			if(graph.addLink(nodes.get(a), nodes.get(b))) {
				residual[a] = residual[a] - 1;
				residual[b] = residual[b] - 1;
				numberOfEdges = numberOfEdges - 1;
			}
			//Create (or not) second edge
			if(graph.addLink(nodes.get(a), nodes.get(c))) {
				residual[a] = residual[a] - 1;
				residual[c] = residual[c] - 1;
				numberOfEdges = numberOfEdges - 1;
			}
			//Create (or not) second edge
			if(graph.addLink(nodes.get(b), nodes.get(c))) {
				residual[b] = residual[b] - 1;
				residual[c] = residual[c] - 1;
				numberOfEdges = numberOfEdges - 1;
			}
			numberOfTriangles = numberOfTriangles - 1;
			//Update the list of stubs
			stubs = new ArrayList<Integer>();
			counter = 0;
			for(int i=0; i<residual.length; i++)
				if(residual[i]>=2) {
					counter = counter + 1;
					for(int d=0; d<residual[i]; d++)
						stubs.add(i);
				}
		}
		//Add remaining edges
		while(numberOfEdges>0 && counter>=2) {
			//Choose first node
			a = stubs.get(rand.nextInt(stubs.size()));
			//Choose second node
			b = stubs.get(rand.nextInt(stubs.size()));
			while(b==a) b = stubs.get(rand.nextInt(stubs.size()));
			if(graph.addLink(nodes.get(a), nodes.get(b))) {
				residual[a] = residual[a] - 1;
				residual[b] = residual[b] - 1;
				numberOfEdges = numberOfEdges - 1;
			}
			//Update the list of stubs
			stubs = new ArrayList<Integer>();
			counter = 0;
			for(int i=0; i<residual.length; i++)
				if(residual[i]>0) {
					counter = counter + 1;
					for(int d=0; d<residual[i]; d++)
						stubs.add(i);
				}
		}	
		return graph;
	}
	
	public static int[] degreeSequence(GenericDistribution distribution, int size) {
		int[] degrees = new int[size];
		for(int i=0; i<size; i++) degrees[i] = (int) distribution.random();
		return degrees;
	}
	
	public static Graph regularRandomGraph(int size, int degree) {
		
		LinkedList<Integer> stubs = new LinkedList<Integer>();
		int start = 0;
				
		for(int i=1; i<=size; i++)
			for(int j=0; j<degree; j++) stubs.add(i);
		Collections.shuffle(stubs);
		
		Graph rrg = new Graph(size);
		while(!stubs.isEmpty()){
			start = stubs.pollLast();
			for(int i=stubs.size()-1; i>=0; i--)
				if(stubs.get(i)!=start && !rrg.containsLink(start, stubs.get(i))){
					rrg.addLink(start, stubs.remove(i));
					break;
				}				
		}
		return rrg;
	}
	
	/***
	 * Generates Stochastic Block Model which is Erdos-Renyi with two probablilities of connecting nodes.
	 * The methods assigns the nodes to m communities. The probability of creating a link within a community is different than
	 * probability of connecting nodes from different communities.
	 * @param m
	 * @param n
	 * @param densInter
	 * @param densOuter
	 * @return
	 */
	public static Graph generateSBM(int m, int n, double densInter, double densOuter) {
		
		Random rand = new Random();
		double prob = 0;
		Graph sbm = new Graph(m*n);
		
		for(int i=0; i<m; i++)
			for(int j=0; j<n; j++)
				sbm.getNodesList().get(i*n+j).setMembership(i+1);
		
		for(int i=1; i<=m*n-1; i++)
			for(int j=i+1; j<=m*n; j++) {
				if(sbm.getNode(i).getMembership()==sbm.getNode(j).getMembership()) prob = densInter;
				else prob = densOuter;
				if(rand.nextDouble()<prob)
					sbm.addLink(i, j);
			}
				
		return sbm;
	}

}