package graphs;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A wrapper for a HashMap with many paths.
 * The keys are constructed in following way: the path between the node with id=1 and the node with id=2 
 * has a key of String type "1->2"
 * @author Robert Paluch
 */

public class PathsRegister {
	
	/**
	 * Data fields
	 */
	
	HashMap<String, ArrayList<Path>> register;
	int numberOfSinglePaths;
	
	/**
	 * Constructors
	 */
	
	public PathsRegister() {
		this.register = new HashMap<String, ArrayList<Path>>();
		numberOfSinglePaths = 0;
	}
	
	/**
	 * Getters and setters
	 */
	
	public HashMap<String, ArrayList<Path>> getRegister() {
		return register;
	}
	
	public int getTotalNumberOfMultiPaths() {
		return register.size();
	}
	
	public int getTotalNumberOfSinglePaths() {
		return numberOfSinglePaths;
	}
	
	public ArrayList<Path> getPaths(int firstNodeId, int lastNodeId) {
		String key = Integer.toString(firstNodeId)+"->"+Integer.toString(lastNodeId);
		return register.get(key);
	}
	
	public ArrayList<Path> getPaths(Node firstNode, Node lastNode) {
		String key = Integer.toString(firstNode.getId())+"->"+Integer.toString(lastNode.getId());
		return register.get(key);
	}
	
	public ArrayList<ArrayList<Path>> getPaths(int firstNodeId, ArrayList<Integer> lastNodesIds) {
		ArrayList<ArrayList<Path>> paths = new ArrayList<ArrayList<Path>>();
		for(int i=0; i<lastNodesIds.size(); i++)
			paths.add(this.getPaths(firstNodeId, lastNodesIds.get(i)));
		return paths;
	}
	
	public ArrayList<ArrayList<Path>> getPaths(Node firstNode, ArrayList<Node> lastNodes) {
		ArrayList<ArrayList<Path>> paths = new ArrayList<ArrayList<Path>>();
		for(int i=0; i<lastNodes.size(); i++)
			paths.add(this.getPaths(firstNode, lastNodes.get(i)));
		return paths;
	}
		
	public int getPathLength(int firstNodeId, int lastNodeId) {
		String key = Integer.toString(firstNodeId)+"->"+Integer.toString(lastNodeId);
		return register.get(key).get(0).getLength();
	}
	
	public int getPathLength(Node firstNode, Node lastNode) {
		if(lastNode==null)
			System.out.println("lastNode is null");
		String key = Integer.toString(firstNode.getId())+"->"+Integer.toString(lastNode.getId());
		return register.get(key).get(0).getLength();
	}
	
	public ArrayList<Integer> getPathLengths(int firstNodeId, ArrayList<Integer> lastNodesIds) {
		ArrayList<Integer> spl = new ArrayList<Integer>();
		for(int i=0; i<lastNodesIds.size(); i++)
			spl.add(this.getPathLength(firstNodeId, lastNodesIds.get(i)));
		return spl;
	}
	
	public ArrayList<Integer> getPathLengths(Node firstNode, ArrayList<Node> lastNodes) {
		ArrayList<Integer> spl = new ArrayList<Integer>();
		for(int i=0; i<lastNodes.size(); i++)
			spl.add(this.getPathLength(firstNode, lastNodes.get(i)));
		return spl;
	}
	
	public double getAveragePathLength(int firstNodeId, ArrayList<Integer> lastNodesIds) {
		double average = 0.0;
		ArrayList<Integer> spl =  getPathLengths(firstNodeId, lastNodesIds);
		for(Integer i : spl)
			average = average + i.doubleValue();
		return average/spl.size();
	}
	
	public int getNumberOfPaths(int firstNodeId, int lastNodeId) {
		String key = Integer.toString(firstNodeId)+"->"+Integer.toString(lastNodeId);
		return register.get(key).size();
	}
	
	public int getNumberOfPaths(Node firstNode, Node lastNode) {
		String key = Integer.toString(firstNode.getId())+"->"+Integer.toString(lastNode.getId());
		return register.get(key).size();
	}
	
	public String getPathsAsString(int firstNodeId, int lastNodeId) {
		String string = "";
		ArrayList<Path> paths = this.getPaths(firstNodeId, lastNodeId);
		for(int i=0; i<paths.size()-1; i++)
			string = string + paths.get(i).toString() + "\n";
		string = string + paths.get(paths.size()-1).toString();
		return string;
	}
	
	public String getPathsAsString(Node firstNode, Node lastNode) {
		String string = "";
		ArrayList<Path> paths = this.getPaths(firstNode, lastNode);
		for(int i=0; i<paths.size()-1; i++)
			string = string + paths.get(i).toString() + "\n";
		string = string + paths.get(paths.size()-1).toString();
		return string;
	}
	
	public String getAllPathsAsString() {
		String string = "";
		for(ArrayList<Path> paths: register.values()) {
			for(int i=0; i<paths.size(); i++)
				string = string + paths.get(i).toString() + "\n";
		}
		return string;
	}
	
	/**
	 * Other methods
	 */
		
	public void add(Path path) {
		if(!path.isEmpty()) {
			String key = Integer.toString(path.getFirst().getId())+"->"+Integer.toString(path.getLast().getId());
			ArrayList<Path> value = register.get(key);
			if(value==null){
				value = new ArrayList<Path>();
			}
			value.add(path);
			numberOfSinglePaths = numberOfSinglePaths + 1;
			register.put(key, value);
		}
	}
	
	public void addAll(ArrayList<Path> paths) {
		for(Path path : paths)
			this.add(path);
	}
	
	public void addAllAll(ArrayList<ArrayList<Path>> allPaths) {
		for(ArrayList<Path> paths : allPaths)
			this.addAll(paths);
	}
}
