package graphs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class NodesSelectors {
	
	public static ArrayList<Node> intersection(ArrayList<Node> list1, ArrayList<Node> list2) {
		ArrayList<Node> intersection = new ArrayList<Node>();
		for(Node node1: list1)
			if(list2.contains(node1))
				intersection.add(node1);
		return intersection;
	}
	
	public static double jaccardIndex(ArrayList<Node> list1, ArrayList<Node> list2) {
		double jaccard = 0;
		ArrayList<Node> intersection = new ArrayList<Node>();
		ArrayList<Node> union = new ArrayList<Node>();
		for(Node node1: list1) {
			union.add(node1);
			if(list2.contains(node1)) intersection.add(node1);
		}
		for(Node node2 : list2)
			if(!intersection.contains(node2)) union.add(node2);
		jaccard = 1.0*intersection.size()/union.size();
		return jaccard;
	}
	
	public static ArrayList<Node> fastCollectiveBetweenness(Graph graph, PathsRegister register, int budget) {
		int size = graph.getNumberOfNodes();
		ArrayList<HashSet<Integer>> nodesToPaths = new ArrayList<HashSet<Integer>>();
		for(int i=0; i<size; i++)
			nodesToPaths.add(new HashSet<Integer>());
		ArrayList<Double> pathsWeights = new ArrayList<Double>();
		ArrayList<Path> pathsToNodes = new ArrayList<Path>();
		
		for(ArrayList<Path> paths: register.getRegister().values())
			for(Path path: paths) {
				pathsWeights.add(1.0/paths.size());
				pathsToNodes.add(path);
				for(int n=1; n<path.getLength(); n++) {
					nodesToPaths.get(path.getNode(n).getIndex()).add(pathsWeights.size()-1);
					path.getNode(n).setBetweenness(path.getNode(n).getBetweenness()+1.0/paths.size());
				}
			}
		
		ArrayList<Node> selectedObservers = new ArrayList<Node>();
		LinkedList<Node> candidates = new LinkedList<Node>();
		for(Node n : graph.getNodesList())
			candidates.add(n);
		Node newObserver = null;
		Path newPath = null;
		while(selectedObservers.size()<budget) {
			newObserver = Collections.max(candidates, GraphTools.nodeBetweennessAscending);
			candidates.remove(newObserver);
			selectedObservers.add(newObserver);
			for(Integer pathId : nodesToPaths.get(newObserver.getIndex())) {
				newPath = pathsToNodes.get(pathId);
				for(int n=1; n<newPath.getLength(); n++) {
					if(newPath.getNode(n).getIndex()!=newObserver.getIndex()) {
						nodesToPaths.get(newPath.getNode(n).getIndex()).remove(pathId);
						newPath.getNode(n).setBetweenness(newPath.getNode(n).getBetweenness()-pathsWeights.get(pathId));
					}
				}
			}
		}
				
		return selectedObservers;
	}
	
	public static ArrayList<Node> leastShortestPaths(Graph graph, int budget, int minLength) {
		ArrayList<Node> nodes = new ArrayList<Node>(graph.getNodesList());
		ArrayList<Integer> sumOfShortestPaths = new ArrayList<Integer>();
		ArrayList<ArrayList<Path>> allPaths;
		
		for(int i=0; i<nodes.size(); i++) {
			sumOfShortestPaths.add(0);
			allPaths = PathsTools.allShortestPaths(graph, nodes.get(i).getId());
			for(ArrayList<Path> paths : allPaths)
				if(paths.get(0).getLength()>=minLength) sumOfShortestPaths.set(i, sumOfShortestPaths.get(i)+paths.size());
		}
		
		nodes.sort(Comparator.comparingInt(s -> sumOfShortestPaths.get(((Node)s).getIndex())));
		return new ArrayList<>(nodes.subList(0, budget));
	}
	
	public static ArrayList<Node> fastCollectiveBetweenness(Graph graph, int budget, int maxLength) {
		int size = graph.getNumberOfNodes();
		// Declarations and initializations of the lists
		ArrayList<HashSet<Integer>> nodesToPaths = new ArrayList<HashSet<Integer>>();
		ArrayList<Path> allPaths = new ArrayList<Path>();
		ArrayList<Double> allPathsWeights = new ArrayList<Double>();
		ArrayList<ArrayList<Path>> allPathsFromGivenSource;
		
		// Initialize the Hashsets and clear the betweenness centrality of all nodes
		for(int i=0; i<size; i++) {
			nodesToPaths.add(new HashSet<Integer>());
			graph.getNodesList().get(i).setBetweenness(0.0);
		}
		
		// Find all limited shortest paths and their weights between all pairs of nodes
		for(Node source : graph.getNodesList()){
			allPathsFromGivenSource = PathsTools.allLimitedShortestPaths(graph, source.getId(), maxLength);
			for(ArrayList<Path> paths: allPathsFromGivenSource)
				for(Path path: paths) {
					allPathsWeights.add(1.0/paths.size());
					allPaths.add(path);
					for(int n=1; n<path.getLength(); n++) {
						nodesToPaths.get(path.getNode(n).getIndex()).add(allPathsWeights.size()-1);
						path.getNode(n).setBetweenness(path.getNode(n).getBetweenness()+1.0/paths.size());
					}
				}
			allPathsFromGivenSource = null;
		}
		
		ArrayList<Node> selectedObservers = new ArrayList<Node>();
		LinkedList<Node> candidates = new LinkedList<Node>();
		for(Node n : graph.getNodesList())
			candidates.add(n);
		Node newObserver = null;
		Path newPath = null;
		while(selectedObservers.size()<budget) {
			newObserver = Collections.max(candidates, GraphTools.nodeBetweennessAscending);
			candidates.remove(newObserver);
			selectedObservers.add(newObserver);
			for(Integer pathId : nodesToPaths.get(newObserver.getIndex())) {
				newPath = allPaths.get(pathId);
				for(int n=1; n<newPath.getLength(); n++) {
					if(newPath.getNode(n).getIndex()!=newObserver.getIndex()) {
						nodesToPaths.get(newPath.getNode(n).getIndex()).remove(pathId);
						newPath.getNode(n).setBetweenness(newPath.getNode(n).getBetweenness()-allPathsWeights.get(pathId));
					}
				}
			}
		}
				
		return selectedObservers;
	}
	
	public static ArrayList<Node> greedyCoverageObservers(Graph graph, int budget) {
		// Initialization
		ArrayList<Node> observers = new ArrayList<Node>();
		LinkedList<Node> candidates = new LinkedList<Node>(graph.getNodesList());
		for(Node c : candidates) c.setValue(c.getDegree());
		double numberOfCoveredNodes = 0;
		Node nextObserver;

		// Keep adding observers until the size of the list is equal to budget
		while(observers.size()<budget) {
			if(numberOfCoveredNodes == graph.getNumberOfNodes()) {
				for(Node c : candidates) c.setValue(c.getDegree());
				for(Node n : graph.getNodesList()) n.setFlag(false);
				numberOfCoveredNodes = 0;
			}
			nextObserver = Collections.max(candidates, GraphTools.nodeValueAscending);
			//System.out.printf("%d\t%d\n",nextObserver.getId(), nextObserver.getDegree());
			candidates.remove(nextObserver);
			observers.add(nextObserver);
			numberOfCoveredNodes = numberOfCoveredNodes + nextObserver.getValue();
			if(!nextObserver.getFlag()) {
				//nextObserver.value = nextObserver.value - 1;
				nextObserver.setFlag(true);
				for(Node neighbour : nextObserver.getNeighbours()) {
					neighbour.value = neighbour.value - 1;
					if(!neighbour.getFlag()) {
						//neighbour.value = neighbour.value - 1;
						neighbour.setFlag(true);
						for(Node nn : neighbour.getNeighbours())
							nn.value = nn.value - 1;
					}
				}
			}
			else {
				for(Node neighbour : nextObserver.getNeighbours()) {
					if(!neighbour.getFlag()) {
						//neighbour.value = neighbour.value - 1;
						neighbour.setFlag(true);
						for(Node nn : neighbour.getNeighbours())
							nn.value = nn.value - 1;
					}
				}
			}	 
		}
		
		return observers;
	}
	
	public static ArrayList<Node> greedyLSPParallel(Graph graph, int budget, int minLength, int numberOfThreads) {
		PathsRegister allPaths = PathsTools.allShortestPathsInGraph(graph);
		GreedyLSPSelector[] selectors = new GreedyLSPSelector[graph.getNumberOfNodes()];
		ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
		ArrayList<Node> nodes = graph.getNodesList();
		for(int i=0; i<nodes.size(); i++) {
			selectors[i] = new GreedyLSPSelector(graph, allPaths, nodes.get(i), budget, minLength);
			executor.execute(selectors[i]);
		}
		executor.shutdown();
		
		while(!executor.isTerminated()) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		ArrayList<Node> bestObservers = null;
		int minNumberOfShortestPaths = Integer.MAX_VALUE;
		for(int i=0; i<nodes.size(); i++)
			if(selectors[i].getSumOfShortestPaths()<minNumberOfShortestPaths) {
				bestObservers = selectors[i].getSelectedObservers();
				minNumberOfShortestPaths = selectors[i].getSumOfShortestPaths();
			}
		return bestObservers;
	}
	
	public static ArrayList<Node> greedyHVObsParallel(Graph graph, int budget, int maxLength, int numberOfThreads){
		PathsRegister allPaths = PathsTools.singleLimitedShortestPathsInGraph(graph, maxLength);
		
		int startersPerThread = (int) Math.ceil(1.0*graph.getNumberOfNodes()/numberOfThreads);
		ArrayList<ArrayList<Node>> starters = new ArrayList<ArrayList<Node>>();
		int s = 0;
		for(int t=0; t<numberOfThreads; t++) {
			starters.add(new ArrayList<Node>());
			while(s<Math.min(startersPerThread*(t+1),graph.getNumberOfNodes())) {
				starters.get(t).add(graph.getNodesList().get(s));
				s = s + 1;
			}
		}
		GreedyHVObsSelector[] selectors = new GreedyHVObsSelector[numberOfThreads];
		ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
		
		for(int i=0; i<numberOfThreads; i++) {
			selectors[i] = new GreedyHVObsSelector(graph, allPaths, starters.get(i), budget, maxLength);
			executor.execute(selectors[i]);
		}
		executor.shutdown();
		
		while(!executor.isTerminated()) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		ArrayList<Node> bestSet = null;
		//ArrayList<Node> aSet = null;
		int bestCoverage = 0;	
		for(int i=0; i<selectors.length; i++) {
			//aSet = selectors[i].getSelectedObservers();
			//for(Node n : aSet)
			//	System.out.printf("%d ", n.getId());
			//System.out.printf("%d\n", selectors[i].getCoverage());
			if(selectors[i].getBestCoverage()>bestCoverage){
				bestSet = selectors[i].getSelectedObservers();
				bestCoverage = selectors[i].getBestCoverage();
			}
		}
		return bestSet;
	}
	
	public static ArrayList<Node> greedyCoverageObsParallel(Graph graph, int budget, int numberOfThreads){
		
		int startersPerThread = (int) Math.ceil(1.0*graph.getNumberOfNodes()/numberOfThreads);
		ArrayList<ArrayList<Node>> starters = new ArrayList<ArrayList<Node>>();
		int s = 0;
		for(int t=0; t<numberOfThreads; t++) {
			starters.add(new ArrayList<Node>());
			while(s<Math.min(startersPerThread*(t+1),graph.getNumberOfNodes())) {
				starters.get(t).add(graph.getNodesList().get(s));
				s = s + 1;
			}
		}
		GreedyCoverageObsSelector[] selectors = new GreedyCoverageObsSelector[numberOfThreads];
		ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
		
		for(int i=0; i<numberOfThreads; i++) {
			selectors[i] = new GreedyCoverageObsSelector(graph, starters.get(i), budget);
			executor.execute(selectors[i]);
		}
		executor.shutdown();
		
		while(!executor.isTerminated()) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		ArrayList<Node> bestSet = null;
		int bestCoverage = 0;	
		for(int i=0; i<selectors.length; i++) {
			if(selectors[i].getBestCoverage()>bestCoverage){
				bestSet = selectors[i].getSelectedObservers();
				bestCoverage = selectors[i].getBestCoverage();
			}
		}
		
		return bestSet;
	}
	
	public static ArrayList<Node> greedyKMedianObsParallel(Graph graph, int[][] distances, int budget, int numberOfThreads){
		
		int startersPerThread = (int) Math.ceil(1.0*graph.getNumberOfNodes()/numberOfThreads);
		ArrayList<ArrayList<Node>> starters = new ArrayList<ArrayList<Node>>();
		int s = 0;
		for(int t=0; t<numberOfThreads; t++) {
			starters.add(new ArrayList<Node>());
			while(s<Math.min(startersPerThread*(t+1),graph.getNumberOfNodes())) {
				starters.get(t).add(graph.getNodesList().get(s));
				s = s + 1;
			}
		}
		GreedyKMedianObsSelector[] selectors = new GreedyKMedianObsSelector[numberOfThreads];
		ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
		
		for(int i=0; i<numberOfThreads; i++) {
			selectors[i] = new GreedyKMedianObsSelector(graph, distances, starters.get(i), budget);
			executor.execute(selectors[i]);
		}
		executor.shutdown();
		
		while(!executor.isTerminated()) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		ArrayList<Node> bestSet = null;
		int bestDistance = Integer.MAX_VALUE;	
		for(int i=0; i<selectors.length; i++) {
			if(selectors[i].getBestDistance()<bestDistance){
				bestSet = selectors[i].getSelectedObservers();
				bestDistance = selectors[i].getBestDistance();
			}
		}
		
		return bestSet;
	}
	
	public static ArrayList<Node> findLocalHubs(Graph graph, int numberOfThreads) {
		int size = graph.getNumberOfNodes();
		LocalHubsSelector[] selectors = new LocalHubsSelector[size];
		ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
		
		for(int i=0; i<size; i++) {
			selectors[i] = new LocalHubsSelector(graph, graph.getNodesList().get(i));
			executor.execute(selectors[i]);
		}
		executor.shutdown();
		
		while(!executor.isTerminated()) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		ArrayList<Node> localHubs = new ArrayList<Node>();
		for(int i=0; i<size; i++)
			if(selectors[i].isLocalHub())
				localHubs.add(selectors[i].getCandidate());
		
		return localHubs;
	}
	
	public static class GreedyLSPSelector implements Runnable {

		/***
		 * Data fields
		 */
		
		Graph graph;
		Node starter;
		int budget;
		int minLength;
		ArrayList<Node> selectedObservers;
		int sumOfShortestPaths;
		PathsRegister register;
		
		/***
		 * Constructor
		 */
		
		public GreedyLSPSelector(Graph graph, PathsRegister register, Node starter, int budget, int minLength) {
			super();
			this.graph = graph;
			this.register = register;
			this.budget = budget;
			this.starter = starter;
			this.minLength = minLength;
			selectedObservers = new ArrayList<Node>();
			sumOfShortestPaths = 0;
		}
		
		/***
		 * Run method
		 */
		
		@Override
		public void run() {
			Node newCandidate = null;
			LinkedList<Node> candidates = new LinkedList<Node>();
			for(Node n : graph.getNodesList()) candidates.add(n);
			selectedObservers.add(starter);
			candidates.remove(starter);
			while(selectedObservers.size()<budget) {
				newCandidate = findNext(selectedObservers, candidates);
				selectedObservers.add(newCandidate);
				candidates.remove(newCandidate);
			}
		}
		
		/***
		 * Private methods
		 */
		
		private Node findNext(ArrayList<Node> observers, LinkedList<Node> candidates) {
			Node bestCandidate = null;
			int minShortestPaths = Integer.MAX_VALUE;
			int numberOfShortestPaths = 0;
			ArrayList<ArrayList<Path>> pathsToCandidate;
			for(Node candidate : candidates) {
				numberOfShortestPaths = 0;
				pathsToCandidate = register.getPaths(candidate, observers);
				for(ArrayList<Path> paths : pathsToCandidate)
					if(paths.get(0).getLength()>=minLength) numberOfShortestPaths = numberOfShortestPaths + paths.size();
				if(numberOfShortestPaths<minShortestPaths) {
					bestCandidate = candidate;
					minShortestPaths = numberOfShortestPaths;
				}
			}
			sumOfShortestPaths = sumOfShortestPaths + minShortestPaths;
			return bestCandidate;
		}
		
		/***
		 * Getters
		 */
		
		public ArrayList<Node> getSelectedObservers() {
			return selectedObservers;
		}
		
		public int getSumOfShortestPaths() {
			return sumOfShortestPaths;
		}
				
	}
	
	public static class LocalHubsSelector implements Runnable {

		/***
		 * Data fields
		 */
		
		Graph graph;
		Node candidate;
		boolean hub;
		
		/***
		 * Constructor
		 */
		
		public LocalHubsSelector(Graph graph, Node candidate) {
			super();
			this.graph = graph;
			this.candidate = candidate;
			this.hub = false;
		}
		
		/***
		 * Run method
		 */
		
		@Override
		public void run() {
			int maxDegree = 2;
			for(Node neighbour : candidate.getNeighbours())
				if(neighbour.getDegree()>maxDegree) maxDegree=neighbour.getDegree();
			if(candidate.getDegree()>=maxDegree) hub=true;
		}
		
		/***
		 * Getters
		 */
		
		public Node getCandidate() {
			return candidate;
		}
		
		public boolean isLocalHub() {
			return hub;
		}
		
	}

	public static class GreedyHVObsSelector implements Runnable {

		/***
		 * Data fields
		 */
		
		Graph graph;
		// The number of nodes in the graph
		int graphSize;
		// The list with the shortest paths between all pairs of the nodes in the graph
		PathsRegister register;
		// The starting nodes - the first nodes in the sets of observers
		ArrayList<Node> starters;
		// The number of observers in the set
		int budget;
		// The maximal distance between an observer and another observer
		int maxLength;
		// The set of observers which maximize the coverage with given constraints
		ArrayList<Node> selectedObservers;
		// The temporal coverage
		int temporalCoverage;
		// The maximum possible coverage with given constraints
		int bestCoverage;
		// The information if a given node is already covered
		int[] covered;
		
		/***
		 * Constructors
		 */
		
		public GreedyHVObsSelector(Graph graph, PathsRegister register, ArrayList<Node> starters, int budget, int maxLength) {
			super();
			this.graph = graph;
			this.graphSize = graph.getNumberOfNodes();
			this.register = register;
			this.starters = starters;
			this.budget = budget;
			this.maxLength = maxLength;
			this.temporalCoverage = 0;
			this.bestCoverage = 0;
		}
		
		/***
		 * Run method
		 */
		
		@Override
		public void run() {
			LinkedList<Node> candidates;
			ArrayList<Node> newSet;
			Node bestCandidate = null;
			
			for(Node starter : starters) {
				temporalCoverage = 0;
				covered = new int[graph.getNumberOfNodes()];
				candidates = new LinkedList<Node>();
				for(Node n : graph.getNodesList())
					candidates.add(n);
				candidates.remove(starter);
				newSet = new ArrayList<Node>();
				newSet.add(starter);
				while(newSet.size()<budget){
					bestCandidate = findNext(newSet, candidates);
					if(bestCandidate!=null) {
						newSet.add(bestCandidate);
						candidates.remove(bestCandidate);
					}
					else rescale();
				}
				//System.out.println(temporalCoverage);
				if(temporalCoverage>bestCoverage){
					selectedObservers = newSet;
					bestCoverage = temporalCoverage;
				}
			}
		}
		
		public Node findNext(ArrayList<Node> observers, LinkedList<Node> candidates) {
			Node bestCandidate = null;
			int maxCoverage = 0;
			HashSet<Node> newCovered = null;
			HashSet<Node> totalCovered = null;
			HashSet<Node> maxCovered = null;
			ArrayList<ArrayList<Path>> allPaths;
			
			for(Node cand: candidates) {
				totalCovered = new HashSet<Node>();
				newCovered = new HashSet<Node>();
				allPaths = register.getPaths(cand, observers);
				for(ArrayList<Path> paths : allPaths)
					if(paths!=null)
						if(paths.get(0).getLength()<=maxLength)
							for(int n=1; n<paths.get(0).getLength(); n++) {
								totalCovered.add(paths.get(0).getNode(n));
								if(covered[paths.get(0).getNode(n).getIndex()]==0)
									newCovered.add(paths.get(0).getNode(n));
							}
				if(newCovered.size()>maxCoverage) {
					maxCoverage = newCovered.size();
					maxCovered = totalCovered;
					bestCandidate = cand;
				}
			}
			
			if(maxCoverage>0) {
				temporalCoverage = temporalCoverage + maxCoverage;
				for(Node n : maxCovered)
					covered[n.getIndex()] = covered[n.getIndex()] + 1;
			}
			
			return bestCandidate;
		}
		
		public void rescale() {
			//System.out.println("Rescale!");
			int counter = 0;
			for(int i=0; i<graphSize; i++) {
				covered[i] = covered[i] - 1;
				if(covered[i]>0)
					counter = counter + 1;
			}
			temporalCoverage = temporalCoverage + counter;
		}

		/***
		 * Getters
		 */
		
		public ArrayList<Node> getSelectedObservers() {
			return selectedObservers;
		}
		
		public int getBestCoverage() {
			return bestCoverage;
		}
		
	};
	
	public static class GreedyCoverageObsSelector implements Runnable {

		/***
		 * Data fields
		 */
		
		Graph graph;
		// The number of nodes in the graph
		int graphSize;
		// The starting nodes - the first nodes in the sets of observers
		ArrayList<Node> starters;
		// The number of observers in the set
		int budget;
		// The set of observers which maximize the coverage with given constraints
		ArrayList<Node> selectedObservers;
		// The temporal coverage
		int temporalCoverage;
		// The maximum possible coverage with given constraints
		int bestCoverage;
		// The information if a given node is already covered
		int[] covered;
		
		/***
		 * Constructors
		 */
		
		public GreedyCoverageObsSelector(Graph graph, ArrayList<Node> starters, int budget) {
			super();
			this.graph = graph;
			this.graphSize = graph.getNumberOfNodes();
			this.starters = starters;
			this.budget = budget;
			this.temporalCoverage = 0;
			this.bestCoverage = 0;
		}
		
		/***
		 * Run method
		 */
		
		@Override
		public void run() {
			LinkedList<Node> candidates;
			ArrayList<Node> newSet;
			Node bestCandidate = null;
			
			for(Node starter : starters) {
				temporalCoverage = 0;
				covered = new int[graphSize];
				candidates = new LinkedList<Node>();
				for(Node n : graph.getNodesList())
					candidates.add(n);
				candidates.remove(starter);
				newSet = new ArrayList<Node>();
				newSet.add(starter);
				while(newSet.size()<budget){
					bestCandidate = findNext(newSet, candidates);
					if(bestCandidate!=null) {
						newSet.add(bestCandidate);
						candidates.remove(bestCandidate);
					}
					else rescale();
				}
				if(temporalCoverage>bestCoverage){
					selectedObservers = newSet;
					bestCoverage = temporalCoverage;
				}
			}
		}
		
		public Node findNext(ArrayList<Node> observers, LinkedList<Node> candidates) {
			Node bestCandidate = null;
			int maxCoverage = 0;
			int newCovered = 0;
			
			for(Node cand: candidates) {
				newCovered = 0;
				for(Node n : cand.getNeighbours())
					if(covered[n.getIndex()]==0)
						newCovered = newCovered + 1;
				if(newCovered>maxCoverage) {
					maxCoverage = newCovered;
					bestCandidate = cand;
				}
			}
			
			if(maxCoverage>0) {
				temporalCoverage = temporalCoverage + maxCoverage;
				for(Node n : bestCandidate.getNeighbours())
					covered[n.getIndex()]=covered[n.getIndex()]+1;
			}
			
			return bestCandidate;
		}
		
		public void rescale() {
			int counter = 0;
			for(int i=0; i<graphSize; i++) {
				covered[i] = covered[i] - 1;
				if(covered[i]>0)
					counter = counter + 1;
			}
			temporalCoverage = temporalCoverage + counter;
		}
		
		/***
		 * Getters
		 */
		
		public ArrayList<Node> getSelectedObservers() {
			return selectedObservers;
		}
		
		public int getBestCoverage() {
			return bestCoverage;
		}
		
	};
	
	public static class GreedyKMedianObsSelector implements Runnable {

		/***
		 * Data fields
		 */
		
		Graph graph;
		// The number of nodes in the graph
		int graphSize;
		// The register with all shortest paths between all pairs of the nodes in the graph
		int[][] allDistances;
		// The starting nodes - the first nodes in the sets of observers
		ArrayList<Node> starters;
		// The number of observers in the set
		int budget;
		// The set of observers which maximize the coverage with given constraints
		ArrayList<Node> selectedObservers;
		// The temporal coverage
		int temporalDistance;
		// The maximum possible coverage with given constraints
		int bestDistance;
		// Array with distances to the nearest observers
		int[] distancesToObservers;
		
		/***
		 * Constructors
		 */
		
		public GreedyKMedianObsSelector(Graph graph, int[][] allDistances, ArrayList<Node> starters, int budget) {
			super();
			this.graph = graph;
			this.graphSize = graph.getNumberOfNodes();
			this.allDistances = allDistances;
			this.starters = starters;
			this.budget = budget;
			this.temporalDistance = Integer.MAX_VALUE;
			this.bestDistance = Integer.MAX_VALUE;
			this.distancesToObservers = new int[graphSize];
		}
		
		/***
		 * Run method
		 */
		
		@Override
		public void run() {
			LinkedList<Node> candidates;
			ArrayList<Node> newSet;
			Node bestCandidate = null;
			
			for(Node starter : starters) {
				candidates = new LinkedList<Node>();
				for(Node n : graph.getNodesList())
					candidates.add(n);
				candidates.remove(starter);
				newSet = new ArrayList<Node>();
				newSet.add(starter);
				initDistances(starter);
				while(newSet.size()<budget){
					bestCandidate = findNext(candidates);
					newSet.add(bestCandidate);
					candidates.remove(bestCandidate);
				}
				if(temporalDistance<bestDistance){
					selectedObservers = newSet;
					bestDistance = temporalDistance;
				}
			}
		}
		
		public void initDistances(Node initNode) {
			temporalDistance = Integer.MAX_VALUE;
			distancesToObservers = new int[graphSize];
			for(int i=0; i<graphSize; i++)
				distancesToObservers[i] = allDistances[initNode.getIndex()][i];
		}
				
		public Node findNext(LinkedList<Node> candidates) {
			Node bestCandidate = null;
			int[] bestDistances = null;
			int[] newDistances = new int[graphSize];
			int sumOfDistances = 0;
			int minSumOfDistances = Integer.MAX_VALUE;
			
			for(Node cand: candidates) {
				sumOfDistances = 0;
				newDistances = new int[graphSize];
				for(int i=0; i<graphSize; i++) {
					newDistances[i] = Math.min(distancesToObservers[i], allDistances[cand.getIndex()][i]);
					sumOfDistances = sumOfDistances + newDistances[i];
				}
				if(sumOfDistances<minSumOfDistances) {
					minSumOfDistances = sumOfDistances;
					bestCandidate = cand;
					bestDistances = newDistances;
				}
			}
			
			distancesToObservers = bestDistances;
			temporalDistance = minSumOfDistances;
			
			return bestCandidate;
		}
		
		/***
		 * Getters
		 */
		
		public ArrayList<Node> getSelectedObservers() {
			return selectedObservers;
		}
		
		public int getBestDistance() {
			return bestDistance;
		}
		
	};

}
