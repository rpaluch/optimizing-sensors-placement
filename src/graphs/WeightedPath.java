package graphs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Basic class for a path, which is sequence of nodes and sequence of weights. It's a wrapper for LinkedList<Node> and LinkedList<Double>
 * @author Robert Paluch
 */

public class WeightedPath {
	
	/**
	 * Data fields
	 */
	
	LinkedList<Node> sequence;
	LinkedList<Double> weights;
	
	/**
	 * Constructors
	 */
	
	public WeightedPath(){
		this.sequence = new LinkedList<Node>();
		this.weights = new LinkedList<Double>();
	}
	
	public WeightedPath(Node node){
		this.sequence = new LinkedList<Node>();
		this.sequence.add(node);
		this.weights = new LinkedList<Double>();
		this.weights.add(0.0);
	}
	
	public WeightedPath(List<Node> sequence) {
		this.sequence = new LinkedList<Node>();
		this.weights = new LinkedList<Double>();
		for(int i=0; i<sequence.size(); i++) {
			this.sequence.add(sequence.get(i));
			this.weights.add(0.0);
		}
	}
	
	public WeightedPath(List<Node> sequence, List<Double> weights) {
		this.sequence = new LinkedList<Node>();
		this.weights = new LinkedList<Double>();
		for(int i=0; i<sequence.size(); i++) {
			this.sequence.add(sequence.get(i));
			this.weights.add(weights.get(i));
		}
	}
	
	public WeightedPath(WeightedPath path) {
		this.sequence = new LinkedList<Node>();
		this.weights = new LinkedList<Double>();
		for(int i=0; i<path.getNumberOfNodes(); i++) {
			this.sequence.add(path.getNode(i));
			this.weights.add(path.getWeight(i));
		}
	}

	/**
	 * Getters and setters
	 */
	
	public LinkedList<Node> getSequence() {
		return sequence;
	}
	
	public LinkedList<Double> getWeights() {
		return weights;
	}

	public void setSequence(LinkedList<Node> sequence) {
		this.sequence = sequence;
	}
	
	public double getLength() {
		double length = 0.0;
		for(int i=1; i<weights.size(); i++) length = length + weights.get(i);
		return length;
	}
	
	public int getNumberOfLinks() {
		if(sequence.size()>0 )
			return sequence.size()-1;
		else return 0;
	}
	
	public int getNumberOfNodes() {
		return sequence.size();
	}
	
	public Node getNode(int index) {
		return sequence.get(index);
	}
	
	public void setNode(int index, Node node) {
		sequence.set(index, node);
	}
	
	public double getWeight(int index) {
		return weights.get(index);
	}
	
	public void setWeight(int index, double weight) {
		weights.set(index, weight);
	}
	
	public Node getFirstNode() {
		return sequence.getFirst();
	}
	
	public Node getLastNode() {
		return sequence.getLast();
	}
	
	/**
	 * Public methods
	 */
	
	public boolean isEmpty() {
		return sequence.isEmpty();
	}
	
	public void add(Node node) {
		sequence.add(node);
		weights.add(0.0);
	}
	
	public void add(Node node, double weight) {
		sequence.add(node);
		weights.add(weight);
	}
	
	public void add(WeightedPath path, double weight) {
		int index = weights.size();
		for(int i=0; i<path.getNumberOfNodes(); i++) {
			sequence.add(path.getNode(i));
			weights.add(path.getWeight(i));
		}
		weights.set(index, weight);
	}
	
	public void push(Node node) {
		sequence.push(node);
		weights.push(0.0);
	}
	
	public void push(Node node, double weight) {
		sequence.push(node);
		weights.push(weight);
	}
		
	public void remove(Node node) {
		int index = sequence.indexOf(node);
		sequence.remove(index);
		weights.remove(index);
	}
	
	public WeightedPath subPath(int fromIndex, int toIndex) {
		return new WeightedPath(sequence.subList(fromIndex, toIndex), weights.subList(fromIndex, toIndex));
	}
	
	public boolean contains(Node node) {
		return sequence.contains(node);
	}
	
	public void fillWeights(double weight) {
		for(int i=1; i<weights.size(); i++)
			weights.set(i, weight);
	}
	
	public ArrayList<Link> union(WeightedPath path) {
		ArrayList<Link> union = new ArrayList<Link>(this.toLinksList());
		ArrayList<Link> complementery = this.complementaryLinks(path);
		union.addAll(complementery);
		return union;
	}
	
	// Returns the links which occur in both paths
	public ArrayList<Link> overlappingLinks(WeightedPath path) {
		ArrayList<Link> overlapped = new ArrayList<Link>();
		LinkedList<Link> list1 = this.toLinksList();
		LinkedList<Link> list2 = path.toLinksList();
		for(Link l1 : list1)
			for(Link l2: list2)
				if(l2.equals(l1))
					overlapped.add(l2);
		return overlapped;
	}
	
	// Returns the links which occur in the argument but don't occur in this path
	public ArrayList<Link> complementaryLinks(WeightedPath path) {
		ArrayList<Link> complementary = new ArrayList<Link>();
		LinkedList<Link> list1 = this.toLinksList();
		LinkedList<Link> list2 = path.toLinksList();
		boolean match;
		for(Link l2 : list2) {
			match = false;
			for(Link l1: list1)
				if(l2.equals(l1)) {
					match = true;
					break;
				}
			if(!match) complementary.add(l2);
		}		
		return complementary;
	}
	
	public int numberOfOverlappingLinksFromStart(WeightedPath path) {
		int shared = 0;
		int i = 0;
		int max = Math.min(this.getNumberOfNodes(), path.getNumberOfNodes())-1;
		if(this.getFirstNode().equals(path.getFirstNode())) {
			while(i<max) {
				if(this.getNode(i+1).equals(path.getNode(i+1))) {
					shared = shared + 1;
					i = i + 1;
				}
				else i = max;
			}
		}
		return shared;
	}
	
	public double lengthOfOverlappingLinksFromStart(WeightedPath path) {
		double length = 0;
		int i = 0;
		int max = Math.min(this.getNumberOfNodes(), path.getNumberOfNodes())-1;
		if(this.getFirstNode().equals(path.getFirstNode())) {
			while(i<max) {
				if(this.getNode(i+1).equals(path.getNode(i+1))) {
					length = length + weights.get(i+1);
					i = i + 1;
				}
				else i = max;
			}
		}
		return length;
	}
	
	public LinkedList<Link> toLinksList() {
		LinkedList<Link> list = new LinkedList<Link>();
		for(int i=0; i<sequence.size()-1; i++)
			list.add(new Link(sequence.get(i), sequence.get(i+1)));
		return list;
	}
	
	public String toString() {
		String string = Integer.toString(sequence.getFirst().getId());
		for(int i=1; i<sequence.size(); i++)
			string = string + "-(" + Double.toString(weights.get(i)) + ")->" + Integer.toString(sequence.get(i).getId());
		return string;
	}
}
