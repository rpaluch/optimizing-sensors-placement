package graphs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.Stack;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/***
 * This class has implemented many methods for transforming graphs and tools for graphs analysis.
 * @author rpaluch
 */

public class GraphTools {
	
	/***
	 * Changes the list of nodes into the list of nodes ids.
	 */
	public static ArrayList<Integer> nodesToIDs(ArrayList<Node> nodes){
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for(Node n : nodes)
			ids.add(n.getId());
		return ids;
	}
	
	/***
	 * Changes the list of nodes' ids into the list of Nodes.
	 */
	public static ArrayList<Node> idsToNodes(Graph graph, ArrayList<Integer> ids){
		ArrayList<Node> nodes = new ArrayList<Node>();
		for(Integer i : ids)
			nodes.add(graph.getNode(i));
		return nodes;
	}
	
	/***
	 * Returns the maximum value from the array.
	 */
	public static int maxValue(int[] array){
		int maxValue = 0;
		for(int i=0; i<array.length; i++)
			if(array[i]>maxValue)
				maxValue = array[i];
		return maxValue;
	}
	
	/***
	 * Returns the index of the maximum value from the array. If there is more than one index with maxium value,
	 * the method returns first occurrence.
	 */
	public static int indexOfMaxValue(int[] array){
		int maxValue = 0;
		int maxIndex = -1;
		for(int i=0; i<array.length; i++)
			if(array[i]>maxValue){
				maxValue = array[i];
				maxIndex = i;
			}
				
		return maxIndex;
	}
	
	/***
	 * Returns a adjacency matrix of a given graph.
	 */
	public static int[][] getAdjacencyMatrix(Graph graph) {
		int[][] a = new int[graph.getNumberOfNodes()][graph.getNumberOfNodes()];
		int i = -1;
		int j = -1;
		for(Link l : graph.getLinksList()) {
			i = l.getStart().getIndex();
			j = l.getEnd().getIndex();
			a[i][j] = 1;
			a[j][i] = 1;
		}
		return a;
	}
		
	/***
	 * Returns the list of the links constructed from two list of nodes.
	 * The first list provides the starting points for links, the second list provides the ending nodes.
	 */
	public static ArrayList<Link> createLinks(ArrayList<Node> starting, ArrayList<Node> ending) {
		ArrayList<Link> links = new ArrayList<Link>();
		if(starting.size()!=ending.size()) return null;
		for(int i=0; i<starting.size(); i++) {
			links.add(new Link(starting.get(i), ending.get(i)));
		}
		return links;
	}
	
	/***
	 * Returns the list of the network components sizes.
	 * The method also assigns the membership to each node of the given network.
	 */
	public static ArrayList<Integer> findComponents(Graph graph) {
		
		ArrayList<Integer> csizes = new ArrayList<Integer>();
		Queue<Node> queue = new LinkedList<Node>();
		ArrayList<Node> allNodes = GraphTools.highestDegreeNodes(graph, graph.getNumberOfNodes());
		Node currentNode = null;
		int counter = 0;
		
		// Reset the membership of each node
		for(Node n : allNodes) n.setMembership(0);
		
		// Assigns the memberships to each node using the propagation method
		for(int i=0; i<allNodes.size(); i++) {
			if(allNodes.get(i).getMembership()==0) {
				counter = counter + 1;
				allNodes.get(i).setMembership(counter);
				queue.add(allNodes.get(i));
				while(!queue.isEmpty()) {
					currentNode = queue.poll();
					for(Node n : currentNode.getNeighbours()) {
						if(n.getMembership()==0) {
							n.setMembership(counter);
							queue.add(n);
						}
					}
				}
			}
		}
		
		// Adds proper number of components to the list
		for(int i=0; i<counter; i++) csizes.add(0);
		
		// Computes the size of each component
		for(Node n : allNodes) csizes.set(n.getMembership()-1, csizes.get(n.getMembership()-1)+1);
		
		return csizes;
	}
	
	/***
	 * Creates the link between two random nodes from two different components.
	 * This method does not check if two components are already connected.
	 * The new component (sum of two components) has component1ID.
	 * The method is based on the membership field of each node therefore it requires precomputing the membership.
	 */
	public static void connectComponents(Graph graph, int component1ID, int component2ID) {
		int node1ID = 0;
		int node2ID = 0;
		for(Node n : graph.getNodesList()) {
			if(n.getMembership()==component1ID) node1ID = n.getId();
			else if(n.getMembership()==component2ID) {
				node2ID = n.getId();
				n.setMembership(component1ID);
			}
			if(node1ID*node2ID>0) {
				graph.addLink(node1ID, node2ID);
				break;
			}
		}
	}
	
	/***
	 * Creates additional link between random nodes in order to connect all components into one contiguous network.
	 * The method is based on the membership field of each node therefore it requires precomputing the membership.
	 */
	public static void connectAllComponents(Graph graph) {
		// Auxiliary lists
		ArrayList<Integer> componentID = new ArrayList<Integer>();
		ArrayList<Integer> componentSize = new ArrayList<Integer>();
		ArrayList<Integer> firstNodeID = new ArrayList<Integer>();
		int member = -1;
		int index = -1;
		// Filling the lists
		for(Node n : graph.getNodesList()) {
			member = n.getMembership();
			if(!componentID.contains(member)) {
				componentID.add(member);
				componentSize.add(1);
				firstNodeID.add(n.getId());
			}
			else {
				index = componentID.indexOf(member);
				componentSize.set(index, componentSize.get(index)+1);
			}
		}
		// Find the biggest component
		int giantIndex = componentSize.indexOf(Collections.max(componentSize));
		int giantCompID = componentID.get(giantIndex);
		int giantNodeID = firstNodeID.get(giantIndex);
		// Adding links between nodes
		firstNodeID.remove(giantIndex);
		for(Integer id : firstNodeID) graph.addLink(giantNodeID, id);
		// Change the membership of the whole network
		for(Node n : graph.getNodesList()) n.setMembership(giantCompID);
	}
	
	/***
	 * Returns the length of the shortest path betwen source and target. If the path does not exists, returns -1.
	 */
	public static int shortestPathLength(Graph graph, int sourceID, int targetID) {
		return PathsTools.shortestPath(graph, sourceID, targetID).size()-1;
	}
	
	/***
	 * Returns the average length of the shortest path between source and a group of other nodes.
	 */
	public static double shortestPathLength(Graph graph, int sourceID, ArrayList<Integer> targetID) {
		double spl = 0.0;
		ArrayList<LinkedList<Node>> paths = PathsTools.shortestPaths(graph, sourceID, targetID);
		for(LinkedList<Node> p : paths) spl = spl + p.size()-1;
		return spl/targetID.size();
	}
	
	/***
	 * Returns the average length of the shortest path between source and a group of other nodes in weighted graph.
	 */
	public static double shortestWeightedPathLength(Graph graph, int sourceID, ArrayList<Integer> targetID) {
		double swpl = 0.0;
		ArrayList<Double> pathsLengths = PathsTools.shortestWeightedPathsLengths(graph, sourceID, targetID);
		for(Double pl : pathsLengths) swpl = swpl + pl;
		return swpl/targetID.size();
	}
	
	/***
	 * Returns the average length of the shortest path between any two nodes in the network
	 */
	public static double averagePathLength(Graph graph) {	
		double apl = 0.0;
		int number_of_paths = graph.getNumberOfNodes()*(graph.getNumberOfNodes()-1);
		ArrayList<LinkedList<Node>> paths;
		for(Node n : graph.getNodesList()) {
			paths = PathsTools.shortestPaths(graph, n.getId());
			for(LinkedList<Node> l : paths) apl = apl + l.size() - 1;
		}
		apl = apl/number_of_paths;
		return apl;
	}
	
	/***
	 * Returns the diameter of the network - the length of the longest path in the network
	 */
	public static int diameter(Graph graph) {	
		int diameter = 0;
		int length;
		ArrayList<LinkedList<Node>> paths;
		for(Node n : graph.getNodesList()) {
			paths = PathsTools.shortestPaths(graph, n.getId());
			for(LinkedList<Node> l : paths){
				length = l.size()-1;
				if(length>diameter)
					diameter = length;
			}
		}
		return diameter;
	}
	
	/***
	 * Returns the breadth-first search tree which a given root
	 */
	public static Graph findBFSTree(Graph graph, int rootID) {
		Graph tree = new Graph();
		ArrayList<LinkedList<Node>> paths = PathsTools.shortestPaths(graph, rootID);
		for(LinkedList<Node> singlePath : paths) {
			for(int i=1; i<singlePath.size(); i++) {
				tree.addLink(singlePath.get(i-1).getId(), singlePath.get(i).getId());
			}
		}
		return tree;
	}
	
	/***
	 * Returns the breadth-first search tree which a given root and leaves
	 */
	public static Graph findBFSTree(Graph graph, int rootID, ArrayList<Integer> leavesID) {
		Graph tree = new Graph();
		ArrayList<LinkedList<Node>> paths = PathsTools.shortestPaths(graph, rootID, leavesID);
		for(LinkedList<Node> singlePath : paths) {
			for(int i=1; i<singlePath.size(); i++) {
				tree.addLink(singlePath.get(i-1).getId(), singlePath.get(i).getId());
			}
		}
		return tree;
	}
	
	/***
	 * Returns the breadth-first search tree (preserving constrains) which a given root.
	 * Returns null if the tree does not exists (if the constrains preclude creating a tree).
	 */
	public static Graph findConstrainedBFSTree(Graph graph, int rootID, ArrayList<Link> constrains) {
		Graph tree = new Graph();
		ArrayList<LinkedList<Node>> paths = PathsTools.shortestConstrainedPaths(graph, rootID, constrains);
		for(LinkedList<Node> singlePath : paths) {
			if(singlePath.size()==0) return null;
			for(int i=1; i<singlePath.size(); i++) {
				tree.addLink(singlePath.get(i-1).getId(), singlePath.get(i).getId());
			}
		}
		return tree;
	}
	
	/***
	 * Returns the breadth-first search tree (preserving constrains) which a given root and leaves
	 * Returns null if the tree does not exists (if the constrains preclude creating a tree).
	 */
	public static Graph findConstrainedBFSTree(Graph graph, int rootID, ArrayList<Integer> leavesID, ArrayList<Link> constrains) {
		Graph tree = new Graph();
		ArrayList<LinkedList<Node>> paths = PathsTools.shortestConstrainedPaths(graph, rootID, leavesID, constrains);
		for(LinkedList<Node> singlePath : paths) {
			if(singlePath.size()==0) return null;
			for(int i=1; i<singlePath.size(); i++) {
				tree.addLink(singlePath.get(i-1).getId(), singlePath.get(i).getId());
			}
		}
		return tree;
	}
	
	/***
	 * For each nodes it computes the mean and the variance of the lengths of shortest paths
	 * between the chosen node and the given set of nodes. 
	 * This method uses PathsRegister with all shortest paths between all pairs of nodes.
	 * @param g
	 * @param register
	 */
	public static double varianceOfShortestPathsLengths(Graph graph, PathsRegister register, Node node, ArrayList<Node> observers) {
		PathsRegister allPaths = register;
		if(allPaths==null) allPaths = PathsTools.allShortestPathsInGraph(graph);
		ArrayList<Integer> nodesIDs = GraphTools.nodesToIDs(observers);
		int size = observers.size();
		double mean = 0;
		double variance = 0;
		ArrayList<Integer> spl = allPaths.getPathLengths(node.getId(), nodesIDs);
		
		for(int i=0; i<size; i++)
			mean = mean + spl.get(i);
		mean = mean/size;
		for(int i=0; i<size; i++)
			variance = variance + (spl.get(i)-mean)*(spl.get(i)-mean);
		variance = variance/(size-1);
			
		return variance;
	}
	
	/***
	 * Computes the closeness centrality for each node in the network 
	 * using PathsRegister with all shortest paths between all pairs of nodes.
	 */
	
	public static void closenessCentrality(Graph graph, PathsRegister register) {
		PathsRegister allPaths = register;
		if(allPaths==null) allPaths = PathsTools.allShortestPathsInGraph(graph);
		int size = graph.getNumberOfNodes();
		double[] distance = new double[size];
		int idi,idj;
		for(int i=0; i<size; i++) {
			idi = graph.getNodesList().get(i).getId();
			for(int j=0; j<size; j++) {
				idj = graph.getNodesList().get(j).getId();
				distance[i] = distance[i] + allPaths.getPathLength(idi, idj);
			}
			graph.getNodesList().get(i).setCloseness((size-1)/distance[i]);
		}
	}
		
	/***
	 * Computes the betweenness centrality for each node in the network 
	 * using PathsRegister with all shortest paths between all pairs of nodes.
	 */
	public static void betweennessCentrality(Graph graph, PathsRegister register) {
		PathsRegister allPaths = register;
		if(allPaths==null) allPaths = PathsTools.allShortestPathsInGraph(graph);
		int size = graph.getNumberOfNodes();
		double[] betweenness = new double[size];
		
		for(ArrayList<Path> paths: register.getRegister().values())
			for(Path path: paths) {
				for(int n=1; n<path.getLength(); n++) {
					betweenness[path.getNode(n).getIndex()]=betweenness[path.getNode(n).getIndex()]+1.0/paths.size();
				}
			}
		for(int i=0; i<size; i++)
			graph.getNodesList().get(i).setBetweenness(2*betweenness[i]/allPaths.getTotalNumberOfSinglePaths());
	}
	
	/***
	 * Computes the limited betweenness centrality for each node in the network 
	 * using PathsRegister with all limited shortest paths in the graph
	 */
	public static void limitedBetweennessCentrality(Graph graph, PathsRegister register, int maxLenght) {
		PathsRegister allPaths = register;
		if(allPaths==null) allPaths = PathsTools.allLimitedShortestPathsInGraph(graph, maxLenght);
		int size = graph.getNumberOfNodes();
		double[] betweenness = new double[size];
		ArrayList<Path> paths;
		for(int i=0; i<size-1; i++)
			for(int j=i+1; j<size; j++) {
				paths = allPaths.getPaths(graph.getNodesList().get(i).getId(), graph.getNodesList().get(j).getId());
				if(paths!=null) {
					for(Path path : paths)
						for(int n=1; n<path.getLength(); n++)
							betweenness[path.getNode(n).getIndex()]=betweenness[path.getNode(n).getIndex()]+1.0/paths.size();
				}
			}
		for(int i=0; i<size; i++)
			graph.getNodesList().get(i).setBetweenness(2*betweenness[i]/allPaths.getTotalNumberOfSinglePaths());
	}
	
	/***
	 * Computes the collective betweenness for a given set of nodes in the network 
	 * using PathsRegister with all shortest paths between all pairs of nodes.
	 */
	public static double collectiveBetweenness(Graph graph, PathsRegister register, ArrayList<Node> nodes) {
		PathsRegister allPaths = register;
		if(allPaths==null) allPaths = PathsTools.allShortestPathsInGraph(graph);
		int size = graph.getNumberOfNodes();
		int[] member = new int[size];
		for(Node n : nodes)
			member[n.getIndex()]=1;
		double betweenness = 0.0;
		ArrayList<Path> paths;
		for(int i=0; i<size-1; i++)
			for(int j=i+1; j<size; j++) {
				paths = allPaths.getPaths(graph.getNodesList().get(i).getId(), graph.getNodesList().get(j).getId());
				if(paths!=null) {
					for(Path path : paths)
						for(int n=1; n<path.getLength(); n++)
							if(member[path.getNode(n).getIndex()]>0) {
								betweenness=betweenness+1.0/paths.size();
								break;
							}
				}
			}
		return betweenness/nodes.size();
	}
	
	public static ArrayList<Node> fastCollectiveBetweenness(Graph graph, PathsRegister register, int budget) {
		int size = graph.getNumberOfNodes();
		ArrayList<HashSet<Integer>> nodesToPaths = new ArrayList<HashSet<Integer>>();
		for(int i=0; i<size; i++)
			nodesToPaths.add(new HashSet<Integer>());
		ArrayList<Double> pathsWeights = new ArrayList<Double>();
		ArrayList<Path> pathsToNodes = new ArrayList<Path>();
		
		for(ArrayList<Path> paths: register.getRegister().values())
			for(Path path: paths) {
				pathsWeights.add(1.0/paths.size());
				pathsToNodes.add(path);
				for(int n=1; n<path.getLength(); n++) {
					nodesToPaths.get(path.getNode(n).getIndex()).add(pathsWeights.size()-1);
					path.getNode(n).setBetweenness(path.getNode(n).getBetweenness()+1.0/paths.size());
				}
			}
		
		ArrayList<Node> selectedObservers = new ArrayList<Node>();
		LinkedList<Node> candidates = new LinkedList<Node>();
		for(Node n : graph.getNodesList())
			candidates.add(n);
		Node newObserver = null;
		Path newPath = null;
		while(selectedObservers.size()<budget) {
			newObserver = Collections.max(candidates, GraphTools.nodeBetweennessAscending);
			candidates.remove(newObserver);
			selectedObservers.add(newObserver);
			for(Integer pathId : nodesToPaths.get(newObserver.getIndex())) {
				newPath = pathsToNodes.get(pathId);
				for(int n=1; n<newPath.getLength(); n++) {
					if(newPath.getNode(n).getIndex()!=newObserver.getIndex()) {
						nodesToPaths.get(newPath.getNode(n).getIndex()).remove(pathId);
						newPath.getNode(n).setBetweenness(newPath.getNode(n).getBetweenness()-pathsWeights.get(pathId));
					}
				}
			}
		}
				
		return selectedObservers;
	}
	
	
	/***
	 * Computes betweenness centrality for each node in the network and save it in internal variable
	 */	
	public static void computeBetweennessCentrality(Graph graph) {
		int size = graph.getNumberOfNodes();
		ArrayList<LinkedList<Node>> predecessors;
		Stack<Node> stack;
		Queue<Node> queue;
		int[] sigma;
		int[] dist;
		double[] delta;
		Node currentNode = null;
		int v,w;
		
		for(int s=0; s<size; s++) {
			predecessors = new ArrayList<LinkedList<Node>>();
			for(int i=0; i<size; i++) predecessors.add(new LinkedList<Node>());
			stack = new Stack<Node>();
			queue = new LinkedList<Node>();
			queue.add(graph.getNodesList().get(s));
			sigma = new int[size];
			sigma[s] = 1;
			dist = new int[size];
			Arrays.fill(dist, -1);
			dist[s] = 0;
			delta = new double[size];
			
			while((currentNode = queue.poll())!=null) {
				
				stack.push(currentNode);
				v = currentNode.getIndex();
				
				for(Node neighbour : currentNode.getNeighbours()) {
					w = neighbour.getIndex();
					if(dist[w]<0) {
						queue.add(neighbour);
						dist[w] = dist[v] + 1;
					}
					if(dist[w]==dist[v]+1) {
						sigma[w] = sigma[w] + sigma[v];
						predecessors.get(w).add(currentNode);
					}
				}
			}
			
			while(!stack.isEmpty()) {
				currentNode = stack.pop();
				w = currentNode.getIndex();
				for(Node node : predecessors.get(w)) {
					v = node.getIndex();
					delta[v] = delta[v] + 1.0*sigma[v]/sigma[w]*(1+delta[w]);
				}
				if(w!=s) currentNode.setBetweenness(currentNode.getBetweenness()+delta[w]);
			}
		}
		// Normalization
		for(Node node : graph.getNodesList()) node.setBetweenness(node.getBetweenness()/((size-1)*(size-2)));
	}
	
	/***
	 * Computes local clustering coefficient for each node in the network and returns it as ArrayList<Double>
	 */	
	public static ArrayList<Double> computeLocalClusteringCoefficient(Graph g) {
		ArrayList<Double> clustering = new ArrayList<Double>();
		ArrayList<Node> checked;
		int counter = 0;
		for(Node v : g.getNodesList()){
			if(v.getDegree()>1){
				checked = new ArrayList<Node>();
				counter = 0;
				for(Node first : v.getNeighbours()) {
					for(Node second : first.getNeighbours())
						if(!checked.contains(second))
							if(v.isNeighbour(second))
								counter = counter + 1;
					checked.add(first);
				}
				clustering.add(2.0*counter/(v.getDegree()*v.getDegree()-v.getDegree()));
			}
			else clustering.add(0.0);
			
		}
		return clustering;
	}
	
	/***
	 * Computes average clustering coefficient for undirected graph
	 */	
	public static double averageClusteringCoefficient(Graph g) {
		double acc = 0.0;
		ArrayList<Double> local = computeLocalClusteringCoefficient(g);
		for(Double coef : local) acc = acc + coef;
		return acc/g.getNumberOfNodes();
	}
	
	/***
	 * Computes global clustering coefficient for undirected graph
	 */
	public static double globalClusteringCoefficient(Graph graph) {
		ArrayList<Triplet> triplets = findAllTriplets(graph);
		int nclosed = 0;
		for(Triplet t : triplets)
			if(t.isClosed())
				nclosed = nclosed + 1;
		return 1.0*nclosed/triplets.size();
	}
	
	/***
	 * Finds all triplets in the graph
	 */
	public static ArrayList<Triplet> findAllTriplets(Graph graph) {
		ArrayList<Triplet> triplets = new ArrayList<Triplet>();
		ArrayList<Node> neighbours;
		for(Node n : graph.getNodesList()) {
			neighbours = n.getNeighbours();
			for(int i=0; i<neighbours.size()-1; i++)
				for(int j=i+1; j<neighbours.size(); j++)
					triplets.add(new Triplet(n,neighbours.get(i),neighbours.get(j)));
		}
		return triplets;
	}
	
	/***
	 * Returns the approximation of the Minimum Dominating Set for a given graph.
	 * The method uses the greedy algorithm.
	 */
	public static ArrayList<Node> greedyMDS(Graph graph) {
		// Copies the input graph
		Graph graphCopy = graph.copy();
		// Prepares the list of nodes from graphCopy for Minimum Dominating Set
		ArrayList<Node> mdsCopy = new ArrayList<Node>();
		// Prepare a list of nodes
		ArrayList<Node> nodesCopy = graphCopy.getNodesList();
		// Assign initial dominating potential to each node
		for(Node n : nodesCopy){
			n.setState(n.getDegree()+1);
		}
		// Temporal variables for a chosen node
		Node chosen;
		// Sort the list of node ascending by their dominating potential
		Collections.sort(nodesCopy, nodeStateDescending);
		//for(Node n: nodes) System.out.printf("%d: %d\n", n.getId(), n.getState());
		while(nodesCopy.size()>0) {
			// Choose the first node on the list - the node with the highest dominating potential
			chosen = nodesCopy.get(0);
			//System.out.printf("Chosen: %d\n", chosen.getId());
			// Add the chosen node to a dominating set
			mdsCopy.add(chosen);
			// Reduce the dominating potential of the neighbours and the second neighbours of the chosen node
			// The "true" value of the flag means that a node is dominated
			for(Node n1 : chosen.getNeighbours()) {
				if(!chosen.getFlag() && !n1.getFlag()) {
					n1.setState(n1.getState()-2);
				}
				else if(!chosen.getFlag() || !n1.getFlag()) {
					n1.setState(n1.getState()-1);
				}	
				for(Node n2 : n1.getNeighbours())
					if(!n1.getFlag()) {
						n2.setState(n2.getState()-1);
					}
				n1.setFlag(true);
			}
			// Flag the chosen node and reduce its dominating potencial to zero
			chosen.setFlag(true);
			chosen.setState(0);
			// Sort the list of node ascending by their dominating potential
			Collections.sort(nodesCopy, nodeStateDescending);
			//for(Node n: nodes) System.out.printf("%d: %d\n", n.getId(), n.getState());
			// Remove nodes without any dominating potential
			while(nodesCopy.get(nodesCopy.size()-1).getState()==0) {
				nodesCopy.remove(nodesCopy.size()-1);
				if(nodesCopy.size()==0) break;
			}
		}
		// Prepares the list of nodes from orignal graph for Minimum Dominating Set
		ArrayList<Node> mds = new ArrayList<Node>();
		for(Node n : mdsCopy) mds.add(graph.getNode(n.getId()));
		return mds;
	}
	
	/***
	 * Returns the list of m nodes with the highest degree
	 */
	public static ArrayList<Node> highestDegreeNodes(Graph graph, int m) {
		ArrayList<Node> nodes = graph.copyOfNodesList();
		Collections.sort(nodes, nodeDegreeDescending);
		return new ArrayList<Node>(nodes.subList(0, m));
	}
	
	/***
	 * Returns the list of m nodes with the highest betweenness
	 */
	public static ArrayList<Node> highestBetweennessNodes(Graph graph, int m) {
		ArrayList<Node> nodes = graph.copyOfNodesList();
		Collections.sort(nodes, nodeBetweennessDescending);
		return new ArrayList<Node>(nodes.subList(0, m));
	}
	
	/***
	 * Returns the list of m nodes with the highest betweenness
	 */
	public static ArrayList<Node> highestBetweennessNodesWithoutSorting(Graph graph, int m) {
		ArrayList<Node> nodes = new ArrayList<Node>();
		LinkedList<Node> candidates = new LinkedList<Node>();
		for(Node n : graph.getNodesList())
			candidates.add(n);
		Node newMax;
		while(nodes.size()<m) {
			newMax = Collections.max(candidates, GraphTools.nodeBetweennessAscending);
			nodes.add(newMax);
			candidates.remove(newMax);
		}
		return nodes;
	}
	
	/***
	 * Returns the list of m nodes with the highest betweenness to degree ratio
	 */
	public static ArrayList<Node> highestBetweennessToDegreeRatioNodes(Graph graph, int m) {
		ArrayList<Node> nodes = graph.copyOfNodesList();
		Collections.sort(nodes, nodeBetweennessToDegreeRatioDescending);
		return new ArrayList<Node>(nodes.subList(0, m));
	}
	
	/***
	 * Returns the length of the longest path in a given tree
	 */
	public static int computeHeightOfTree(Graph tree, int rootID) {
		ArrayList<LinkedList<Node>> paths = PathsTools.shortestPaths(tree, rootID);
		int longest = 0;
		for(LinkedList<Node> singlePath : paths) {
			if(singlePath.size()-1>longest) longest = singlePath.size()-1;
		}
		return longest;
	}
	
	/***
	 * This method uses "membership" field of each node in a graph.
	 * It transforms communities into nodes and leaves only links between communities.
	 */
	public static Graph coarseGraph(Graph graph) {
		
		Graph coarseGrained = new Graph();
		// Create the list of communities
		ArrayList<Integer> communities = new ArrayList<Integer>();
		for(Node n : graph.getNodesList()) 
			if(!communities.contains(n.getMembership()))
				communities.add(n.getMembership());
		// Add new nodes for the coarse-grained graph
		for(Integer id : communities) coarseGrained.addNode(id);
		// Add links between nodes in the coarse-grained graph
		for(Link l : graph.getLinksList()) coarseGrained.addLink(l.getStart().getMembership(), l.getEnd().getMembership());
		
		return coarseGrained;
	}
	
	/***
	 * Returns the subgraph which contains only the nodes from a given community and links between these nodes.
	 */
	public static Graph extractCommunity(Graph graph, int membership) {
		
		Graph subgraph = new Graph();
		// Add nodes which belongs to the community
		for(Node n : graph.getNodesList())
			if(n.getMembership()==membership)
				subgraph.addNode(new Node(n.getId()));
		// Add links which are between nodes from the community
		for(Link l : graph.getLinksList())
			if(l.getStart().getMembership()==membership && l.getEnd().getMembership()==membership)
				subgraph.addLink(l.getStart().getId(), l.getEnd().getId());
		
		return subgraph;
	}
	
	/***
	 * Returns the subgraph which contains only the nodes from given communities and links between these nodes.
	 */
	public static Graph extractCommunity(Graph graph, ArrayList<Integer> communities) {
		
		Graph subgraph = new Graph();
		// Add nodes which belongs to the community
		for(Node n : graph.getNodesList())
			if(communities.contains(n.getMembership()))
				subgraph.addNode(new Node(n.getId()));
		// Add links which are between nodes from the community
		for(Link l : graph.getLinksList())
			if(communities.contains(l.getStart().getMembership()) && communities.contains(l.getEnd().getMembership()))
				subgraph.addLink(l.getStart().getId(), l.getEnd().getId());
		
		return subgraph;
	}
	
	/***
	 * Returns the subgraph which contains only the nodes belonging to the largest component in the network and links between these nodes.
	 */
	public static Graph extractLargestComponent(Graph graph) {
		
		Graph graphCopy = graph.copy();
		// Find all disjoint components
		ArrayList<Integer> csizes = GraphTools.findComponents(graphCopy);
		// Find id of the largest component
		int membership = 0;
		int maxSize = 0;
		for(int i=0; i<csizes.size(); i++) 
			if(csizes.get(i)>maxSize){
				maxSize = csizes.get(i);
				membership = i+1;
			}
		// Initialize the subgraph
		Graph subgraph = new Graph();
		// Add nodes which belongs to the community
		for(Node n : graphCopy.getNodesList())
			if(n.getMembership()==membership)
				subgraph.addNode(new Node(n.getId()));
		// Add links which are between nodes from the community
		for(Link l : graphCopy.getLinksList())
			if(l.getStart().getMembership()==membership && l.getEnd().getMembership()==membership)
				subgraph.addLink(l.getStart().getId(), l.getEnd().getId());
		
		return subgraph;
	}
	
	/***
	 * Returns the list of nodes which are dominated in a given graph by a given set of nodes. 
	 */
	public static ArrayList<Node> findDominatedNodes(Graph graph, ArrayList<Node> dominatingSet) {
		ArrayList<Node> dominatedNodes = new ArrayList<Node>();
		for(Node d : dominatingSet) {
			if(!dominatedNodes.contains(d)) dominatedNodes.add(d);
			for(Node n : d.getNeighbours())
				if(!dominatedNodes.contains(n)) dominatedNodes.add(n);
		}
		return dominatedNodes;
	}
	
	/***
	 * Returns the subgraph which contains only the nodes from the list and links between these nodes.
	 */
	public static Graph extractSubgraph(Graph graph, ArrayList<Node> selected) {
		
		Graph subgraph = new Graph();
		// Add nodes which belongs to the list
		for(Node n : selected) subgraph.addNode(new Node(n.getId()));
		// Add links which are between nodes from the list
		for(Link l : graph.getLinksList())
			if(selected.contains(l.getStart()) && selected.contains(l.getEnd()))
				subgraph.addLink(l.getStart().getId(), l.getEnd().getId());
		return subgraph;
	}
		
	/***
	 * Returns the set containing the all nodes with are below or equal the given distance from the given set of nodes
	 * including this set.
	 */
	public static ArrayList<Node> findFurtherNeighbours(Graph graph, ArrayList<Node> nodes, int distance) {
		if(distance<0) return null;
		ArrayList<Node> visited = new ArrayList<Node>(nodes);
		ArrayList<ArrayList<Node>> neighbours = new ArrayList<ArrayList<Node>>();
		// The neighbours in the distance of 0
		neighbours.add(new ArrayList<Node>(nodes));
		// The lists of further neighbours
		for(int d=0; d<distance; d++) {
			neighbours.add(new ArrayList<Node>());
			for(Node node : neighbours.get(d))
				for(Node neighbour : node.getNeighbours())
					if(!visited.contains(neighbour)) {
						neighbours.get(d+1).add(neighbour);
						visited.add(neighbour);
					}
		}
		return visited;
	}
	
	/***
	 * Returns the list of the lists containing all nodes with are below or equal the given distance from the chosen node
	 * including this node.
	 */
	public static ArrayList<ArrayList<Node>> findFurtherNeighbours(Graph graph, Node source, int distance) {
		if(distance<0) return null;
		ArrayList<Node> visited = new ArrayList<Node>();
		visited.add(source);
		ArrayList<ArrayList<Node>> neighbours = new ArrayList<ArrayList<Node>>();
		// The neighbours in the distance of 0
		neighbours.add(new ArrayList<Node>());
		neighbours.get(0).add(source);
		// The lists of further neighbours
		for(int d=0; d<distance; d++) {
			neighbours.add(new ArrayList<Node>());
			for(Node node : neighbours.get(d))
				for(Node neighbour : node.getNeighbours())
					if(!visited.contains(neighbour)) {
						neighbours.get(d+1).add(neighbour);
						visited.add(neighbour);
					}
		}
		return neighbours;
	}
	
	/***
	 * Computes the average degree of the i-th order neighbours
	 */
	public static double averageDegreeOfFurtherNeighbours(Graph graph, ArrayList<Node> nodes, int distance) {
		if(distance<0) return 0.0;
		double averageDegree = 0.0;
		ArrayList<Node> neighbours = findFurtherNeighbours(graph, nodes, distance);
		for(Node n : neighbours)
			averageDegree = averageDegree + n.getDegree();
		return averageDegree/neighbours.size();
	}
	
	/***
	 * Computes and returns the average degree of the set of nodes
	 */
	public static double averageDegree(ArrayList<Node> nodes) {
		double sum = 0.0;
		for(Node n : nodes)
			sum = sum + n.getDegree();
		return sum/nodes.size();
	}
	
	/***
	 * Returns the list of links which exists in both input graphs
	 */
	public static ArrayList<Link> commonLinks(Graph g1, Graph g2) {
		ArrayList<Link> links = new ArrayList<Link>();
		for(Link l : g1.getLinksList())
			if(g2.containsLink(l))
				links.add(l);
		return links;
	}
	
	/***
	 * Returns the similarity between two graphs which is a real number in range [0,1].
	 * @param g1
	 * @param g2
	 * @return
	 */
	public static double graphsSimilarity(Graph g1, Graph g2) {
		double sim = 2.0*commonLinks(g1,g2).size()/(g1.getNumberOfLinks()+g2.getNumberOfLinks());
		return sim;
	}
	
	/***
	 * This functions checks if the given set of observers S is the Double Resolving Set by checking node by node.
	 * It returns the fraction of pairs (u,v) for which there exist (s_i,s_j) in S, for which
	 * d(u,s_i) - d(u,s_j) != d(v,s_i) - d(v,s_j)
	 * @param g
	 * @param observersID
	 * @return
	 */
	public static double examineDoubleResolvingSet(Graph g, ArrayList<Integer> observersID) {
		int size = g.getNumberOfNodes();
		int numberOfResolvedPairs = 0;
		double fractionOfResolvedPairs;
		ArrayList<ArrayList<Integer>> allShortestPaths = new ArrayList<ArrayList<Integer>>();
		int d_ui, d_uj, d_vi, d_vj;
		
		for(Node n : g.getNodesList())
			allShortestPaths.add(PathsTools.shortestPathsLengths(g, n.getId(), observersID));
		
		for(int u=0; u<size-1; u++)
			for(int v=u+1; v<size; v++)
				for(int i=0; i<observersID.size()-1; i++)
					for(int j=i+1; j<observersID.size(); j++){
						d_ui = allShortestPaths.get(u).get(i);
						d_uj = allShortestPaths.get(u).get(j);
						d_vi = allShortestPaths.get(v).get(i);
						d_vj = allShortestPaths.get(v).get(j);
						if(d_ui-d_uj!=d_vi-d_vj){
							numberOfResolvedPairs = numberOfResolvedPairs + 1;
							j = observersID.size();
							i = observersID.size();
						}
					}
	
		fractionOfResolvedPairs = 2.0*numberOfResolvedPairs/(size*(size-1));
		return fractionOfResolvedPairs;
	}
	
	/***
	 * Greedy algorithm for observer placement for the high-variance setting.
	 * Implementation of Algorithm 2 from paper Spinelli, Celis, Thiran "Observer placement 
	 * for source localization: The effect of budgets and transmission variance"
	 * @param graph
	 * @param maxLength
	 * @return
	 */
	public static ArrayList<Node> fastGreedyHVObs(Graph graph, PathsRegister register, int budget, int maxLength){
		PathsRegister allPaths = register;
		if(allPaths==null) allPaths = PathsTools.allShortestPathsInGraph(graph);
		int newCoverage = 0;
		int bestCoverage = 0;
		Node bestCandidate = null;
		LinkedList<Node> candidates;
		ArrayList<Node> bestSet;
		ArrayList<Node> newSet;
		ArrayList<Node> bestBestSet = new ArrayList<Node>();
		int bestBestCoverage = 0;
		
		for(Node start : graph.getNodesList()) {
			candidates = new LinkedList<Node>();
			for(Node n : graph.getNodesList())
				candidates.add(n);
			candidates.remove(start);
			bestSet = new ArrayList<Node>();
			bestSet.add(start);
			bestCoverage = 0;
			while(bestSet.size()<budget && bestCoverage<graph.getNumberOfNodes()) {
				for(Node candidate : candidates) {
					newSet = new ArrayList<Node>(bestSet);
					newSet.add(candidate);
					newCoverage = GraphTools.examinePathCovering(graph, allPaths, newSet, maxLength);
					if(newCoverage>bestCoverage){
						bestCandidate = candidate;
						bestCoverage = newCoverage;
					}		
				}
				bestSet.add(bestCandidate);
				candidates.remove(bestCandidate);
			}
			if(bestCoverage>bestBestCoverage) {
				bestBestSet = bestSet;
				bestBestCoverage = bestCoverage;
			}
		}
		return bestBestSet;
	}
	
	/***
	 * Greedy algorithm for observer placement for the high-variance setting.
	 * Implementation of Algorithm 2 from paper Spinelli, Celis, Thiran "Observer placement 
	 * for source localization: The effect of budgets and transmission variance"
	 * @param graph
	 * @param maxLength
	 * @return
	 */
	public static ArrayList<Node> greedyHVObsParallel(Graph graph, PathsRegister register, int budget, int maxLength, int numberOfThreads){
		PathsRegister allPaths = register;
		if(allPaths==null) allPaths = PathsTools.singleLimitedShortestPathsInGraph(graph,maxLength);
		
		int startersPerThread = (int) Math.ceil(1.0*graph.getNumberOfNodes()/numberOfThreads);
		ArrayList<ArrayList<Node>> starters = new ArrayList<ArrayList<Node>>();
		int s = 0;
		for(int t=0; t<numberOfThreads; t++) {
			starters.add(new ArrayList<Node>());
			while(s<Math.min(startersPerThread*(t+1),graph.getNumberOfNodes())) {
				starters.get(t).add(graph.getNodesList().get(s));
				s = s + 1;
			}
		}
		GreedyHVObsSelector[] selectors = new GreedyHVObsSelector[numberOfThreads];
		ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
		
		for(int i=0; i<numberOfThreads; i++) {
			selectors[i] = new GraphTools.GreedyHVObsSelector(graph, allPaths, starters.get(i), budget, maxLength);
			executor.execute(selectors[i]);
		}
		executor.shutdown();
		
		while(!executor.isTerminated()) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		ArrayList<Node> bestSet = null;
		//ArrayList<Node> aSet = null;
		int bestCoverage = 0;	
		for(int i=0; i<selectors.length; i++) {
			//aSet = selectors[i].getSelectedObservers();
			//for(Node n : aSet)
			//	System.out.printf("%d ", n.getId());
			//System.out.printf("%d\n", selectors[i].getCoverage());
			if(selectors[i].getBestCoverage()>bestCoverage){
				bestSet = selectors[i].getSelectedObservers();
				bestCoverage = selectors[i].getBestCoverage();
			}
		}
		return bestSet;
	}
	
	public static ArrayList<Node> greedyCoverageObsParallel(Graph graph, int budget, int numberOfThreads){
		
		int startersPerThread = (int) Math.ceil(1.0*graph.getNumberOfNodes()/numberOfThreads);
		ArrayList<ArrayList<Node>> starters = new ArrayList<ArrayList<Node>>();
		int s = 0;
		for(int t=0; t<numberOfThreads; t++) {
			starters.add(new ArrayList<Node>());
			while(s<Math.min(startersPerThread*(t+1),graph.getNumberOfNodes())) {
				starters.get(t).add(graph.getNodesList().get(s));
				s = s + 1;
			}
		}
		GreedyCoverageObsSelector[] selectors = new GreedyCoverageObsSelector[numberOfThreads];
		ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
		
		for(int i=0; i<numberOfThreads; i++) {
			selectors[i] = new GraphTools.GreedyCoverageObsSelector(graph, starters.get(i), budget);
			executor.execute(selectors[i]);
		}
		executor.shutdown();
		
		while(!executor.isTerminated()) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		ArrayList<Node> bestSet = null;
		int bestCoverage = 0;	
		for(int i=0; i<selectors.length; i++) {
			if(selectors[i].getBestCoverage()>bestCoverage){
				bestSet = selectors[i].getSelectedObservers();
				bestCoverage = selectors[i].getBestCoverage();
			}
		}
		
		return bestSet;
	}
	
	public static ArrayList<Node> greedyKMedianObsParallel(Graph graph, int[][] distances, int budget, int numberOfThreads){
		
		int startersPerThread = (int) Math.ceil(1.0*graph.getNumberOfNodes()/numberOfThreads);
		ArrayList<ArrayList<Node>> starters = new ArrayList<ArrayList<Node>>();
		int s = 0;
		for(int t=0; t<numberOfThreads; t++) {
			starters.add(new ArrayList<Node>());
			while(s<Math.min(startersPerThread*(t+1),graph.getNumberOfNodes())) {
				starters.get(t).add(graph.getNodesList().get(s));
				s = s + 1;
			}
		}
		GreedyKMedianObsSelector[] selectors = new GreedyKMedianObsSelector[numberOfThreads];
		ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
		
		for(int i=0; i<numberOfThreads; i++) {
			selectors[i] = new GraphTools.GreedyKMedianObsSelector(graph, distances, starters.get(i), budget);
			executor.execute(selectors[i]);
		}
		executor.shutdown();
		
		while(!executor.isTerminated()) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		ArrayList<Node> bestSet = null;
		int bestDistance = Integer.MAX_VALUE;	
		for(int i=0; i<selectors.length; i++) {
			if(selectors[i].getBestDistance()<bestDistance){
				bestSet = selectors[i].getSelectedObservers();
				bestDistance = selectors[i].getBestDistance();
			}
		}
		
		return bestSet;
	}
	
	/***
	 * Returns the number of nodes which lies on on a shortest path of length at most L (maxLength)
	 * between any two observers in the set S (observers)
	 * @param graph
	 * @param observers
	 * @return
	 */
	public static int examinePathCovering(Graph graph, PathsRegister register, ArrayList<Node> observers, int maxLength){
		int[] covered = new int[graph.getNumberOfNodes()];
		ArrayList<Path> paths;
		for(int i=0; i<observers.size()-1; i++)
			for(int j=i+1; j<observers.size(); j++) {
				paths = register.getPaths(observers.get(i).getId(), observers.get(j).getId());
				for(Path path : paths)
					if(path.getLength()<=maxLength)
						for(int n=0; n<path.getLength(); n++)
							covered[path.getNode(n).getIndex()]=1;
			}
		int sum = 0;
		for(int i=0; i<covered.length; i++)
			sum = sum + covered[i];
		return sum;
	}
	
	public static int examineKMedianSet(Graph graph, PathsRegister register, ArrayList<Node> observers) {
		int size = graph.getNumberOfNodes();
		int[] minDist = new int[size];
		int dist;
		int id;
		for(int i=0; i<size; i++) {
			id = graph.getNodesList().get(i).getId();
			minDist[i] = size+1;
			for(Node observer : observers) {
				dist = register.getPathLength(id, observer.getId());
				if(dist<minDist[i]) minDist[i] = dist;
			}
		}
		int sum = 0;
		for(int i=0; i<minDist.length; i++)
			sum = sum + minDist[i];
		return sum;
	}
	
	/***
	 * This function returns the random set of nodes (IDs) with the property that the hop distance between any pair of nodes
	 * within this set is greater or equal than a given value
	 * @param graph
	 * @param degreeOfSeparation
	 * @return
	 */
	public static ArrayList<Node> findSeparatedNodes(Graph graph, int degreeOfSeparation){
		if(degreeOfSeparation<=1) return null;
		Random rand = new Random();
		ArrayList<Node> potentialNodes = new ArrayList<Node>();
		for(Node n : graph.getNodesList())
			potentialNodes.add(n);
		ArrayList<Node> separatedNodes = new ArrayList<Node>();
		ArrayList<ArrayList<Node>> neighbours;
		Node seed;
		while(potentialNodes.size()>0){
			seed = potentialNodes.get(rand.nextInt(potentialNodes.size()));
			separatedNodes.add(seed);
			potentialNodes.remove(seed);
			neighbours = new ArrayList<ArrayList<Node>>();
			neighbours.add(new ArrayList<Node>());
			neighbours.get(0).add(seed);
			for(int d=0; d<degreeOfSeparation-1; d++){
				neighbours.add(new ArrayList<Node>());
				for(Node node : neighbours.get(d))
					for(Node neighbour : node.getNeighbours()){
					neighbours.get(d+1).add(neighbour);
					potentialNodes.remove(neighbour);
					}
			}
		}
		return separatedNodes;
	}

	public static ArrayList<Node> findSeparatedNodesByDegree(Graph graph, int degreeOfSeparation){
		if(degreeOfSeparation<=1) return null;
		ArrayList<Node> potentialNodes = new ArrayList<Node>();
		for(Node n : graph.getNodesList())
			potentialNodes.add(n);
		Collections.sort(potentialNodes, GraphTools.nodeDegreeAscending);
		ArrayList<Node> separatedNodes = new ArrayList<Node>();
		ArrayList<ArrayList<Node>> neighbours;
		Node seed;
		while(potentialNodes.size()>0){
			seed = potentialNodes.get(potentialNodes.size()-1);
			separatedNodes.add(seed);
			potentialNodes.remove(seed);
			neighbours = new ArrayList<ArrayList<Node>>();
			neighbours.add(new ArrayList<Node>());
			neighbours.get(0).add(seed);
			for(int d=0; d<degreeOfSeparation-1; d++){
				neighbours.add(new ArrayList<Node>());
				for(Node node : neighbours.get(d))
					for(Node neighbour : node.getNeighbours()){
					neighbours.get(d+1).add(neighbour);
					potentialNodes.remove(neighbour);
					}
			}
		}
		return separatedNodes;
	}
	
	public static ArrayList<Node> findSeparatedNodesByHigestBTDR(Graph graph, int degreeOfSeparation){
		if(degreeOfSeparation<=1) return null;
		ArrayList<Node> potentialNodes = new ArrayList<Node>();
		for(Node n : graph.getNodesList())
			potentialNodes.add(n);
		Collections.sort(potentialNodes, GraphTools.nodeBetweennessToDegreeRatioAscending);
		ArrayList<Node> separatedNodes = new ArrayList<Node>();
		ArrayList<ArrayList<Node>> neighbours;
		Node seed;
		while(potentialNodes.size()>0){
			seed = potentialNodes.get(potentialNodes.size()-1);
			separatedNodes.add(seed);
			potentialNodes.remove(seed);
			neighbours = new ArrayList<ArrayList<Node>>();
			neighbours.add(new ArrayList<Node>());
			neighbours.get(0).add(seed);
			for(int d=0; d<degreeOfSeparation-1; d++){
				neighbours.add(new ArrayList<Node>());
				for(Node node : neighbours.get(d))
					for(Node neighbour : node.getNeighbours()){
					neighbours.get(d+1).add(neighbour);
					potentialNodes.remove(neighbour);
					}
			}
		}
		return separatedNodes;
	}
	
	public static double averagePathsOverlap(Graph graph, PathsRegister register, int nodeId1, int nodeId2) {
		double apo = 0;
		ArrayList<Node> nodes = graph.getNodesList();
		for(Node n : nodes)
			if(n.getId()!=nodeId1 && n.getId()!=nodeId2)
				apo = apo + PathsTools.pathsOverlapEPL(register.getPaths(n.getId(), nodeId1), register.getPaths(n.getId(), nodeId2));
		apo = apo/(nodes.size()-2);
		return apo;
	}
		
	public static double[][] averagePathsOverlapForAllPairs(Graph graph, PathsRegister register) {
		ArrayList<Node> nodes = graph.getNodesList();
		double[][] apoAll = new double[nodes.size()][nodes.size()];
		double apo = 0;
		for(int i=0; i<nodes.size()-1; i++)
			for(int j=i+1; j<nodes.size(); j++) {
				apo = averagePathsOverlap(graph, register, nodes.get(i).getId(), nodes.get(j).getId());
				apoAll[i][j] = apo;
				apoAll[j][i] = apo;
			}
		return apoAll;
	}
	
	/**
	 * Comparators
	 */
	
	public static Comparator<Node> nodeStateAscending = new Comparator<Node>() {

		public int compare(Node node1, Node node2) {

		   int state1 = node1.getState();
		   int state2 = node2.getState();

		   /*For ascending order*/
		   return state1-state2;
	   }
	};
	
	public static Comparator<Node> nodeStateDescending = new Comparator<Node>() {

		public int compare(Node node1, Node node2) {

		   int state1 = node1.getState();
		   int state2 = node2.getState();

		   /*For descending order*/
		   return state2-state1;
	   }
	};
	
	public static Comparator<Node> nodeDegreeAscending = new Comparator<Node>() {

		public int compare(Node node1, Node node2) {

		   int degree1 = node1.getDegree();
		   int degree2 = node2.getDegree();

		   /*For ascending order*/
		   return degree1-degree2;
	   }
	};
	
	public static Comparator<Node> nodeDegreeDescending = new Comparator<Node>() {

		public int compare(Node node1, Node node2) {

		   int degree1 = node1.getDegree();
		   int degree2 = node2.getDegree();

		   /*For descending order*/
		   return degree2-degree1;
	   }
	};
	
	public static Comparator<Node> nodeBetweennessAscending = new Comparator<Node>() {

		public int compare(Node node1, Node node2) {

		   double betweenness1 = node1.getBetweenness();
		   double betweenness2 = node2.getBetweenness();

		   /*For ascending order*/
		   return betweenness1>betweenness2?1:(betweenness1<betweenness2?-1:0);
	   }
	};
	
	public static Comparator<Node> nodeBetweennessDescending = new Comparator<Node>() {

		public int compare(Node node1, Node node2) {

			double betweenness1 = node1.getBetweenness();
			double betweenness2 = node2.getBetweenness();

		   /*For descending order*/
		   return betweenness1<betweenness2?1:(betweenness1>betweenness2?-1:0);
	   }
	};
	
	public static Comparator<Node> nodeBetweennessToDegreeRatioDescending = new Comparator<Node>() {

		public int compare(Node node1, Node node2) {

			Double betweenness1 = node1.getBetweenness()/(node1.getDegree()*node1.getDegree());
			Double betweenness2 = node2.getBetweenness()/(node2.getDegree()*node2.getDegree());

		   /*For descending order*/
		   return betweenness2.compareTo(betweenness1);
	   }
	};
	
	public static Comparator<Node> nodeBetweennessToDegreeRatioAscending = new Comparator<Node>() {

		public int compare(Node node1, Node node2) {

			Double betweenness1 = node1.getBetweenness()/(node1.getDegree()*node1.getDegree());
			Double betweenness2 = node2.getBetweenness()/(node2.getDegree()*node2.getDegree());

		   /*For descending order*/
		   return betweenness1.compareTo(betweenness2);
	   }
	};
	
	public static Comparator<Node> nodeValueAscending = new Comparator<Node>() {

		@Override
		public int compare(Node node1, Node node2) {
			
			Double value1 = node1.getValue();
			Double value2 = node2.getValue();
			
			/* For ascending order*/
			return value1.compareTo(value2);
		}
		
	};
	
	public static Comparator<Node> nodeValueDescending = new Comparator<Node>() {

		@Override
		public int compare(Node node1, Node node2) {
			
			Double value1 = node1.getValue();
			Double value2 = node2.getValue();
			
			/* For ascending order*/
			return value2.compareTo(value1);
		}
		
	};
		
	public static class GreedyHVObsSelector implements Runnable {

		/***
		 * Data fields
		 */
		
		Graph graph;
		// The number of nodes in the graph
		int graphSize;
		// The register with all shortest paths between all pairs of the nodes in the graph
		PathsRegister register;
		// The starting nodes - the first nodes in the sets of observers
		ArrayList<Node> starters;
		// The number of observers in the set
		int budget;
		// The maximal distance between an observer and another observer
		int maxLength;
		// The set of observers which maximize the coverage with given constraints
		ArrayList<Node> selectedObservers;
		// The temporal coverage
		int temporalCoverage;
		// The maximum possible coverage with given constraints
		int bestCoverage;
		// The information if a given node is already covered
		int[] covered;
		
		/***
		 * Constructors
		 */
		
		public GreedyHVObsSelector(Graph graph, PathsRegister register, ArrayList<Node> starters, int budget, int maxLength) {
			super();
			this.graph = graph;
			this.graphSize = graph.getNumberOfNodes();
			this.register = register;
			this.starters = starters;
			this.budget = budget;
			this.maxLength = maxLength;
			this.temporalCoverage = 0;
			this.bestCoverage = 0;
		}
		
		/***
		 * Run method
		 */
		
		@Override
		public void run() {
			LinkedList<Node> candidates;
			ArrayList<Node> newSet;
			Node bestCandidate = null;
			
			for(Node starter : starters) {
				temporalCoverage = 0;
				covered = new int[graph.getNumberOfNodes()];
				candidates = new LinkedList<Node>();
				for(Node n : graph.getNodesList())
					candidates.add(n);
				candidates.remove(starter);
				newSet = new ArrayList<Node>();
				newSet.add(starter);
				while(newSet.size()<budget){
					bestCandidate = findNext(newSet, candidates);
					if(bestCandidate!=null) {
						newSet.add(bestCandidate);
						candidates.remove(bestCandidate);
					}
					else rescale();
				}
				//System.out.println(temporalCoverage);
				if(temporalCoverage>bestCoverage){
					selectedObservers = newSet;
					bestCoverage = temporalCoverage;
				}
			}
		}
		
		public Node findNext(ArrayList<Node> observers, LinkedList<Node> candidates) {
			Node bestCandidate = null;
			int maxCoverage = 0;
			HashSet<Node> newCovered = null;
			HashSet<Node> totalCovered = null;
			HashSet<Node> maxCovered = null;
			ArrayList<ArrayList<Path>> allPaths;
			
			for(Node cand: candidates) {
				totalCovered = new HashSet<Node>();
				newCovered = new HashSet<Node>();
				allPaths = register.getPaths(cand, observers);
				for(ArrayList<Path> paths : allPaths)
					if(paths!=null)
						if(paths.get(0).getLength()<=maxLength)
							for(Path path : paths)
								for(int n=1; n<path.getLength(); n++) {
									totalCovered.add(path.getNode(n));
									if(covered[path.getNode(n).getIndex()]==0)
										newCovered.add(path.getNode(n));
								}
				if(newCovered.size()>maxCoverage) {
					maxCoverage = newCovered.size();
					maxCovered = totalCovered;
					bestCandidate = cand;
				}
			}
			
			if(maxCoverage>0) {
				temporalCoverage = temporalCoverage + maxCoverage;
				for(Node n : maxCovered)
					covered[n.getIndex()] = covered[n.getIndex()] + 1;
			}
			
			return bestCandidate;
		}
		
		public void rescale() {
			//System.out.println("Rescale!");
			int counter = 0;
			for(int i=0; i<graphSize; i++) {
				covered[i] = covered[i] - 1;
				if(covered[i]>0)
					counter = counter + 1;
			}
			temporalCoverage = temporalCoverage + counter;
		}

		/***
		 * Getters
		 */
		
		public ArrayList<Node> getSelectedObservers() {
			return selectedObservers;
		}
		
		public int getBestCoverage() {
			return bestCoverage;
		}
		
	};
	
	public static class GreedyCoverageObsSelector implements Runnable {

		/***
		 * Data fields
		 */
		
		Graph graph;
		// The number of nodes in the graph
		int graphSize;
		// The starting nodes - the first nodes in the sets of observers
		ArrayList<Node> starters;
		// The number of observers in the set
		int budget;
		// The set of observers which maximize the coverage with given constraints
		ArrayList<Node> selectedObservers;
		// The temporal coverage
		int temporalCoverage;
		// The maximum possible coverage with given constraints
		int bestCoverage;
		// The information if a given node is already covered
		int[] covered;
		
		/***
		 * Constructors
		 */
		
		public GreedyCoverageObsSelector(Graph graph, ArrayList<Node> starters, int budget) {
			super();
			this.graph = graph;
			this.graphSize = graph.getNumberOfNodes();
			this.starters = starters;
			this.budget = budget;
			this.temporalCoverage = 0;
			this.bestCoverage = 0;
		}
		
		/***
		 * Run method
		 */
		
		@Override
		public void run() {
			LinkedList<Node> candidates;
			ArrayList<Node> newSet;
			Node bestCandidate = null;
			
			for(Node starter : starters) {
				temporalCoverage = 0;
				covered = new int[graphSize];
				candidates = new LinkedList<Node>();
				for(Node n : graph.getNodesList())
					candidates.add(n);
				candidates.remove(starter);
				newSet = new ArrayList<Node>();
				newSet.add(starter);
				while(newSet.size()<budget){
					bestCandidate = findNext(newSet, candidates);
					if(bestCandidate!=null) {
						newSet.add(bestCandidate);
						candidates.remove(bestCandidate);
					}
					else rescale();
				}
				if(temporalCoverage>bestCoverage){
					selectedObservers = newSet;
					bestCoverage = temporalCoverage;
				}
			}
		}
		
		public Node findNext(ArrayList<Node> observers, LinkedList<Node> candidates) {
			Node bestCandidate = null;
			int maxCoverage = 0;
			int newCovered = 0;
			
			for(Node cand: candidates) {
				newCovered = 0;
				for(Node n : cand.getNeighbours())
					if(covered[n.getIndex()]==0)
						newCovered = newCovered + 1;
				if(newCovered>maxCoverage) {
					maxCoverage = newCovered;
					bestCandidate = cand;
				}
			}
			
			if(maxCoverage>0) {
				temporalCoverage = temporalCoverage + maxCoverage;
				for(Node n : bestCandidate.getNeighbours())
					covered[n.getIndex()]=covered[n.getIndex()]+1;
			}
			
			return bestCandidate;
		}
		
		public void rescale() {
			int counter = 0;
			for(int i=0; i<graphSize; i++) {
				covered[i] = covered[i] - 1;
				if(covered[i]>0)
					counter = counter + 1;
			}
			temporalCoverage = temporalCoverage + counter;
		}
		
		/***
		 * Getters
		 */
		
		public ArrayList<Node> getSelectedObservers() {
			return selectedObservers;
		}
		
		public int getBestCoverage() {
			return bestCoverage;
		}
		
	};
	
	public static class GreedyKMedianObsSelector implements Runnable {

		/***
		 * Data fields
		 */
		
		Graph graph;
		// The number of nodes in the graph
		int graphSize;
		// The register with all shortest paths between all pairs of the nodes in the graph
		int[][] allDistances;
		// The starting nodes - the first nodes in the sets of observers
		ArrayList<Node> starters;
		// The number of observers in the set
		int budget;
		// The set of observers which maximize the coverage with given constraints
		ArrayList<Node> selectedObservers;
		// The temporal coverage
		int temporalDistance;
		// The maximum possible coverage with given constraints
		int bestDistance;
		// Array with distances to the nearest observer
		int[] distancesToObserver;
		
		/***
		 * Constructors
		 */
		
		public GreedyKMedianObsSelector(Graph graph, int[][] allDistances, ArrayList<Node> starters, int budget) {
			super();
			this.graph = graph;
			this.graphSize = graph.getNumberOfNodes();
			this.allDistances = allDistances;
			this.starters = starters;
			this.budget = budget;
			this.temporalDistance = Integer.MAX_VALUE;
			this.bestDistance = Integer.MAX_VALUE;
			this.distancesToObserver = new int[graphSize];
		}
		
		/***
		 * Run method
		 */
		
		@Override
		public void run() {
			LinkedList<Node> candidates;
			ArrayList<Node> newSet;
			Node bestCandidate = null;
			
			for(Node starter : starters) {
				candidates = new LinkedList<Node>();
				for(Node n : graph.getNodesList())
					candidates.add(n);
				candidates.remove(starter);
				newSet = new ArrayList<Node>();
				newSet.add(starter);
				initDistances(starter);
				while(newSet.size()<budget){
					bestCandidate = findNext(candidates);
					newSet.add(bestCandidate);
					candidates.remove(bestCandidate);
				}
				if(temporalDistance<bestDistance){
					selectedObservers = newSet;
					bestDistance = temporalDistance;
				}
			}
		}
		
		public void initDistances(Node initNode) {
			temporalDistance = Integer.MAX_VALUE;
			distancesToObserver = new int[graphSize];
			for(int i=0; i<graphSize; i++)
				distancesToObserver[i] = allDistances[initNode.getIndex()][i];
		}
		
		public Node findNext(LinkedList<Node> candidates) {
			Node bestCandidate = null;
			int[] bestDistances = null;
			int[] newDistances = new int[graphSize];
			int sumOfDistances = 0;
			int minSumOfDistances = Integer.MAX_VALUE;
			
			for(Node cand: candidates) {
				sumOfDistances = 0;
				newDistances = new int[graphSize];
				for(int i=0; i<graphSize; i++) {
					newDistances[i] = Math.min(distancesToObserver[i], allDistances[cand.getIndex()][i]);
					sumOfDistances = sumOfDistances + newDistances[i];
				}
				if(sumOfDistances<minSumOfDistances) {
					minSumOfDistances = sumOfDistances;
					bestCandidate = cand;
					bestDistances = newDistances;
				}
			}
			
			distancesToObserver = bestDistances;
			temporalDistance = minSumOfDistances;
			
			return bestCandidate;
		}

		/***
		 * Getters
		 */
		
		public ArrayList<Node> getSelectedObservers() {
			return selectedObservers;
		}
		
		public int getBestDistance() {
			return bestDistance;
		}
		
	};
}