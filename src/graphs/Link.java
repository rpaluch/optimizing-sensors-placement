package graphs;

/***
 * Basic class for an undirected link.
 * @author rpaluch
 */

public class Link {

	/**
	 * Data fields
	 */
	
	Node start;
	Node end;
	double weight;
	
	/**
	 * Constructors
	 */
		
	public Link(Node start, Node end) {
		this.start = start;
		this.end = end;
		this.weight = 1.0;
	}
		
	public Link(Node start, Node end, double weight) {
		this.start = start;
		this.end = end;
		this.weight = weight;
	}
		
	/**
	 * Getters and setters
	 */
	
	public Node getStart() {
		return start;
	}
	
	public Node getEnd() {
		return end;
	}
	
	public double getWeight() {
		return weight;
	}
	
	public void setStart(Node start) {
		this.start = start;
	}
	
	public void setEnd(Node end) {
		this.end = end;
	}
	
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	/**
	 * Other methods
	 */
		
	// Return true if the ids of given nodes match to "start" and "end"
	public boolean equals(int nodeId1, int nodeId2) {
		if(start.getId()==nodeId1 && end.getId()==nodeId2) {
			return true;
		}
		else if(start.getId()==nodeId2 && end.getId()==nodeId1) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean equals(Node node1, Node node2) {
		if(start==node1 && end==node2) {
			return true;
		}
		else if(start==node2 && end==node1) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean equals(Link link) {
		if(start.getId()==link.getStart().getId() && end.getId()==link.getEnd().getId()) {
			return true;
		}
		else if(start.getId()==link.getEnd().getId() && end.getId()==link.getStart().getId()) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public void print() {
		System.out.printf("%d->%d\n", start.getId(), end.getId());
	}
	
	public void printWithLayersIds() {
		System.out.printf("%d.%d->%d.%d\n", start.getLayerId(), start.getId(), end.getLayerId(), end.getId());
	}
	
	public void printWeight() {
		System.out.printf("%d\t%d\t%f\n", start.getId(), end.getId(), weight);
	}
	
	public String toString() {
		String string = String.format("%d->%d", start.getId(), end.getId());
		return string;
	}
}
