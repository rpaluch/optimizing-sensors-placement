package graphs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Random;

/***
 * Basic class for undirected graph. This graph is a set of nodes.
 * @author rpaluch
 */

public class Graph {

	/***
	 * Data fields
	 */
	
	// Set of nodes
	LinkedHashMap<Integer,Node> idToNode;
	// Set of links
	ArrayList<Link> links;
	
	/***
	 * Constructors
	 */
	
	public Graph() {
		idToNode = new LinkedHashMap<Integer,Node>();
		links = new ArrayList<Link>();
	}
	
	// Create a graph from a list of nodes and their neighbours
	public Graph(ArrayList<Node> initialNodes) {
		idToNode = new LinkedHashMap<Integer,Node>();
		links = new ArrayList<Link>();
		for(Node n : initialNodes)
			for(Node neighbour : n.getNeighbours())
				addLink(n.getId(), neighbour.getId());
	}
	
	public Graph(int size) {
		idToNode = new LinkedHashMap<Integer,Node>();
		links = new ArrayList<Link>();
		for(int i=1; i<=size; i++) addNode(i);
	}
	
	
	/***
	 * Getters
	 */
	
	// Return the number of nodes in the graph
	public int getNumberOfNodes() {
		return idToNode.size();
	}
	
	// Return the number of links in the graph
	public int getNumberOfLinks() {
		return links.size();
	}
	
	// Return a list of nodes in order they were added
	public ArrayList<Node> getNodesList() {
		return new ArrayList<Node>(idToNode.values());
	}
	
	// Return a hashmap of nodes
	public LinkedHashMap<Integer,Node> getNodesMap() {
		return idToNode;
	}
	
	// Return a node of a given id
	public Node getNode(int nodeId) {
		return idToNode.get(nodeId);
	}
	/*
	// Return an index of a given node
	public int getNodeIndex(int nodeId) {
		ArrayList<Integer> keys = new ArrayList<Integer>(idToNode.keySet());
		return keys.indexOf(nodeId);
	}
	*/
	// Return a list of links
	public ArrayList<Link> getLinksList() {
		return links;
	}
	
	// Return a particular link
	public Link getLink(int nodeId1, int nodeId2) {
		for(Link l : links)
			if(l.equals(nodeId1, nodeId2))
				return l;
		return null;
	}
	
	/***
	 * Other methods
	 */
	
	public boolean addNode(Node newNode) {
		if(!containsNode(newNode.getId())){
			idToNode.put(newNode.getId(),newNode);
			newNode.setIndex(getNodesList().size()-1);
			return true;
		}
		else return false;
	}
	
	public boolean addNode(int nodeId) {
		if(!containsNode(nodeId)){
			Node newNode = new Node(nodeId);
			idToNode.put(newNode.getId(),newNode);
			newNode.setIndex(getNodesList().size()-1);
			return true;
		}
		else return false;
	}
	
	public boolean containsNode(int nodeId) {
		if(getNode(nodeId)!=null) {
			return true;
		}
		else {
			return false;
		}
	}
	
	// This method does not re-index nodes!
	public void removeNode(int nodeId) {
		if(containsNode(nodeId)) {
			cutNode(nodeId);
			idToNode.remove(nodeId);
		}
	}
	
	public void removeNodeAndReindexNodes(int nodeId) {
		if(containsNode(nodeId)) {
			cutNode(nodeId);
			idToNode.remove(nodeId);
			reindexNodes();
		}
	}
	
	public void reindexNodes() {
		ArrayList<Node> allNodes = this.getNodesList();
		for(int i=0; i<allNodes.size(); i++)
			allNodes.get(i).setIndex(i);
	}
		
	// If a node doesn't exist - creates new one, adds to graph and return
	public Node getOrAddNode(int nodeId) {
		Node node = getNode(nodeId);
		if(node == null) {
			node = new Node(nodeId);
			addNode(node);
		}
		return node;
	}
	
	// Returns true if a link was added, otherwise returns false
	public boolean addLink(int nodeId1, int nodeId2) {
		// Get first node or create
		Node node1 = getOrAddNode(nodeId1);
		Node node2 = getOrAddNode(nodeId2);
		if(node1.addNeighbour(node2) && node2.addNeighbour(node1)) {
			links.add(new Link(node1,node2));
			return true;
		}
		else return false;
	}
		
	// Returns true if a link was added, otherwise returns false
	public boolean addLink(int nodeId1, int nodeId2, double weight) {
		// Get first node or create
		Node node1 = getOrAddNode(nodeId1);
		Node node2 = getOrAddNode(nodeId2);
		if(node1.addNeighbour(node2) && node2.addNeighbour(node1)) {
			links.add(new Link(node1,node2,weight));
			return true;
		}
		else return false;
	}
	
	// Returns true if a link was added, otherwise returns false
	public boolean addLink(Node node1, Node node2) {
		// Add nodes to the graph if they were not in the graph already
		addNode(node1);
		addNode(node2);
		// Create a link
		if(node1.addNeighbour(node2) && node2.addNeighbour(node1)) {
			links.add(new Link(node1,node2));
			return true;
		}
		else return false;
	}
	
	// Returns true if a link was added, otherwise returns false
	public boolean addLink(Node node1, Node node2, double weight) {
		// Add nodes to the graph if they were not in the graph already
		addNode(node1);
		addNode(node2);
		// Create a link
		if(node1.addNeighbour(node2) && node2.addNeighbour(node1)) {
			links.add(new Link(node1,node2,weight));
			return true;
		}
		else return false;
	}
	
	// Returns true if a link between given nodes exists
	public boolean containsLink(int nodeId1, int nodeId2) {
		for(Link l : links)
			if(l.equals(nodeId1, nodeId2))
				return true;
		return false;
	}
	
	// Returns true if a link between given nodes exists
	public boolean containsLink(Node node1, Node node2) {
		for(Link l : links)
			if(l.equals(node1, node2))
				return true;
		return false;
	}
	
	// Returns true if a link between given nodes exists
	public boolean containsLink(Link link) {
		for(Link l : links)
			if(l.equals(link))
				return true;
		return false;
	}
	
	// Returns false if the link does not exist
	public void removeLink(int nodeId1, int nodeId2) {
		Node node1 = getNode(nodeId1);
		Node node2 = getNode(nodeId2);
		node1.removeNeighbour(node2);
		node2.removeNeighbour(node1);
		links.remove(getLink(nodeId1,nodeId2));
	}
	
	// Removes all links of given node
	public void cutNode(int nodeId) {
		Node node = getNode(nodeId);
		for(Node n : node.getNeighbours()) {
			n.removeNeighbour(node);
			links.remove(getLink(nodeId,n.getId()));
		}
		node.removeAllNeighbours();
	}
	
	// Returns one random from the list of nodes - each node has equal probability to be chosen
	public Node getRandomNode() {
		Random rand = new Random();
		return getNodesList().get(rand.nextInt(getNumberOfNodes()));
	}

	// Returns one random node from the list of nodes - the probability of taking given node is proportional to its degree.
	public Node getRandomNodeByDegree() {
		Random rand = new Random();
		ArrayList<Node> lottery = new ArrayList<Node>();
		for(Node n : this.getNodesList())
			for(int k=0; k<n.getDegree(); k++)
				lottery.add(n);
		return lottery.get(rand.nextInt(lottery.size()));
	}
	
	// Returns a given number of random nodes without repetition - each node has equal probability to be chosen
	// The methods has two variants:
	// 		1. The first variants is applied when the number of random nodes is much smaller than than the size of the network (n<<N)
	//		2. The second variant is applied otherwise and it is based on shuffling the whole list of the nodes.
	public ArrayList<Node> getRandomNodes(int howMany) {
		
		ArrayList<Integer> randomNumbers = new ArrayList<Integer>();
		int size = getNumberOfNodes();
		
		// Variant 1: when n<<N
		if(howMany<0.3*getNumberOfNodes()) {
			int number = -1;
			Random rand = new Random();
			while(randomNumbers.size()<howMany) {
				number = rand.nextInt(size);
				if(!randomNumbers.contains(number)) randomNumbers.add(number);
			}
		} // Variant 2: when n~N
		else {
			for(int i=0; i<size; i++) randomNumbers.add(i);
			Collections.shuffle(randomNumbers);
		}
		
		// Add the number nodes to the list
		ArrayList<Node> randomNodes = new ArrayList<Node>();
		for(int i=0; i<howMany; i++) randomNodes.add(getNodesList().get(randomNumbers.get(i)));
		return randomNodes;
	}
	
	// Returns a given number of random nodes without repetition - each node has equal probability to be chosen except some selected nodes 
	// which are given as the array list.
		// The methods has two variants:
		// 		1. The first variants is applied when the number of random nodes is much smaller than than the size of the network (n<<N)
		//		2. The second variant is applied otherwise and it is based on shuffling the whole list of the nodes.
		public ArrayList<Node> getRandomNodes(int howMany, ArrayList<Node> except) {
			
			ArrayList<Integer> randomNumbers = new ArrayList<Integer>();
			ArrayList<Integer> exceptions = new ArrayList<Integer>();
			for(Node n : except) exceptions.add(n.getIndex());
			int size = getNumberOfNodes();
			
			// Variant 1: when n<<N
			if(howMany<0.3*getNumberOfNodes()) {
				int number = -1;
				Random rand = new Random();
				while(randomNumbers.size()<howMany) {
					number = rand.nextInt(size);
					if(!randomNumbers.contains(number) && !exceptions.contains(number)) randomNumbers.add(number);
				}
			} // Variant 2: when n~N
			else {
				for(int i=0; i<size; i++) randomNumbers.add(i);
				for(Integer i : exceptions) randomNumbers.remove(i);
				Collections.shuffle(randomNumbers);
			}
			
			// Add the number nodes to the list
			ArrayList<Node> randomNodes = new ArrayList<Node>();
			for(int i=0; i<howMany; i++) randomNodes.add(getNodesList().get(randomNumbers.get(i)));
			return randomNodes;
		}
	
	// Returns a given number of random nodes without repetition - the probability of taking given node is proportional to its degree.
	public ArrayList<Node> getRandomNodesByDegree(int howMany) {
		
		ArrayList<Node> randomNodes = new ArrayList<Node>();
		ArrayList<Node> lottery = new ArrayList<Node>();
		
		for(Node n : this.getNodesList())
			for(int k=0; k<n.getDegree(); k++)
				lottery.add(n);
		
		int size = lottery.size();
		int number = -1;
		Random rand = new Random();
		
		while(randomNodes.size()<howMany) {
			number = rand.nextInt(size);
			if(!randomNodes.contains(lottery.get(number))) randomNodes.add(lottery.get(number));
		}
		
		return randomNodes;
	}
		
	// Computes and returns an average degree of network
	public double averageDegree() {
		double sum = 0.0;
		for(Entry<Integer,Node> entry : idToNode.entrySet()) sum = sum + entry.getValue().getDegree();
		return sum/this.getNumberOfNodes();
	}
		
	// Checks if there exists a path between a random node and any other node
	public boolean isContiguous() {
		ArrayList<LinkedList<Node>> paths = PathsTools.shortestPaths(this, this.getRandomNode().getId());
		boolean contiguous = true;
		for(LinkedList<Node> p : paths) if(p.size()<1) contiguous = false;
		return contiguous;
	}
	
	// Returns a copy of network - nodes have the same ID and neighbours. This method does not copy states of nodes. 
	public Graph copy() {
		Graph graphCopy = new Graph();
		for(Node n : this.getNodesList()) graphCopy.addNode(n.getId());
		for(Link l : this.getLinksList()) graphCopy.addLink(l.getStart().getId(), l.getEnd().getId(), l.getWeight());
		return graphCopy;
	}
	
	// Returns a copy of the nodes list - it does not create a copy o nodes.
	public ArrayList<Node> copyOfNodesList() {
		ArrayList<Node> copyNodes = new ArrayList<Node>();
		int size = this.getNodesList().size();
		for(int i=0; i<size; i++)
			copyNodes.add(this.getNodesList().get(i));
		return copyNodes;
	}
		
	public void printStats() {
		System.out.printf("N = %d, E = %d, <k> = %f\n", this.getNumberOfNodes(), this.getNumberOfLinks(), this.averageDegree());
	}
	
	public void printNodes() {
		for(Node n : this.getNodesList()) n.print();
	}
	
	public void printLinks() {
		for(Link l : this.getLinksList()) l.print();
	}
	
	public void printAll() {
		this.printStats();
		this.printNodes();
		this.printLinks();
	}
}
