package dynamics;

import java.util.ArrayList;
import java.util.Collections;

import graphs.Graph;
import graphs.Link;
import graphs.Node;
import graphs.PathsTools;
import jdistlib.generic.GenericDistribution;
import tracking.Observer;

/***
 * This class is used to process spreading on the graph with a given time delays distribution.
 * @author rpaluch
 */

public class GenericSpreading {

	/***
	 * Data fields
	 */
	
	// Distribution for time delays 
	GenericDistribution distribution;
	// Underlying graph
	Graph graph;
	// List of the sources - a source is an node which is infected at the time t=0
	ArrayList<Node> sources;
	// List of the observers - an observer is an node which state will be recorded in each step
	ArrayList<Node> observers;
	// Contains the number of step in which the observer has been infected
	ArrayList<Double> infectionTimes;
	// Minimal and maximal time delay
	double minimalDelay,maximalDelay;
	
	/***
	 * Constructors
	 */
	
	public GenericSpreading(Graph graph, GenericDistribution distribution) {
		this.graph = graph;
		this.distribution = distribution;
		this.sources = new ArrayList<Node>();
		this.sources.add(graph.getRandomNode());
		this.observers = new ArrayList<Node>();
		this.infectionTimes = new ArrayList<Double>();
		this.minimalDelay = Double.NEGATIVE_INFINITY;
		this.maximalDelay = Double.POSITIVE_INFINITY;
	}
	
	/***
	 * This method draws a positive numbers from a given distribution 
	 * and assign this number to the links of the graph using method setWeight(double weight)
	 */
	protected  void randomTimeDelays() {
		double randomTime;
		for(Link l : graph.getLinksList()) {
			randomTime = distribution.random();
			while(randomTime<minimalDelay || randomTime>maximalDelay) randomTime = distribution.random();
			l.setWeight(randomTime);
		}
			
	} 
	
	public void addSource(Node newSource) {
		sources.add(newSource);
	}
	
	public void addSources(ArrayList<Node> newSources) {
		sources.addAll(newSources);
	}
	
	public void addRandomSources(int howMany) {
		addSources(graph.getRandomNodes(howMany));
	}
	
	public void addObserver(Node newObserver) {
		observers.add(newObserver);
		infectionTimes.add(Double.POSITIVE_INFINITY);
	}
	
	public void addObservers(ArrayList<Node> newObservers) {
		observers.addAll(newObservers);
		for(int i=0; i<newObservers.size(); i++) {
			infectionTimes.add(Double.POSITIVE_INFINITY);
		}
	}
	
	public void addRandomObservers(int howMany) {
		addObservers(graph.getRandomNodes(howMany));
	}
	
	public void run() {
		randomTimeDelays();
		ArrayList<Integer> observersIds = new ArrayList<Integer>();
		for(Node obs : observers)
			observersIds.add(obs.getId());
		ArrayList<Double> possibleInfectionTimes;
		for(Node s : sources) {
			possibleInfectionTimes = PathsTools.shortestWeightedPathsLengths(graph, s.getId(), observersIds);
			for(int i=0; i<possibleInfectionTimes.size(); i++)
				if(possibleInfectionTimes.get(i)<infectionTimes.get(i))
					infectionTimes.set(i, possibleInfectionTimes.get(i));
		}
	}
	
	public double computeMeanDelay() {
		double mean = 0.0;
		for(Link l : graph.getLinksList())
			mean = mean + l.getWeight();
		return mean/graph.getNumberOfLinks();
	}
	
	public double computeVarianceOfDelays() {
		double variance = 0.0;
		double mean = this.computeMeanDelay();
		for(Link l : graph.getLinksList())
			variance = variance + Math.pow(l.getWeight()-mean,2);
		return variance/graph.getNumberOfLinks();
	}
	
	/***
	 * Getters and setters
	 */
	
	public GenericDistribution getDistribution() {
		return distribution;
	}
	
	public void setDistribution(GenericDistribution distribution) {
		this.distribution = distribution;
	}
	
	public Graph getGraph() {
		return graph;
	}
	
	public void setGraph(Graph graph) {
		this.graph = graph;
	}
	
	public ArrayList<Node> getSources() {
		return sources;
	}
	
	public void setSources(ArrayList<Node> sources) {
		this.sources = sources;
	}
	
	public void setOneSource(Node source) {
		sources = new ArrayList<Node>();
		sources.add(source);
	}
	
	public ArrayList<Node> getObservers() {
		return observers;
	}
	
	public void setObservers(ArrayList<Node> observers) {
		this.observers = observers;
		this.infectionTimes = new ArrayList<Double>();
		for(int i=0; i<observers.size(); i++) {
			infectionTimes.add(Double.POSITIVE_INFINITY);
		}
	}
	
	public double getMinimalTimeDelay() {
		return minimalDelay;
	}
	
	public void setMinimalTimeDelay(double minimalDelay) {
		this.minimalDelay = minimalDelay;
	}
	
	public double getMaximalTimeDelay() {
		return maximalDelay;
	}
	
	public void setMaximalTimeDelay(double maximalDelay) {
		this.maximalDelay = maximalDelay;
	}
	
	public ArrayList<Double> getInfectionTimes() {
		return infectionTimes;
	}
	
	public ArrayList<Observer> getObserversAndDelays(int numberOfActiveObservers) {
		ArrayList<Observer> observersSorted = new ArrayList<Observer>();
		for(int i=0; i<observers.size(); i++) observersSorted.add(new Observer(observers.get(i), infectionTimes.get(i).doubleValue()));
		Collections.sort(observersSorted, Observer.delaysAscending);
		ArrayList<Observer> observersActive = new ArrayList<Observer>(observersSorted.subList(0, numberOfActiveObservers));
		ArrayList<Observer> observersPassive = new ArrayList<Observer>();
		for(int i=numberOfActiveObservers; i<observersSorted.size(); i++) {
			observersSorted.get(i).setDelay(Double.POSITIVE_INFINITY);
			observersSorted.get(i).setInfectionVector(null);
			observersPassive.add(observersSorted.get(i));
		}
		Collections.shuffle(observersPassive);
		observersActive.addAll(observersPassive);
		return observersActive;
	}
	
	public Node getNearestObserver() {
		ArrayList<Observer> observersList = new ArrayList<Observer>();
		for(int i=0; i<observers.size(); i++) observersList.add(new Observer(observers.get(i), infectionTimes.get(i).doubleValue()));
		Collections.sort(observersList, Observer.delaysAscending);
		return observersList.get(0).getNode();
	}
	
	public ArrayList<Observer> getNearestObservers(int howMany) {
		ArrayList<Observer> observersList = new ArrayList<Observer>();
		for(int i=0; i<observers.size(); i++) observersList.add(new Observer(observers.get(i), infectionTimes.get(i).doubleValue()));
		Collections.sort(observersList, Observer.delaysAscending);
		return new ArrayList<Observer>(observersList.subList(0, howMany));
	}
		
}
