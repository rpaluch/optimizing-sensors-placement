package dynamics;

import graphs.Graph;
import graphs.Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

import tracking.Observer;

/***
 * This class is used to process Susceptible-Infected (SI) dynamic on a given graph.
 * @author rpaluch
 */

public class SusceptibleInfected {
	
	/***
	 * Data fields
	 */
	
	// Underlying graph for SI
	Graph graph;
	// Graph for storing propagation cascade
	Graph cascade;
	// Number of steps of a single realization
	int steps;
	// Probability for each link to transfer the infection
	double infectionRate;
	// List of the sources - a source is an node which is infected at the time t=0
	ArrayList<Node> sources;
	// List of the observers - an observer is an node which state will be recorded in each step
	ArrayList<Node> observers;
	// Observations - each position of the list contains the time series of the state of a particular node
	ArrayList<ArrayList<Integer>> observations;
	// Contains the number of step in which the observer has been infected
	ArrayList<Integer> infectionTimes;
	// The list which information who infected who.
	ArrayList<Node> infectionVectors;
	// Counter of infected observers
	int infectedObserversCounter;
	// Time of the start and the end of simulation in milliseconds
	long startTime, stopTime;
	
	/***
	 * Constructors
	 */
		
	public SusceptibleInfected(Graph initGraph) {
		graph = initGraph;
		this.resetStates();
		cascade = new Graph();
		steps = 1000*initGraph.getNumberOfNodes();
		infectionRate = 0.5;
		sources = new ArrayList<Node>();
		sources.add(initGraph.getRandomNode());
		observers = new ArrayList<Node>();
		observations = new ArrayList<ArrayList<Integer>>();
		infectionTimes = new ArrayList<Integer>();
		infectionVectors = new ArrayList<Node>();
		infectedObserversCounter = 0;
	}
	
	public SusceptibleInfected(Graph initGraph, int initSteps) {
		graph = initGraph;
		this.resetStates();
		cascade = new Graph();
		steps = initSteps;
		infectionRate = 0.5;
		sources = new ArrayList<Node>();
		sources.add(initGraph.getRandomNode());
		observers = new ArrayList<Node>();
		observations = new ArrayList<ArrayList<Integer>>();
		infectionTimes = new ArrayList<Integer>();
		infectionVectors = new ArrayList<Node>();
		infectedObserversCounter = 0;
	}
	
	public SusceptibleInfected(Graph initGraph, int initSteps, double initRate) {
		graph = initGraph;
		this.resetStates();
		cascade = new Graph();
		steps = initSteps;
		infectionRate = initRate;
		sources = new ArrayList<Node>();
		sources.add(initGraph.getRandomNode());
		observers = new ArrayList<Node>();
		observations = new ArrayList<ArrayList<Integer>>();
		infectionTimes = new ArrayList<Integer>();
		infectionVectors = new ArrayList<Node>();
		infectedObserversCounter = 0;
	}
		
	public SusceptibleInfected(Graph initGraph, int initSteps, double initRate, int initSourcesNumber) {
		graph = initGraph;
		this.resetStates();
		cascade = new Graph();
		steps = initSteps;
		infectionRate = initRate;
		sources = new ArrayList<Node>();
		this.addRandomSources(initSourcesNumber);
		observers = new ArrayList<Node>();
		observations = new ArrayList<ArrayList<Integer>>();
		infectionTimes = new ArrayList<Integer>();
		infectionVectors = new ArrayList<Node>();
		infectedObserversCounter = 0;
	}
	
	public SusceptibleInfected(Graph initGraph, int initSteps, double initRate, ArrayList<Node> initSources) {
		graph = initGraph;
		this.resetStates();
		cascade = new Graph();
		steps = initSteps;
		infectionRate = initRate;
		sources = initSources;
		observers = new ArrayList<Node>();
		observations = new ArrayList<ArrayList<Integer>>();
		infectionTimes = new ArrayList<Integer>();
		infectionVectors = new ArrayList<Node>();
		infectedObserversCounter = 0;
	}
	
	public SusceptibleInfected(Graph initGraph, int initSteps, double initRate, int initSourcesNumber, ArrayList<Node> initObs) {
		graph = initGraph;
		this.resetStates();
		cascade = new Graph();
		steps = initSteps;
		infectionRate = initRate;
		sources = new ArrayList<Node>();
		this.addRandomSources(initSourcesNumber);
		observers = initObs;
		observations = new ArrayList<ArrayList<Integer>>();
		infectionTimes = new ArrayList<Integer>();
		infectionVectors = new ArrayList<Node>();
		for(int i=0; i<observers.size(); i++) {
			observations.add(new ArrayList<Integer>());
			infectionTimes.add(Integer.MAX_VALUE);
			infectionVectors.add(null);
		}
		infectedObserversCounter = 0;
	}
	
	public SusceptibleInfected(Graph initGraph, int initSteps, double initRate, ArrayList<Node> initSources, ArrayList<Node> initObs) {
		graph = initGraph;
		this.resetStates();
		cascade = new Graph();
		steps = initSteps;
		infectionRate = initRate;
		sources = initSources;
		observers = initObs;
		observations = new ArrayList<ArrayList<Integer>>();
		infectionTimes = new ArrayList<Integer>();
		infectionVectors = new ArrayList<Node>();
		for(int i=0; i<observers.size(); i++) {
			observations.add(new ArrayList<Integer>());
			infectionTimes.add(Integer.MAX_VALUE);
			infectionVectors.add(null);
		}	
		infectedObserversCounter = 0;
	}
	
	/***
	 * Getters and setters
	 */
	
	public Graph getGraph() {
		return graph;
	}
	
	public Graph getCascade() {
		return cascade;
	}
	
	public int getSteps() {
		return steps;
	}
	
	public double getInfectionRate() {
		return infectionRate;
	}
	
	public ArrayList<Node> getSources() {
		return sources;
	}
	
	public ArrayList<Node> getObservers() {
		return observers;
	}
		
	public ArrayList<ArrayList<Integer>> getObservations() {
		return observations;
	}

	public ArrayList<Integer> getInfectionTime() {
		return infectionTimes;
	}
	
	public ArrayList<Double> getInfectionTimeAsDouble() {
		ArrayList<Double> doubleInfectionTimes = new ArrayList<Double>();
		for(Integer i : infectionTimes)
			doubleInfectionTimes.add(i.doubleValue());
		return doubleInfectionTimes;
	}
	
	public ArrayList<Double> getInfectionTimesForGivenObservers(ArrayList<Node> observers) {
		ArrayList<Double> doubleInfectionTimes = new ArrayList<Double>();
		for(Node obs : observers)
			doubleInfectionTimes.add(infectionTimes.get(this.observers.indexOf(obs)).doubleValue());
		return doubleInfectionTimes;
	}
	
	public ArrayList<Node> getInfectionVectors() {
		return infectionVectors;
	}
	
	public int getInfectedObserversCounter() {
		return infectedObserversCounter;
	}
	
	public Node getNearestObserver() {
		ArrayList<Observer> observersList = new ArrayList<Observer>();
		for(int i=0; i<observers.size(); i++) observersList.add(new Observer(observers.get(i), infectionTimes.get(i).doubleValue()));
		Collections.sort(observersList, Observer.delaysAscending);
		return observersList.get(0).getNode();
	}
	
	public ArrayList<Observer> getNearestObservers(int howMany) {
		ArrayList<Observer> observersList = new ArrayList<Observer>();
		for(int i=0; i<observers.size(); i++) observersList.add(new Observer(observers.get(i), infectionTimes.get(i).doubleValue()));
		Collections.sort(observersList, Observer.delaysAscending);
		return new ArrayList<Observer>(observersList.subList(0, howMany));
	}
	
	public ArrayList<Observer> getInfectedObservers() {
		ArrayList<Observer> observersList = new ArrayList<Observer>();
		for(int i=0; i<observers.size(); i++) observersList.add(new Observer(observers.get(i), infectionTimes.get(i).doubleValue()));
		Collections.sort(observersList, Observer.delaysAscending);
		return new ArrayList<Observer>(observersList.subList(0, infectedObserversCounter));
	}
	
	public long getStartTime() {
		return startTime;
	}

	public long getStopTime() {
		return stopTime;
	}
	
	public long getSimulationTime() {
		return (stopTime-startTime);
	}
	
	public int getExecutedSteps() {
		return observations.get(0).size();
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
		this.resetStates();
	}
	
	public void setSteps(int steps) {
		this.steps = steps;
	}
		
	public void setInfectionRate(double rate) {
		this.infectionRate = rate;
	}
	
	public void setSources(ArrayList<Node> sources) {
		this.sources = sources;
	}
	
	public void setOneSource(Node source) {
		ArrayList<Node> sources = new ArrayList<Node>();
		sources.add(source);
		this.sources = sources;
	}
	
	public void setObservers(ArrayList<Node> observers) {
		this.observers = observers;
		for(int i=0; i<observers.size(); i++) {
			observations.add(new ArrayList<Integer>());
			infectionTimes.add(Integer.MAX_VALUE);
			infectionVectors.add(null);
		}
	}
	
	/***
	 * Other methods
	 */
	
	public void addSource(Node newSource) {
		sources.add(newSource);
	}
	
	public void addSources(ArrayList<Node> newSources) {
		sources.addAll(newSources);
	}
	
	public void addRandomSources(int howMany) {
		addSources(graph.getRandomNodes(howMany));
	}
	
	public void addObserver(Node newObserver) {
		observers.add(newObserver);
		observations.add(new ArrayList<Integer>());
		infectionTimes.add(Integer.MAX_VALUE);
		infectionVectors.add(null);
	}
	
	public void addObservers(ArrayList<Node> newObservers) {
		observers.addAll(newObservers);
		for(int i=0; i<newObservers.size(); i++) {
			observations.add(new ArrayList<Integer>());
			infectionTimes.add(Integer.MAX_VALUE);
			infectionVectors.add(null);
		}
	}
	
	public void addRandomObservers(int howMany) {
		addObservers(graph.getRandomNodes(howMany));
	}
	
	public void addObservation(int step) {
		for(int i=0; i<observers.size(); i++) {
			observations.get(i).add(observers.get(i).getState());
			if(observers.get(i).getState()==1 && infectionTimes.get(i)==Integer.MAX_VALUE) {
				infectionTimes.set(i, step);
				infectedObserversCounter = infectedObserversCounter + 1;
			}
		}
	}
	
	public void resetStates() {
		for(Node n : graph.getNodesList())
			n.setState(0);
	}
		
	public void runUntilAllSteps() {
		
		startTime = System.currentTimeMillis();
		Random rand = new Random();
		int observerIndex = -1;
		ArrayList<Node> infected = new ArrayList<Node>(sources);
		for(Node infNode : infected) {
			infNode.setState(1);
			observerIndex = observers.indexOf(infNode);
			if(observerIndex>=0) infectionVectors.set(observerIndex, infNode);
		}
		Queue<Node> newlyInfected = new LinkedList<Node>();
		
		addObservation(0);
		for(int t=0; t<steps; t++) {
			for(Node infNode : infected) {
				for(Node neighbour : infNode.getNeighbours()) {
					if(neighbour.getState()==0)
						if(rand.nextDouble()<infectionRate) {
							neighbour.setState(1);
							newlyInfected.add(neighbour);
							cascade.addLink(infNode.getId(), neighbour.getId());
							observerIndex = observers.indexOf(neighbour);
							if(observerIndex>=0) infectionVectors.set(observerIndex, infNode);
						}
				}
			}	
			while(newlyInfected.size()>0) infected.add(newlyInfected.poll());
			addObservation(t+1);
		}
		stopTime = System.currentTimeMillis();
	}

	public void runUntilAllInfected() {
		
		startTime = System.currentTimeMillis();
		Random rand = new Random();
		int observerIndex = -1;
		ArrayList<Node> infected = new ArrayList<Node>(sources);
		for(Node infNode : infected) {
			infNode.setState(1);
			observerIndex = observers.indexOf(infNode);
			if(observerIndex>=0) infectionVectors.set(observerIndex, infNode);
		}
		Queue<Node> newlyInfected = new LinkedList<Node>();

		addObservation(0);
		for(int t=0; t<steps; t++) {
			if(infected.size()==graph.getNumberOfNodes()) break;
			for(Node infNode : infected) {
				for(Node neighbour : infNode.getNeighbours()) {
					if(neighbour.getState()==0)
						if(rand.nextDouble()<infectionRate) {
							neighbour.setState(1);
							newlyInfected.add(neighbour);
							cascade.addLink(infNode.getId(), neighbour.getId());
							observerIndex = observers.indexOf(neighbour);
							if(observerIndex>=0) infectionVectors.set(observerIndex, infNode);
						}
				}
			}	
			while(newlyInfected.size()>0) infected.add(newlyInfected.poll());
			addObservation(t+1);
		}
		stopTime = System.currentTimeMillis();
	}
	
	public void runUntilAllObserversInfected() {
		
		startTime = System.currentTimeMillis();
		Random rand = new Random();
		int observerIndex = -1;
		ArrayList<Node> infected = new ArrayList<Node>(sources);
		for(Node infNode : infected) {
			infNode.setState(1);
			observerIndex = observers.indexOf(infNode);
			if(observerIndex>=0) infectionVectors.set(observerIndex, infNode);
		}
		Queue<Node> newlyInfected = new LinkedList<Node>();

		addObservation(0);
		for(int t=0; t<steps; t++) {
			if(infectedObserversCounter>=observers.size()) break;
			for(Node infNode : infected) {
				for(Node neighbour : infNode.getNeighbours()) {
					if(neighbour.getState()==0)
						if(rand.nextDouble()<infectionRate) {
							neighbour.setState(1);
							newlyInfected.add(neighbour);
							cascade.addLink(infNode.getId(), neighbour.getId());
							observerIndex = observers.indexOf(neighbour);
							if(observerIndex>=0) infectionVectors.set(observerIndex, infNode);
						}
				}
			}	
			while(newlyInfected.size()>0) infected.add(newlyInfected.poll());
			addObservation(t+1);
		}
		stopTime = System.currentTimeMillis();
	}
	
	public void runUntilObserversInfected(int howMany) {
		
		startTime = System.currentTimeMillis();
		Random rand = new Random();
		int observerIndex = -1;
		ArrayList<Node> infected = new ArrayList<Node>(sources);
		for(Node infNode : infected) {
			infNode.setState(1);
			observerIndex = observers.indexOf(infNode);
			if(observerIndex>=0) infectionVectors.set(observerIndex, infNode);
		}
		Queue<Node> newlyInfected = new LinkedList<Node>();

		addObservation(0);
		for(int t=0; t<steps; t++) {
			if(infectedObserversCounter>=howMany) break;
			for(Node infNode : infected) {
				for(Node neighbour : infNode.getNeighbours()) {
					if(neighbour.getState()==0)
						if(rand.nextDouble()<infectionRate) {
							neighbour.setState(1);
							newlyInfected.add(neighbour);
							cascade.addLink(infNode.getId(), neighbour.getId());
							observerIndex = observers.indexOf(neighbour);
							if(observerIndex>=0) infectionVectors.set(observerIndex, infNode);
						}
				}
			}	
			while(newlyInfected.size()>0) infected.add(newlyInfected.poll());
			addObservation(t+1);
		}
		stopTime = System.currentTimeMillis();
	}
	
}