package tracking;

import java.util.Comparator;

import graphs.Node;

/**
 * Basic class for supposed source for the single source estimator 
 * @author Robert Paluch
 */

public class SupposedSource {
	
	/**
	 * Data fields
	 */
	
	Node node;
	Double score;
	
	/**
	 * Constructors
	 */
	
	public SupposedSource(Node node) {
		this.node = node;
		this.score = 0.0;
	}
	
	public SupposedSource(Node node, Double score) {
		this.node = node;
		this.score = score;
	}

	/**
	 * Getters and setters
	 */
	
	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}
	
	/**
	 * Comparators
	 */
	
	public static Comparator<SupposedSource> scoresAscending = new Comparator<SupposedSource>() {

		public int compare(SupposedSource obs1, SupposedSource obs2) {

		   Double score1 = obs1.getScore();
		   Double score2 = obs2.getScore();

		   /*For ascending order*/
		   return score1.compareTo(score2);
	   }
	};
	
	public static Comparator<SupposedSource> scoresDescending = new Comparator<SupposedSource>() {

		public int compare(SupposedSource obs1, SupposedSource obs2) {

			Double score1 = obs1.getScore();
			Double score2 = obs2.getScore();

			/*For descending order*/
			return score2.compareTo(score1);
	   }
	};

}
