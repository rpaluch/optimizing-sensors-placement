package tracking;

import java.util.ArrayList;

import org.jblas.Decompose;
import org.jblas.Decompose.LUDecomposition;
import org.jblas.DoubleMatrix;
import org.jblas.Solve;

import graphs.Graph;
import graphs.Node;
import graphs.Path;
import graphs.PathsTools;


/***
 * An implementation of the limited (without infection vectors) Pinto-Thiran-Vetterli algorithm 
 * for searching the sources of diffusion processes.
 * @author rpaluch
 */

public class ScoreEstimatorLPTV2 implements Runnable {

	/***
	 * Data fields
	 */
	
	Graph graph;
	// The list of observers 
	ArrayList<Node> observers;
	// The list of observers ID 
	ArrayList<Integer> observersID;
	// The reference node is the last observer on the list of the observers
	Node reference;
	// The difference in delays between the reference node and other observers 
	DoubleMatrix relativeDelays;
	// The mean delay (greek mu)
	double delayMean;
	// The variance of delay (greek sigma square)
	double delayVariance;
	// The list of nodes which will be examined by Pinto algorithm 
	SupposedSource supposedSource;
	
	/***
	 * Constructors
	 */
		
	public ScoreEstimatorLPTV2(Graph graph, ArrayList<Node> observers, DoubleMatrix relativeDelays,
			                  double delayMean, double delayVariance, SupposedSource supposedSource) {
		this.graph = graph;
		this.observers = observers;
		this.observersID = new ArrayList<Integer>();
		for(Node obs : observers) observersID.add(obs.getId());
		this.reference = observers.get(0);
		this.relativeDelays = relativeDelays;
		this.delayMean = delayMean;
		this.delayVariance = delayVariance;
		this.supposedSource = supposedSource;
	}
	
	/***
	 * Run method
	 */
	
	public void run() {
		supposedSource.setScore(computeScore(supposedSource.getNode()));
	}
		
	/***
	 * Private methods
	 */
	/*	
	private double computeScore(int nodeID) {
		if(observersID.indexOf(nodeID)>0) return Double.NEGATIVE_INFINITY;
		Graph tree = GraphTools.findBFSTree(graph, nodeID, observersID);
		if(tree==null) return Double.NEGATIVE_INFINITY;
		DoubleMatrix deterministicDelays = computeDeterministicDelays(tree, nodeID);
		DoubleMatrix delayCovariance = computeDelayCovariance(tree);
		DoubleMatrix rmd = relativeDelays.div(delayMean).sub(deterministicDelays);
		DoubleMatrix rmdt = rmd.transpose();
		DoubleMatrix dci = Solve.solve(delayCovariance, DoubleMatrix.eye(observers.size()-1));
		double exponent = -0.5*(delayMean*delayMean/delayVariance)*rmdt.mmul(dci).mmul(rmd).get(0);
		double logdet = Math.log(computeDeterminant(delayCovariance));
		return -0.5*logdet+exponent;
	}
	*/
	
	private double computeScore(Node node) {
		if(observers.indexOf(node)>0) return Double.NEGATIVE_INFINITY;
		ArrayList<Path> paths = PathsTools.shortestPaths(graph, node, observers);
		DoubleMatrix deterministicDelays = computeDeterministicDelays(paths);
		DoubleMatrix delayCovariance = computeDelayCovariance(paths);
		DoubleMatrix rmd = relativeDelays.sub(deterministicDelays);
		DoubleMatrix rmdt = rmd.transpose();
		DoubleMatrix dci = Solve.solve(delayCovariance, DoubleMatrix.eye(observers.size()-1));
		double exponent = -0.5*rmdt.mmul(dci).mmul(rmd).get(0);
		double logdet = -0.5*Math.log(computeDeterminant(delayCovariance));
		return logdet+exponent;
	}
	
	private double computeDeterminant(DoubleMatrix a) {
		LUDecomposition<DoubleMatrix> lu = Decompose.lu(a);
		return Math.abs(lu.u.diag().prod());
	}
		
	private DoubleMatrix computeDeterministicDelays(ArrayList<Path> paths) {
		DoubleMatrix detDel = new DoubleMatrix(observers.size()-1);
		int sourceToReferenceDistance = paths.get(0).getLength();
		for(int i=0; i<observers.size()-1; i++) detDel.put(i, paths.get(i+1).getLength()-sourceToReferenceDistance);
		return detDel.mul(delayMean);
	}
	
	private DoubleMatrix computeDelayCovariance(ArrayList<Path> paths) {
		int dim = observers.size()-1;
		int length;
		// The list of all paths between reference observer and other observers
		ArrayList<Path> refToObs = findPathsFromReferenceToObservers(paths);
		DoubleMatrix delCov = new DoubleMatrix(dim, dim);
		// First fill diagonal elements
		for(int i=0; i<dim; i++) delCov.put(i,i,refToObs.get(i).getLength());
		// Next fill the rest of matrix
		for(int i=0; i<dim-1; i++) {
			for(int j=i+1; j<dim; j++) {
				length = refToObs.get(i).lengthOfOverlappingLinksFromStart(refToObs.get(j));
				delCov.put(i,j,length);
				delCov.put(j,i,length);
			}
		}
		return delCov.mul(delayVariance);
	}
	
	private ArrayList<Path> findPathsFromReferenceToObservers(ArrayList<Path> paths) {
		ArrayList<Path> refToObs = new ArrayList<Path>();
		Path refToSource = PathsTools.reverse(paths.get(0));
		for(int i=0; i<observers.size()-1; i++) {
			refToObs.add(PathsTools.mergeWithoutOverlap(refToSource, paths.get(i+1)));
		}
		return refToObs;
	}
}
