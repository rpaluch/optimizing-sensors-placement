package tracking;

import java.util.ArrayList;
import java.util.LinkedList;

import org.ejml.simple.SimpleMatrix;

import graphs.Graph;
import graphs.GraphTools;
import graphs.Link;
import graphs.Node;
import graphs.PathsTools;

/***
 * Basic implementation of Pinto algorithm for searching the sources of diffusion processes.
 * @author rpaluch
 */

public class SingleScoreEstimator implements Runnable {

	/***
	 * Data fields
	 */
	
	Graph graph;
	// The list of observers 
	ArrayList<Node> observers;
	// The reference node is the last observer on the list of the observers
	Node reference;
	// The difference in delays between the reference node and other observers 
	SimpleMatrix relativeDelays;
	// The mean delay (greek mu)
	double delayMean;
	// The variance of delay (greek sigma square)
	double delayVariance;
	// The list of nodes which will be examined by Pinto algorithm 
	SupposedSource source;
	
	/***
	 * Constructors
	 */
		
	public SingleScoreEstimator(Graph graph, ArrayList<Node> observers, SimpleMatrix relativeDelays, double delayMean, double delayVariance, SupposedSource source) {
		this.graph = graph;
		this.observers = observers;
		this.reference = observers.get(0);
		this.relativeDelays = relativeDelays;
		this.delayMean = delayMean;
		this.delayVariance = delayVariance;
		this.source = source;
	}
	
	/***
	 * Run method
	 */
	
	public void run() {
		source.setScore(estimateScoreForNode(source.getNode().getId()));
	}
		
	/***
	 * Other methods
	 */
		
	private double estimateScoreForNode(int nodeID) {
		Graph tree = GraphTools.findBFSTree(graph, nodeID);
		SimpleMatrix deterministicDelays = computeDeterministicDelays(tree, nodeID);
		SimpleMatrix delayCovariance = computeDelayCovariance(tree);
		SimpleMatrix rmd = relativeDelays.minus(deterministicDelays);
		SimpleMatrix rmdt = rmd.transpose();
		SimpleMatrix dci = delayCovariance.invert();
		double exponent = rmdt.mult(dci).mult(rmd).get(0);
		double factor = Math.sqrt(delayCovariance.determinant());
		return Math.exp(-0.5*exponent)/factor;
	}
	
	private int lengthOfCommonPart(LinkedList<Node> path1, LinkedList<Node> path2) {
		int length = 0;
		int maxLength = Math.min(path1.size(), path2.size())-1;
		Link link1 = new Link(path1.get(0), path1.get(1));
		Link link2 = new Link(path2.get(0), path2.get(1));
		while(link1.equals(link2)) {
			length = length + 1;
			if(length+1>maxLength) break;
			link1.setStart(path1.get(length));
			link1.setEnd(path1.get(length+1));
			link2.setStart(path2.get(length));
			link2.setEnd(path2.get(length+1));
		}
		return length;
	}
		
	private SimpleMatrix computeDeterministicDelays(Graph tree, int sourceID) {
		SimpleMatrix detDel = new SimpleMatrix(observers.size()-1,1);
		int sourceToReferenceDistance = GraphTools.shortestPathLength(tree, sourceID, reference.getId());
		for(int i=0; i<observers.size()-1; i++) detDel.set(i, 0, GraphTools.shortestPathLength(tree, sourceID, observers.get(i+1).getId())-sourceToReferenceDistance);
		return detDel.scale(delayMean);
	}
	
	private SimpleMatrix computeDelayCovariance(Graph tree) {
		int dim = observers.size()-1;
		int commonLength;
		LinkedList<Node> path1,path2;
		SimpleMatrix delCov = new SimpleMatrix(dim, dim);
		// First fill diagonal elements
		for(int i=0; i<dim; i++) delCov.set(i,i,GraphTools.shortestPathLength(tree, observers.get(i+1).getId(), reference.getId()));
		// Next fill the rest of matrix
		for(int i=0; i<dim-1; i++) {
			for(int j=i+1; j<dim; j++) {
				path1 = PathsTools.shortestPath(tree, reference.getId(), observers.get(i+1).getId());
				path2 = PathsTools.shortestPath(tree, reference.getId(), observers.get(j+1).getId());
				commonLength = lengthOfCommonPart(path1,path2);
				delCov.set(i,j,commonLength);
				delCov.set(j,i,commonLength);
			}
		}
		return delCov.scale(delayVariance);
	}
				
}
