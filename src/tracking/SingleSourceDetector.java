package tracking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jblas.DoubleMatrix;
import org.jblas.Solve;

import graphs.Graph;
import graphs.GraphTools;
import graphs.Node;
import graphs.Path;
import graphs.PathsTools;

/***
 * The implementation of Gradient Maximum Likelihood Algorithm for searching the sources of diffusion processes with infection vectors.
 * @author rpaluch
 */

public class SingleSourceDetector {

	/***
	 * Data fields
	 */
	
	
	Graph graph; //Original graph
	ArrayList<Node> observers; //The list of observers 
	ArrayList<Double> delays; //The time after which the signal arrives to given observer
	ArrayList<Node> infectionVectors; //The infection vectors - the nodes which infected the observers
	ArrayList<Node> nearestObservers; //The list of nearest observers 
	ArrayList<Double> nearestDelays; //The list of delays for nearest observers
	ArrayList<Node> nearestVectors; //The list of infection vectors for nearest observers
	DoubleMatrix relativeDelays; //The difference in delays between the reference node and other observers 
	double delayMean; //The mean delay per link
	double delayVariance; //The variance of delay per link
	ArrayList<SupposedSource> supposedSources; //The list of nodes for which the score will be or was computed
	long startTime, stopTime; // Variables for time storing
	int numberOfNearestObservers; // The number of nearest observers
	int numberOfActiveObservers; // The number of active observers for LPTVA-PO algorithm
	double maxScore; // The maximum score
	int numberOfGradientWalks; // The number of gradients walks, each starts in different observer
	int maxThreads; // The maximum number of threads
	
	/***
	 * Constructors
	 */
	
	public SingleSourceDetector(Graph graph) {
		this.graph = graph;
		this.observers = new ArrayList<Node>();
		this.delays = new ArrayList<Double>();
		this.infectionVectors = new ArrayList<Node>();
		this.delayMean = 1.0;
		this.delayVariance = 0.5;
		this.supposedSources = new ArrayList<SupposedSource>();
		this.numberOfNearestObservers = 4;
		this.numberOfActiveObservers = 3;
		this.maxScore = Double.NEGATIVE_INFINITY;
		this.numberOfGradientWalks = 0;
		this.maxThreads = 10;
	}
	
	public SingleSourceDetector(Graph graph, ArrayList<Node> observers, ArrayList<Double> delays, double delayMean, double delayVariance) {
		this.graph = graph;
		this.observers = observers;
		this.delays = delays;
		this.infectionVectors = observers; //infectionVectors must not be empty
		this.delayMean = delayMean;
		this.delayVariance = delayVariance;
		this.supposedSources = new ArrayList<SupposedSource>();
		this.numberOfNearestObservers = observers.size();
		this.numberOfActiveObservers = observers.size()-1;
		this.maxScore = Double.NEGATIVE_INFINITY;
		this.numberOfGradientWalks = 0;
		this.maxThreads = 10;
	}
	
	/***
	 * Public methods
	 */
		
	public void computeScores(String estimatorAcronim) {
		
		startTime = System.currentTimeMillis();
		findNearestObservers(numberOfNearestObservers);
		relativeDelays = computeRelativeDelays();
		
		ArrayList<SupposedSource> temporalSupposedSources;
		ArrayList<Integer> checkedNodesId = null;
		SupposedSource highestScore = null;
		int counter = 0;
		
		// numberOfGradientWalks==0 means that no gradient should be apply and scores for all supposedSources should be computed
		if(numberOfGradientWalks==0) {
			highestScore = findHighestScore(supposedSources, estimatorAcronim);
		}
		else {
			checkedNodesId = new ArrayList<Integer>();
			highestScore = new SupposedSource(nearestObservers.get(counter), Double.NEGATIVE_INFINITY);
		}
		// This while loop if for gradient walk and it starts only if numberOfGradientWalks>0
		while(counter<numberOfGradientWalks) {
			maxScore = highestScore.getScore();
			temporalSupposedSources = new ArrayList<SupposedSource>();
			for(Node n : highestScore.getNode().getNeighbours()) {
				if(!checkedNodesId.contains(n.getId())) {
				temporalSupposedSources.add(new SupposedSource(n, 0.0));
				checkedNodesId.add(n.getId());
				}
			}
			if(temporalSupposedSources.size()>0) {
				highestScore = findHighestScore(temporalSupposedSources, estimatorAcronim);
				supposedSources.addAll(temporalSupposedSources);
			}
			if(temporalSupposedSources.size()==0 || highestScore.getScore()<maxScore) {
				counter = counter + 1;
				highestScore = new SupposedSource(nearestObservers.get(counter), 0.0);
			}
		}
		stopTime = System.currentTimeMillis();
		Collections.sort(supposedSources, SupposedSource.scoresDescending);
	}
	
	// Returns the list of nodes with the highest score. Applicable only after using method computeScores()
	public ArrayList<Integer> findNodesWithHighestScores() {
		// Prepare the list
		ArrayList<Integer> nodesID = new ArrayList<Integer>(); 
		// Find maximum value of score
		double max = supposedSources.get(0).getScore();
		// Find the nodes with maximum score
		int i=0;
		while(i<supposedSources.size() && supposedSources.get(i).getScore()==max) {
			nodesID.add(supposedSources.get(i).getNode().getId());
			i = i + 1;
		}
		return nodesID;
	}
	
	// Returns the number of nodes which have greater than or equal score as a given node
	public int findRankOfNode(int nodeID) {
		double ascore = Double.NaN;
		int rank = 0;
		for(SupposedSource source : supposedSources) if(source.getNode().getId()==nodeID) ascore = source.getScore();
		if(ascore == Double.NaN) return graph.getNumberOfNodes();
		for(SupposedSource source : supposedSources) {
			if(source.getScore()<ascore) break;
			else rank = rank + 1;
		}		
		return rank;
	}
	
	/***
	 * Private methods
	 */
	
	// Sort the observers ascending in terms of delay. Add the nearest to the list.
	private void findNearestObservers(int number) {
		ArrayList<Observer> observersList = new ArrayList<Observer>();
		for(int i=0; i<observers.size(); i++) observersList.add(new Observer(observers.get(i), delays.get(i), infectionVectors.get(i)));
		Collections.sort(observersList, Observer.delaysAscending);
		nearestObservers = new ArrayList<Node>();
		nearestDelays = new ArrayList<Double>();
		nearestVectors = new ArrayList<Node>();
		for(int i=0; i<number; i++) {
			nearestObservers.add(observersList.get(i).getNode());
			nearestDelays.add(observersList.get(i).getDelay());
			nearestVectors.add(observersList.get(i).getInfectionVector());
		}
	}
	
	// Takes the nearest observer as a reference and subtracts its delay from the delays of the rest of observers.
	private DoubleMatrix computeRelativeDelays() {
		DoubleMatrix relDel = new DoubleMatrix(nearestObservers.size()-1);
		for(int i=0; i<nearestObservers.size()-1; i++) relDel.put(i, nearestDelays.get(i+1)-nearestDelays.get(0));
		return relDel;
	}
	
	// Computes the scores for a list of supposed sources and returns the source with the highest score.
	private SupposedSource findHighestScore(ArrayList<SupposedSource> supposedSourcesList, String estimatorAcronim) {
		
		ExecutorService executor = Executors.newFixedThreadPool(maxThreads);
		Runnable[] estimators;
		DoubleMatrix inverseDelayCovariance = null;
		
		switch(estimatorAcronim) {
		
			case "LPTV": estimators = new ScoreEstimatorLPTV[supposedSourcesList.size()];
						 for(int i=0; i<supposedSourcesList.size(); i++) {
							 estimators[i] = new ScoreEstimatorLPTV(graph, nearestObservers, relativeDelays, delayMean, delayVariance, supposedSourcesList.get(i));
							 executor.execute(estimators[i]);
						 }
						 executor.shutdown();
						 break;
						 
			case "LPTV2": estimators = new ScoreEstimatorLPTV2[supposedSourcesList.size()];
			 			  for(int i=0; i<supposedSourcesList.size(); i++) {
							  estimators[i] = new ScoreEstimatorLPTV2(graph, nearestObservers, relativeDelays, delayMean, delayVariance, supposedSourcesList.get(i));
							  executor.execute(estimators[i]);
						  }
						  executor.shutdown();
						  break;
						 
			case "PTV":  estimators = new ScoreEstimatorPTV[supposedSourcesList.size()];
			 			 for(int i=0; i<supposedSourcesList.size(); i++) {
			 				 estimators[i] = new ScoreEstimatorPTV(graph, nearestObservers, infectionVectors, relativeDelays, delayMean, delayVariance, supposedSourcesList.get(i));
			 				 executor.execute(estimators[i]);
			 			 }
			 			 executor.shutdown();
			 			 break;
			 			 
			case "EPP":  estimators = new ScoreEstimatorEPP[supposedSourcesList.size()];
						 inverseDelayCovariance = Solve.solve(computeDelayCovarianceEPP(), DoubleMatrix.eye(nearestObservers.size()-1));
			 			 for(int i=0; i<supposedSourcesList.size(); i++) {
			 				 estimators[i] = new ScoreEstimatorEPP(graph, nearestObservers, relativeDelays, delayMean, delayVariance,
			 						 							   inverseDelayCovariance, supposedSourcesList.get(i));
			 				 executor.execute(estimators[i]);
			 			 }
			 			 executor.shutdown();
			 			 break;
			 			 
			case "EPL":  estimators = new ScoreEstimatorEPP[supposedSourcesList.size()];
			 			 inverseDelayCovariance = Solve.solve(computeDelayCovarianceEPL(), DoubleMatrix.eye(nearestObservers.size()-1));
			 			 for(int i=0; i<supposedSourcesList.size(); i++) {
			 				estimators[i] = new ScoreEstimatorEPP(graph, nearestObservers, relativeDelays, delayMean, delayVariance,
		 							   							  inverseDelayCovariance, supposedSourcesList.get(i));
			 				executor.execute(estimators[i]);
			 			 }
			 			 executor.shutdown();
			 			 break;
			 			 						 
			default:	 estimators = new ScoreEstimatorLPTV[supposedSourcesList.size()];
			 			 for(int i=0; i<supposedSourcesList.size(); i++) {
			 				 estimators[i] = new ScoreEstimatorLPTV(graph, nearestObservers, relativeDelays, delayMean, delayVariance, supposedSourcesList.get(i));
			 				 executor.execute(estimators[i]);
			 			 }
			 			 executor.shutdown();
			 			 break;
		}
		
		while(!executor.isTerminated()) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Collections.sort(supposedSourcesList, SupposedSource.scoresDescending);
		return supposedSourcesList.get(0);
	}
	
	private DoubleMatrix computeDelayCovarianceEPP() {
		int dim = nearestObservers.size()-1;
		ArrayList<Integer> nearestObserversID = GraphTools.nodesToIDs(nearestObservers);
		// The list of all paths between reference observer and other observers
		ArrayList<ArrayList<Path>> pathsFromReferenceToObservers = PathsTools.allShortestPaths(graph, nearestObserversID.get(0), nearestObserversID);
		DoubleMatrix delCov = new DoubleMatrix(dim, dim);
		DoubleMatrix corrCoeff = computeCorrelationCoefficients(pathsFromReferenceToObservers);
		double obsCorr;
		// First fill diagonal elements
		for(int i=0; i<dim; i++)
			delCov.put(i,i,delayVariance*pathsFromReferenceToObservers.get(i+1).get(0).getLength()-corrCoeff.get(i));
		// Next fill the rest of matrix
		for(int i=0; i<dim-1; i++) {
			for(int j=i+1; j<dim; j++) {
				ArrayList<Path> paths1 = pathsFromReferenceToObservers.get(i+1);
				ArrayList<Path> paths2 = pathsFromReferenceToObservers.get(j+1);
				obsCorr = delayVariance*PathsTools.pathsOverlapEPP(paths1, paths2);
				delCov.put(i,j,obsCorr);
				delCov.put(j,i,obsCorr);
			}
		}
		return delCov;
	}
	
	private DoubleMatrix computeDelayCovarianceEPL() {
		int dim = nearestObservers.size()-1;
		ArrayList<Integer> nearestObserversID = GraphTools.nodesToIDs(nearestObservers);
		// The list of all paths between reference observer and other observers
		ArrayList<ArrayList<Path>> pathsFromReferenceToObservers = PathsTools.allShortestPaths(graph, nearestObserversID.get(0), nearestObserversID);
		DoubleMatrix delCov = new DoubleMatrix(dim, dim);
		DoubleMatrix corrCoeff = computeCorrelationCoefficients(pathsFromReferenceToObservers);
		double obsCorr;
		// First fill diagonal elements
		for(int i=0; i<dim; i++)
			delCov.put(i,i,delayVariance*pathsFromReferenceToObservers.get(i+1).get(0).getLength()-corrCoeff.get(i));
		// Next fill the rest of matrix
		for(int i=0; i<dim-1; i++) {
			for(int j=i+1; j<dim; j++) {
				ArrayList<Path> paths1 = pathsFromReferenceToObservers.get(i+1);
				ArrayList<Path> paths2 = pathsFromReferenceToObservers.get(j+1);
				obsCorr = Math.sqrt(delCov.get(i,i)*delCov.get(j,j))*PathsTools.pathsOverlapEPL(paths1, paths2);
				delCov.put(i,j,obsCorr);
				delCov.put(j,i,obsCorr);
			}
		}
		return delCov;
	}
	
	private DoubleMatrix computeCorrelationCoefficients(ArrayList<ArrayList<Path>> pathsFromReferenceToObservers) {
		DoubleMatrix corrCoeff = new DoubleMatrix(nearestObservers.size()-1);
		double C = 0.5641895835; // phi(0)*sqrt(2), where phi is the probability d.f. of the standard normal distribution
		// The vector corrCoeff stores the values of 1-rho instead of rho
		for(int i=0; i<nearestObservers.size()-1; i++) {
			if(pathsFromReferenceToObservers.get(i+1).size()>1)
				corrCoeff.put(i, 1-findLeastCorrelation(pathsFromReferenceToObservers.get(i+1)));
			else
				corrCoeff.put(i, 0);
		}
		// For further computation we need term delayVariance*(1-rho)*C^2
		return corrCoeff.mmul(C*C*delayVariance);
	}
	
	private double findLeastCorrelation(ArrayList<Path> paths) {
		double least = 1;
		double cor = 0;
		for(int i=0; i<paths.size()-1; i++)
			for(int j=i+1; j<paths.size(); j++) {
				cor = PathsTools.twoPathsCorrelation(paths.get(i), paths.get(j));
				if(cor<least) least=cor;
			}
		return least;
	}
					
	/***
	 * Getters and setters
	 */

	public Graph getGraph() {
		return graph;
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
	}

	public ArrayList<Node> getObservers() {
		return observers;
	}

	public void setObservers(ArrayList<Node> observers) {
		this.observers = observers;
		this.infectionVectors = observers; //infectionVectors must not be empty
		this.numberOfNearestObservers = observers.size(); //default value of nearest observers
	}

	public ArrayList<Double> getDelays() {
		return delays;
	}

	public void setDelays(ArrayList<Double> delays) {
		this.delays = delays;
	}
	
	public void setObserversAndDelays(ArrayList<Observer> observers) {
		this.observers = new ArrayList<Node>();
		this.infectionVectors = new ArrayList<Node>();
		this.delays = new ArrayList<Double>();
		for(Observer obs : observers) {
			this.observers.add(obs.getNode());
			this.infectionVectors.add(obs.getInfectionVector());
			this.delays.add(obs.getDelay());
		}
		this.numberOfNearestObservers = observers.size();
	}
	
	public ArrayList<Node> getInfectionVectors() {
		return infectionVectors;
	}

	public void setInfectionVectors(ArrayList<Node> vectors) {
		this.infectionVectors = vectors;
	}

	public double getDelayMean() {
		return delayMean;
	}

	public void setDelayMean(double delayMean) {
		this.delayMean = delayMean;
	}

	public double getDelayVariance() {
		return delayVariance;
	}

	public void setDelayVariance(double delayVariance) {
		this.delayVariance = delayVariance;
	}	
	
	public long getStartTime() {
		return startTime;
	}

	public long getStopTime() {
		return stopTime;
	}
	
	public long getComputingTime() {
		return (stopTime-startTime);
	}
	
	public int getNumberOfNearestObservers() {
		return numberOfNearestObservers;
	}

	public void setNumberOfNearestObservers(int numberOfNearestObservers) {
		this.numberOfNearestObservers = numberOfNearestObservers;
	}
	
	public void setNumberOfActiveObservers(int numberOfActiveObservers) {
		this.numberOfActiveObservers = numberOfActiveObservers;
	}
	
	public void setNumberOfGradientWalks(int numberOfGradientWalks) {
		this.numberOfGradientWalks = numberOfGradientWalks;
	}
		
	public ArrayList<SupposedSource> getSupposedSources() {
		return supposedSources;
	}
	
	public void setSupposedSources(ArrayList<Node> supposedSources) {
		this.supposedSources = new ArrayList<SupposedSource>();
		for(Node node : supposedSources) this.supposedSources.add(new SupposedSource(node, Double.NaN));
	}
	
	public ArrayList<Node> getNearestObservers() {
		return nearestObservers;
	}
	
	public void setMaxThreads(int max) {
		this.maxThreads = max;
	}	
}
