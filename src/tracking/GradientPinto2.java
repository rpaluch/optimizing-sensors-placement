package tracking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jblas.DoubleMatrix;

import graphs.Graph;
import graphs.Node;

/***
 * Basic implementation of Pinto algorithm for searching the sources of diffusion processes.
 * @author rpaluch
 */

public class GradientPinto2 {

	/***
	 * Data fields
	 */
	
	// Original graph
	Graph graph;
	// The list of observers 
	ArrayList<Node> observers;
	// The time after which the signal arrives to given observer
	ArrayList<Double> delays;
	// The list of nearest observers 
	ArrayList<Node> nearestObservers;
	// The list of delays for nearest observers
	ArrayList<Double> nearestDelays;
	// The difference in delays between the reference node and other observers 
	DoubleMatrix relativeDelays;
	// The mean delay (greek mu)
	double delayMean;
	// The variance of delay (greek sigma square)
	double delayVariance;
	// The list of nodes which will be examined by Pinto algorithm 
	ArrayList<SupposedSource> supposedSources;
	// Variables for time storing
	long startTime, stopTime;
	long cumulativeTreeTime, cumulativeMatrixTime;
	// The number of nearest observers
	int numberOfNearestObservers;
	// The maximum score
	double maxScore;
	// The number of initial nodes for searching maximum score
	int initialNodes;
	int jumps;
	// The maximum number of threads
	int maxThreads;
	// The accumulated tree size
	int cumulativeTreeSize;
	// The accumulated tree height
	int cumulativeTreeHeight;
	// The offset controls which nearest observers the algorithm selects
	int offset;
	
	/***
	 * Constructors
	 */
	
	public GradientPinto2(Graph graph) {
		this.graph = graph;
		this.supposedSources = new ArrayList<SupposedSource>();
		this.maxScore = Double.NEGATIVE_INFINITY;
		this.initialNodes = 1;
		this.maxThreads = 10;
		this.cumulativeTreeTime = 0;
		this.cumulativeMatrixTime = 0;
		this.cumulativeTreeSize = 0;
		this.cumulativeTreeHeight = 0;
		this.offset = 0;
	}
	
	public GradientPinto2(Graph graph, ArrayList<Node> observers, ArrayList<Double> delays, double delayMean, double delayVariance, int numberOfNearestObservers, int initialNodes) {
		this.graph = graph;
		this.observers = observers;
		this.delays = delays;
		this.delayMean = delayMean;
		this.delayVariance = delayVariance;
		this.supposedSources = new ArrayList<SupposedSource>();
		this.numberOfNearestObservers = numberOfNearestObservers;
		this.maxScore = Double.NEGATIVE_INFINITY;
		this.initialNodes = initialNodes;
		this.maxThreads = 10;
		this.cumulativeTreeTime = 0;
		this.cumulativeMatrixTime = 0;
		this.cumulativeTreeSize = 0;
		this.cumulativeTreeHeight = 0;
		this.offset = 0;
	}
	
	/***
	 * Getters and setters
	 */

	public Graph getGraph() {
		return graph;
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
	}

	public ArrayList<Node> getObservers() {
		return observers;
	}

	public void setObservers(ArrayList<Node> observers) {
		this.observers = observers;
		this.numberOfNearestObservers = observers.size()-1;
	}
	
	public void setObserversAndDelays(ArrayList<Observer> observers) {
		this.observers = new ArrayList<Node>();
		this.delays = new ArrayList<Double>();
		for(Observer obs : observers) {
			this.observers.add(obs.getNode());
			this.delays.add(obs.getDelay());
		}
		this.numberOfNearestObservers = this.observers.size()-1;
	}

	public ArrayList<Double> getDelays() {
		return delays;
	}

	public void setDelays(ArrayList<Double> delays) {
		this.delays = delays;
	}

	public double getDelayMean() {
		return delayMean;
	}

	public void setDelayMean(double delayMean) {
		this.delayMean = delayMean;
	}

	public double getDelayVariance() {
		return delayVariance;
	}

	public void setDelayVariance(double delayVariance) {
		this.delayVariance = delayVariance;
	}	
	
	public long getStartTime() {
		return startTime;
	}

	public long getStopTime() {
		return stopTime;
	}
	
	public long getComputingTime() {
		return (stopTime-startTime);
	}
	
	public double getTreeTimePerNode() {
		return 0.001*cumulativeTreeTime/supposedSources.size();
	}
	
	public double getMatrixTimePerNode() {
		return 0.001*cumulativeMatrixTime/supposedSources.size();
	}
	
	public int getNumberOfNearestObservers() {
		return numberOfNearestObservers;
	}

	public void setNumberOfNearestObservers(int numberOfNearestObservers) {
		this.numberOfNearestObservers = numberOfNearestObservers;
	}
	
	public double getInitialNodes() {
		return initialNodes;
	}

	public void setInitialNodes(int initialNodes) {
		this.initialNodes = initialNodes;
	}
	
	public int getJumps() {
		return jumps;
	}
	
	public ArrayList<SupposedSource> getSupposedSources() {
		return supposedSources;
	}
	
	public ArrayList<Node> getNearestObservers() {
		return nearestObservers;
	}
	
	public void setMaxThreads(int max) {
		this.maxThreads = max;
	}
	
	public double getAverageTreeSize() {
		return 1.0*cumulativeTreeSize/supposedSources.size();
	}
	
	public double getAverageTreeHeight() {
		return 1.0*cumulativeTreeHeight/supposedSources.size();
	}
	
	public void setOffset(int offset) {
		this.offset = offset;
	}

	/***
	 * Other methods
	 */
	
	// Sort the observers ascending in terms of delay. Add the nearest to the list.
	private void findNearestObservers(int number) {
		ArrayList<Observer> observersList = new ArrayList<Observer>();
		for(int i=0; i<observers.size(); i++) observersList.add(new Observer(observers.get(i), delays.get(i)));
		Collections.sort(observersList, Observer.delaysAscending);
		nearestObservers = new ArrayList<Node>();
		nearestDelays = new ArrayList<Double>();
		for(int i=0; i<number; i++) {
			nearestObservers.add(observersList.get(i+offset).getNode());
			nearestDelays.add(observersList.get(i+offset).getDelay());
		}
	}
	
	// Takes the nearest observer as a reference and subtracts its delay from the delays of the rest of observers.
	private DoubleMatrix computeRelativeDelays() {
		DoubleMatrix relDel = new DoubleMatrix(nearestObservers.size()-1);
		for(int i=0; i<nearestObservers.size()-1; i++) relDel.put(i, nearestDelays.get(i+1)-nearestDelays.get(0));
		return relDel;
	}
		
	public void estimateScoresForNodes() {
		
		startTime = System.currentTimeMillis();
		findNearestObservers(numberOfNearestObservers);
		relativeDelays = computeRelativeDelays();
		//System.out.printf("Nearest observers:\n");
		//for(Node n : nearestObservers) {
			//System.out.printf("%d, %d\n", n.getId(), n.getState());
		//}
		
		ArrayList<SupposedSource> temporalSupposedSources;
		ArrayList<Integer> checkedNodesId = new ArrayList<Integer>();
		int counter = 0;
		jumps = 0;
		SupposedSource highestScore = new SupposedSource(nearestObservers.get(counter), Double.NEGATIVE_INFINITY);
		
		while(counter<initialNodes) {
			maxScore = highestScore.getScore();
			jumps = jumps + 1;
			temporalSupposedSources = new ArrayList<SupposedSource>();
			//System.out.println("Neighbours:");
			for(Node n : highestScore.getNode().getNeighbours()) {
				//n.print();
				if(!checkedNodesId.contains(n.getId())) {
				temporalSupposedSources.add(new SupposedSource(n, 0.0));
				checkedNodesId.add(n.getId());
				}
			}
			//System.out.printf("id=%d, score=%e, degree=%d, to check: %d, runtime:%.2f\n", highestScore.getNode().getId(), highestScore.getScore(), 
					//highestScore.getNode().getDegree(), temporalSupposedSources.size(), 0.001*(System.currentTimeMillis()-startTime));
			if(temporalSupposedSources.size()>0) {
				//System.out.println("List:");
				//for(int z=0; z<temporalSupposedSources.size(); z++) temporalSupposedSources.get(z).getNode().print();
				highestScore = findHighestScore(temporalSupposedSources);
				supposedSources.addAll(temporalSupposedSources);
			}
			if(temporalSupposedSources.size()==0 || highestScore.getScore()<maxScore) {
				counter = counter + 1;
				highestScore = new SupposedSource(nearestObservers.get(counter), 0.0);
			}
		}
		stopTime = System.currentTimeMillis();
		Collections.sort(supposedSources, SupposedSource.scoresDescending);
	}
	
	// Computes the scores for a list of supposed sources and returns the source with the highest score.
	private SupposedSource findHighestScore(ArrayList<SupposedSource> supposedSourcesList) {
		
		int numberOfThreads = maxThreads;
		if(supposedSourcesList.size()<maxThreads) numberOfThreads = supposedSourcesList.size();
		ArrayList<ArrayList<SupposedSource>> partitionedList = partitionSupposedSources(supposedSourcesList, numberOfThreads);
		/*
		System.out.println("List of list:");
		for(int i=0; i<partitionedList.size(); i++) {
			for(int j=0; j<partitionedList.get(i).size(); j++) partitionedList.get(i).get(j).getNode().print();
			System.out.println();
		}
		*/	
		ScoreEstimator2[] estimators = new ScoreEstimator2[numberOfThreads];
		ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
		
		for(int i=0; i<numberOfThreads; i++) {
			estimators[i] = new ScoreEstimator2(graph, nearestObservers, relativeDelays, delayMean, delayVariance, partitionedList.get(i));
			executor.execute(estimators[i]);
		}
		executor.shutdown();
		
		while(!executor.isTerminated()) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		for(int i=0; i<numberOfThreads; i++) {
			cumulativeTreeTime = cumulativeTreeTime + estimators[i].getTreeTime();
			cumulativeMatrixTime = cumulativeMatrixTime + estimators[i].getMatrixTime();
			cumulativeTreeSize = cumulativeTreeSize + estimators[i].getTreeSize();
			cumulativeTreeHeight = cumulativeTreeHeight + estimators[i].getTreeHeight();
		}
		
		Collections.sort(supposedSourcesList, SupposedSource.scoresDescending);
		return supposedSourcesList.get(0);
	}
	
	private ArrayList<ArrayList<SupposedSource>> partitionSupposedSources(ArrayList<SupposedSource> supposedSourcesList, int numberOfThreads) {
		int nodesPerThread = (int) Math.ceil(1.0*supposedSourcesList.size()/numberOfThreads);
		ArrayList<ArrayList<SupposedSource>> partition = new ArrayList<ArrayList<SupposedSource>>();
		int i = 0;
		for(int p=0; p<numberOfThreads; p++) {
			partition.add(new ArrayList<SupposedSource>());
			while(i<Math.min(nodesPerThread*(p+1),supposedSourcesList.size())) {
				partition.get(p).add(supposedSourcesList.get(i));
				i = i + 1;
			}
		}
		return partition;
	}
				
	// Returns the list of nodes with the highest score.
	// Applicable only after using method estimateScoresForNodes()
	public ArrayList<Integer> findNodesWithHighestScores() {
		// Prepare the list
		ArrayList<Integer> nodesID = new ArrayList<Integer>(); 
		// Find maximum value of score
		double max = supposedSources.get(0).getScore();
		// Find the nodes with maximum score
		int i=0;
		while(i<supposedSources.size() && supposedSources.get(i).getScore()==max) {
			nodesID.add(supposedSources.get(i).getNode().getId());
			i = i + 1;
		}
		return nodesID;
	}
	
	// Returns the number of nodes which have greater than or equal score as a given node
	public int findRankOfNode(int nodeID) {
		
		double ascore = 0.0;
		int rank = 0;
		for(SupposedSource source : supposedSources) if(source.getNode().getId()==nodeID) ascore = source.getScore();
		if(ascore == 0.0) return graph.getNumberOfNodes();
		for(SupposedSource source : supposedSources) {
			if(source.getScore()<ascore) break;
			else rank = rank + 1;
		}		
		return rank;
	}
	
	public double findRelativeScoreOfNode(int nodeID) {
		double ascore = 0.0;
		double sum = 0.0;
		for(SupposedSource source : supposedSources) {
			sum = sum + source.getScore();
			if(source.getNode().getId()==nodeID) ascore = source.getScore();
		}
		return ascore/sum;
	}
	
}
