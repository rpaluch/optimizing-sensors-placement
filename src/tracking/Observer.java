package tracking;

import java.util.Comparator;

import graphs.Node;

/**
 * Basic class for observer for the Pinto algorithm. 
 * @author Robert Paluch
 */

public class Observer {
	
	/**
	 * Data fields
	 */
	
	Node node;
	double delay;
	int votes;
	double score;
	Node infectionVector;
	
	/**
	 * Constructors
	 */
	
	public Observer(Node node) {
		this.node = node;
		this.delay = -1.0;
		this.votes = 0;
		this.score = 0.0;
		this.infectionVector = null;
	}
	
	public Observer(Node node, double delay) {
		this.node = node;
		this.delay = delay;
		this.votes = 0;
		this.score = 0.0;
		this.infectionVector = null;
	}
	
	public Observer(Node node, int votes, double score) {
		this.node = node;
		this.delay = -1;
		this.votes = votes;
		this.score = score;
		this.infectionVector = null;
	}
		
	public Observer(Node node, double delay, Node vector) {
		this.node = node;
		this.delay = delay;
		this.votes = 0;
		this.score = 0.0;
		this.infectionVector = vector;
	}

	/**
	 * Getters and setters
	 */
	
	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	public double getDelay() {
		return delay;
	}

	public void setDelay(double delay) {
		this.delay = delay;
	}
	
	public int getVotes() {
		return votes;
	}

	public void setVotes(int votes) {
		this.votes = votes;
	}
	
	public double getScore() {
		return score;
	}
	
	public void setScore(double score) {
		this.score = score;
	}

	public Node getInfectionVector() {
		return infectionVector;
	}

	public void setInfectionVector(Node vector) {
		this.infectionVector = vector;
	}



	/**
	 * Comparators
	 */
	
	public static Comparator<Observer> delaysAscending = new Comparator<Observer>() {

		public int compare(Observer obs1, Observer obs2) {

		   Double delay1 = obs1.getDelay();
		   Double delay2 = obs2.getDelay();

		   /*For ascending order*/
		   return delay1.compareTo(delay2);
	   }
	};
	
	public static Comparator<Observer> delaysDescending = new Comparator<Observer>() {

		public int compare(Observer obs1, Observer obs2) {

		   Double delay1 = obs1.getDelay();
		   Double delay2 = obs2.getDelay();

		   /*For descending order*/
		   return delay2.compareTo(delay1);
	   }
	};
	
	public static Comparator<Observer> scoreAscending = new Comparator<Observer>() {

		public int compare(Observer obs1, Observer obs2) {

		   Double score1 = obs1.getScore();
		   Double score2 = obs2.getScore();

		   /*For ascending order*/
		   return score1.compareTo(score2);
	   }
	};
	
	public static Comparator<Observer> scoreDescending = new Comparator<Observer>() {

		public int compare(Observer obs1, Observer obs2) {

		   Double score1 = obs1.getScore();
		   Double score2 = obs2.getScore();

		   /*For descending order*/
		   return score2.compareTo(score1);
	   }
	};
	
	public static Comparator<Observer> votesAscending = new Comparator<Observer>() {

		public int compare(Observer obs1, Observer obs2) {

		   Integer votes1 = obs1.getVotes();
		   Integer votes2 = obs2.getVotes();

		   /*For ascending order*/
		   return votes1.compareTo(votes2);
	   }
	};
	
	public static Comparator<Observer> votesDescending = new Comparator<Observer>() {

		public int compare(Observer obs1, Observer obs2) {

			Integer votes1 = obs1.getVotes();
			   Integer votes2 = obs2.getVotes();

		   /*For descending order*/
		   return votes2.compareTo(votes1);
	   }
	};

}
