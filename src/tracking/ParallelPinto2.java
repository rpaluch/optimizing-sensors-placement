package tracking;

import graphs.Graph;
import graphs.Node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jblas.DoubleMatrix;

/***
 * Basic implementation of Pinto algorithm for searching the sources of diffusion processes.
 * @author rpaluch
 */

public class ParallelPinto2 {

	/***
	 * Data fields
	 */
	
	Graph graph;
	// The list of observers 
	ArrayList<Node> observers;
	// The time after which the signal arrives to given observer
	ArrayList<Double> delays;
	// The reference node is the last observer on the list of the observers
	Node reference;
	// The difference in delays between the reference node and other observers 
	DoubleMatrix relativeDelays;
	// The mean delay (greek mu)
	double delayMean;
	// The variance of delay (greek sigma square)
	double delayVariance;
	// The list of nodes which will be examined by Pinto algorithm 
	ArrayList<SupposedSource> supposedSources;
	// Variables for time storing
	long startTime, stopTime;
	long cumulativeTreeTime, cumulativeMatrixTime;
	// The number of threads
	int numberOfThreads;
	// The number of supposed sources per one thread
	int nodesPerThread;
	
	/***
	 * Constructors
	 */
	
	public ParallelPinto2(Graph graph) {
		this.graph = graph;
		this.numberOfThreads = 4;
		this.supposedSources = new ArrayList<SupposedSource>();
		this.cumulativeTreeTime = 0;
		this.cumulativeMatrixTime = 0;
	}
	
	public ParallelPinto2(Graph graph, ArrayList<Node> observers, ArrayList<Double> delays, double delayMean, double delayVariance, ArrayList<Node> supposedSources, int numberOfThreads) {
		this.graph = graph;
		this.observers = observers;
		this.delays = delays;
		this.reference = observers.get(0);
		this.delayMean = delayMean;
		this.delayVariance = delayVariance;
		this.supposedSources = new ArrayList<SupposedSource>();
		for(Node node : supposedSources) this.supposedSources.add(new SupposedSource(node, 0.0));
		this.numberOfThreads = numberOfThreads;
		this.cumulativeTreeTime = 0;
		this.cumulativeMatrixTime = 0;
	}
	
	/***
	 * Getters and setters
	 */

	public Graph getGraph() {
		return graph;
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
	}

	public ArrayList<Node> getObservers() {
		return observers;
	}

	public void setObservers(ArrayList<Node> observers) {
		this.observers = observers;
		this.reference = observers.get(0);
	}
	
	public void setObserversAndDelays(ArrayList<Observer> observers) {
		this.observers = new ArrayList<Node>();
		this.delays = new ArrayList<Double>();
		for(Observer obs : observers) {
			this.observers.add(obs.getNode());
			this.delays.add(obs.getDelay());
		}
		this.reference = this.observers.get(0);
	}

	public ArrayList<Double> getDelays() {
		return delays;
	}

	public void setDelays(ArrayList<Double> delays) {
		this.delays = delays;
	}

	public double getDelayMean() {
		return delayMean;
	}

	public void setDelayMean(double delayMean) {
		this.delayMean = delayMean;
	}

	public double getDelayVariance() {
		return delayVariance;
	}

	public void setDelayVariance(double delayVariance) {
		this.delayVariance = delayVariance;
	}	

	public ArrayList<SupposedSource> getSupposedSources() {
		return supposedSources;
	}

	public void setSupposedSources(ArrayList<Node> supposedSources) {
		this.supposedSources = new ArrayList<SupposedSource>();
		for(Node node : supposedSources) this.supposedSources.add(new SupposedSource(node, 0.0));
	}
	
	public long getStartTime() {
		return startTime;
	}

	public long getStopTime() {
		return stopTime;
	}
	
	public long getComputingTime() {
		return (stopTime-startTime);
	}
	
	public double getTreeTimePerNode() {
		return 0.001*cumulativeTreeTime/supposedSources.size();
	}
	
	public double getMatrixTimePerNode() {
		return 0.001*cumulativeMatrixTime/supposedSources.size();
	}
	
	public int getNumberOfThreads() {
		return numberOfThreads;
	}

	public void setNumberOfThreads(int numberOfThreads) {
		this.numberOfThreads = numberOfThreads;
	}

	/***
	 * Other methods
	 */
			
	private DoubleMatrix computeRelativeDelays() {
        DoubleMatrix relDel = new DoubleMatrix(observers.size()-1);
		for(int i=0; i<observers.size()-1; i++) relDel.put(i, delays.get(i+1)-delays.get(0));
		return relDel;
	}
		
	public void estimateScoresForNodes() {
		
		startTime = System.currentTimeMillis();
		relativeDelays = computeRelativeDelays();
		nodesPerThread = supposedSources.size()/numberOfThreads;
		if(numberOfThreads*nodesPerThread<supposedSources.size()) nodesPerThread = nodesPerThread + 1;
		
		ArrayList<ArrayList<SupposedSource>> supposedSourcesList = partitionSupposedSources();
		ScoreEstimator2[] estimators = new ScoreEstimator2[numberOfThreads];
		ExecutorService executor = Executors.newFixedThreadPool(numberOfThreads);
		
		for(int i=0; i<supposedSourcesList.size(); i++) {
			estimators[i] = new ScoreEstimator2(graph, observers, relativeDelays, delayMean, delayVariance, supposedSourcesList.get(i));
			executor.execute(estimators[i]);
		}
		executor.shutdown();
		
		while(!executor.isTerminated()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		stopTime = System.currentTimeMillis();
		
		for(int i=0; i<numberOfThreads; i++) {
			cumulativeTreeTime = cumulativeTreeTime + estimators[i].getTreeTime();
			cumulativeMatrixTime = cumulativeMatrixTime + estimators[i].getMatrixTime();
		}
		
		Collections.sort(supposedSources, SupposedSource.scoresDescending);
	}
	
	private ArrayList<ArrayList<SupposedSource>> partitionSupposedSources() {
		ArrayList<ArrayList<SupposedSource>> partition = new ArrayList<ArrayList<SupposedSource>>();
		int i = 0;
		for(int p=0; p<numberOfThreads; p++) {
			partition.add(new ArrayList<SupposedSource>());
			while(i<Math.min(nodesPerThread*(p+1),supposedSources.size())) {
				partition.get(p).add(supposedSources.get(i));
				i = i + 1;
			}
		}
		return partition;
	}
			
	// Returns the list of nodes with the highest score.
	// Applicable only after using method estimateScoresForNodes()
	public ArrayList<Integer> findNodesWithHighestScores() {
		// Prepare the list
		ArrayList<Integer> nodesID = new ArrayList<Integer>(); 
		// Find maximum value of score
		double max = supposedSources.get(0).getScore();
		// Find the nodes with maximum score
		int i=0;
		while(supposedSources.get(i).getScore()==max) {
			nodesID.add(supposedSources.get(i).getNode().getId());
			i = i + 1;
			if(i==supposedSources.size()) break;
		}
		return nodesID;
	}
	
	// Returns the number of nodes which have greater than or equal score as a given node
	public int findRankOfNode(int nodeID) {
		
		double ascore = 0.0;
		int rank = 0;
		for(SupposedSource source : supposedSources) if(source.getNode().getId()==nodeID) ascore = source.getScore();
		for(SupposedSource source : supposedSources) {
			if(source.getScore()<ascore) break;
			else rank = rank + 1;
		}		
		return rank;
	}
	
	public double findRelativeScoreOfNode(int nodeID) {
		double ascore = 0.0;
		double sum = 0.0;
		for(SupposedSource source : supposedSources) {
			sum = sum + source.getScore();
			if(source.getNode().getId()==nodeID) ascore = source.getScore();
		}
		return ascore/sum;
	}
}
