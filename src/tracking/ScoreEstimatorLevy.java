package tracking;

import java.util.ArrayList;
import java.util.LinkedList;

import org.jblas.DoubleMatrix;

import graphs.Graph;
import graphs.GraphTools;
import graphs.Node;
import graphs.PathsTools;

/***
 * Basic implementation of Pinto algorithm for searching the sources of diffusion processes.
 * @author rpaluch
 */

public class ScoreEstimatorLevy implements Runnable {

	/***
	 * Data fields
	 */
	
	Graph graph;
	// The list of observers 
	ArrayList<Node> observers;
	// The list of observers ID 
	ArrayList<Integer> observersID;
	// The reference node is the last observer on the list of the observers
	Node reference;
	// The difference in delays between the reference node and other observers 
	DoubleMatrix relativeDelays;
	// The mean delay (greek mu)
	double locationParameter;
	// The variance of delay (greek sigma square)
	double scaleParameter;
	// The list of nodes which will be examined by Pinto algorithm 
	ArrayList<SupposedSource> supposedSources;
	// The accumulated time of creating a tree for one node
	long treeTime;
	// The accumulated time of matrix operation
	long matrixTime;
	// The accumulated size of BFS tree
	int treeSize;
	// The accumulated height of BFS tree
	int treeHeight;
	
	/***
	 * Constructors
	 */
		
	public ScoreEstimatorLevy(Graph graph, ArrayList<Node> observers, DoubleMatrix relativeDelays, double locationParameter, double scaleParameter, ArrayList<SupposedSource> supposedSources) {
		this.graph = graph;
		this.observers = observers;
		this.observersID = new ArrayList<Integer>();
		for(Node obs : observers) observersID.add(obs.getId());
		this.reference = observers.get(0);
		this.relativeDelays = relativeDelays;
		this.locationParameter = locationParameter;
		this.scaleParameter = scaleParameter;
		this.supposedSources = supposedSources;
		this.treeTime = 0;
		this.matrixTime = 0;
		this.treeSize = 0;
		this.treeSize = 0;
	}
	
	/***
	 * Run method
	 */
	
	public void run() {
		for(SupposedSource source : supposedSources) source.setScore(estimateScoreForNode(source.getNode().getId()));
	}
		
	/***
	 * Other methods
	 */
	
	private double estimateScoreForNode(int nodeID) {
		Graph tree = GraphTools.findBFSTree(graph, nodeID);
		DoubleMatrix relativeDistances = findRelativeDistances(tree, nodeID);
		DoubleMatrix observersToReferenceDistances = findObserversToReferenceDistances(tree);
		DoubleMatrix independentPDFs = new DoubleMatrix(observers.size()-1);
		double location = 0;
		double scale = 0;
		double logPDF = 0;
		for(int i=0; i<observers.size()-1; i++) {
			location = relativeDistances.get(i)*locationParameter;
			scale = observersToReferenceDistances.get(i)*observersToReferenceDistances.get(i)*scaleParameter;
			logPDF = Math.log(scale/Math.pow(relativeDelays.get(i)-location, 3))-scale/(relativeDelays.get(i)-location);
			//System.out.println(observersToReferenceDistances.get(i));
			independentPDFs.put(i, logPDF);
		}
		return independentPDFs.sum();
	}
	
	/*	
	private double estimateScoreForNode(int nodeID) {
		DoubleMatrix relativeDistances = findRelativeDistances(graph, nodeID);
		DoubleMatrix locationVector = relativeDelays.sub(relativeDistances.mul(locationParameter));
		DoubleMatrix lnLocationVector = computeLogarithm(locationVector);
		DoubleMatrix scaleVector = relativeDistances.mul(relativeDistances).mul(scaleParameter);
		DoubleMatrix lnScaleVector = computeLogarithm(scaleVector);
		DoubleMatrix scaleVectorDivLocationVector = scaleVector.div(locationVector);
		DoubleMatrix lnScaleVectorSubScaleVectorDivLocationVector = lnScaleVector.sub(scaleVectorDivLocationVector);
		System.out.println(locationVector.sum());
		return lnScaleVectorSubScaleVectorDivLocationVector.sum()-3*lnLocationVector.sum();
	}
	*/		
	private DoubleMatrix findRelativeDistances(Graph g, int sourceID) {
		DoubleMatrix relDist = new DoubleMatrix(observers.size()-1);
		ArrayList<LinkedList<Node>> allPaths = PathsTools.shortestPaths(g, sourceID, observersID);
		int sourceToReferenceDistance = allPaths.get(0).size()-1;
		for(int i=0; i<observers.size()-1; i++) relDist.put(i, allPaths.get(i+1).size()-1-sourceToReferenceDistance);
		return relDist;
	}
	
	private DoubleMatrix findObserversToReferenceDistances(Graph g) {
		DoubleMatrix otrd = new DoubleMatrix(observers.size()-1);
		ArrayList<LinkedList<Node>> allPaths = PathsTools.shortestPaths(g, reference.getId(), observersID);
		for(int i=0; i<observers.size()-1; i++) otrd.put(i, allPaths.get(i+1).size()-1);
		return otrd;
	}
		
	/*
	private DoubleMatrix computeLogarithm(DoubleMatrix vec) {
		int dim = vec.length;
		DoubleMatrix lnVec = new DoubleMatrix(dim);
		for(int i=0; i<dim; i++) lnVec.put(i,Math.log(vec.get(i)));
		return lnVec;
	}
	*/
	
	/***
	 * Getters
	 */
	
	public long getTreeTime() {
		return treeTime;
	}
	
	public long getMatrixTime() {
		return matrixTime;
	}
	
	public int getTreeSize() {
		return treeSize;
	}
	
	public int getTreeHeight() {
		return treeHeight;
	}
}
