package tracking;

import java.util.ArrayList;
import java.util.LinkedList;

import org.jblas.Decompose;
import org.jblas.Decompose.LUDecomposition;
import org.jblas.DoubleMatrix;
import org.jblas.Solve;

import graphs.Graph;
import graphs.GraphTools;
import graphs.Link;
import graphs.Node;
import graphs.PathsTools;

/***
 * An implementation of the limited (without infection vectors) Pinto-Thiran-Vetterli algorithm 
 * for searching the sources of diffusion processes.
 * @author rpaluch
 */

public class ScoreEstimatorLPTV implements Runnable {

	/***
	 * Data fields
	 */
	
	Graph graph;
	// The list of observers 
	ArrayList<Node> observers;
	// The list of observers ID 
	ArrayList<Integer> observersID;
	// The reference node is the last observer on the list of the observers
	Node reference;
	// The difference in delays between the reference node and other observers 
	DoubleMatrix relativeDelays;
	// The mean delay (greek mu)
	double delayMean;
	// The variance of delay (greek sigma square)
	double delayVariance;
	// The list of nodes which will be examined by Pinto algorithm 
	SupposedSource supposedSource;
	
	/***
	 * Constructors
	 */
		
	public ScoreEstimatorLPTV(Graph graph, ArrayList<Node> observers, DoubleMatrix relativeDelays,
			                  double delayMean, double delayVariance, SupposedSource supposedSource) {
		this.graph = graph;
		this.observers = observers;
		this.observersID = new ArrayList<Integer>();
		for(Node obs : observers) observersID.add(obs.getId());
		this.reference = observers.get(0);
		this.relativeDelays = relativeDelays;
		this.delayMean = delayMean;
		this.delayVariance = delayVariance;
		this.supposedSource = supposedSource;
	}
	
	/***
	 * Run method
	 */
	
	public void run() {
		supposedSource.setScore(computeScore(supposedSource.getNode().getId()));
	}
		
	/***
	 * Private methods
	 */
	/*	
	private double computeScore(int nodeID) {
		if(observersID.indexOf(nodeID)>0) return Double.NEGATIVE_INFINITY;
		Graph tree = GraphTools.findBFSTree(graph, nodeID, observersID);
		if(tree==null) return Double.NEGATIVE_INFINITY;
		DoubleMatrix deterministicDelays = computeDeterministicDelays(tree, nodeID);
		DoubleMatrix delayCovariance = computeDelayCovariance(tree);
		DoubleMatrix rmd = relativeDelays.div(delayMean).sub(deterministicDelays);
		DoubleMatrix rmdt = rmd.transpose();
		DoubleMatrix dci = Solve.solve(delayCovariance, DoubleMatrix.eye(observers.size()-1));
		double exponent = -0.5*(delayMean*delayMean/delayVariance)*rmdt.mmul(dci).mmul(rmd).get(0);
		double logdet = Math.log(computeDeterminant(delayCovariance));
		return -0.5*logdet+exponent;
	}
	*/
	
	private double computeScore(int nodeID) {
		if(observersID.indexOf(nodeID)>0) return Double.NEGATIVE_INFINITY;
		Graph tree = GraphTools.findBFSTree(graph, nodeID, observersID);
		if(tree==null) return Double.NEGATIVE_INFINITY;
		DoubleMatrix deterministicDelays = computeDeterministicDelays(tree, nodeID);
		DoubleMatrix delayCovariance = computeDelayCovariance(tree);
		DoubleMatrix rmd = relativeDelays.sub(deterministicDelays);
		DoubleMatrix rmdt = rmd.transpose();
		DoubleMatrix dci = Solve.solve(delayCovariance, DoubleMatrix.eye(observers.size()-1));
		double exponent = -0.5*rmdt.mmul(dci).mmul(rmd).get(0);
		double logdet = -0.5*Math.log(computeDeterminant(delayCovariance));
		/*
		GraphWriter gwriter = new GraphWriter(tree);
		gwriter.exportEdgeList("/home/rpaluch/Workspace/ReverseEngineering/outputs/testLPTV/bfs_tree.txt");
		ExpressWriter writer1 = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/testLPTV/observers_delays.txt");
		writer1.init();
		writer1.println("obs.id\trelative.delay\tdetermin.delay");
		ExpressWriter writer2 = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/testLPTV/cov_matrix.txt");
		writer2.init();
		for(int i=0; i<observersID.size()-1; i++){
			writer1.printf("%d\t%.3f\t%.3f\n", observers.get(i+1).getId(), relativeDelays.get(i), deterministicDelays.get(i));
			writer2.printf("%d\t", observers.get(i+1).getId());
		}
		writer2.printf("\n");
		for(int i=0; i<observersID.size()-1; i++){
			writer2.printf("%d\t", observers.get(i+1).getId());
			for(int j=0; j<observersID.size()-1; j++)
				writer2.printf("%.3f\t", delayCovariance.get(i,j));
			writer2.printf("\n");
		}
		writer1.close();
		writer2.close();
		*/
		return logdet+exponent;
	}
	
	private double computeDeterminant(DoubleMatrix a) {
		LUDecomposition<DoubleMatrix> lu = Decompose.lu(a);
		return Math.abs(lu.u.diag().prod());
	}
	
	private int lengthOfCommonPart(LinkedList<Node> path1, LinkedList<Node> path2) {
		int length = 0;
		int maxLength = Math.min(path1.size(), path2.size())-1;
		Link link1 = new Link(path1.get(0), path1.get(1));
		Link link2 = new Link(path2.get(0), path2.get(1));
		while(link1.equals(link2)) {
			length = length + 1;
			if(length+1>maxLength) break;
			link1.setStart(path1.get(length));
			link1.setEnd(path1.get(length+1));
			link2.setStart(path2.get(length));
			link2.setEnd(path2.get(length+1));
		}
		return length;
	}
		
	private DoubleMatrix computeDeterministicDelays(Graph tree, int sourceID) {
		DoubleMatrix detDel = new DoubleMatrix(observers.size()-1);
		ArrayList<LinkedList<Node>> allPaths = PathsTools.shortestPaths(tree, sourceID, observersID);
		int sourceToReferenceDistance = allPaths.get(0).size()-1;
		for(int i=0; i<observers.size()-1; i++) detDel.put(i, allPaths.get(i+1).size()-1-sourceToReferenceDistance);
		return detDel.mul(delayMean);
	}
	
	private DoubleMatrix computeDelayCovariance(Graph tree) {
		int dim = observers.size()-1;
		int commonLength;
		// The list of all paths between reference observer and other observers
		ArrayList<LinkedList<Node>> allPaths = PathsTools.shortestPaths(tree, reference.getId(), observersID);
		DoubleMatrix delCov = new DoubleMatrix(dim, dim);
		// First fill diagonal elements
		for(int i=0; i<dim; i++) delCov.put(i,i,allPaths.get(i+1).size()-1);
		// Next fill the rest of matrix
		for(int i=0; i<dim-1; i++) {
			for(int j=i+1; j<dim; j++) {
				commonLength = lengthOfCommonPart(allPaths.get(i+1),allPaths.get(j+1));
				delCov.put(i,j,commonLength);
				delCov.put(j,i,commonLength);
			}
		}
		return delCov.mul(delayVariance);
	}
}
