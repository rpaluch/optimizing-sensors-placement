package tracking;

import java.util.ArrayList;

import org.jblas.DoubleMatrix;

import graphs.Graph;
import graphs.Node;
import graphs.Path;
import graphs.PathsTools;

/***
 * The class which estimates the likelihood of being the spread source for some supposed nodes using method EPP
 * from the paper "Multiple propagation paths enhance locating the source of diffusion in complex networks".
 * @author rpaluch
 */

public class ScoreEstimatorEPP implements Runnable {

	final static double C = 0.5641895835; // phi(0)*sqrt(2), where phi is the probability d.f. of the standard normal distribution
	/***
	 * Data fields
	 */
	
	Graph graph;
	// The list of observers 
	ArrayList<Node> observers;
	// The list of observers ID 
	ArrayList<Integer> observersID;
	// The reference node is the last observer on the list of the observers
	Node reference;
	// The difference in delays between the reference node and other observers 
	DoubleMatrix relativeDelays;
	// The mean delay (greek mu)
	double delayMean;
	// The variance of delay (greek sigma square)
	double delayVariance;
	// The inversion of the covariance matrix which is the same for each source in EPP method
	DoubleMatrix inverseDelayCovariance;
	// The list of nodes which will be examined by Pinto algorithm 
	SupposedSource supposedSource;
	
	/***
	 * Constructors
	 */
		
	public ScoreEstimatorEPP(Graph graph, ArrayList<Node> observers, DoubleMatrix relativeDelays, double delayMean, double delayVariance,
							 DoubleMatrix inverseDelayCovariance, SupposedSource supposedSource) {
		this.graph = graph;
		this.observers = observers;
		this.observersID = new ArrayList<Integer>();
		for(Node obs : observers) observersID.add(obs.getId());
		this.reference = observers.get(0);
		this.relativeDelays = relativeDelays;
		this.delayMean = delayMean;
		this.delayVariance = delayVariance;
		this.inverseDelayCovariance = inverseDelayCovariance;
		this.supposedSource = supposedSource;
	}
	
	/***
	 * Run method
	 */
	
	public void run() {
		supposedSource.setScore(computeScore(supposedSource.getNode().getId()));
	}
		
	/***
	 * Other methods
	 */
		
	private double computeScore(int nodeID) {
		if(observersID.indexOf(nodeID)>0) return Double.NEGATIVE_INFINITY;
		ArrayList<ArrayList<Path>> pathsFromSourceToObservers = PathsTools.allShortestPaths(graph, nodeID, observersID);
		DoubleMatrix deterministicDelays = computeDeterministicDelays(pathsFromSourceToObservers);
		DoubleMatrix rmd = relativeDelays.sub(deterministicDelays);
		DoubleMatrix rmdt = rmd.transpose();
		double exponent = -0.5*rmdt.mmul(inverseDelayCovariance).mmul(rmd).get(0);
		return exponent;
	}
	
	private DoubleMatrix computeCorrelationCoefficients(ArrayList<ArrayList<Path>> pathsFromSourceToObservers) {
		DoubleMatrix corrCoeff = new DoubleMatrix(observers.size()-1);
		// The vector corrCoeff stores the values of 1-rho instead of rho
		for(int i=0; i<observers.size()-1; i++) {
			if(pathsFromSourceToObservers.get(i+1).size()>1)
				corrCoeff.put(i, 1-findLeastCorrelation(pathsFromSourceToObservers.get(i+1)));
			else
				corrCoeff.put(i, 0);
		}
		// For further computation we need term delayVariance*(1-rho)*C^2
		return corrCoeff.mmul(C*C*delayVariance);
	}
	
	private double findLeastCorrelation(ArrayList<Path> paths) {
		double least = 1;
		double cor = 0;
		for(int i=0; i<paths.size()-1; i++)
			for(int j=i+1; j<paths.size(); j++) {
				cor = PathsTools.twoPathsCorrelation(paths.get(i), paths.get(j));
				if(cor<least) least=cor;
			}
		return least;
	}
		
	private DoubleMatrix computeDeterministicDelays(ArrayList<ArrayList<Path>> pathsFromSourceToObservers) {
		DoubleMatrix detDel = new DoubleMatrix(observers.size()-1);
		DoubleMatrix corrCoeff = computeCorrelationCoefficients(pathsFromSourceToObservers);
		//ArrayList<ArrayList<Path>> pathsFromSourceToObservers = PathsTools.allShortestPaths(graph, sourceID, observersID);
		int sourceToReferenceDistance = pathsFromSourceToObservers.get(0).get(0).getLength();
		int relativeDistance = 0;
		for(int i=0; i<observers.size()-1; i++) {
			relativeDistance = pathsFromSourceToObservers.get(i+1).get(0).getLength()-sourceToReferenceDistance;
			detDel.put(i, delayMean*relativeDistance-Math.sqrt(corrCoeff.get(i)));
		}
		return detDel;
	}
}
