package io;

import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ExpressWriter 
{
	//Data fields
	PrintWriter pwriter = null;
	String path = null;
	
	//Constructors
	public ExpressWriter(){}
	public ExpressWriter(String path_in){path = path_in;}
	
	//Public methods
	public void init()
	{
		try
		{
			System.out.println("Preparing the output file " + path + " for writing data...");
			pwriter = new PrintWriter(new FileWriter(path));
			System.out.println("The output file " + path + " is ready for writing data.");
		} 
		catch (IOException e) 
		{
			System.out.println("Problem with the output file. Check if you set the proper path to the file.");
			e.printStackTrace();
		}
		
	}
	public void init(String path_in)
	{
		path = path_in;
		try
		{
			System.out.println("Preparing the output file " + path + " for writing data...");
			pwriter = new PrintWriter(new FileWriter(path));
			System.out.println("The output file " + path + " is ready for writing data.");
		} 
		catch (IOException e) 
		{
			System.out.println("Problem with the output file. Check if you set the proper path to the file.");
			e.printStackTrace();
		}
	}
	public void print(String text)
	{
			pwriter.print(text);
			pwriter.flush();
	}
	public void println(String text)
	{
			pwriter.println(text);
			pwriter.flush();
	}
	public void printf(String text, Object... arguments)
	{
		pwriter.printf(text, arguments);
		pwriter.flush();
	}
	public void close()
	{
			pwriter.close(); 
	}
}
