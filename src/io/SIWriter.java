package io;

import dynamics.SusceptibleInfected;

/***
 * Basic class for writing results from SI run to the text file.
 * @author rpaluch
 */

public class SIWriter {

	/***
	 * Data fields
	 */
	
	SusceptibleInfected si;
	String path;
	ExpressWriter writer;
	
	/***
	 * Constructors
	 */
	
	public SIWriter(SusceptibleInfected si) {
		this.si = si;
		this.path = "";
		this.writer = new ExpressWriter();
	}
	
	public SIWriter(SusceptibleInfected si, String path) {
		this.si = si;
		this.path = path;
		this.writer = new ExpressWriter(path);
	}

	/***
	 * Getters and setters
	 */
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
		this.writer = new ExpressWriter(path);
	}
	
	/***
	 * Other methods
	 */
	
	public void writeInfectionTimes() {
		writer.init();
		for(int i=0; i<si.getObservers().size(); i++)
			writer.printf("%d %d\n", si.getObservers().get(i).getId(), si.getInfectionTime().get(i));
		writer.close();
	}
	
	public void writeInfectionTimes(String path) {
		setPath(path);
		writer.init();
		for(int i=0; i<si.getObservers().size(); i++)
			writer.printf("%d %d\n", si.getObservers().get(i).getId(), si.getInfectionTime().get(i));
		writer.close();
	}
}
