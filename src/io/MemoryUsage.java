package io;

public class MemoryUsage extends Thread {

	private static final long MEGABYTE = 1024L * 1024L;
	
	long min,max,diff;
	boolean stop;
	
	public MemoryUsage() {
		super();
		min = Runtime.getRuntime().totalMemory();
		max = 0;
		diff = 0;
		stop = false;
	}
	
	public void stopMeasurement() {
		stop = true;
	}
	
	public long getMaxMemoryUsage() {
		return bytesToMegabytes(max);
	}
	
	public long getMinMemoryUsage() {
		return bytesToMegabytes(min);
	}
	
	public long getDiffMemoryUsage() {
		return bytesToMegabytes(max-min);
	}

    public long bytesToMegabytes(long bytes) {
        return bytes / MEGABYTE;
    }
    
	@Override
	public void run() {
		// TODO Auto-generated method stub
		long measurement = 1;
		while(!stop){
			measurement = Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
			if(measurement<min)
				min = measurement;
			else if(measurement>max)
				max = measurement;
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}	
}
