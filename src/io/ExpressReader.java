package io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ExpressReader 
{
	//Data fields
	BufferedReader br = null;
	String path = null;
	
	//Constructors
	public ExpressReader(){}
	public ExpressReader(String path_in){path = path_in;}
	
	//Public methods
	public void init()
	{
		try 
		{
			System.out.println("Opening the input file " + path + " for reading data...");
			br = new BufferedReader(new FileReader(path));
			System.out.println("The input file " + path + " is ready for reading data.");
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("The input file " + path + " is missing or the path is incorrect.");
			e.printStackTrace();
		}
	}
	public void init(String path_in)
	{
		path = path_in;
		try 
		{
			System.out.println("Opening the input file " + path + " for reading data...");
			br = new BufferedReader(new FileReader(path));
			System.out.println("The input file " + path + " is ready for reading data.");
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("The input file " + path + " is missing or the path is incorrect.");
			e.printStackTrace();
		}
	}
	public String readLine()
	{
		String line = null;
		try 
		{
			line = br.readLine();
		} 
		catch (IOException e) 
		{
			System.out.println("Problem with reading new line.");
			e.printStackTrace();
		}
		return line;
	}
	public void close()
	{
		try 
		{
			br.close();
		} 
		catch (IOException e) 
		{
			System.out.println("Problem occured during closing the input file.");
			e.printStackTrace();
		}
	}

}
