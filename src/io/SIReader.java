package io;

import java.util.ArrayList;

import graphs.Node;

/***
 * Basic class for reading graphs from the text file.
 * @author rpaluch
 */

public class SIReader {

	/***
	 * Data fields
	 */
	
	String path;
	ExpressReader reader;
	// List of the observers - an observer is an node which state will be recorded in each step
	ArrayList<Node> observers;
	// Contains the number of step in which the observer has been infected
	ArrayList<Integer> infectionTimes;
	
	/***
	 * Constructors
	 */
	
	public SIReader() {
		this.path = "";
		this.reader = new ExpressReader();
		this.observers = new ArrayList<Node>();
		this.infectionTimes = new ArrayList<Integer>();
	}
	
	public SIReader(String path) {
		this.path = path;
		this.reader = new ExpressReader(path);
		this.observers = new ArrayList<Node>();
		this.infectionTimes = new ArrayList<Integer>();
	}

	/***
	 * Getters and setters
	 */
	
	public String getPath() {
		return path;
	}
	
	public ArrayList<Node> getObservers() {
		return observers;
	}

	public ArrayList<Integer> getInfectionTimes() {
		return infectionTimes;
	}

	public void setPath(String path) {
		this.path = path;
		this.reader = new ExpressReader(path);
	}
	
	/***
	 * Other methods
	 */
	
	public ArrayList<Integer> readInfectionTimes() {
		String newline;
		String[] elements;
		reader.init();
		while((newline = reader.readLine())!=null) {
			elements = newline.split("\\s");
			observers.add(new Node(Integer.parseInt(elements[0])));
			infectionTimes.add(Integer.parseInt(elements[1]));
		}
		reader.close();
		return infectionTimes;
	}
	
}
