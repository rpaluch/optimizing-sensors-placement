package io;

import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.IOException;

public class MultiExpressWriter 
{
	//Data fields
	PrintWriter[] pwriters = null;
	String[] names = null;
	String[] paths = null;
	
	//Constructors
	public MultiExpressWriter(int numberOfWriters) {
		this.pwriters = new PrintWriter[numberOfWriters];
		this.paths = new String[numberOfWriters];
		this.names = new String[numberOfWriters];
	}
	
	public MultiExpressWriter(String[] paths) {
		this.pwriters = new PrintWriter[paths.length];
		this.paths = new String[paths.length];
		this.names = new String[paths.length];
		for(int i=0; i<paths.length; i++) this.paths[i] = paths[i];
	}
	
	public MultiExpressWriter(String[] paths, String[] names) {
		this.pwriters = new PrintWriter[paths.length];
		this.paths = new String[paths.length];
		this.names = new String[paths.length];
		for(int i=0; i<paths.length; i++) {
			this.paths[i] = paths[i];
			this.names[i] = names[i];
		}
	}
	
	//Public methods
	public void initAll() {
		for(int i=0; i<pwriters.length; i++) init(i);		
	}
	
	public void initAll(String[] paths) {
		for(int i=0; i<paths.length; i++) init(i, paths[i]);
	}
	
	public void initAll(String[] paths, String[] names) {
		for(int i=0; i<paths.length; i++) init(i, paths[i], names[i]);
	}
	
	public void init(int index) {
		try {
			System.out.println("Preparing the output file " + paths[index] + " for writing data...");
			pwriters[index] = new PrintWriter(new FileWriter(paths[index]));
			System.out.println("The output file " + paths[index] + " is ready for writing data.");
		} 
		catch (IOException e) {
			System.out.println("Problem with the output file " + paths[index] + " Check if you set the proper path to the file.");
			e.printStackTrace();
		}
	}
	
	public void init(int index, String path) {
		this.paths[index] = path;
		try {
			System.out.println("Preparing the output file " + paths[index] + " for writing data...");
			pwriters[index] = new PrintWriter(new FileWriter(paths[index]));
			System.out.println("The output file " + paths[index] + " is ready for writing data.");
		} 
		catch (IOException e) {
			System.out.println("Problem with the output file " + paths[index] + " Check if you set the proper path to the file.");
			e.printStackTrace();
		}
	}
	
	public void init(int index, String path, String name) {
		this.paths[index] = path;
		this.names[index] = name;
		try {
			System.out.println("Preparing the output file " + paths[index] + " for writing data...");
			pwriters[index] = new PrintWriter(new FileWriter(paths[index]));
			System.out.println("The output file " + paths[index] + " is ready for writing data.");
		} 
		catch (IOException e) {
			System.out.println("Problem with the output file " + paths[index] + " Check if you set the proper path to the file.");
			e.printStackTrace();
		}
	}
	
	public void print(int index, String text) {
		pwriters[index].print(text);
		pwriters[index].flush();
	}
	
	public void print(String name, String text) {
		int index = getIndex(name);
		pwriters[index].print(text);
		pwriters[index].flush();
	}
	
	public void printAll(String text) {
		for(int i=0; i<pwriters.length; i++) print(i, text);
	}
	
	public void println(int index, String text) {
		pwriters[index].println(text);
		pwriters[index].flush();
	}
	
	public void println(String name, String text) {
		int index = getIndex(name);
		pwriters[index].println(text);
		pwriters[index].flush();
	}
	
	public void printlnAll(String text) {
		for(int i=0; i<pwriters.length; i++) println(i, text);
	}
	
	public void printf(int index, String text, Object... arguments) {
		pwriters[index].printf(text, arguments);
		pwriters[index].flush();
	}
	
	public void printf(String name, String text, Object... arguments) {
		int index = getIndex(name);
		pwriters[index].printf(text, arguments);
		pwriters[index].flush();
	}
	
	public void printfAll(String text, Object... arguments) {
		for(int i=0; i<pwriters.length; i++) printf(i, text, arguments);
	}
	
	public void printlnHeader(int index, int[] header, String prefix, String separator) {
		String pre = prefix;
		String sep = separator;
		if(prefix==null) pre="";
		if(separator==null) sep="\t";
		String text = pre+header[0];
		for(int i=1; i<header.length; i++) text = text + sep + pre + header[i];
		pwriters[index].println(text);
		pwriters[index].flush();
	}
	
	public void printlnHeader(String name, int[] header, String prefix, String separator) {
		String pre = prefix;
		String sep = separator;
		if(prefix==null) pre="";
		if(separator==null) sep="\t";
		String text = pre+header[0];
		for(int i=1; i<header.length; i++) text = text + sep + pre + header[i];
		int index = getIndex(name);
		pwriters[index].println(text);
		pwriters[index].flush();
	}
	
	public void printlnHeaderAll(int[] header, String prefix, String separator) {
		for(int i=0; i<pwriters.length; i++) printlnHeader(i, header, prefix, separator);
	}
	
	public void printlnHeader(int index, double[] header, String prefix, String separator) {
		String pre = prefix;
		String sep = separator;
		if(prefix==null) pre="";
		if(separator==null) sep="\t";
		String text = pre+header[0];
		for(int i=1; i<header.length; i++) text = text + sep + pre + header[i];
		pwriters[index].println(text);
		pwriters[index].flush();
	}
	
	public void printlnHeader(String name, double[] header, String prefix, String separator) {
		String pre = prefix;
		String sep = separator;
		if(prefix==null) pre="";
		if(separator==null) sep="\t";
		String text = pre+header[0];
		for(int i=1; i<header.length; i++) text = text + sep + pre + header[i];
		int index = getIndex(name);
		pwriters[index].println(text);
		pwriters[index].flush();
	}
	
	public void printlnHeaderAll(double[] header, String prefix, String separator) {
		for(int i=0; i<pwriters.length; i++) printlnHeader(i, header, prefix, separator);
	}
	
	public void newLine(int index) {
		pwriters[index].printf("\n");
		pwriters[index].flush();
	}
	
	public void newLine(String name) {
		int index = getIndex(name);
		pwriters[index].printf("\n");
		pwriters[index].flush();
	}
	
	public void newLineAll() {
		for(int i=0; i<pwriters.length; i++) newLine(i);
	}
	
	public void close(int index) {
		pwriters[index].close();
	}
	
	public void close(String name) {
		pwriters[getIndex(name)].close();
	}
	
	public void closeAll() {
		for(int i=0; i<pwriters.length; i++) close(i); 
	}
	
	public int getIndex(String name) {
		for(int i=0; i<names.length; i++) if(names[i].equals(name)) return i;
		return -1;
	}
	
	public String getName(int index) {
		return names[index];
	}
	
	public String getPath(int index) {
		return paths[index];
	}
	
	public int getNumberOfWriters() {
		return pwriters.length;
	}
}
