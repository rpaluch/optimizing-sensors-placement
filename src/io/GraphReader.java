package io;

import java.util.ArrayList;

import graphs.Graph;
import graphs.Node;

/***
 * Basic class for reading graphs from the text file.
 * @author rpaluch
 */

public class GraphReader {

	/***
	 * Data fields
	 */
	
	String path;
	ExpressReader reader;
	
	/***
	 * Constructors
	 */
	
	public GraphReader() {
		path = "";
		reader = new ExpressReader();
	}
	
	public GraphReader(String path) {
		this.path = path;
		this.reader = new ExpressReader(path);
	}

	/***
	 * Getters and setters
	 */
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
		this.reader = new ExpressReader(path);
	}
	
	/***
	 * Other methods
	 */
	
	public Graph readGraphFromPajek() {
		Graph graph = new Graph();
		String newline;
		String[] nodesId;
		reader.init();
		newline = reader.readLine(); // read header
		while(!newline.toLowerCase().equals("*arcs")) newline = reader.readLine();
		while((newline = reader.readLine())!=null) {
			nodesId = newline.split("\\s");
			graph.addLink(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1]));
		}
		reader.close();
		return graph;
	}
	
	public Graph readGraphFromPajek(int mode) {
		Graph graph = new Graph();
		String newline;
		String[] nodesId;
		reader.init();
		newline = reader.readLine(); // read header
		switch(mode) {
		case 1:
			while(!newline.toLowerCase().equals("*arcs")){
				newline = reader.readLine();
				nodesId = newline.split("\\s");
				graph.addNode(new Node(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1])));
			}
			while((newline = reader.readLine())!=null) {
				nodesId = newline.split("\\s");
				graph.addLink(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1]));
			}
			break;
		case 2:
			while(!newline.toLowerCase().equals("*arcs")) newline = reader.readLine();
			while((newline = reader.readLine())!=null) {
				nodesId = newline.split("\\s");
				graph.addLink(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1]), Double.parseDouble(nodesId[2]));
			}
			break;
		case 3:
			while(!(newline = reader.readLine()).toLowerCase().equals("*arcs")){
				//newline = reader.readLine();
				nodesId = newline.split("\\s");
				graph.addNode(new Node(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1])));
			}
			while((newline = reader.readLine())!=null) {
				nodesId = newline.split("\\s");
				graph.addLink(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1]), Double.parseDouble(nodesId[2]));
			}
			break;
		default:
			while(!newline.toLowerCase().equals("*arcs")) newline = reader.readLine();
			while((newline = reader.readLine())!=null) {
				nodesId = newline.split("\\s");
				graph.addLink(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1]));
			}
			break;
		}	
		reader.close();
		return graph;
	}
	
	public Graph readGraphFromPajek(String path) {
		setPath(path);
		Graph graph = new Graph();
		String newline;
		String[] nodesId;
		reader.init();
		newline = reader.readLine(); // read header
		while(!newline.toLowerCase().equals("*arcs")) newline = reader.readLine();
		while((newline = reader.readLine())!=null) {
			nodesId = newline.split("\\s");
			graph.addLink(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1]));
		}
		reader.close();
		return graph;
	}
	
	public Graph readGraphFromPajek(String path, int mode) {
		setPath(path);
		Graph graph = new Graph();
		String newline;
		String[] nodesId;
		reader.init();
		newline = reader.readLine(); // read header
		switch(mode) {
		case 1:
			while(!newline.toLowerCase().equals("*arcs")){
				newline = reader.readLine();
				nodesId = newline.split("\\s");
				graph.addNode(new Node(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1])));
			}
			while((newline = reader.readLine())!=null) {
				nodesId = newline.split("\\s");
				graph.addLink(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1]));
			}
			break;
		case 2:
			while(!newline.toLowerCase().equals("*arcs")) newline = reader.readLine();
			while((newline = reader.readLine())!=null) {
				nodesId = newline.split("\\s");
				graph.addLink(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1]), Double.parseDouble(nodesId[2]));
			}
			break;
		case 3:
			while(!newline.toLowerCase().equals("*arcs")){
				newline = reader.readLine();
				nodesId = newline.split("\\s");
				graph.addNode(new Node(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1])));
			}
			while((newline = reader.readLine())!=null) {
				nodesId = newline.split("\\s");
				graph.addLink(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1]), Double.parseDouble(nodesId[2]));
			}
			break;
		default:
			while(!newline.toLowerCase().equals("*arcs")) newline = reader.readLine();
			while((newline = reader.readLine())!=null) {
				nodesId = newline.split("\\s");
				graph.addLink(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1]));
			}
			break;
		}	
		reader.close();
		return graph;
	}
	
	public Graph readGraphFromEdgeList() {
		Graph graph = new Graph();
		String newline;
		String[] nodesId;
		reader.init();
		while((newline = reader.readLine())!=null) {
			nodesId = newline.split("\\s");
			graph.addLink(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1]));
		}
		reader.close();
		return graph;
	}
	
	public Graph readGraphFromEdgeList(int edges) {
		Graph graph = new Graph();
		String newline;
		String[] nodesId;
		reader.init();
		while((newline = reader.readLine())!=null) {
			nodesId = newline.split("\\s");
			graph.addLink(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1]));
			if(graph.getNumberOfLinks()>=edges) break;
		}
		reader.close();
		return graph;
	}
	
	public Graph readGraphFromEdgeList(String path) {
		setPath(path);
		Graph graph = new Graph();
		String newline;
		String[] nodesId;
		reader.init();
		while((newline = reader.readLine())!=null) {
			nodesId = newline.split("\\s");
			graph.addLink(Integer.parseInt(nodesId[0]), Integer.parseInt(nodesId[1]));
		}
		reader.close();
		return graph;
	}
	
	public ArrayList<Graph> readTwitterCascades() {
		ArrayList<Graph> cascades = new ArrayList<Graph>();
		cascades.add(new Graph());
		ExpressReader reader = new ExpressReader(path);
		String newline;
		String[] elements;
		String interaction;
		int idNodeA, idNodeB;
		int time;
		double weight;
		boolean flagA, flagB, flagC;
		reader.init();
		while((newline=reader.readLine())!=null){
			elements = newline.split(" ");
			idNodeB = Integer.parseInt(elements[0]);
			idNodeA = Integer.parseInt(elements[1]);
			time = Integer.parseInt(elements[2].substring(3));
			interaction = elements[3];
			weight = 0.0;
			if(interaction.equals("MT"))
				weight = 1.0;
			else if(interaction.equals("RE"))
				weight = 2.0;
			flagA = flagB = flagC = false;
			for(int i=cascades.size()-1; i>=0; i--){
				for(Node n : cascades.get(i).getNodesList()){
					if(n.getId()==idNodeA) flagA = true;
					else if(n.getId()==idNodeB) flagB = true;
				}
				if(flagA && !flagB){
					cascades.get(i).addLink(idNodeA, idNodeB, weight);
					cascades.get(i).getNode(idNodeB).setState(time);
					flagC = true;
					break;
				}
			}
			if(!flagC){
				Graph g = new Graph();
				g.addLink(idNodeA, idNodeB, weight);
				g.getNode(idNodeB).setState(time);
				cascades.add(g);
			}
		}
		reader.close();
		return cascades;
	}
}
