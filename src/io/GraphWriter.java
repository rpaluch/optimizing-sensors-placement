package io;

import graphs.Graph;
import graphs.Link;
import graphs.Node;

/***
 * Basic class for writing graphs to the text file.
 * @author rpaluch
 */

public class GraphWriter {

	/***
	 * Data fields
	 */
	
	Graph graph;
	String path;
	ExpressWriter writer;
	
	/***
	 * Constructors
	 */
	
	public GraphWriter(Graph graph) {
		this.graph = graph;
		this.path = "";
		this.writer = new ExpressWriter();
	}
	
	public GraphWriter(Graph graph, String path) {
		this.graph = graph;
		this.path = path;
		this.writer = new ExpressWriter(path);
	}

	/***
	 * Getters and setters
	 */
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
		this.writer = new ExpressWriter(path);
	}
	
	/***
	 * Other methods
	 */
	
	public void writeGraphToPajek() {
		writer.init();
		writer.println("*vertices "+graph.getNumberOfNodes());
		writer.println("*arcs");
		for(Link l : graph.getLinksList()) writer.printf("%d %d\n", l.getStart().getId(), l.getEnd().getId());
		writer.close();
	}
	
	public void writeGraphToPajek(int mode) {
		writer.init();
		writer.println("*vertices "+graph.getNumberOfNodes());
		switch(mode) {
			case 1:
				for(Node n : graph.getNodesList())
					writer.printf("%d %d\n", n.getId(), n.getState());
				writer.println("*arcs");
				for(Link l : graph.getLinksList()) writer.printf("%d %d\n", l.getStart().getId(), l.getEnd().getId());
			case 2:
				writer.println("*arcs");
				for(Link l : graph.getLinksList()) writer.printf("%d %d %f\n", l.getStart().getId(), l.getEnd().getId(), l.getWeight());
			case 3:
				for(Node n : graph.getNodesList())
					writer.printf("%d %d\n", n.getId(), n.getState());
				writer.println("*arcs");
				for(Link l : graph.getLinksList()) writer.printf("%d %d %f\n", l.getStart().getId(), l.getEnd().getId(), l.getWeight());
			default:
				writer.println("*arcs");
				for(Link l : graph.getLinksList()) writer.printf("%d %d\n", l.getStart().getId(), l.getEnd().getId());
		}	
		writer.close();
	}
	
	public void writeGraphToPajek(String path) {
		setPath(path);
		writer.init();
		writer.println("*vertices "+graph.getNumberOfNodes());
		writer.println("*arcs");
		for(Link l : graph.getLinksList()) writer.printf("%d %d\n", l.getStart().getId(), l.getEnd().getId());
		writer.close();
	}
	
	public void writeGraphToPajek(String path, int mode) {
		setPath(path);
		writer.init();
		writer.println("*vertices "+graph.getNumberOfNodes());
		switch(mode) {
			case 1:
				for(Node n : graph.getNodesList())
					writer.printf("%d %d\n", n.getId(), n.getState());
				writer.println("*arcs");
				for(Link l : graph.getLinksList()) writer.printf("%d %d\n", l.getStart().getId(), l.getEnd().getId());
				break;
			case 2:
				writer.println("*arcs");
				for(Link l : graph.getLinksList()) writer.printf("%d %d %.3f\n", l.getStart().getId(), l.getEnd().getId(), l.getWeight());
				break;
			case 3:
				for(Node n : graph.getNodesList())
					writer.printf("%d %d\n", n.getId(), n.getState());
				writer.println("*arcs");
				for(Link l : graph.getLinksList()) writer.printf("%d %d %.3f\n", l.getStart().getId(), l.getEnd().getId(), l.getWeight());
				break;
			default:
				writer.println("*arcs");
				for(Link l : graph.getLinksList()) writer.printf("%d %d\n", l.getStart().getId(), l.getEnd().getId());
				break;
		}	
		writer.close();
	}
	
	public void exportEdgeList(String path) {
		setPath(path);
		writer.init();
		for(Link l : graph.getLinksList()) writer.printf("%d %d\n", l.getStart().getId(), l.getEnd().getId());
		writer.close();
	}
	
	public void exportEdgeListWithWeights(String path) {
		setPath(path);
		writer.init();
		for(Link l : graph.getLinksList()) writer.printf("%d %d %.3f\n", l.getStart().getId(), l.getEnd().getId(), l.getWeight());
		writer.close();
	}
}
