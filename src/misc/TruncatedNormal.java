package misc;

import jdistlib.Normal;
import jdistlib.generic.GenericDistribution;

public class TruncatedNormal extends GenericDistribution {

	Normal standardNormal;
	double mean,sd,a,b,alfa,beta,zet,phiAlfa,phiBeta,truncMean,truncVariance;
	
	public TruncatedNormal() {
		super();
		standardNormal = new Normal();
		mean = 0;
		sd = 1;
		a = -sd;
		b = sd;
		alfa = (a-mean)/sd;
		beta = (b-mean)/sd;
		zet = standardNormal.cumulative(beta)-standardNormal.cumulative(alfa);
		phiAlfa = standardNormal.density(alfa, false);
		phiBeta = standardNormal.density(beta, false);
		truncMean = mean + sd*(phiAlfa-phiBeta)/zet;
		truncVariance = sd*sd*(1+(alfa*phiAlfa-beta*phiBeta)/zet-Math.pow((phiAlfa-phiBeta)/zet,2));
	}
	
	public TruncatedNormal(double mean, double sd, double a, double b) {
		super();
		standardNormal = new Normal();
		this.mean = mean;
		this.sd = sd;
		this.a = a;
		this.b = b;
		alfa = (a-mean)/sd;
		beta = (b-mean)/sd;
		zet = standardNormal.cumulative(beta)-standardNormal.cumulative(alfa);
		phiAlfa = standardNormal.density(alfa, false);
		phiBeta = standardNormal.density(beta, false);
		truncMean = mean + sd*(phiAlfa-phiBeta)/zet;
		truncVariance = sd*sd*(1+(alfa*phiAlfa-beta*phiBeta)/zet-Math.pow((phiAlfa-phiBeta)/zet,2));
	}

	@Override
	public double cumulative(double x, boolean lower_tail, boolean log) {
		double value = 0;
		if(lower_tail) {
			if(log) value = Math.log((standardNormal.cumulative((x-mean)/sd)-standardNormal.cumulative(alfa))/zet);
			else value = (standardNormal.cumulative((x-mean)/sd)-standardNormal.cumulative(alfa))/zet;
		}
		else {
			if(log) value = Math.log(1-(standardNormal.cumulative((x-mean)/sd)-standardNormal.cumulative(alfa))/zet);
			else value = 1-(standardNormal.cumulative((x-mean)/sd)-standardNormal.cumulative(alfa))/zet;
		}
		return value;
	}

	@Override
	public double density(double x, boolean log) {
		double value = 0;
		if(log) value = standardNormal.density((x-mean)/sd, log)-Math.log(sd*zet);
		else value = standardNormal.density((x-mean)/sd, log)/(sd*zet);
		return value;
	}

	@Override
	public double quantile(double arg0, boolean arg1, boolean arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double random() {
		Normal normal = new Normal(mean,sd);
		double value = normal.random();
		while(value<a || value>b) value = normal.random();
		return value;
	}
	
	public double getTruncatedMean() {
		return truncMean;
	}
	
	public double getTruncatedVariance() {
		return truncVariance;
	}

	public static void main(String[] args) {
		double[] mu = {1.0,1.5,2.0,2.5,3.0,3.5,4.0};
		double sd = 1;
		double min = 0;
		TruncatedNormal tnorm;
		
		for(int i=0; i<mu.length; i++) {
			tnorm = new TruncatedNormal(mu[i],sd,min,2*mu[i]);
			System.out.printf("mu=%.3f, sd=%.3f, mu/sd=%.3f\n", tnorm.getTruncatedMean(), Math.sqrt(tnorm.getTruncatedVariance()), tnorm.getTruncatedMean()/Math.sqrt(tnorm.getTruncatedVariance()));
		}
	}
}
