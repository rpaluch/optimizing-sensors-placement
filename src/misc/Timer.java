package misc;

public class Timer {

	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}
	
}
