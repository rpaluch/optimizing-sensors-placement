package tests;

import java.util.ArrayList;

import dynamics.SusceptibleInfected;
import graphs.Graph;
import graphs.GraphGenerator;
import io.MultiExpressWriter;
import tracking.SingleSourceDetector;

public class TestSingleSourceDetector {

	public static void main(String[] args) {
		
		int size = 2000;
		int degree = 8;
		int m = degree/2;
		double densityOfObservers = 0.2;
		double[] rate = {0.3,0.4,0.5,0.6,0.7,0.8};
		String header = "rate0.3\trate0.4\trate0.5\trate0.6\trate0.7\trate0.8";
		int realizations = 5;
		Graph g;		
		SusceptibleInfected si;
		int sourceID;
		String[] methods = {"LPTV","EPP","EPL"};
		SingleSourceDetector detector;
		ArrayList<Integer> supposedSourcesId;
		
		String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/epp_tests/ba_N2000_k8_O"+densityOfObservers+"_rate";
		String[] files = {dir+"_LPTV_accuracy.txt",
						  dir+"_LPTV_rank.txt",
						  dir+"_EPP_accuracy.txt",
						  dir+"_EPP_rank.txt",
						  dir+"_EPL_accuracy.txt",
						  dir+"_EPL_rank.txt",
						  dir+"_LPTV_gradient_accuracy.txt",
						  dir+"_LPTV_gradient_rank.txt",
						  dir+"_EPP_gradient_accuracy.txt",
						  dir+"_EPP_gradient_rank.txt",
						  dir+"_EPL_gradient_accuracy.txt",
						  dir+"_EPL_gradient_rank.txt"};
		String[] names = {"accuracyLPTV","rankLPTV","accuracyEPP","rankEPP","accuracyEPL","rankEPL",
						  "accuracy_gradientLPTV","rank_gradientLPTV","accuracy_gradientEPP","rank_gradientEPP","accuracy_gradientEPL","rank_gradientEPL"};
		MultiExpressWriter writer = new MultiExpressWriter(files, names);
		writer.initAll();
		writer.printlnAll(header);
		long tstart = System.currentTimeMillis();
		for(int i=0; i<realizations; i++) {
			
			g = GraphGenerator.barabasiAlbert(m+2, m, size);
			//g = GraphGenerator.contiguousErdosRenyi(size, degree, "degree");
			
			for(int r=0; r<rate.length; r++) {
				
				si = new SusceptibleInfected(g);
				si.setObservers(g.getRandomNodes((int)(densityOfObservers*size)));
				si.setInfectionRate(rate[r]);
				si.runUntilAllObserversInfected();
				sourceID = si.getSources().get(0).getId();
				
				for(String method: methods) {
					detector = new SingleSourceDetector(g);
					detector.setObservers(si.getObservers());
					detector.setDelays(si.getInfectionTimeAsDouble());
					detector.setDelayMean(1/rate[r]);
					detector.setDelayVariance((1-rate[r])/(rate[r]*rate[r]));
					detector.setMaxThreads(4);
					detector.setSupposedSources(g.getNodesList());
					detector.computeScores(method);
					supposedSourcesId = detector.findNodesWithHighestScores();
					if(supposedSourcesId.contains(sourceID)) writer.printf("accuracy"+method,"%.3f\t",1.0/supposedSourcesId.size());
					else writer.printf("accuracy"+method,"0.000\t");
					writer.printf("rank"+method,"%d\t",detector.findRankOfNode(sourceID));
				}
				
				for(String method: methods) {
					detector = new SingleSourceDetector(g);
					detector.setObservers(si.getObservers());
					detector.setDelays(si.getInfectionTimeAsDouble());
					detector.setDelayMean(1/rate[r]);
					detector.setDelayVariance((1-rate[r])/(rate[r]*rate[r]));
					detector.setMaxThreads(4);
					detector.setNumberOfGradientWalks(1);
					detector.setNumberOfNearestObservers(20);
					detector.computeScores(method);
					supposedSourcesId = detector.findNodesWithHighestScores();
					if(supposedSourcesId.contains(sourceID)) writer.printf("accuracy_gradient"+method,"%.3f\t",1.0/supposedSourcesId.size());
					else writer.printf("accuracy_gradient"+method,"0.000\t");
					writer.printf("rank_gradient"+method,"%d\t",detector.findRankOfNode(sourceID));
				}
			}
			writer.newLineAll();
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, i+1, realizations));
		}
		writer.closeAll();
	}

	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}
}
