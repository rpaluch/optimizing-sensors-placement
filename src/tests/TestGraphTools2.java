package tests;

import java.util.ArrayList;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Node;
import graphs.PathsRegister;
import io.ExpressWriter;

public class TestGraphTools2 {

	public static void main(String[] args) {
		
		String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/observers_placement3/hvobs_ba_k6_O0.1";
		//ExpressWriter writer1 = new ExpressWriter(dir+"_time.txt");
		//writer1.init();
		//writer1.println("N50\tN100\tN200\tN300\tN500\tN1000");
		
		//long tstart, t0;
		int size = 100;
		int k = 4;
		Graph graph;
		//PathsRegister register;
		//PathsRegister limitedRegister;
		//int maxLength = 3;
		ArrayList<Node> observers = new ArrayList<Node>();
		//ArrayList<Node> observers2 = new ArrayList<Node>();
		//ArrayList<Node> union;
		double[] budget = {0.1,0.11,0.12,0.13,0.14,0.15,0.20,0.25,0.26,0.27,0.28,0.29,0.3};
		System.out.println("0.1\t0.11\t0.12\t0.13\t0.14\t0.15\t0.20\t0.25\t0.26\t0.27\t0.28\t0.29\t0.3");
		int realizations = 5;
		//int counts;
		
		//tstart = System.currentTimeMillis();
		for(int r=0; r<realizations; r++) {
			
			//counts = 0;
			
			for(int i=0; i<budget.length; i++) {
				
				//tstart = System.currentTimeMillis();
				graph = GraphGenerator.barabasiAlbert(k+2, k, size);
				//System.out.printf("%d\t", System.currentTimeMillis()-tstart);
				
				//tstart = System.currentTimeMillis();
				//register = GraphTools.allLimitedShortestPathsInGraph(graph,4);
				//limitedRegister = GraphTools.allLimitedShortestPathsInGraph(graph,3);
				//System.out.printf("%d\t", System.currentTimeMillis()-tstart);
				
				//GraphTools.betweennessCentrality(graph, register);
				
				//	System.out.printf("%d\t%.3f\n", n.getId(), n.getBetweenness());
				
				//t0 = System.currentTimeMillis();
				//observers = GraphTools.greedyCollectiveBetweenness(graph, register, (int)(size[i]*budget));
				//System.out.printf("%d\n", System.currentTimeMillis()-t0);
				//System.out.printf("%d\t", System.currentTimeMillis()-t0);
				//for(Node n : observers)
					//System.out.println(x);
					//writer1.printf("%.3f ", n.getBetweenness());
				//writer1.printf("\n");
				//GraphTools.betweennessCentrality(graph, register);
				//limitedRegister = GraphTools.allLimitedShortestPathsInGraph(graph,maxLength);
				//observers2 = GraphTools.highestBetweennessNodes(graph, size[i]);
				//for(Node n : graph.getNodesList())
				//for(Node n : observers)
					//System.out.printf("%d ", observers2.indexOf(n)+1);
				//System.out.printf("\n");
				
				//for(Node n : graph.getNodesList())
				//	n.setBetweenness(0.0);
				//t0 = System.currentTimeMillis();
				observers = GraphTools.greedyCoverageObsParallel(graph,(int)(size*budget[i]), 15);
				//writer1.printf("%d\t", System.currentTimeMillis()-t0);
				//for(Node n : observers)
					//System.out.printf("%d ", observers2.indexOf(n)+1);
				//System.out.printf("\n");

				//tstart = System.currentTimeMillis();
				//observers2 = GraphTools.greedyCollectiveBetweennessParallel(graph, limitedRegister, (int)(size[i]*budget), 10);
				//System.out.printf("%d\n", System.currentTimeMillis()-tstart);
				/*
				union = new ArrayList<Node>();
				for(Node n : observers)
					if(observers2.contains(n))
						union.add(n);
				counts = counts + union.size();
				*/
				
			}
			//System.out.println(estimateTime(System.currentTimeMillis()-tstart, r+1, realizations));
			//System.out.printf("%.2f\n", 1.0*counts/(realizations*size[i]));
			//writer1.printf("\n");
			System.out.println();
		}
		//writer1.close();
		
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}

}
