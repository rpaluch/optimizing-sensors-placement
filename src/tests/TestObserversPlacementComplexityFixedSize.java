package tests;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.PathsTools;
import graphs.PathsRegister;
import io.ExpressWriter;

public class TestObserversPlacementComplexityFixedSize {

	public static void main(String[] args) {
		
		String outputdir = args[0];
		String graphType = args[1];
		int degree = Integer.parseInt(args[2]);
		int m = degree/2;
		int size = Integer.parseInt(args[3]);
		int realizations = Integer.parseInt(args[4]);
		
		String header = "";
		double[] densityOfObservers = new double[args.length-5];
		for(int i=0; i<densityOfObservers.length; i++) {
			densityOfObservers[i] = Double.parseDouble(args[i+5]);
			header = header + "O" + args[i+5] + "\t";
		}

		String path = outputdir+"/"+graphType+"_k"+degree+"_N"+size;
		ExpressWriter writerRegister = new ExpressWriter(path+"_Register_fixedSize.txt");
		ExpressWriter writerCoverage = new ExpressWriter(path+"_Coverage_fixedSize.txt");
		ExpressWriter writerKMedian = new ExpressWriter(path+"_KMedian_fixedSize.txt");
		ExpressWriter writerHV = new ExpressWriter(path+"_HV_fixedSize.txt");
		ExpressWriter writerBC = new ExpressWriter(path+"_BC_fixedSize.txt");
		ExpressWriter writerCB = new ExpressWriter(path+"_CB_fixedSize.txt");
		writerRegister.init();
		writerRegister.println(header);
		writerCoverage.init();
		writerCoverage.println(header);
		writerKMedian.init();
		writerKMedian.println(header);
		writerHV.init();
		writerHV.println(header);
		writerBC.init();
		writerBC.println(header);
		writerCB.init();
		writerCB.println(header);
		
		Graph g;
		PathsRegister register;
		long tstartRegister = 0;
		long time = 0;
		long tstart = System.currentTimeMillis();
		for(int i=0; i<realizations; i++) {
			// Generate graph
			switch(graphType) {
				case "ba":	g = GraphGenerator.barabasiAlbert(m+2, m, size);
					    	break;
				case "er":  g = GraphGenerator.contiguousErdosRenyi(size, degree, "degree");
							break;
				case "rrg": g = GraphGenerator.regularRandomGraph(size, degree);
							while(!g.isContiguous()) g = GraphGenerator.regularRandomGraph(size, degree);
							break;
				case "sfn": g = GraphGenerator.scaleFreeNetwork(size, m, size-m, 3);
							while(!g.isContiguous()) g = GraphGenerator.scaleFreeNetwork(size, m, size-m, 3);
							break;
				default:	g = GraphGenerator.contiguousErdosRenyi(size, degree, "degree");
							break;
						
			}
			// Find all shortest paths
			tstartRegister = System.currentTimeMillis();
			register = PathsTools.allShortestPathsInGraph(g);
			writerRegister.printf("%d\t", System.currentTimeMillis()-tstartRegister);
			for(int d=0; d<densityOfObservers.length; d++) {
				System.out.printf("densityOfObservers = %.2f, status: ", densityOfObservers[d]);
				time = System.currentTimeMillis();
				// Find observers
				writerCoverage.printf("%d\t", timeOfCoverage(g, (int)(densityOfObservers[d]*size)));
				writerKMedian.printf("%d\t", timeOfKMedian(g, register, (int)(densityOfObservers[d]*size)));
				writerHV.printf("%d\t", timeOfHighVariance(g, register, (int)(densityOfObservers[d]*size)));
				writerCB.printf("%d\t", timeOfCollectiveBetweenness(g, register, (int)(densityOfObservers[d]*size)));
				writerBC.printf("%d\t", timeOfBetweennessCentrality(g, register, (int)(densityOfObservers[d]*size)));
				System.out.printf("finished in %d ms\n",System.currentTimeMillis()-time);
			}
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, i+1, realizations));
			writerRegister.printf("\n");
			writerCoverage.printf("\n");
			writerKMedian.printf("\n");
			writerHV.printf("\n");
			writerBC.printf("\n");
			writerCB.printf("\n");
		}
		writerRegister.close();
		writerCoverage.close();
		writerKMedian.close();
		writerHV.close();
		writerBC.close();
		writerCB.close();
	}
	
	public static long timeOfCoverage(Graph g, int numberOfObservers) {
		long tstart = System.currentTimeMillis();
		GraphTools.greedyCoverageObsParallel(g, numberOfObservers, 1);
		return System.currentTimeMillis()-tstart;
	}
	
	public static long timeOfKMedian(Graph g, PathsRegister register, int numberOfObservers) {
		long tstart = System.currentTimeMillis();
		GraphTools.greedyKMedianObsParallel(g, PathsTools.allDistancesInGraph(g, register), numberOfObservers, 1);
		return System.currentTimeMillis()-tstart;
	}
	
	public static long timeOfHighVariance(Graph g, PathsRegister register, int numberOfObservers) {
		long tstart = System.currentTimeMillis();
		GraphTools.greedyHVObsParallel(g, register, numberOfObservers, 3, 1);
		return System.currentTimeMillis()-tstart;
	}
	
	public static long timeOfBetweennessCentrality(Graph g, PathsRegister register, int numberOfObservers) {
		long tstart = System.currentTimeMillis();
		GraphTools.betweennessCentrality(g, register);
		GraphTools.highestBetweennessNodes(g, numberOfObservers);
		return System.currentTimeMillis()-tstart;
	}
	
	public static long timeOfCollectiveBetweenness(Graph g, PathsRegister register, int numberOfObservers) {
		long tstart = System.currentTimeMillis();
		GraphTools.fastCollectiveBetweenness(g, register, numberOfObservers);
		return System.currentTimeMillis()-tstart;
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}

}
