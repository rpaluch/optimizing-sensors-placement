package tests;

import io.ExpressWriter;
import jdistlib.Poisson;
import jdistlib.evd.GeneralizedPareto;

import java.util.ArrayList;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Node;
import graphs.NodesSelectors;
import graphs.PathsTools;

public class TestObserversPlacementOverlap {

	public static void main(String[] args) {
		
		// Read the main parameters
		String graphType = args[0];
		int size = Integer.parseInt(args[1]);
		int m = Integer.parseInt(args[2]);
		double densityOfObservers = Double.parseDouble(args[3]);
		int numberOfGraphs = Integer.parseInt(args[4]);
		int numberOfThreads = Integer.parseInt(args[5]);
				
		// Initialize variables
		long time = 0;
		int numberOfObservers = (int)(densityOfObservers*size);
		int maxLengthCB = 3;
		int maxLengthHV = 3;
		if(densityOfObservers <= 0.1) {
			maxLengthHV = 4;
			if(densityOfObservers <= 0.05) maxLengthHV = 5;
		}
		
		// Initialize observers sets
		String[] sets = {"Random","Coverage","KMedian","HVObs","CB","BC"};
		Graph graph;
		ArrayList<ArrayList<Node>> observers;
				
		// Prepare output files
		String dir = "../outputs/observers_placement_overlap/";
		ExpressWriter writer = new ExpressWriter(dir+graphType+"_N"+size+"_k"+2*m+"_O"+densityOfObservers+"_overlap.txt");
		writer.init();
		// Print header
		for(int i=0; i<sets.length-1; i++)
			for(int j=i+1; j<sets.length; j++)
				writer.printf("%s.%s\t", sets[i],sets[j]);
		writer.printf("\n");
		
		// Main loop
		long tstart = System.currentTimeMillis();
		for(int g=0; g<numberOfGraphs; g++) {
			// Generate graph
			switch(graphType) {
				case "ba":	 graph = GraphGenerator.barabasiAlbert(m+2, m, size);
						     break;
				case "er":   graph = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
							 break;
				case "rrg":  graph = GraphGenerator.regularRandomGraph(size, 2*m);
							 GraphTools.findComponents(graph);
							 GraphTools.connectAllComponents(graph);
							 break;
				case "sfn":  graph = GraphGenerator.scaleFreeNetwork(size, m, size-m, 3);
							 GraphTools.findComponents(graph);
							 GraphTools.connectAllComponents(graph);
							 break;
				case "pois": graph = GraphGenerator.configurationModelWithClustering(GraphGenerator.degreeSequence(new Poisson(2*m), size), 0.75);
							 GraphTools.findComponents(graph);
							 GraphTools.connectAllComponents(graph);
							 break;
				case "par":  graph = GraphGenerator.configurationModelWithClustering(GraphGenerator.degreeSequence(new GeneralizedPareto(m,m/2.0,0.5), size), 0.75);
							 GraphTools.findComponents(graph);
							 GraphTools.connectAllComponents(graph);
							 break;
				default:	 graph = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
							 break;				
			}
			// Prepare observers sets
			observers = new ArrayList<ArrayList<Node>>();		
			// (0) Random observers
			observers.add(graph.getRandomNodes(numberOfObservers));
			// (1) Coverage observers
			time = System.currentTimeMillis();
			observers.add(NodesSelectors.greedyCoverageObsParallel(graph, numberOfObservers, numberOfThreads));
			System.out.printf("Coverage observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
			// (2) KMedian observers
			time = System.currentTimeMillis();
			observers.add(NodesSelectors.greedyKMedianObsParallel(graph, PathsTools.allDistancesInGraph(graph), numberOfObservers, numberOfThreads));
			System.out.printf("KMedian observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
			// (3) HVObs observers
			time = System.currentTimeMillis();
			observers.add(NodesSelectors.greedyHVObsParallel(graph, numberOfObservers, maxLengthHV, numberOfThreads));
			System.out.printf("HVObs observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
			// (4) CB3 observers
			time = System.currentTimeMillis();
			observers.add(NodesSelectors.fastCollectiveBetweenness(graph, numberOfObservers, maxLengthCB));
			System.out.printf("CB3 observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
			// (5) BC observers
			time = System.currentTimeMillis();
			GraphTools.computeBetweennessCentrality(graph);
			observers.add(GraphTools.highestBetweennessNodes(graph, numberOfObservers));
			System.out.printf("BC observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
			
			for(int i=0; i<sets.length-1; i++)
				for(int j=i+1; j<sets.length; j++)
					writer.printf("%.3f\t", 1.0*NodesSelectors.intersection(observers.get(i), observers.get(j)).size()/observers.get(i).size());
			writer.printf("\n");
			
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, g+1, numberOfGraphs));
			
		}
		writer.close();
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}

}
