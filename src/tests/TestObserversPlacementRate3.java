package tests;

import java.util.ArrayList;

import tracking.ParallelPinto2;
import tracking.GradientPinto2;
import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Node;
import graphs.NodesSelectors;
import io.ExpressWriter;
import dynamics.SusceptibleInfected;

public class TestObserversPlacementRate3 {

	public static void main(String[] args) {
		
		String graphType = args[0];
		double densityOfObservers = Double.parseDouble(args[1]);
		int numberOfGraphs = Integer.parseInt(args[2]);
		int numberOfTestsPerGraph = Integer.parseInt(args[3]);
		int numberOfThreads = Integer.parseInt(args[4]);
		int part = Integer.parseInt(args[5]);
		
		//String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/observers_placement/pinto_ba_N100_k6_beta0.5_";
		String dir1 = "../outputs/observers_placement_ptva_"+graphType+"/ptva_"+graphType+"_N1000_k8_O"+densityOfObservers+"_part"+part;
		String dir2 = "../outputs/observers_placement_gmla_"+graphType+"/gmla_"+graphType+"_N1000_k8_O"+densityOfObservers+"_part"+part;

		ExpressWriter accuracy_hv = new ExpressWriter(dir1+"_HVObs_accuracy.txt");
		ExpressWriter rank_hv = new ExpressWriter(dir1+"_HVObs_rank.txt");
		ExpressWriter distance_hv = new ExpressWriter(dir1+"_HVObs_distance.txt");
		
		ExpressWriter accuracy_gradient_hv = new ExpressWriter(dir2+"_HVObs_accuracy.txt");
		ExpressWriter rank_gradient_hv = new ExpressWriter(dir2+"_HVObs_rank.txt");
		ExpressWriter distance_gradient_hv = new ExpressWriter(dir2+"_HVObs_distance.txt");
		
		
		String header = "sigma0.2\tsigma0.25\tsigma0.3\tsigma0.35\tsigma0.4\tsigma0.45\tsigma0.5\tsigma0.55\tsigma0.6\tsigma0.65\tsigma0.7\tsigma0.75\tsigma0.8\tsigma0.85\tsigma0.9\tsigma0.95";
		
		long tstart;
		
		Graph g;
		int size = 1000;
		int m = 4;
		int realizations = numberOfGraphs*numberOfTestsPerGraph;
		
		SusceptibleInfected si;
		double[] sigma = {0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95};
		double rate = 0;
		int numberOfObservers = (int)(densityOfObservers*size);
		int maxLength = 3;
		if(densityOfObservers <= 0.1) {
			maxLength = 4;
			if(densityOfObservers <= 0.05) maxLength = 5;
		}
		
		ArrayList<Node> sources;
		ArrayList<Node> observersHV;

		ParallelPinto2 ptva = null;
		GradientPinto2 gmla = null;
		int numberOfNearestObservers = 20;
		ArrayList<Integer> supposedSourcesId;
		
		accuracy_hv.init();	
		rank_hv.init();
		distance_hv.init();
		accuracy_hv.println(header);
		rank_hv.println(header);
		distance_hv.println(header);
		
		accuracy_gradient_hv.init();	
		rank_gradient_hv.init();
		distance_gradient_hv.init();
		accuracy_gradient_hv.println(header);
		rank_gradient_hv.println(header);
		distance_gradient_hv.println(header);
		
		tstart = System.currentTimeMillis();
		int counter = 0;
		for(int n=0; n<numberOfGraphs; n++) {
			
			// Generate graph
			switch(graphType) {
				case "ba":	g = GraphGenerator.barabasiAlbert(m+2, m, size);
						    break;
				case "er":  g = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
							break;
				case "rrg": g = GraphGenerator.regularRandomGraph(size, 2*m);
							while(!g.isContiguous()) g = GraphGenerator.regularRandomGraph(size, 2*m);
							break;
				case "sfn": g = GraphGenerator.scaleFreeNetwork(size, m, size-m, 3);
							while(!g.isContiguous()) g = GraphGenerator.scaleFreeNetwork(size, m, size-m, 3);
							break;
				default:	g = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
							break;
							
			}
			
			sources = g.getRandomNodes(numberOfTestsPerGraph);
						
			observersHV = NodesSelectors.greedyHVObsParallel(g, numberOfObservers, maxLength, numberOfThreads);
			System.out.println("observersHV prepared.");
						
			for(Node source : sources) {
				
						for(int i=0; i<sigma.length; i++) {
							
							rate = 1-sigma[i]*sigma[i];
									
							// Run propagation with all nodes as observers
							si = new SusceptibleInfected(g);
							si.setObservers(observersHV);
							si.setOneSource(source);
							si.setInfectionRate(rate);
							si.runUntilAllObserversInfected();
							
							// PTVA with HVObs
							ptva = new ParallelPinto2(g);
							ptva.setSupposedSources(g.getNodesList());
							ptva.setObservers(observersHV);
							ptva.setDelays(si.getInfectionTimeAsDouble());
							ptva.setDelayMean(1/rate);
							ptva.setDelayVariance((1-rate)/(rate*rate));
							ptva.setNumberOfThreads(numberOfThreads);
							ptva.estimateScoresForNodes();
							supposedSourcesId = ptva.findNodesWithHighestScores();
							if(supposedSourcesId.contains(source.getId())) accuracy_hv.printf("%.3f\t", 1.0/supposedSourcesId.size());
							else accuracy_hv.print("0.000\t");
							rank_hv.printf("%d\t", ptva.findRankOfNode(source.getId()));
							distance_hv.printf("%.3f\t", GraphTools.shortestPathLength(g, source.getId(), supposedSourcesId));
							
							// GMLA with HVObs
							gmla = new GradientPinto2(g);
							gmla.setObservers(observersHV);
							gmla.setDelays(si.getInfectionTimeAsDouble());
							gmla.setDelayMean(1/rate);
							gmla.setDelayVariance((1-rate)/(rate*rate));
							gmla.setNumberOfNearestObservers(numberOfNearestObservers);
							gmla.setMaxThreads(numberOfThreads);
							gmla.estimateScoresForNodes();
							supposedSourcesId = gmla.findNodesWithHighestScores();
							if(supposedSourcesId.contains(source.getId())) accuracy_gradient_hv.printf("%.3f\t", 1.0/supposedSourcesId.size());
							else accuracy_gradient_hv.print("0.000\t");
							rank_gradient_hv.printf("%d\t", gmla.findRankOfNode(source.getId()));
							distance_gradient_hv.printf("%.3f\t", GraphTools.shortestPathLength(g, source.getId(), supposedSourcesId));
						}
						counter = counter + 1;
						System.out.println(estimateTime(System.currentTimeMillis()-tstart, counter, realizations));
						accuracy_hv.printf("\n");
						rank_hv.printf("\n");
						distance_hv.printf("\n");
						accuracy_gradient_hv.printf("\n");
						rank_gradient_hv.printf("\n");
						distance_gradient_hv.printf("\n");
				}
				
		}
		accuracy_hv.close();
		rank_hv.close();
		distance_hv.close();
		accuracy_gradient_hv.close();
		rank_gradient_hv.close();
		distance_gradient_hv.close();
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}
}
