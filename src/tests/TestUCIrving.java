package tests;

import io.GraphReader;
import io.MultiExpressWriter;
import tracking.SingleSourceDetector;

import java.util.ArrayList;

import dynamics.SusceptibleInfected;
import graphs.Graph;

public class TestUCIrving {

	public static void main(String[] args) {
		
		GraphReader reader = new GraphReader("/home/rpaluch/Workspace/ReverseEngineering/data/UC_Irving_messages/UCIrving.net");
		Graph ucirving = reader.readGraphFromPajek();
		//ucirving.printStats();
		
		double densityOfObservers = 0.2;
		double rate = 0.5;
		int[] nearestObservers = {5,8,10,12,14,16,18,20,22,24,26,28,30,35,40};
		String header = "5\t8\t10\t12\t14\t16\t18\t20\t22\t24\t26\t28\t30\t35\t40";
		int realizations = 2000;		
		SusceptibleInfected si;
		int sourceID;
		SingleSourceDetector detector;
		ArrayList<Integer> supposedSourcesId;
		
		String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/ucirginv_tests/ucirving_O"+densityOfObservers+"_rate0.5";
		String[] files = {dir+"_accuracy.txt",
						  dir+"_rank.txt"};
		String[] names = {"accuracy","rank"};
		MultiExpressWriter writer = new MultiExpressWriter(files, names);
		writer.initAll();
		writer.printlnAll(header);
		long tstart = System.currentTimeMillis();
		for(int i=0; i<realizations; i++) {
			
			si = new SusceptibleInfected(ucirving);
			si.setObservers(ucirving.getRandomNodes((int)(densityOfObservers*ucirving.getNumberOfNodes())));
			si.setInfectionRate(rate);
			si.runUntilAllObserversInfected();
			sourceID = si.getSources().get(0).getId();
						
			for(int n=0; n<nearestObservers.length; n++) {
				
				detector = new SingleSourceDetector(ucirving);
				detector.setObservers(si.getObservers());
				detector.setDelays(si.getInfectionTimeAsDouble());
				detector.setDelayMean(1/rate);
				detector.setDelayVariance((1-rate)/(rate*rate));
				detector.setMaxThreads(10);
				//detector.setSupposedSources(g.getNodesList());
				detector.setNumberOfGradientWalks(1);
				detector.setNumberOfNearestObservers(nearestObservers[n]);
				detector.computeScores("LPTV");
				supposedSourcesId = detector.findNodesWithHighestScores();
				if(supposedSourcesId.contains(sourceID)) writer.printf("accuracy","%.3f\t",1.0/supposedSourcesId.size());
				else writer.printf("accuracy","0.000\t");
				writer.printf("rank","%d\t",detector.findRankOfNode(sourceID));
				
			}
			writer.newLineAll();
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, i+1, realizations));
		}
		writer.closeAll();	
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}

}
