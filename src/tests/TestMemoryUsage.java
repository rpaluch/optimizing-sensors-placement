package tests;

import io.MemoryUsage;

public class TestMemoryUsage {

	public static void main(String[] args) {
		
		int size = 100000000;
		long mega = 1024L * 1024L;
		
		System.out.printf("Integer: %d\n", Integer.BYTES);
		System.out.printf("Long: %d\n", Long.BYTES);
		System.out.printf("Double: %d\n", Double.BYTES);
		
		MemoryUsage mem = new MemoryUsage();
		mem.start();
		long measurement = Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		int[] inty = new int[size];
		for(int i=0; i<inty.length; i++) inty[i]=i;
		System.out.printf("int[100kk]: %d\n", (Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()-measurement)/mega);
		mem.stopMeasurement();
		//System.out.printf("int[100kk]: %d\n", mem.getDiffMemoryUsage());
		
		mem = new MemoryUsage();
		mem.start();
		measurement = Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		double[] dable = new double[size];
		for(int i=0; i<dable.length; i++) dable[i]=0.5*i;
		System.out.printf("dable[100kk]: %d\n", (Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()-measurement)/mega);
		mem.stopMeasurement();
		//System.out.printf("double[100kk]: %d\n", mem.getDiffMemoryUsage());
		
		mem = new MemoryUsage();
		mem.start();
		measurement = Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		Integer[] inty2 = new Integer[size];
		System.out.printf("Integer[100kk]: %d\n", (Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()-measurement)/mega);
		mem.stopMeasurement();
		//System.out.printf("Integer[100kk]: %d\n", mem.getDiffMemoryUsage());
		
		mem = new MemoryUsage();
		mem.start();
		measurement = Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		Double[] dable2 = new Double[size];
		System.out.printf("Double[100kk]: %d\n", (Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()-measurement)/mega);
		mem.stopMeasurement();
		//System.out.printf("Double[100kk]: %d\n", mem.getDiffMemoryUsage());
	}

}
