package tests;

import io.ExpressWriter;

import java.util.ArrayList;
import java.util.LinkedList;

import tracking.Observer;
import dynamics.SusceptibleInfected;
import graphs.Graph;
import graphs.GraphTools;
import graphs.Node;
import graphs.PathsTools;
import graphs.GraphGenerator;

public class TestSusceptibleInfected2 {

	public static void main(String[] args) {
		
		ExpressWriter writer1 = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/diffusion_studies/er_N1000_k6_O0.4_beta0.5_delayserrors.txt");
		//ExpressWriter writer2 = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/bfs_similarity/ba_N1000_k6_O0.2_bfs_similarity.txt");
		int realizations = 1000;
		int size = 1000;
		int m = 3;
		int numberOfObservers = 400;
		double rate = 0.5;
		//double[] rate = {0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9};
		//String header = "beta0.2\tbeta0.3\tbeta0.4\tbeta0.5\tbeta0.6\tbeta0.7\tbeta0.8\tbeta0.9";
		ArrayList<Observer> observers;
		ArrayList<Integer> observersIds;
		Node source;
		//Graph limitedCascade;
		Graph g, bfs;
		SusceptibleInfected si;
		ArrayList<LinkedList<Node>> allPaths;
		int sourceToReferenceDistance;
		
		writer1.init();
		//writer2.init();
		//writer1.println(header);
		//writer2.println(header);
		
		long tstart = System.currentTimeMillis();
		for(int r=0; r<realizations; r++){
			//for(int b=0; b<rate.length; b++){
				
				g = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
				g.printStats();
				
				si = new SusceptibleInfected(g);
				si.setInfectionRate(rate);
				si.addRandomObservers(numberOfObservers);
				si.runUntilAllObserversInfected();
				
				source = si.getSources().get(0);
				observers = si.getInfectedObservers();				
				observersIds = new ArrayList<Integer>();
				for(int i=0; i<numberOfObservers; i++)
					observersIds.add(observers.get(i).getNode().getId());
				
				//limitedCascade = GraphTools.findBFSTree(si.getCascade(), source.getId(), observersIds);
				bfs = GraphTools.findBFSTree(g, source.getId(), observersIds);
				allPaths = PathsTools.shortestPaths(bfs, source.getId(), observersIds);
				sourceToReferenceDistance = allPaths.get(0).size()-1;
				
				//writer1.printf("%.3f\t", GraphTools.graphsSimilarity(limitedCascade, bfs));
				
				for(int i=0; i<observers.size()-1; i++) 
					writer1.printf("%.1f\n", (1/rate)*(allPaths.get(i+1).size()-1-sourceToReferenceDistance)-(observers.get(i+1).getDelay()-observers.get(0).getDelay()));
			//}
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, r+1, realizations));
			//writer1.printf("\n");
		}
		writer1.close();
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d", currentReal, 
																					   totalReal, 
																					   0.001*elapsed, 
																					   0.001*elapsed/3600,
																					   hour, 
																					   minute, 
																					   second);
	}

}
