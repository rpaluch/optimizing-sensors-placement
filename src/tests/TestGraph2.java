package tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import graphs.Graph;
import graphs.GraphTools;
import graphs.Node;
import graphs.PathsTools;

public class TestGraph2 {

	public static void main(String[] args) {
		
		Graph g = new Graph();
		g.addNode(1);
		g.addNode(2);
		Node n = new Node(3);
		g.addNode(n);
		g.addLink(1, 2);
		g.addLink(1, 3);
		g.addLink(3, 4);
		g.addLink(4, 3);
		//g.printAll();
		for(int i=0; i<g.getNumberOfNodes(); i++){
			int id = g.getNodesList().get(i).getId();
			int k = g.getNodesList().get(i).getDegree();
			System.out.printf("%d: %d\n",id,k);
		}
		LinkedList<Node> path = PathsTools.shortestPath(g, 1, 4);
		for(Node v : path) v.print();
		g.addNode(1);
		g.addNode(1);
		g.addNode(2);
		for(Node s : g.getNodesList())
			System.out.printf("%d-%d\n", s.getIndex(), s.getId());
		
		ArrayList<Node> alist = new ArrayList<Node>();
		Collections.shuffle(alist);
		
	}

}
