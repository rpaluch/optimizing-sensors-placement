package tests;

import java.util.ArrayList;

import dynamics.SusceptibleInfected;
import graphs.Graph;
import graphs.GraphGenerator;
import graphs.Node;
import io.ExpressWriter;
import tracking.SingleSourceDetector;

public class TestSourceDetectionLineGraph {

	public static void main(String[] args) {

		int realizations = 10000;
		Graph lineGraph = GraphGenerator.lineGraph(21);
		int sourceId = 11;
		int observerOneId = 9;
		int observerTwoId = 13;
		SusceptibleInfected si;
		ArrayList<Node> observers;
		SingleSourceDetector detector;
		ArrayList<Integer> supposedSourcesId;
		double rate = 0.3;
		
		String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/lineGraph/";
		ExpressWriter writer = new ExpressWriter(dir+"ptva_lineGraph_N21_S"+sourceId+"_Oone"+observerOneId+"_Otwo"+observerTwoId+"_rate"+rate+".txt");
		writer.init();
		writer.println("0\t1\t2\t3\t4\t5\t6\t7\t8");
		
		for(int i=0; i<realizations; i++) {
			
			si = new SusceptibleInfected(lineGraph);
			si.setObservers(lineGraph.getNodesList());
			si.setOneSource(lineGraph.getNode(sourceId));
			si.setInfectionRate(rate);
			si.runUntilAllObserversInfected();
			
			for(int l=0; l<9; l++) {
				
				observers = new ArrayList<Node>();
				observers.add(lineGraph.getNode(observerOneId));
				observers.add(lineGraph.getNode(observerTwoId));
				if(l>0) observers.add(lineGraph.getNode(observerTwoId+l));
				
				detector = new SingleSourceDetector(lineGraph);
				detector.setObservers(observers);
				detector.setDelays(si.getInfectionTimesForGivenObservers(observers));
				detector.setDelayMean(1/rate);
				detector.setDelayVariance((1-rate)/(rate*rate));
				detector.setMaxThreads(1);
				detector.setSupposedSources(lineGraph.getNodesList());
				detector.computeScores("LPTV");
				supposedSourcesId = detector.findNodesWithHighestScores();
				if(supposedSourcesId.contains(sourceId)) 
					writer.printf("%.3f\t",1.0/supposedSourcesId.size());
				else writer.printf("0.000\t");
			}
			writer.printf("\n");
		}
		writer.close();
	}

}
