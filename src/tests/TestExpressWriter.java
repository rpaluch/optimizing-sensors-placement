package tests;

import io.ExpressWriter;
import io.MultiExpressWriter;

public class TestExpressWriter {

	public static void main(String[] args) {
		
		String dir = "/home/rpaluch/Workspace/ReverseEngineering/";
		
		String[] names = {"memoryCB3","memoryCB4","memoryCB5","memoryCB"};
		
		String[] paths = {dir+"CB3_memory.txt",
						  dir+"CB4_memory.txt",
						  dir+"CB5_memory.txt",
						  dir+"CB_memory.txt"};
						  
		MultiExpressWriter mwriter = new MultiExpressWriter(paths, names);
		mwriter.initAll();
		mwriter.printlnAll("header");
		mwriter.newLineAll();
		mwriter.printfAll("%.2f\n", 3.14152);
		mwriter.println("memoryCB4","line 4");
		mwriter.close(0);
		mwriter.close(1);
		mwriter.close(2);
		mwriter.printf("memoryCB", "only CB_memory.txt");
		mwriter.close("memoryCB");

	}
	
	public static void init(ExpressWriter[] writers) {
		for(int i=0; i<writers.length; i++) writers[i].init();
	}
	
	public static void close(ExpressWriter[] writers) {
		for(int i=0; i<writers.length; i++) writers[i].close();
	}
	
	public static void newline(ExpressWriter[] writers) {
		for(int i=0; i<writers.length; i++) writers[i].printf("\n");
	}
	
	public static void println(ExpressWriter[] writers, String line) {
		for(int i=0; i<writers.length; i++) writers[i].println(line);
	}

}
