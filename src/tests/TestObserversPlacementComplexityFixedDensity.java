package tests;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.NodesSelectors;
import graphs.PathsTools;
import io.ExpressWriter;

public class TestObserversPlacementComplexityFixedDensity {

	public static void main(String[] args) {
		
		String outputdir = args[0];
		String graphType = args[1];
		int degree = Integer.parseInt(args[2]);
		int m = degree/2;
		double densityOfObservers = Double.parseDouble(args[3]);
		int realizations = Integer.parseInt(args[4]);
		int numberOfThreads = Integer.parseInt(args[5]);
		
		String header = "";
		int[] size = new int[args.length-6];
		for(int i=0; i<size.length; i++) {
			size[i] = Integer.parseInt(args[i+6]);
			header = header + args[i+6] + "\t";
		}

		String path = outputdir+"/"+graphType+"_k"+degree+"_O"+densityOfObservers;
		ExpressWriter writerCoverage = new ExpressWriter(path+"_Coverage_fixedDensity.txt");
		ExpressWriter writerKMedian = new ExpressWriter(path+"_KMedian_fixedDensity.txt");
		ExpressWriter writerHV = new ExpressWriter(path+"_HVObs_fixedDensity.txt");
		ExpressWriter writerBC = new ExpressWriter(path+"_BC_fixedDensity.txt");
		ExpressWriter writerCB = new ExpressWriter(path+"_CB3_fixedDensity.txt");
		writerCoverage.init();
		writerCoverage.println(header);
		writerKMedian.init();
		writerKMedian.println(header);
		writerHV.init();
		writerHV.println(header);
		writerBC.init();
		writerBC.println(header);
		writerCB.init();
		writerCB.println(header);
		
		Graph g;
		long time = 0;
		long tstart = System.currentTimeMillis();
		for(int i=0; i<realizations; i++) {
			for(int s=0; s<size.length; s++) {
				System.out.printf("\nN = %d, status:\n", size[s]);
				time = System.currentTimeMillis();
				// Generate graph
				switch(graphType) {
					case "ba":	g = GraphGenerator.barabasiAlbert(m+2, m, size[s]);
						    	break;
					case "er":  g = GraphGenerator.contiguousErdosRenyi(size[s], degree, "degree");
								break;
					case "rrg": g = GraphGenerator.regularRandomGraph(size[s], degree);
								while(!g.isContiguous()) g = GraphGenerator.regularRandomGraph(size[s], degree);
								break;
					case "sfn": g = GraphGenerator.scaleFreeNetwork(size[s], m, size[s]-m, 3);
								while(!g.isContiguous()) g = GraphGenerator.scaleFreeNetwork(size[s], m, size[s]-m, 3);
								break;
					default:	g = GraphGenerator.contiguousErdosRenyi(size[s], degree, "degree");
								break;
							
				}
				// Find observers
				writerCoverage.printf("%d\t", timeOfCoverage(g, (int)(densityOfObservers*size[s]), numberOfThreads));
				System.out.printf("Coverage ready. Elapsed %d ms.\n",System.currentTimeMillis()-time);
				writerCB.printf("%d\t", timeOfCollectiveBetweenness(g, (int)(densityOfObservers*size[s])));
				System.out.printf("CB3 ready. Elapsed %d ms.\n",System.currentTimeMillis()-time);
				writerBC.printf("%d\t", timeOfBetweennessCentrality(g, (int)(densityOfObservers*size[s])));
				System.out.printf("BC ready. Elapsed %d ms.\n",System.currentTimeMillis()-time);
				if(size[s]<=3000) {
					writerKMedian.printf("%d\t", timeOfKMedian(g, (int)(densityOfObservers*size[s]), numberOfThreads));
					System.out.printf("KMedian ready. Elapsed %d ms.\n",System.currentTimeMillis()-time);
					writerHV.printf("%d\t", timeOfHighVariance(g, (int)(densityOfObservers*size[s]), numberOfThreads));
					System.out.printf("HVObs ready. Elapsed %d ms.\n",System.currentTimeMillis()-time);
				}
				System.out.printf("N = %d finished in %d ms\n",size[s],System.currentTimeMillis()-time);
			}
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, i+1, realizations));
			writerCoverage.printf("\n");
			writerKMedian.printf("\n");
			writerHV.printf("\n");
			writerBC.printf("\n");
			writerCB.printf("\n");
		}
		writerCoverage.close();
		writerKMedian.close();
		writerHV.close();
		writerBC.close();
		writerCB.close();
	}
	
	public static long timeOfCoverage(Graph g, int numberOfObservers, int numberOfThreads) {
		long tstart = System.currentTimeMillis();
		NodesSelectors.greedyCoverageObsParallel(g, numberOfObservers, numberOfThreads);
		return System.currentTimeMillis()-tstart;
	}
	
	public static long timeOfKMedian(Graph g, int numberOfObservers, int numberOfThreads) {
		long tstart = System.currentTimeMillis();
		NodesSelectors.greedyKMedianObsParallel(g, PathsTools.allDistancesInGraph(g), numberOfObservers, numberOfThreads);
		return System.currentTimeMillis()-tstart;
	}
	
	public static long timeOfHighVariance(Graph g, int numberOfObservers, int numberOfThreads) {
		long tstart = System.currentTimeMillis();
		NodesSelectors.greedyHVObsParallel(g, numberOfObservers, 3, numberOfThreads);
		return System.currentTimeMillis()-tstart;
	}
	
	public static long timeOfBetweennessCentrality(Graph g, int numberOfObservers) {
		long tstart = System.currentTimeMillis();
		GraphTools.computeBetweennessCentrality(g);
		GraphTools.highestBetweennessNodes(g, numberOfObservers);
		return System.currentTimeMillis()-tstart;
	}
	
	public static long timeOfCollectiveBetweenness(Graph g, int numberOfObservers) {
		long tstart = System.currentTimeMillis();
		NodesSelectors.fastCollectiveBetweenness(g, numberOfObservers, 3);
		return System.currentTimeMillis()-tstart;
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}

}
