package tests;
import io.ExpressWriter;
import jdistlib.Normal;
import jdistlib.generic.GenericDistribution;

public class TestGenericDistribution {

	public static void main(String[] args) {
		
		int realizations = 10000;
		double min = 0L;
		double mu = 4;
		double sd = mu/1.41;
		GenericDistribution distribution = new Normal(mu,sd);

		ExpressWriter writer = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/testGenericDist/normal_mu"+mu+"_v2.txt");
		writer.init();
		
		double rn = min - 1;
		for(int ii=0; ii<realizations; ii++) {
			rn = min - 1;
			while(rn<min) rn = distribution.random();
			writer.printf("%f\n", rn);
		}
	}

}
