package tests;

import java.util.ArrayList;
import java.util.LinkedList;

import dynamics.GenericSpreading;
import graphs.Graph;
import graphs.GraphGenerator;
import graphs.Node;
import graphs.PathsTools;
import io.GraphWriter;
import jdistlib.Levy;
import jdistlib.generic.GenericDistribution;

public class TestGenericSpreading {

	public static void main(String[] args) {
		
		int size = 6;
		int edges = 10;
		double m = 0.0;
		double s = 3;
		
		Graph graph = GraphGenerator.contiguousErdosRenyi(size, edges);
		GenericDistribution distribution = new Levy(m,s);
		GenericSpreading spreading = new GenericSpreading(graph, distribution);
		spreading.addObservers(graph.getNodesList());
		spreading.addRandomSources(1);
		spreading.run();
		
		GraphWriter gwrite = new GraphWriter(graph);
		gwrite.exportEdgeListWithWeights("/home/rpaluch/Workspace/R/levy/er.net");
		
		System.out.printf("Source is the node %d\nPaths:\n", spreading.getSources().get(0).getId());
		ArrayList<Integer> targetsID = new ArrayList<Integer>();
		for(Node n : graph.getNodesList())
			targetsID.add(n.getId());
		ArrayList<LinkedList<Integer>> paths = PathsTools.shortestWeightedPaths(graph, spreading.getSources().get(0).getId(), targetsID);
		System.out.printf("%d %d %d\n", graph.getNodesList().size(), targetsID.size(), paths.size());
		for(LinkedList<Integer> list : paths) {
			for(Integer i : list)
				System.out.printf("%d ", i);
			System.out.println();
		}
		
		for(int i=0; i<size; i++)
			System.out.printf("Node %d - %.3f\n", spreading.getObservers().get(i).getId(), spreading.getInfectionTimes().get(i));
		
	}

}
