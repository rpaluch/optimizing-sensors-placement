package tests;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.Link;
import graphs.Node;
import graphs.Path;
import graphs.PathsTools;
import graphs.WeightedPath;

public class TestPathsTools {

	public static void main(String[] args) {
		
		Graph g = GraphGenerator.randomTree(20, 1);
		Random rand = new Random();
		for(Link l : g.getLinksList()) {
			l.setWeight(rand.nextDouble());
			//l.printWeight();
		}
		
		int sourceId = 5;
		ArrayList<LinkedList<Node>> paths1 = PathsTools.shortestWeightedPaths(g, sourceId);
		
		Path path1 = new Path(paths1.get(0));
		Path path2 = new Path(paths1.get(10));
		path1 = PathsTools.reverse(path1);
		System.out.println(path1.toString());
		System.out.println(path2.toString());
		
		Path merged = PathsTools.mergeWithoutOverlap(path1,path2);
		System.out.println(merged.toString());
		
		System.out.println();
		WeightedPath wp1 = new WeightedPath();
		wp1.add(new Node(1));
		wp1.add(new Node(2), 0.5);
		//wp1.add(new Node(3), 1.5);
		//wp1.add(new Node(4), 1.0);
		System.out.println(wp1.toString());
		System.out.println();
		
		WeightedPath wp2 = new WeightedPath();
		wp2.add(wp1.getNode(1));
		wp2.add(wp1.getNode(0), 0.5);
		wp2.add(new Node(5), 1.2);
		wp2.add(new Node(6), 0.7);
		System.out.println(wp2.toString());
			
		WeightedPath wp3 = PathsTools.mergeWithoutOverlap(wp1, wp2);
		System.out.println(wp3.toString());
	}

}
