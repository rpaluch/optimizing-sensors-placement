package tests;

import java.util.ArrayList;

import tracking.ParallelPinto2;
import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Node;
import graphs.PathsTools;
import graphs.PathsRegister;
import io.ExpressWriter;
import dynamics.SusceptibleInfected;

public class TestObserversPlacementRate2 {

	public static void main(String[] args) {
		
		String graphType = args[0];
		double densityOfObservers = Double.parseDouble(args[1]);
		int numberOfGraphs = Integer.parseInt(args[2]);
		int numberOfTestsPerGraph = Integer.parseInt(args[3]);
		int numberOfThreads = Integer.parseInt(args[4]);
		int part = Integer.parseInt(args[5]);
		
		//String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/observers_placement/pinto_ba_N100_k6_beta0.5_";
		String dir = "../outputs/observers_placement_ptva_"+graphType+"/ptva_"+graphType+"_N500_k6_O"+densityOfObservers+"_part"+part;
		ExpressWriter accuracy_cb4 = new ExpressWriter(dir+"_CB4_accuracy.txt");
		ExpressWriter rank_cb4 = new ExpressWriter(dir+"_CB4_rank.txt");
		ExpressWriter time_cb4 = new ExpressWriter(dir+"_CB4_time.txt");
		ExpressWriter accuracy_cb3 = new ExpressWriter(dir+"_CB3_accuracy.txt");
		ExpressWriter rank_cb3 = new ExpressWriter(dir+"_CB3_rank.txt");
		ExpressWriter time_cb3 = new ExpressWriter(dir+"_CB3_time.txt");
		ExpressWriter accuracy_bc = new ExpressWriter(dir+"_BC_accuracy.txt");
		ExpressWriter rank_bc = new ExpressWriter(dir+"_BC_rank.txt");
		ExpressWriter accuracy_cb = new ExpressWriter(dir+"_CB_accuracy.txt");
		ExpressWriter rank_cb = new ExpressWriter(dir+"_CB_rank.txt");
		ExpressWriter time_cb = new ExpressWriter(dir+"_CB_time.txt");
		
		
		String header = "sigma0.2\tsigma0.25\tsigma0.3\tsigma0.35\tsigma0.4\tsigma0.45\tsigma0.5\tsigma0.55\tsigma0.6\tsigma0.65\tsigma0.7\tsigma0.75\tsigma0.8\tsigma0.85\tsigma0.9\tsigma0.95";
		
		long tstart, time;
		
		Graph g;
		int size = 500;
		int m = 3;
		int realizations = numberOfGraphs*numberOfTestsPerGraph;
		
		PathsRegister register;
		PathsRegister register3;
		PathsRegister register4;
		SusceptibleInfected si;
		double[] sigma = {0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95};
		double rate = 0;
		int numberOfObservers = (int)(densityOfObservers*size);
		
		ArrayList<Node> sources;
		ArrayList<Node> observersBC;
		ArrayList<Node> observersCB;
		ArrayList<Node> observersCB3;
		ArrayList<Node> observersCB4;

		ParallelPinto2 ptva = null;
		ArrayList<Integer> supposedSourcesId;
				
		accuracy_cb4.init();	
		rank_cb4.init();
		time_cb4.init();
		accuracy_cb4.println(header);
		rank_cb4.println(header);
		
		accuracy_cb3.init();	
		rank_cb3.init();
		time_cb3.init();
		accuracy_cb3.println(header);
		rank_cb3.println(header);
		
		accuracy_bc.init();	
		rank_bc.init();
		accuracy_bc.println(header);
		rank_bc.println(header);
		
		accuracy_cb.init();	
		rank_cb.init();
		time_cb.init();
		accuracy_cb.println(header);
		rank_cb.println(header);
		
		tstart = System.currentTimeMillis();
		int counter = 0;
		for(int n=0; n<numberOfGraphs; n++) {
			
			// Generate graph
			switch(graphType) {
				case "ba":	g = GraphGenerator.barabasiAlbert(m+2, m, size);
						    break;
				case "er":  g = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
							break;
				case "rrg": g = GraphGenerator.regularRandomGraph(size, 2*m);
							while(!g.isContiguous()) g = GraphGenerator.regularRandomGraph(size, 2*m);
							break;
				case "sfn": g = GraphGenerator.scaleFreeNetwork(size, m, size-m, 3);
							while(!g.isContiguous()) g = GraphGenerator.scaleFreeNetwork(size, m, size-m, 3);
							break;
				default:	g = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
							break;
							
			}
			
			sources = g.getRandomNodes(numberOfTestsPerGraph);
			
			// Find all limited (maxLenght=3) shortest paths
			time = System.currentTimeMillis();
			register3 = PathsTools.allLimitedShortestPathsInGraph(g,3);
			System.out.println("All limited (3) shortest paths founded.");
			observersCB3 = GraphTools.fastCollectiveBetweenness(g, register3, numberOfObservers);
			time_cb3.printf("%d\n", System.currentTimeMillis()-time);
			System.out.println("observersCB3 prepared.");
			
			// Find all limited (maxLength=4) shortest paths
			time = System.currentTimeMillis();
			register4 = PathsTools.allLimitedShortestPathsInGraph(g,4);
			System.out.println("All limited (4) shortest paths founded.");
			observersCB4 = GraphTools.fastCollectiveBetweenness(g, register4, numberOfObservers);
			time_cb4.printf("%d\n", System.currentTimeMillis()-time);
			System.out.println("observersCB3 prepared.");
			
			// Find all shortest paths
			time = System.currentTimeMillis();
			register = PathsTools.allShortestPathsInGraph(g);
			System.out.println("All shortest paths founded.");
			observersCB = GraphTools.fastCollectiveBetweenness(g, register, numberOfObservers);
			time_cb.printf("%d\n", System.currentTimeMillis()-time);
			System.out.println("observersCB prepared.");
			// Compute the betweenness centrality
			GraphTools.betweennessCentrality(g, register);
			observersBC = GraphTools.highestBetweennessNodes(g, numberOfObservers);
			System.out.println("observersBC prepared.");
			
			//System.out.printf("Finding optimal observers took %.0f s.\n", 0.001*(System.currentTimeMillis()-tstart));
			
			for(Node source : sources) {
				
						for(int i=0; i<sigma.length; i++) {
							
							rate = 1-sigma[i]*sigma[i];
									
							// Run propagation with all nodes as observers
							si = new SusceptibleInfected(g);
							si.setObservers(g.getNodesList());
							si.setOneSource(source);
							si.setInfectionRate(rate);
							si.runUntilAllObserversInfected();
								
							// PTVA with CB4 observers
							ptva = new ParallelPinto2(g);
							ptva.setSupposedSources(g.getNodesList());
							ptva.setObservers(observersCB4);
							ptva.setDelays(si.getInfectionTimesForGivenObservers(observersCB4));
							ptva.setDelayMean(1/rate);
							ptva.setDelayVariance((1-rate)/(rate*rate));
							ptva.setNumberOfThreads(numberOfThreads);
							ptva.estimateScoresForNodes();
							supposedSourcesId = ptva.findNodesWithHighestScores();
							if(supposedSourcesId.contains(source.getId())) accuracy_cb4.printf("%.3f\t", 1.0/supposedSourcesId.size());
							else accuracy_cb4.print("0.000\t");
							rank_cb4.printf("%d\t", ptva.findRankOfNode(source.getId()));
							
							// PTVA with CB3 observers
							ptva = new ParallelPinto2(g);
							ptva.setSupposedSources(g.getNodesList());
							ptva.setObservers(observersCB3);
							ptva.setDelays(si.getInfectionTimesForGivenObservers(observersCB3));
							ptva.setDelayMean(1/rate);
							ptva.setDelayVariance((1-rate)/(rate*rate));
							ptva.setNumberOfThreads(numberOfThreads);
							ptva.estimateScoresForNodes();
							supposedSourcesId = ptva.findNodesWithHighestScores();
							if(supposedSourcesId.contains(source.getId())) accuracy_cb3.printf("%.3f\t", 1.0/supposedSourcesId.size());
							else accuracy_cb3.print("0.000\t");
							rank_cb3.printf("%d\t", ptva.findRankOfNode(source.getId()));
							
							// PTVA with high BC observers
							ptva = new ParallelPinto2(g);
							ptva.setSupposedSources(g.getNodesList());
							ptva.setObservers(observersBC);
							ptva.setDelays(si.getInfectionTimesForGivenObservers(observersBC));
							ptva.setDelayMean(1/rate);
							ptva.setDelayVariance((1-rate)/(rate*rate));
							ptva.setNumberOfThreads(numberOfThreads);
							ptva.estimateScoresForNodes();
							supposedSourcesId = ptva.findNodesWithHighestScores();
							if(supposedSourcesId.contains(source.getId())) accuracy_bc.printf("%.3f\t", 1.0/supposedSourcesId.size());
							else accuracy_bc.print("0.000\t");
							rank_bc.printf("%d\t", ptva.findRankOfNode(source.getId()));
							
							// GPTVA with high CB observers
							ptva = new ParallelPinto2(g);
							ptva.setSupposedSources(g.getNodesList());
							ptva.setObservers(observersCB);
							ptva.setDelays(si.getInfectionTimesForGivenObservers(observersCB));
							ptva.setDelayMean(1/rate);
							ptva.setDelayVariance((1-rate)/(rate*rate));
							ptva.setNumberOfThreads(numberOfThreads);
							ptva.estimateScoresForNodes();
							supposedSourcesId = ptva.findNodesWithHighestScores();
							if(supposedSourcesId.contains(source.getId())) accuracy_cb.printf("%.3f\t", 1.0/supposedSourcesId.size());
							else accuracy_cb.print("0.000\t");
							rank_cb.printf("%d\t", ptva.findRankOfNode(source.getId()));
							
						}
						counter = counter + 1;
						System.out.println(estimateTime(System.currentTimeMillis()-tstart, counter, realizations));
						accuracy_cb4.printf("\n");
						rank_cb4.printf("\n");
						accuracy_cb3.printf("\n");
						rank_cb3.printf("\n");
						accuracy_bc.printf("\n");
						rank_bc.printf("\n");
						accuracy_cb.printf("\n");
						rank_cb.printf("\n");
				}
				
		}
		accuracy_cb4.close();
		rank_cb4.close();
		time_cb4.close();
		accuracy_cb3.close();
		rank_cb3.close();
		time_cb3.close();
		accuracy_bc.close();
		rank_bc.close();
		accuracy_cb.close();
		rank_cb.close();
		time_cb.close();
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}
}
