package tests;

import graphs.Graph;
import graphs.GraphGenerator;
import io.ExpressWriter;

import tracking.GradientPinto2;
import tracking.ParallelPinto2;
import dynamics.SusceptibleInfected;

public class TestTimeER {

	public static void main(String[] args) {
		
		int realizations = Integer.parseInt(args[0]);
		int no = Integer.parseInt(args[1]);
		
		String dir = "../outputs/testGradient40c/gradient_er_k6_O0.2_beta0.5_no"+no+"_";
		ExpressWriter gradient_time = new ExpressWriter(dir+"time.txt");
		
		dir = "../outputs/testGradient40c/pinto_er_k6_O0.2_beta0.5_no"+no+"_";
		ExpressWriter pinto_time = new ExpressWriter(dir+"time.txt");
		
		long tstart;
		
		Graph er;
		int[] size = {200,250,300,400,500,700,1000,1300,1600,2000,2500,3000,4000,5000,7000,10000,13000,16000,20000,25000,30000,40000,50000,70000,100000,150000,200000};
		int m = 3;
		
		SusceptibleInfected si;
		double rate = 0.5;

		ParallelPinto2 pinto = null;
		GradientPinto2 gradientPinto = null;

		gradient_time.init();
		pinto_time.init();
		
		tstart = System.currentTimeMillis();
		for(int r=0; r<realizations; r++) {
			for(int i=0; i<size.length; i++) {
				// Generate graph
				er = GraphGenerator.contiguousErdosRenyi(size[i], size[i]*m);
				
				// Run propagation
				si = new SusceptibleInfected(er);
				si.addRandomObservers(2*size[i]/10);
				si.setInfectionRate(rate);
				if(size[i]<=5000)
					si.runUntilAllObserversInfected();
				else
					si.runUntilObserversInfected((int)(0.5*Math.sqrt(size[i]))+1);
				
				// The general Pinto
				if(size[i]<=5000){
					System.out.printf("PTVA for %d nodes... ", size[i]);
					pinto = new ParallelPinto2(er);
					pinto.setObservers(si.getObservers());
					pinto.setSupposedSources(er.getNodesList());
					pinto.setDelays(si.getInfectionTimeAsDouble());
					pinto.setDelayMean(1/rate);
					pinto.setDelayVariance((1-rate)/(rate*rate));
					pinto.setNumberOfThreads(1);
					pinto.estimateScoresForNodes();
					pinto_time.printf("%d\t", pinto.getComputingTime());
					System.out.print("Done. ");
				}
				
				// The gradient Pinto
				System.out.printf("GMLA for %d nodes... ", size[i]);
				gradientPinto = new GradientPinto2(er);
				gradientPinto.setObservers(si.getObservers());
				gradientPinto.setNumberOfNearestObservers((int)(0.5*Math.sqrt(size[i])));
				gradientPinto.setDelays(si.getInfectionTimeAsDouble());
				gradientPinto.setDelayMean(1/rate);
				gradientPinto.setDelayVariance((1-rate)/(rate*rate));
				gradientPinto.setMaxThreads(1);
				gradientPinto.estimateScoresForNodes();
				gradient_time.printf("%d\t", gradientPinto.getComputingTime());
				System.out.println("Done.");
			}
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, r+1, realizations));
			gradient_time.printf("\n");
			pinto_time.printf("\n");		
		}
		gradient_time.close();
		pinto_time.close();
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}
}
