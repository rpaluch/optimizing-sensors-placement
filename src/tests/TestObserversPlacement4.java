package tests;

import java.util.ArrayList;

import tracking.ParallelPinto2;
import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Node;
import graphs.PathsTools;
import graphs.PathsRegister;
import io.ExpressWriter;
import io.GraphWriter;
import dynamics.SusceptibleInfected;

public class TestObserversPlacement4 {

	public static void main(String[] args) {
		
		int numberOfThreads = Integer.parseInt(args[0]);
		int graphId = Integer.parseInt(args[1]);
		
		//String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/observers_placement/pinto_ba_N1000_k6_O0.1_beta0.5_graphId";
		//String dir = "/people/plgrpaluch/outputs/observers_placement/pinto_ba_N1000_k6_O0.1_beta0.5_graphId";
		String dir = "/net/people/plgrpaluch/outputs/observers_placement/pinto_ba_N1000_k6_O0.1_beta0.5_graphId";
		ExpressWriter writer = new ExpressWriter(dir+graphId+".txt");
		writer.init();
		writer.println("sourceId\taccuracy\tdegree\tbetweenness\tcloseness\tvar");
		
		int size = 1000;
		int m = 3;
		Graph g = GraphGenerator.barabasiAlbert(m+2, m, size);
		GraphWriter graphWriter = new GraphWriter(g);
		//String graphDir = "/home/rpaluch/Workspace/ReverseEngineering/networks/synthetic/ba_N1000_k6_id";
		//String graphDir = "/people/plgrpaluch/networks/synthetic/ba_N1000_k6_id";
		String graphDir = "/net/people/plgrpaluch/networks/synthetic/ba_N1000_k6_id";
		graphWriter.exportEdgeList(graphDir+graphId+".net");
		
		PathsRegister register = PathsTools.allShortestPathsInGraph(g);
		GraphTools.betweennessCentrality(g, register);
		GraphTools.closenessCentrality(g, register);
		
		int observersSetsSize = 1;
		int numberOfObservers = 100;
		ArrayList<ArrayList<Node>> observersSets = new ArrayList<ArrayList<Node>>();
		for(int i=0; i<observersSetsSize; i++)
			observersSets.add(g.getRandomNodes(numberOfObservers));
		
		SusceptibleInfected si;
		double rate = 0.5;
		Node source;
		ArrayList<Node> observers;
		int numberOfSources = 500;
		double sourcesAccuracy;
		double acc;
		
		ParallelPinto2 pinto = null;
		ArrayList<Integer> supposedSourcesId;
		
		
		int testPerSource = 100;
		int realizations = observersSetsSize*numberOfSources*testPerSource;
		int r=0;
		
		long tstart = System.currentTimeMillis();
		for(int i=0; i<observersSetsSize; i++) {
			observers = observersSets.get(i);
			for(int s=0; s<numberOfSources; s++) {
				source = g.getNodesList().get(s);
				sourcesAccuracy = 0;
				for(int t=0; t<testPerSource; t++) {
					
					si = new SusceptibleInfected(g);
					si.setObservers(observers);
					si.setOneSource(source);
					si.setInfectionRate(rate);
					si.runUntilAllObserversInfected();
					
					pinto = new ParallelPinto2(g);
					pinto.setObservers(si.getObservers());
					pinto.setDelays(si.getInfectionTimeAsDouble());
					pinto.setSupposedSources(g.getNodesList());	
					pinto.setDelayMean(1/rate);
					pinto.setDelayVariance((1-rate)/(rate*rate));
					pinto.setNumberOfThreads(numberOfThreads);
					pinto.estimateScoresForNodes();
					supposedSourcesId = pinto.findNodesWithHighestScores();
					if(supposedSourcesId.contains(source.getId())) 	acc = 1.0/supposedSourcesId.size();
					else acc = 0.0;
					sourcesAccuracy = sourcesAccuracy + acc;
					
					r = r + 1;
					System.out.println(estimateTime(System.currentTimeMillis()-tstart, r+1, realizations));
				}
				sourcesAccuracy = sourcesAccuracy/testPerSource;
				
				writer.printf("%d\t%.3f\t%d\t%.3f\t%.3f\t%.3f\n", source.getId(),
																  sourcesAccuracy,
															  	  source.getDegree(),
															  	  source.getBetweenness(),
															  	  source.getCloseness(),
															  	  GraphTools.varianceOfShortestPathsLengths(g, register, source, observers));
			}
		}
		writer.close();
	}
		
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}
}