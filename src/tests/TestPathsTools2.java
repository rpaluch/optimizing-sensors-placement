package tests;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.Link;
import graphs.Node;
import graphs.Path;
import graphs.PathsTools;
import graphs.WeightedPath;

public class TestPathsTools2 {

	public static void main(String[] args) {
		
		Graph g = GraphGenerator.randomTree(20, 1);
		
		ArrayList<Path> paths1 = PathsTools.shortestPaths(g, g.getNode(5), g.getNodesList());
		for(Path p : paths1)
			System.out.println(p.toString());
				
		Path merged = PathsTools.mergeWithoutOverlap(PathsTools.reverse(paths1.get(0)),paths1.get(10));
		System.out.println(merged.toString());
		
	}

}
