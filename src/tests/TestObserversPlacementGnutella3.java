package tests;

import io.ExpressReader;
import io.GraphReader;
import io.MultiExpressWriter;
import tracking.SingleSourceDetector;

import java.util.ArrayList;

import dynamics.SusceptibleInfected;
import graphs.Graph;
import graphs.GraphTools;
import graphs.Node;

public class TestObserversPlacementGnutella3 {

	public static void main(String[] args) {
		
		// Read the main parameters
		double densityOfObservers = Double.parseDouble(args[0]);
		int realizations = Integer.parseInt(args[1]);
		int numberOfThreads = Integer.parseInt(args[2]);
		int part = Integer.parseInt(args[3]);
		
		// Read the network from the file
		GraphReader greader = new GraphReader("../data/Gnutella/p2p-Gnutella08.txt");
		Graph gnutella = GraphTools.extractLargestComponent(greader.readGraphFromEdgeList());
		gnutella.printStats();
		System.out.println("Graph ready.");
		
		// Initialize variables
		int numberOfObservers = (int)(densityOfObservers*gnutella.getNumberOfNodes());
		//int numberOfNearestObservers = 55;
		double rate = 0;
		double[] sigma = {0.25,0.5,0.75};
		
		// Spreading and detection
		SusceptibleInfected si;
		Node source;
		SingleSourceDetector detector;
		ArrayList<Integer> supposedSourcesId;
		
		// Prepare observers sets
		String[] sets = {"Random","Coverage","KMedian","CB","BC"};
		String path = "../outputs/real_network_observers/gnutella_";
		ExpressReader oreader = new ExpressReader();
		ArrayList<ArrayList<Node>> observers = new ArrayList<ArrayList<Node>>();		
		// Random observers
		observers.add(gnutella.getRandomNodes(numberOfObservers));
		// The rest of observers
		for(int s=1; s<sets.length; s++) {
			oreader.init(path+sets[s]+"_O"+densityOfObservers+".txt");
			observers.add(new ArrayList<Node>());
			for(int i=0; i<numberOfObservers; i++)
				observers.get(s).add(gnutella.getNodesList().get(Integer.parseInt(oreader.readLine())));
			oreader.close();
		}
				
		// Prepare output files
		String[] files = new String[15];
		String[] names = new String[15];
		String dir1 = "../outputs/gnutella_tests/ptva_gnutella_O"+densityOfObservers+"_part"+part;
		//String dir2 = "../outputs/gnutella_tests/gmla_gnutella_O"+densityOfObservers;
		int counter = 0;
		for(int op=0; op<sets.length; op++) {
			files[counter] = dir1+"_"+sets[op]+"_accuracy.txt";
			names[counter] = "accuracy_ptva_"+sets[op];
			counter++;
			files[counter] = dir1+"_"+sets[op]+"_rank.txt";
			names[counter] = "rank_ptva_"+sets[op];
			counter++;
			files[counter] = dir1+"_"+sets[op]+"_distance.txt";
			names[counter] = "distance_ptva_"+sets[op];
			counter++;
		}
		/*
		for(int op=0; op<sets.length; op++) {
			files[counter] = dir2+"_"+sets[op]+"_accuracy.txt";
			names[counter] = "accuracy_gmla_"+sets[op];
			counter++;
			files[counter] = dir2+"_"+sets[op]+"_rank.txt";
			names[counter] = "rank_gmla_"+sets[op];
			counter++;
			files[counter] = dir2+"_"+sets[op]+"_distance.txt";
			names[counter] = "distance_gmla_"+sets[op];
			counter++;
		}
		*/			
		MultiExpressWriter writer = new MultiExpressWriter(files, names);
		writer.initAll();
		writer.printlnHeaderAll(sigma, "sigma", null);
		
		// Main loop
		long tstart = System.currentTimeMillis();
		for(int i=0; i<realizations; i++) {
			for(int r=0; r<sigma.length; r++) {
				
				rate = 1-sigma[r]*sigma[r];
				source = gnutella.getRandomNode();
				
				si = new SusceptibleInfected(gnutella);
				si.setOneSource(source);
				si.setObservers(gnutella.getNodesList());
				si.setInfectionRate(rate);
				si.runUntilAllObserversInfected();
				
				observers.set(0,gnutella.getRandomNodes(numberOfObservers));
				
				for(int s=0; s<sets.length; s++) {
					System.out.println(sets[s]+" "+observers.get(s).size()+" "+sigma[r]);
					//PTVA-LI
					detector = new SingleSourceDetector(gnutella);
					detector.setObservers(observers.get(s));
					detector.setDelays(si.getInfectionTimesForGivenObservers(observers.get(s)));
					detector.setDelayMean(1/rate);
					detector.setDelayVariance((1-rate)/(rate*rate));
					detector.setMaxThreads(numberOfThreads);
					detector.setSupposedSources(gnutella.getNodesList());
					detector.computeScores("LPTV");
					supposedSourcesId = detector.findNodesWithHighestScores();
					if(supposedSourcesId.contains(source.getId())) 
						writer.printf("accuracy_ptva_"+sets[s],"%.3f\t",1.0/supposedSourcesId.size());
					else writer.printf("accuracy_ptva_"+sets[s],"0.000\t");
					writer.printf("rank_ptva_"+sets[s],"%d\t",detector.findRankOfNode(source.getId()));
					writer.printf("distance_ptva_"+sets[s],"%.3f\t", GraphTools.shortestPathLength(gnutella, source.getId(), supposedSourcesId));

					//GMLA
					/*
					detector = new SingleSourceDetector(gnutella);
					detector.setObservers(observers.get(s));
					detector.setDelays(si.getInfectionTimesForGivenObservers(observers.get(s)));
					detector.setDelayMean(1/rate);
					detector.setDelayVariance((1-rate)/(rate*rate));
					detector.setMaxThreads(numberOfThreads);
					detector.setNumberOfGradientWalks(1);
					detector.setNumberOfNearestObservers(numberOfNearestObservers);
					detector.computeScores("LPTV");
					supposedSourcesId = detector.findNodesWithHighestScores();
					if(supposedSourcesId.contains(source.getId())) 
						writer.printf("accuracy_gmla_"+sets[s],"%.3f\t",1.0/supposedSourcesId.size());
					else writer.printf("accuracy_gmla_"+sets[s],"0.000\t");
					writer.printf("rank_gmla_"+sets[s],"%d\t",detector.findRankOfNode(source.getId()));
					writer.printf("distance_gmla_"+sets[s],"%.3f\t", GraphTools.shortestPathLength(gnutella, source.getId(), supposedSourcesId));
					*/
				}
			}
			writer.newLineAll();
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, i+1, realizations));
		}
		writer.closeAll();
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}

}
