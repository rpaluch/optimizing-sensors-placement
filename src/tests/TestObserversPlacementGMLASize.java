package tests;

import java.util.ArrayList;

import tracking.GradientPinto2;
import graphs.Graph;
import graphs.GraphGenerator;
import graphs.Node;
import graphs.NodesSelectors;
import io.MemoryUsage;
import io.MultiExpressWriter;
import dynamics.SusceptibleInfected;

public class TestObserversPlacementGMLASize {

	public static void main(String[] args) {
		
		String graphType = args[0];
		int degree = 8;
		int m = degree/2;
		double densityOfObservers = 0.2;
		double rate = 0.5;
		int numberOfGraphs = Integer.parseInt(args[1]);
		int numberOfTestsPerGraph = Integer.parseInt(args[2]);
		int numberOfThreads = Integer.parseInt(args[3]);
		int part = Integer.parseInt(args[4]);
		
		String header = "";
		int[] size = new int[args.length-5];
		for(int i=0; i<size.length; i++) {
			size[i] = Integer.parseInt(args[i+5]);
			header = header + "N" + args[i+5] + "\t";
		}
		
		//String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/observers_placement/pinto_ba_N100_k6_beta0.5_";
		String dir = "../outputs/cb_gmla_"+graphType+"/gmla_"+graphType+"_k8_O"+densityOfObservers+"_rate"+rate+"_part"+part;
		String[] paths1 = {dir+"_CB3_memory.txt",
						   dir+"_CB4_memory.txt", 
						   dir+"_CB5_memory.txt", 
						   dir+"_CB_memory.txt",  
						   dir+"_CB3_time.txt",   
						   dir+"_CB4_time.txt",   
						   dir+"_CB5_time.txt",   
						   dir+"_CB_time.txt"};
		String[] paths2 = {dir+"_CB3_accuracy.txt",
						   dir+"_CB4_accuracy.txt",
						   dir+"_CB5_accuracy.txt",
						   dir+"_CB_accuracy.txt",
						   dir+"_CB3_rank.txt",
						   dir+"_CB4_rank.txt",
						   dir+"_CB5_rank.txt",
						   dir+"_CB_rank.txt"};
		String[] names1 = {"memoryCB3","memoryCB4","memoryCB5","memoryCB","timeCB3","timeCB4","timeCB5","timeCB"};
		String[] names2 = {"accuracyCB3","accuracyCB4","accuracyCB5","accuracyCB","rankCB3","rankCB4","rankCB5","rankCB"};
		
		MultiExpressWriter writer1 = new MultiExpressWriter(paths1, names1);
		MultiExpressWriter writer2 = new MultiExpressWriter(paths2, names2);
		writer1.initAll();
		writer1.printlnAll(header);
		writer2.initAll();
		writer2.printlnAll(header);
		
		Graph g;		
		SusceptibleInfected si;
		int numberOfObservers = 0;
		int numberOfNearestObservers = 0;

		ArrayList<Node> sources;
		ArrayList<Node> observersCB3;
		ArrayList<Node> observersCB4;
		ArrayList<Node> observersCB5;
		ArrayList<Node> observersCB;

		GradientPinto2 gmla = null;
		ArrayList<Integer> supposedSourcesId;
		ArrayList<ArrayList<Double>> accuracyCB3;
		ArrayList<ArrayList<Double>> accuracyCB4;
		ArrayList<ArrayList<Double>> accuracyCB5;
		ArrayList<ArrayList<Double>> accuracyCB;
		ArrayList<ArrayList<Integer>> rankCB3;
		ArrayList<ArrayList<Integer>> rankCB4;
		ArrayList<ArrayList<Integer>> rankCB5;
		ArrayList<ArrayList<Integer>> rankCB;
		
		MemoryUsage memoryUsage;		
		long tstartCB, tstart, tstart2 = System.currentTimeMillis();
		int counter = 0;
		for(int n=0; n<numberOfGraphs; n++) {
			
			accuracyCB3 = new ArrayList<ArrayList<Double>>();
			accuracyCB4 = new ArrayList<ArrayList<Double>>();
			accuracyCB5 = new ArrayList<ArrayList<Double>>();
			accuracyCB = new ArrayList<ArrayList<Double>>();
			rankCB3 = new ArrayList<ArrayList<Integer>>();
			rankCB4 = new ArrayList<ArrayList<Integer>>();
			rankCB5 = new ArrayList<ArrayList<Integer>>();
			rankCB = new ArrayList<ArrayList<Integer>>();
			
			for(int s=0; s<size.length; s++) {
				
				System.out.printf("N = %d\n", size[s]);
				
				accuracyCB3.add(new ArrayList<Double>());
				accuracyCB4.add(new ArrayList<Double>());
				accuracyCB5.add(new ArrayList<Double>());
				accuracyCB.add(new ArrayList<Double>());
				rankCB3.add(new ArrayList<Integer>());
				rankCB4.add(new ArrayList<Integer>());
				rankCB5.add(new ArrayList<Integer>());
				rankCB.add(new ArrayList<Integer>());
			
				// Generate graph
				switch(graphType) {
					case "ba":	g = GraphGenerator.barabasiAlbert(m+2, m, size[s]);
							    break;
					case "er":  g = GraphGenerator.contiguousErdosRenyi(size[s], 2*m, "degree");
								break;
					case "rrg": g = GraphGenerator.regularRandomGraph(size[s], 2*m);
								while(!g.isContiguous()) g = GraphGenerator.regularRandomGraph(size[s], 2*m);
								break;
					case "sfn": g = GraphGenerator.scaleFreeNetwork(size[s], m, size[s]-m, 3);
								while(!g.isContiguous()) g = GraphGenerator.scaleFreeNetwork(size[s], m, size[s]-m, 3);
								break;
					default:	g = GraphGenerator.contiguousErdosRenyi(size[s], 2*m, "degree");
								break;
								
				}
				
				sources = g.getRandomNodes(numberOfTestsPerGraph);
				numberOfObservers = (int)(densityOfObservers*size[s]);
				numberOfNearestObservers = (int)(1.44*Math.pow(size[s],0.42));
				
				System.gc();
				memoryUsage = new MemoryUsage();
				memoryUsage.start();
				tstartCB = System.currentTimeMillis();
				observersCB3 = NodesSelectors.fastCollectiveBetweenness(g, numberOfObservers, 3);
				writer1.printf("timeCB3","%d\t", System.currentTimeMillis()-tstartCB);
				System.out.printf("CB3 found in %d ms\n", System.currentTimeMillis()-tstartCB);
				memoryUsage.stopMeasurement();
				writer1.printf("memoryCB3","%d\t", memoryUsage.getDiffMemoryUsage());
				
				System.gc();
				memoryUsage = new MemoryUsage();
				memoryUsage.start();
				tstartCB = System.currentTimeMillis();
				observersCB4 = NodesSelectors.fastCollectiveBetweenness(g, numberOfObservers, 4);
				writer1.printf("timeCB4","%d\t", System.currentTimeMillis()-tstartCB);
				System.out.printf("CB4 found in %d ms\n", System.currentTimeMillis()-tstartCB);
				memoryUsage.stopMeasurement();
				writer1.printf("memoryCB4","%d\t", memoryUsage.getDiffMemoryUsage());
				
				System.gc();
				memoryUsage = new MemoryUsage();
				memoryUsage.start();
				tstartCB = System.currentTimeMillis();
				observersCB5 = NodesSelectors.fastCollectiveBetweenness(g, numberOfObservers, 5);
				writer1.printf("timeCB5","%d\t", System.currentTimeMillis()-tstartCB);
				System.out.printf("CB5 found in %d ms\n", System.currentTimeMillis()-tstartCB);
				memoryUsage.stopMeasurement();
				writer1.printf("memoryCB5","%d\t", memoryUsage.getDiffMemoryUsage());
				
				System.gc();
				memoryUsage = new MemoryUsage();
				memoryUsage.start();
				tstartCB = System.currentTimeMillis();
				observersCB = NodesSelectors.fastCollectiveBetweenness(g, numberOfObservers, 100);
				writer1.printf("timeCB","%d\t", System.currentTimeMillis()-tstartCB);
				System.out.printf("CB found in %d ms\n", System.currentTimeMillis()-tstartCB);
				memoryUsage.stopMeasurement();
				writer1.printf("memoryCB","%d\t", memoryUsage.getDiffMemoryUsage());
				
				counter = 1;
				tstart = System.currentTimeMillis();
				for(Node source : sources) {
						
					// Run propagation with all nodes as observers
					si = new SusceptibleInfected(g);
					si.setObservers(g.getNodesList());
					si.setOneSource(source);
					si.setInfectionRate(rate);
					si.runUntilAllObserversInfected();
					
					// GMLA with CB3
					gmla = new GradientPinto2(g);
					gmla.setObservers(observersCB3);
					gmla.setDelays(si.getInfectionTimesForGivenObservers(observersCB3));
					gmla.setDelayMean(1/rate);
					gmla.setDelayVariance((1-rate)/(rate*rate));
					gmla.setNumberOfNearestObservers(numberOfNearestObservers);
					gmla.setMaxThreads(numberOfThreads);
					gmla.estimateScoresForNodes();
					supposedSourcesId = gmla.findNodesWithHighestScores();
					if(supposedSourcesId.contains(source.getId())) accuracyCB3.get(s).add(1.0/supposedSourcesId.size());
					else accuracyCB3.get(s).add(0.0);
					rankCB3.get(s).add(gmla.findRankOfNode(source.getId()));
					
					// GMLA with CB4
					gmla = new GradientPinto2(g);
					gmla.setObservers(observersCB4);
					gmla.setDelays(si.getInfectionTimesForGivenObservers(observersCB4));
					gmla.setDelayMean(1/rate);
					gmla.setDelayVariance((1-rate)/(rate*rate));
					gmla.setNumberOfNearestObservers(numberOfNearestObservers);
					gmla.setMaxThreads(numberOfThreads);
					gmla.estimateScoresForNodes();
					supposedSourcesId = gmla.findNodesWithHighestScores();
					if(supposedSourcesId.contains(source.getId())) accuracyCB4.get(s).add(1.0/supposedSourcesId.size());
					else accuracyCB4.get(s).add(0.0);
					rankCB4.get(s).add(gmla.findRankOfNode(source.getId()));
					
					// GMLA with CB5
					gmla = new GradientPinto2(g);
					gmla.setObservers(observersCB5);
					gmla.setDelays(si.getInfectionTimesForGivenObservers(observersCB5));
					gmla.setDelayMean(1/rate);
					gmla.setDelayVariance((1-rate)/(rate*rate));
					gmla.setNumberOfNearestObservers(numberOfNearestObservers);
					gmla.setMaxThreads(numberOfThreads);
					gmla.estimateScoresForNodes();
					supposedSourcesId = gmla.findNodesWithHighestScores();
					if(supposedSourcesId.contains(source.getId())) accuracyCB5.get(s).add(1.0/supposedSourcesId.size());
					else accuracyCB5.get(s).add(0.0);
					rankCB5.get(s).add(gmla.findRankOfNode(source.getId()));
					
					// GMLA with CB
					gmla = new GradientPinto2(g);
					gmla.setObservers(observersCB);
					gmla.setDelays(si.getInfectionTimesForGivenObservers(observersCB));
					gmla.setDelayMean(1/rate);
					gmla.setDelayVariance((1-rate)/(rate*rate));
					gmla.setNumberOfNearestObservers(numberOfNearestObservers);
					gmla.setMaxThreads(numberOfThreads);
					gmla.estimateScoresForNodes();
					supposedSourcesId = gmla.findNodesWithHighestScores();
					if(supposedSourcesId.contains(source.getId())) accuracyCB.get(s).add(1.0/supposedSourcesId.size());
					else accuracyCB.get(s).add(0.0);
					rankCB.get(s).add(gmla.findRankOfNode(source.getId()));
					
					System.out.println(estimateTime(System.currentTimeMillis()-tstart, counter, numberOfTestsPerGraph));
					counter = counter + 1;
				}
			
			}
						
			for(int i=0; i<numberOfTestsPerGraph; i++) {
				for(int j=0; j<size.length; j++) {
					writer2.printf("accuracyCB3", "%.3f\t", accuracyCB3.get(j).get(i));
					writer2.printf("accuracyCB4", "%.3f\t", accuracyCB4.get(j).get(i));
					writer2.printf("accuracyCB5", "%.3f\t", accuracyCB5.get(j).get(i));
					writer2.printf("accuracyCB", "%.3f\t", accuracyCB.get(j).get(i));
					writer2.printf("rankCB3", "%d\t", rankCB3.get(j).get(i));
					writer2.printf("rankCB4", "%d\t", rankCB4.get(j).get(i));
					writer2.printf("rankCB5", "%d\t", rankCB5.get(j).get(i));
					writer2.printf("rankCB", "%d\t", rankCB.get(j).get(i));
				}
				writer2.newLineAll();
			}
			writer1.newLineAll();
			System.out.print("\nGraph: ");
			System.out.println(estimateTime(System.currentTimeMillis()-tstart2, n+1, numberOfGraphs));
			System.out.println();
		}
	
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}
}
