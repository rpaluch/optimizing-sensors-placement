package tests;

import graphs.Node;

public class TestNode {

	public static void main(String[] args) {
		
		Node node1 = new Node(1);
		Node node2 = new Node(2);
		Node node3 = new Node(3);
		Node node4 = new Node(3);
		Node node5 = new Node(10);
		
		System.out.printf("hashcode:%h, id:%d, degree=%d\n", node1, node1.getId(), node1.getDegree());
		System.out.printf("hashcode:%h, id:%d, degree=%d\n", node2, node2.getId(), node2.getDegree());
		System.out.printf("hashcode:%h, id:%d, degree=%d\n", node3, node3.getId(), node3.getDegree());
		System.out.printf("hashcode:%h, id:%d, degree=%d\n", node4, node4.getId(), node4.getDegree());
		System.out.printf("hashcode:%h, id:%d, degree=%d\n", node5, node5.getId(), node5.getDegree());
		
		node1.addNeighbour(node2);
		node1.addNeighbour(node3);
		node1.addNeighbour(node4);
		node1.addNeighbour(node5);
		
		node1.print();
		node1.printNeighbours();
		for(Node n : node1.getNeighbours()) {
			System.out.printf("hashcode:%h, id:%d, degree=%d\n", n, n.getId(), n.getDegree());
		}
		
		node1.removeNeighbour(node4);
		node1.print();
		node1.printNeighbours();
		for(Node n : node1.getNeighbours()) {
			System.out.printf("hashcode:%h, id:%d, degree=%d\n", n, n.getId(), n.getDegree());
		}
		
		node1.removeNeighbour(node4);
		node1.print();
		for(Node n : node1.getNeighbours()) {
			System.out.printf("hashcode:%h, id:%d, degree=%d\n", n, n.getId(), n.getDegree());
		}
		
		node1.removeNeighbour(node5);
		node1.print();
		for(Node n : node1.getNeighbours()) {
			System.out.printf("hashcode:%h, id:%d, degree=%d\n", n, n.getId(), n.getDegree());
		}
		
		node1.removeAllNeighbours();
		node1.print();
		node1.printNeighbours();
		for(Node n : node1.getNeighbours()) {
			System.out.printf("hashcode:%h, id:%d, degree=%d\n", n, n.getId(), n.getDegree());
		}
	}

}
