package tests;

import java.util.ArrayList;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.Node;
import graphs.NodesSelectors;

public class TestLeastShortestPaths {

	public static void main(String[] args) {
		
		int size = 100;
		int m = 4;
		int budget = 50;
		int minLength = 3;
		
		Graph graph = GraphGenerator.barabasiAlbert(m+2, m, size);
		ArrayList<Node> lsp = NodesSelectors.leastShortestPaths(graph, budget, minLength);

	}

}
