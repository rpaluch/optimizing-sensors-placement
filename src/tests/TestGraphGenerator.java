package tests;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;

public class TestGraphGenerator {

	public static void main(String[] args) {
		
		int realizations = 100;
		Graph erg;
		int size = 1000;
		int m = 3;
		double[] prob = {0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,0.95,0.98};
		double[] clustering = new double[prob.length];
		
		for(int r=0; r<realizations; r++){
			for(int i=0; i<prob.length; i++) {
				erg = GraphGenerator.evolvingExponentialRandomGraph(m+2, m, size, prob[i]);
				clustering[i] = clustering[i] + GraphTools.averageClusteringCoefficient(erg)/realizations;
			}
		}
		
		for(int i=0; i<prob.length; i++) System.out.printf("p=%.2f\tcoef=%.3f\n", prob[i], clustering[i]);
	}

}
