package tests;

import io.ExpressWriter;
import io.GraphReader;

import java.util.ArrayList;

import graphs.Graph;
import graphs.GraphTools;
import graphs.Node;
import graphs.NodesSelectors;
import graphs.PathsTools;

public class TestObserversPlacementJaccard2 {

	public static void main(String[] args) {
		
		// Read the main parameters
		String networkName = args[0];
		String networkPath = args[1];
		int numberOfThreads = Integer.parseInt(args[2]);
		double[] densityOfObservers = {0.05,0.1,0.15,0.2,0.25,0.3};
		
		// Read the network from the file
		GraphReader reader = new GraphReader(networkPath);
		Graph graph = reader.readGraphFromEdgeList();
		graph.printStats();
		System.out.println("Graph ready.");
				
		// Initialize variables
		long time = 0;
		int numberOfObservers;
		int maxLengthCB = 3;
		int maxLengthHV = 3;
		
		// Initialize observers sets
		String[] sets = {"Random","Coverage","KMedian","HVObs","CB","BC"};
		ArrayList<ArrayList<Node>> observers;
				
		// Prepare output files
		String dir1 = "../outputs/observers_placement_jaccard/";
		String dir2 = "../outputs/observers_placement_overlap/";
		
		ExpressWriter writer1 = new ExpressWriter(dir1+networkName+"_jaccard.txt");
		ExpressWriter writer2 = new ExpressWriter(dir2+networkName+"_overlap.txt");
		writer1.init();
		writer2.init();
		// Print header
		for(int i=0; i<sets.length-1; i++)
			for(int j=i+1; j<sets.length; j++) {
				writer1.printf("%s.%s\t", sets[i],sets[j]);
				writer2.printf("%s.%s\t", sets[i],sets[j]);
			}
		writer1.printf("\n");
		writer2.printf("\n");
		
		// Main loop
		for(int d=0; d<densityOfObservers.length; d++) {
			
			numberOfObservers = (int)(densityOfObservers[d]*graph.getNumberOfNodes());
			if(densityOfObservers[d] <= 0.1) {
				maxLengthHV = 4;
				if(densityOfObservers[d] <= 0.05) maxLengthHV = 5;
			}
			
			// Prepare observers sets
			observers = new ArrayList<ArrayList<Node>>();		
			// (0) Random observers
			observers.add(graph.getRandomNodes(numberOfObservers));
			// (1) Coverage observers
			time = System.currentTimeMillis();
			observers.add(NodesSelectors.greedyCoverageObsParallel(graph, numberOfObservers, numberOfThreads));
			System.out.printf("Coverage observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
			// (2) KMedian observers
			time = System.currentTimeMillis();
			observers.add(NodesSelectors.greedyKMedianObsParallel(graph, PathsTools.allDistancesInGraph(graph), numberOfObservers, numberOfThreads));
			System.out.printf("KMedian observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
			// (3) HVObs observers
			time = System.currentTimeMillis();
			observers.add(NodesSelectors.greedyHVObsParallel(graph, numberOfObservers, maxLengthHV, numberOfThreads));
			System.out.printf("HVObs observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
			// (4) CB3 observers
			time = System.currentTimeMillis();
			observers.add(NodesSelectors.fastCollectiveBetweenness(graph, numberOfObservers, maxLengthCB));
			System.out.printf("CB3 observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
			// (5) BC observers
			time = System.currentTimeMillis();
			GraphTools.computeBetweennessCentrality(graph);
			observers.add(GraphTools.highestBetweennessNodes(graph, numberOfObservers));
			System.out.printf("BC observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
			
			for(int i=0; i<sets.length-1; i++)
				for(int j=i+1; j<sets.length; j++) {
					writer1.printf("%.3f\t", NodesSelectors.jaccardIndex(observers.get(i), observers.get(j)));
					writer2.printf("%.3f\t", 1.0*NodesSelectors.intersection(observers.get(i), observers.get(j)).size()/observers.get(i).size());
				}
			writer1.printf("\n");
			writer2.printf("\n");
		}
		writer1.close();
		writer2.close();
	}
}
