package tests;

import java.util.ArrayList;

import dynamics.GenericSpreading;
import graphs.Graph;
import io.GraphReader;
import io.MultiExpressWriter;
import jdistlib.Normal;
import tracking.SingleSourceDetector;

public class TestSingleSourceDetector3 {

	public static void main(String[] args) {
		
		// Read the main parameters
		String networkName = args[0];
		String networkPath = args[1];
		double mu = Double.parseDouble(args[2]);
		double sd = Double.parseDouble(args[3]);
		int realizations = Integer.parseInt(args[4]);
		int numberOfThreads = Integer.parseInt(args[5]);
		
		// Read the network from the file
		GraphReader reader = new GraphReader(networkPath);
		Graph graph = reader.readGraphFromEdgeList();
		graph.printStats();
		System.out.println("Graph ready.");
				
		
		double[] densityOfObservers = {0.02,0.04,0.06,0.08,0.1,0.12,0.14,0.16,0.18,0.20};		
		GenericSpreading gs;
		int sourceID;
		String[] methods = {"LPTV","EPP","EPL"};
		SingleSourceDetector detector;
		ArrayList<Integer> supposedSourcesId;
		
		String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/epp_tests/"+networkName+"_mu"+mu+"_sd"+sd;
		String[] files = {dir+"_LPTV_accuracy3.txt",
						  dir+"_LPTV_rank3.txt",
						  dir+"_EPP_accuracy3.txt",
						  dir+"_EPP_rank3.txt",
						  dir+"_EPL_accuracy3.txt",
						  dir+"_EPL_rank3.txt"};
		String[] names = {"accuracyLPTV","rankLPTV","accuracyEPP","rankEPP","accuracyEPL","rankEPL"};
		MultiExpressWriter writer = new MultiExpressWriter(files, names);
		writer.initAll();
		writer.printlnHeaderAll(densityOfObservers, "", "\t");
		long tstart = System.currentTimeMillis();
		for(int i=0; i<realizations; i++) {
						
			for(int d=0; d<densityOfObservers.length; d++) {
				
				gs = new GenericSpreading(graph, new Normal(mu,sd));
				gs.setMinimalTimeDelay(0.0);
				gs.addRandomObservers((int)(densityOfObservers[d]*graph.getNumberOfNodes()));
				gs.run();
				sourceID = gs.getSources().get(0).getId();
				
				for(String method: methods) {
					detector = new SingleSourceDetector(graph);
					detector.setObserversAndDelays(gs.getObserversAndDelays((int)(densityOfObservers[d]*graph.getNumberOfNodes())));
					detector.setDelayMean(mu);
					detector.setDelayVariance(sd*sd);
					detector.setMaxThreads(numberOfThreads);
					detector.setSupposedSources(graph.getNodesList());
					detector.computeScores(method);
					supposedSourcesId = detector.findNodesWithHighestScores();
					if(supposedSourcesId.contains(sourceID)) writer.printf("accuracy"+method,"%.3f\t",1.0/supposedSourcesId.size());
					else writer.printf("accuracy"+method,"0.000\t");
					writer.printf("rank"+method,"%d\t",detector.findRankOfNode(sourceID));
				}
			}
			writer.newLineAll();
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, i+1, realizations));
		}
		writer.closeAll();
	}

	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}
}
