package tests;

import java.util.ArrayList;
import java.util.LinkedList;

import dynamics.GenericSpreading;
import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Node;
import graphs.PathsTools;
import io.GraphWriter;
import jdistlib.Geometric;
import jdistlib.Normal;

public class TestGenericSpreading2 {

	public static void main(String[] args) {
		
		int size = 1000;
		int m = 4;
		double rate = 0.5;
		double mu = 1.41;
		double sd = 1;
		
		Graph graph = GraphGenerator.barabasiAlbert(m+2, m, size);
		GenericSpreading spreading = new GenericSpreading(graph, new Normal(mu,sd));
		Node source = graph.getNode(50);
		ArrayList<Node> observers = new ArrayList<Node>();
		for(int i=100; i<151; i++) observers.add(graph.getNode(i));
		spreading.setOneSource(source);
		spreading.setObservers(observers);
		spreading.run();
		
		GraphWriter gwrite = new GraphWriter(graph);
		gwrite.exportEdgeListWithWeights("/home/rpaluch/Workspace/ReverseEngineering/networks/ba_N1000_k8.net");
		
		System.out.printf("Source is the node %d\nPaths:\n", spreading.getSources().get(0).getId());
		ArrayList<Integer> targetsID = new ArrayList<Integer>();
		for(Node n : spreading.getObservers())
			targetsID.add(n.getId());
		ArrayList<LinkedList<Integer>> paths = PathsTools.shortestWeightedPaths(graph, spreading.getSources().get(0).getId(), targetsID);
		System.out.printf("%d %d %d\n", graph.getNodesList().size(), targetsID.size(), paths.size());
		
		for(LinkedList<Integer> list : paths) {
			for(Integer i : list)
				System.out.printf("%d ", i);
			System.out.println();
		}
		
		for(int i=0; i<targetsID.size(); i++)
			System.out.printf("Node %d - %.3f\n", spreading.getObservers().get(i).getId(), spreading.getInfectionTimes().get(i));
		/*
		System.out.println();
		ArrayList<Double> times = PathsTools.shortestWeightedPathsLengths(graph, spreading.getSources().get(0).getId(), targetsID);
		for(int i=0; i<times.size(); i++) System.out.printf("%f\n", times.get(i));
		*/
	}

}
