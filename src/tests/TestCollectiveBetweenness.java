package tests;


import graphs.Graph;
import graphs.GraphTools;
import graphs.PathsTools;
import graphs.PathsRegister;

public class TestCollectiveBetweenness {

	public static void main(String[] args) {
		
		Graph g = new Graph();
		g.addLink(1, 2);
		g.addLink(1, 3);
		g.addLink(1, 4);
		g.addLink(1, 5);
		g.addLink(2, 6);
		g.addLink(2, 7);
		g.addLink(3, 6);
		g.addLink(5, 8);
		g.addLink(8, 9);
		g.addLink(9, 7);
		g.addLink(7, 10);
		g.addLink(6, 10);
		g.addLink(1, 11);
		g.addLink(11, 12);
		g.addLink(3, 13);
		
		PathsRegister register = PathsTools.allLimitedShortestPathsInGraph(g,3);
		//ArrayList<Path> paths;
		System.out.println(register.getAllPathsAsString());
		/*
		for(int i=0; i<g.getNumberOfNodes()-1; i++)
			for(int j=i+1; j<g.getNumberOfNodes(); j++) {
				paths = register.getPaths(i+1, j+1);
				for(Path path : paths)
					if(path.contains(g.getNode(1)))
						System.out.println(path.toString()+" "+1.0/paths.size());
			}
		
		GraphTools.betweennessCentrality(g, register);
		for(Node n : g.getNodesList())
			System.out.printf("%d: %.3f\n", n.getId(), n.getBetweenness());
		
		ArrayList<Node> nodes = new ArrayList<Node>();
		nodes.add(g.getNode(8));
		nodes.add(g.getNode(10));
		System.out.printf("Collective betweenness dla węzłów 8 i 10: %.3f\n", GraphTools.collectiveBetweenness(g, register, nodes));
		GraphTools.greedyCollectiveBetweennessParallel(g, register, 2, 1);
		*/
	}

}
