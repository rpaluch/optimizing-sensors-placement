package tests;

import java.util.ArrayList;
import java.util.Collections;

import org.jblas.DoubleMatrix;

import dynamics.SusceptibleInfected;
import graphs.Graph;
import graphs.GraphGenerator;
import graphs.Node;
import tracking.ParallelPinto2;

public class TestStarGraph {

	public static void main(String[] args) {
	
		ArrayList<Integer> degrees = new ArrayList<Integer>();
		degrees.add(10);
		degrees.add(6);
		degrees.add(4);
		
		Graph g = GraphGenerator.multiStar(degrees);
		int size = g.getNumberOfNodes();
		//g.printNodes();
		
		SusceptibleInfected si;
		double rate = 0.5;
		int sourceID;
		int percentOfObservers = 20;
		//ArrayList<Node> observers = new ArrayList<Node>(g.copyOfNodesList().subList(1, size));
		ArrayList<Node> observers = g.copyOfNodesList();
		//ArrayList<Node> observers = new ArrayList<Node>();
		//for(int i=0; i<20; i++)
		//	observers.add(g.getNode(20+i));
		
		ParallelPinto2 pinto = null;
		//GradientPinto2 gradient = null;
		ArrayList<Integer> supposedSourcesId;
		int realizations = 1000;
		DoubleMatrix accuracy = new DoubleMatrix(realizations);
		
		long tstart = System.currentTimeMillis();
		for(int r=0; r<realizations; r++) {	
			
			int numberOfObservers = percentOfObservers*size/100;
			Collections.shuffle(observers);
			
			// Run SI with random observers
			si = new SusceptibleInfected(g);
			si.setObservers(new ArrayList<Node>(observers.subList(0, numberOfObservers)));
			si.setOneSource(g.getNodesList().get(4));
			si.setInfectionRate(rate);
			si.runUntilAllObserversInfected();
			sourceID = si.getSources().get(0).getId();
			
			// PTVA-LI with random observers
			pinto = new ParallelPinto2(g);
			pinto.setObservers(si.getObservers());
			pinto.setDelays(si.getInfectionTimeAsDouble());
			pinto.setSupposedSources(g.getNodesList());	
			pinto.setDelayMean(1/rate);
			pinto.setDelayVariance((1-rate)/(rate*rate));
			pinto.setNumberOfThreads(4);
			pinto.estimateScoresForNodes();
			supposedSourcesId = pinto.findNodesWithHighestScores();
			if(supposedSourcesId.contains(sourceID)) accuracy.put(r, 1.0/supposedSourcesId.size());
			else accuracy.put(r, 0.0);
			/*
			// The gradient Pinto
			gradient = new GradientPinto2(g);
			gradient.setObservers(si.getObservers());
			gradient.setNumberOfNearestObservers(numberOfObservers);
			gradient.setDelays(si.getInfectionTimeAsDouble());
			gradient.setDelayMean(1/rate);
			gradient.setDelayVariance((1-rate)/(rate*rate));
			gradient.setMaxThreads(4);
			gradient.estimateScoresForNodes();
			supposedSourcesId = gradient.findNodesWithHighestScores();
			if(supposedSourcesId.contains(sourceID)) System.out.printf("%.3f\n", 1.0/supposedSourcesId.size());
			else System.out.print("0.000\n");
			*/
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, r+1, realizations));
		}
		System.out.printf("%.3f\n", accuracy.mean());

	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}

}