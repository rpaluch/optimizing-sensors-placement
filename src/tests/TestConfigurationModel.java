package tests;

import java.util.ArrayList;
import java.util.Arrays;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Node;
import io.GraphReader;
import jdistlib.Poisson;
import jdistlib.evd.GeneralizedPareto;

public class TestConfigurationModel {

	public static void main(String[] args) {
		
		double clustering = 0.2;
		int size = 1000;
		int sum = 0;
		double kmin = 4;
		double alfa = 2;
		int[] degrees = GraphGenerator.degreeSequence(new Poisson(8), size);
		//int[] degrees = GraphGenerator.degreeSequence(new GeneralizedPareto(kmin,kmin/alfa,1/alfa), size);

		//int[] degrees = new int[size];
		//Arrays.fill(degrees, 6);
		Graph graph = GraphGenerator.configurationModelWithClustering(degrees, clustering);
		//ArrayList<Integer> csizes = GraphTools.findComponents(graph);
		//System.out.println(csizes.size());
		//GraphTools.connectAllComponents(graph);
		//System.out.println(graph.isContiguous());
		/*
		Graph graph = new Graph();
		graph.addLink(1, 2);
		graph.addLink(1, 3);
		graph.addLink(1, 4);
		graph.addLink(2, 3);
		graph.addLink(2, 4);
		graph.addLink(3, 4);
		*/
		graph.printStats();
		System.out.printf("C=%f\n", GraphTools.globalClusteringCoefficient(graph));
		
		Graph graph2 = GraphGenerator.contiguousErdosRenyi(size, 8, "degree");
		System.out.printf("C=%f\n", GraphTools.globalClusteringCoefficient(graph2));
		
		Graph graph3 = GraphGenerator.barabasiAlbert(6, 4, size);
		System.out.printf("C=%f\n", GraphTools.globalClusteringCoefficient(graph3));
		
		Graph graph4 = GraphGenerator.scaleFreeNetwork(size, 4, size-4, 3);
		System.out.printf("C=%f\n", GraphTools.globalClusteringCoefficient(graph4));
		
		GraphReader reader = new GraphReader("/home/rpaluch/Workspace/ReverseEngineering/data/Rovira/arenas-email/rovira.txt");
		graph = reader.readGraphFromEdgeList();
		ArrayList<Node> nodes = GraphTools.highestDegreeNodes(graph, 1);
		graph.printStats();
		System.out.printf("kmax=%d, APL=%f, Diam=%d, C=%f\n", nodes.get(0).getDegree(),
															  GraphTools.averagePathLength(graph),
															  GraphTools.diameter(graph),
															  GraphTools.globalClusteringCoefficient(graph));
		
		reader = new GraphReader("/home/rpaluch/Workspace/ReverseEngineering/data/UC_Irving_messages/UCIrving.net");
		graph = reader.readGraphFromPajek();
		nodes = GraphTools.highestDegreeNodes(graph, 1);
		graph.printStats();
		System.out.printf("kmax=%d, APL=%f, Diam=%d, C=%f\n", nodes.get(0).getDegree(),
															  GraphTools.averagePathLength(graph),
															  GraphTools.diameter(graph),
															  GraphTools.globalClusteringCoefficient(graph));
		
		reader = new GraphReader("/home/rpaluch/Workspace/ReverseEngineering/data/sociopatterns-infectious/infectious.txt");
		graph = reader.readGraphFromEdgeList();
		nodes = GraphTools.highestDegreeNodes(graph, 1);
		graph.printStats();
		System.out.printf("kmax=%d, APL=%f, Diam=%d, C=%f\n", nodes.get(0).getDegree(),
															  GraphTools.averagePathLength(graph),
															  GraphTools.diameter(graph),
															  GraphTools.globalClusteringCoefficient(graph));
	}

}
