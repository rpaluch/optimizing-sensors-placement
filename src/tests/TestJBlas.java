package tests;

import java.util.Arrays;

import org.jblas.DoubleMatrix;

public class TestJBlas {

	public static void main(String[] args) {
		
		double[] data1 = new double[16];
		Arrays.fill(data1, 1.0);
		DoubleMatrix ident = new DoubleMatrix(4,4,data1);
		printMatrix(ident);
		
		double[] data2 = new double[16];
		for(int i=0; i<data2.length; i++)
			data2[i] = i+1;
		
		DoubleMatrix naturals = new DoubleMatrix(4,4,data2);
		printMatrix(naturals);
		
		DoubleMatrix div = ident.add(naturals);
		printMatrix(div);
		
	}
	
	public static void printMatrix(DoubleMatrix matrix) {
		System.out.println();
		for(int i=0; i<matrix.rows; i++) {
			for(int j=0; j<matrix.columns; j++)
				System.out.printf("%3f ", matrix.get(i,j));
			System.out.println();
		}
	}

}
