package tests;

import io.GraphReader;
import io.MultiExpressWriter;

import java.util.ArrayList;

import graphs.Graph;
import graphs.GraphTools;
import graphs.Node;
import graphs.NodesSelectors;
import graphs.PathsTools;

public class TestObserversPlacementRealNetwork {

	public static void main(String[] args) {
		
		// Read the main parameters
		String networkName = args[0];
		String networkFormat = args[1];
		String networkPath = args[2];
		String algorithm = args[3];
		int numberOfThreads = Integer.parseInt(args[4]);
	
		// Read the network from the file
		GraphReader reader = new GraphReader(networkPath);
		Graph graph = null;
		switch(networkFormat){
			case "edgelist": graph = GraphTools.extractLargestComponent(reader.readGraphFromEdgeList());
							 break;
			case "pajek": 	 graph = GraphTools.extractLargestComponent(reader.readGraphFromPajek());
			 				 break;
			default:		 graph = GraphTools.extractLargestComponent(reader.readGraphFromEdgeList());
			 				 break;
		}
		graph.printStats();
		System.out.println("Graph ready.");
		
		// Initialize variables
		long time = 0;
		double[] densityOfObservers = {0.05,0.1,0.15,0.2,0.25,0.3};
		int numberOfObservers = 0;
		int maxLengthCB = 5;
		int maxLengthHV = 5;
		ArrayList<Node> observers = null;
		
		// Prepare output files
		String dir = "../outputs/real_network_observers/";
		String[] files = new String[6];
		for(int i=0; i<densityOfObservers.length; i++) files[i] = dir+networkName+"_"+algorithm+"_O"+densityOfObservers[i]+".txt";
		MultiExpressWriter writer = new MultiExpressWriter(files);
		
		// Main loop
		if(algorithm.equals("BC")) {
			numberOfObservers = (int)(0.3*graph.getNumberOfNodes());
			time = System.currentTimeMillis();
			GraphTools.computeBetweennessCentrality(graph);
			observers = GraphTools.highestBetweennessNodes(graph, numberOfObservers);
			System.out.printf("BC observers found in %.2f sec.\n", 0.001*(System.currentTimeMillis()-time));
		}
		else if(algorithm.equals("CB")) {
			numberOfObservers = (int)(0.3*graph.getNumberOfNodes());
			time = System.currentTimeMillis();
			observers = NodesSelectors.fastCollectiveBetweenness(graph, numberOfObservers, maxLengthCB);
			System.out.printf("CB observers found in %.2f sec.\n", 0.001*(System.currentTimeMillis()-time));
		}
		for(int i=0; i<densityOfObservers.length; i++){
			numberOfObservers = (int)(densityOfObservers[i]*graph.getNumberOfNodes());
			maxLengthHV = 5;
			if(densityOfObservers[i] <= 0.1) {
				maxLengthHV = 6;
				if(densityOfObservers[i] <= 0.05) maxLengthHV = 7;
			}
			time = System.currentTimeMillis();
			switch(algorithm){
				case "Coverage":observers = NodesSelectors.greedyCoverageObsParallel(graph, numberOfObservers, numberOfThreads);
								System.out.printf("Coverage observers (dens=%.2f) found in %.2f sec.\n", densityOfObservers[i], 0.001*(System.currentTimeMillis()-time));
								break;
				case "KMedian":	observers = NodesSelectors.greedyKMedianObsParallel(graph, PathsTools.allDistancesInGraph(graph), numberOfObservers, numberOfThreads);
								System.out.printf("KMedian observers (dens=%.2f) found in %.2f sec.\n", densityOfObservers[i], 0.001*(System.currentTimeMillis()-time));
								break;
				case "HVObs":	observers = NodesSelectors.greedyHVObsParallel(graph, numberOfObservers, maxLengthHV, numberOfThreads);
								System.out.printf("HVObs observers (dens=%.2f) found in %.2f sec.\n", densityOfObservers[i], 0.001*(System.currentTimeMillis()-time));
								break;
				default:		break;
			}
			writer.init(i);
			for(int j=0; j<numberOfObservers; j++)
				writer.printf(i, "%d\n", observers.get(j).getIndex());
			writer.close(i);
		}
		
	}
}
