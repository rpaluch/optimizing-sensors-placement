package tests;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.PathsTools;
import io.ExpressWriter;

public class TestGraphTools {

	public static void main(String[] args) {
		
		ExpressWriter shortest_paths_time = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/shortest_paths_time.txt");
		int realizations = 11;
		long tstart, tstop, totaltime;
		
		int[] size = {100,150,200,300,400,500};
		int k = 4;
		Graph er;
		
		shortest_paths_time.init();
		for(int r=0; r<realizations; r++) {
			for(int i=0; i<size.length; i++) {
				er = GraphGenerator.contiguousErdosRenyi(size[i], size[i]*k/2);
				totaltime = 0;
				for(int j=1; j<size[i]; j++)
					for(int m=j+1; m<=size[i]; m++){
						tstart = System.nanoTime();
						PathsTools.shortestPath(er, j, m);
						tstop = System.nanoTime();
						totaltime = totaltime + (tstop-tstart);
					}
				shortest_paths_time.printf("%.0f\t", 2.0*totaltime/(size[i]*(size[i]-1)));
				System.out.printf("%.0f\t", 2.0*totaltime/(size[i]*(size[i]-1)));
			}
			shortest_paths_time.printf("\n");
			System.out.println();
		}
		shortest_paths_time.close();
	}

}
