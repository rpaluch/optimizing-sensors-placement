package tests;

import java.util.ArrayList;
import java.util.LinkedList;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Node;
import graphs.Path;
import graphs.PathsTools;

public class TestPathsRegister {

	public static void main(String[] args) {
		
		Graph g = new Graph();
		g.addLink(1, 2);
		g.addLink(1, 3);
		g.addLink(1, 4);
		g.addLink(1, 5);
		g.addLink(2, 6);
		g.addLink(2, 7);
		g.addLink(2, 8);
		g.addLink(3, 4);
		g.addLink(3, 6);
		g.addLink(5, 8);
		g.addLink(8, 9);
		g.addLink(9, 7);
		g.addLink(7, 10);
		g.addLink(6, 10);
		
		//g = GraphGenerator.contiguousErdosRenyi(5000, 8, "degree");
		
		//ArrayList<Integer> observers = GraphTools.nodesToIDs(g.getRandomNodes(500));
		ArrayList<Integer> observers = new ArrayList<Integer>();
		observers.add(1);
		observers.add(9);
		observers.add(10);
		
		ArrayList<ArrayList<Path>> paths1 = PathsTools.allShortestPaths(g, 1, observers);
		
		//ArrayList<LinkedList<Node>> paths3 = PathsTools.shortestPaths(g, 1, observers); 
		/*
		long tstart = System.nanoTime();
		paths1 = PathsTools.allShortestPaths(g, 1, observers);
		System.out.println(0.001*(System.nanoTime()-tstart));
		*/
		
		for(ArrayList<Path> paths: paths1){
			System.out.println(PathsTools.pathsToString(paths));
			System.out.println();
		}
		System.out.println();
		System.out.println();
		/*
		//tstart = System.nanoTime();
		
		//System.out.println(0.001*(System.nanoTime()-tstart));
		/*
		for(ArrayList<Path> paths: paths2){
			System.out.println(PathsTools.pathsToString(paths));
			System.out.println();
		}
		*/
		//tstart = System.nanoTime();
		//paths3 = PathsTools.shortestPaths(g, 1, observers);
		//System.out.println(0.001*(System.nanoTime()-tstart));
		
		/*
		PathsRegister register = PathsTools.allShortestPathsInGraph(g);
		System.out.println(register.getAllPathsAsString());
		System.out.println(PathsTools.pathsOverlapEPL(register.getPaths(1, 9), register.getPaths(1, 10)));
		ArrayList<Link> union1 = PathsTools.union(register.getPaths(1, 9));
		for(Link l : union1)
			l.print();
		ArrayList<Link> union2 = PathsTools.union(register.getPaths(1, 10));
		for(Link l : union2)
			l.print();
		*/
		
		
	}

}
