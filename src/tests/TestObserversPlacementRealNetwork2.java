package tests;

import io.GraphReader;
import io.MultiExpressWriter;
import tracking.SingleSourceDetector;

import java.util.ArrayList;

import dynamics.SusceptibleInfected;
import graphs.Graph;
import graphs.GraphTools;
import graphs.Node;
import graphs.NodesSelectors;
import graphs.PathsTools;

public class TestObserversPlacementRealNetwork2 {

	public static void main(String[] args) {
		
		// Read the main parameters
		String networkName = args[0];
		String networkPath = args[1];
		double densityOfObservers = Double.parseDouble(args[2]);
		int realizations = Integer.parseInt(args[3]);
		int numberOfThreads = Integer.parseInt(args[4]);
		String part = args[5];
		
		// Read the network from the file
		GraphReader reader = new GraphReader(networkPath);
		Graph graph = reader.readGraphFromEdgeList();
		graph.printStats();
		System.out.println("Graph ready.");
		
		// Initialize variables
		long time = 0;
		int numberOfObservers = (int)(densityOfObservers*graph.getNumberOfNodes());
		int numberOfNearestObservers = 20;
		int maxLengthCB = 3;
		int maxLengthHV = 3;
		if(densityOfObservers <= 0.1) {
			maxLengthHV = 4;
			if(densityOfObservers <= 0.05) maxLengthHV = 5;
		}
		double rate = 0;
		double[] sigma = {0.95};
		
		// Spreading and detection
		SusceptibleInfected si;
		int sourceId;
		SingleSourceDetector detector;
		ArrayList<Integer> supposedSourcesId;
		
		// Prepare observers sets
		String[] sets = {"Random","Coverage","KMedian","HVObs","CB","BC"};
		ArrayList<ArrayList<Node>> observers = new ArrayList<ArrayList<Node>>();		
		// (0) Random observers
		observers.add(graph.getRandomNodes(numberOfObservers));
		// (1) Coverage observers
		time = System.currentTimeMillis();
		observers.add(NodesSelectors.greedyCoverageObsParallel(graph, numberOfObservers, numberOfThreads));
		System.out.printf("Coverage observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
		// (2) KMedian observers
		time = System.currentTimeMillis();
		observers.add(NodesSelectors.greedyKMedianObsParallel(graph, PathsTools.allDistancesInGraph(graph), numberOfObservers, numberOfThreads));
		System.out.printf("KMedian observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
		// (3) HVObs observers
		time = System.currentTimeMillis();
		observers.add(NodesSelectors.greedyHVObsParallel(graph, numberOfObservers, maxLengthHV, numberOfThreads));
		System.out.printf("HVObs observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
		// (4) CB3 observers
		time = System.currentTimeMillis();
		observers.add(NodesSelectors.fastCollectiveBetweenness(graph, numberOfObservers, maxLengthCB));
		System.out.printf("CB3 observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
		// (5) BC observers
		time = System.currentTimeMillis();
		GraphTools.computeBetweennessCentrality(graph);
		observers.add(GraphTools.highestBetweennessNodes(graph, numberOfObservers));
		System.out.printf("BC observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
		
		// Prepare output files
		String[] files = new String[24];
		String[] names = new String[24];
		String dir1 = "../outputs/observers_placement_real_networks/ptva_"+networkName+"_O"+densityOfObservers+"_part"+part;
		String dir2 = "../outputs/observers_placement_real_networks/gmla_"+networkName+"_O"+densityOfObservers+"_part"+part;
		int counter = 0;
		for(int op=0; op<sets.length; op++) {
			files[counter] = dir1+"_"+sets[op]+"_precision.txt";
			names[counter] = "precision_ptva_"+sets[op];
			counter++;
			files[counter] = dir1+"_"+sets[op]+"_rank.txt";
			names[counter] = "rank_ptva_"+sets[op];
			counter++;
		}
		for(int op=0; op<sets.length; op++) {
			files[counter] = dir2+"_"+sets[op]+"_precision.txt";
			names[counter] = "precision_gmla_"+sets[op];
			counter++;
			files[counter] = dir2+"_"+sets[op]+"_rank.txt";
			names[counter] = "rank_gmla_"+sets[op];
			counter++;
		}			
		MultiExpressWriter writer = new MultiExpressWriter(files, names);
		writer.initAll();
		writer.printlnHeaderAll(sigma, "sigma", "\t");
		
		// Main loop
		long tstart = System.currentTimeMillis();
		for(int i=0; i<realizations; i++) {
			for(int r=0; r<sigma.length; r++) {
				
				rate = 1-sigma[r]*sigma[r];
				
				si = new SusceptibleInfected(graph);
				si.setObservers(graph.getNodesList());
				si.setInfectionRate(rate);
				si.runUntilAllObserversInfected();
				sourceId = si.getSources().get(0).getId();
				observers.set(0,graph.getRandomNodes(numberOfObservers));
				
				for(int s=0; s<sets.length; s++) {
					//PTVA-LI
					detector = new SingleSourceDetector(graph);
					detector.setObservers(observers.get(s));
					detector.setDelays(si.getInfectionTimesForGivenObservers(observers.get(s)));
					detector.setDelayMean(1/rate);
					detector.setDelayVariance((1-rate)/(rate*rate));
					detector.setMaxThreads(numberOfThreads);
					detector.setSupposedSources(graph.getNodesList());
					detector.computeScores("LPTV");
					supposedSourcesId = detector.findNodesWithHighestScores();
					if(supposedSourcesId.contains(sourceId)) 
						writer.printf("precision_ptva_"+sets[s],"%.3f\t",1.0/supposedSourcesId.size());
					else writer.printf("precision_ptva_"+sets[s],"0.000\t");
					writer.printf("rank_ptva_"+sets[s],"%d\t",detector.findRankOfNode(sourceId));

					//GMLA
					detector = new SingleSourceDetector(graph);
					detector.setObservers(observers.get(s));
					detector.setDelays(si.getInfectionTimesForGivenObservers(observers.get(s)));
					detector.setDelayMean(1/rate);
					detector.setDelayVariance((1-rate)/(rate*rate));
					detector.setMaxThreads(numberOfThreads);
					detector.setNumberOfGradientWalks(1);
					detector.setNumberOfNearestObservers(numberOfNearestObservers);
					detector.computeScores("LPTV");
					supposedSourcesId = detector.findNodesWithHighestScores();
					if(supposedSourcesId.contains(sourceId)) 
						writer.printf("precision_gmla_"+sets[s],"%.3f\t",1.0/supposedSourcesId.size());
					else writer.printf("precision_gmla_"+sets[s],"0.000\t");
					writer.printf("rank_gmla_"+sets[s],"%d\t",detector.findRankOfNode(sourceId));
				}
			}
			writer.newLineAll();
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, i+1, realizations));
		}
		writer.closeAll();
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}

}
