package tests;

import io.ExpressWriter;
import graphs.GraphTools;
import graphs.Link;
import graphs.Node;
import graphs.Graph;
import graphs.GraphGenerator;

public class TestGraphGenerator2 {

	public static void main(String[] args) {
		
		ExpressWriter writer1 = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/networks/networksProperties/erg2_degrees.txt");
		ExpressWriter writer2 = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/networks/networksProperties/erg2_pairs.txt");
		ExpressWriter writer3 = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/networks/networksProperties/erg2_clustering.txt");
		int realizations = 1000;
		Graph graph;
		int size = 1000;
		double lambda = 0.3;
		
		writer1.init();
		writer2.init();
		writer3.init();
		for(int r=0; r<realizations; r++){
			
			graph = GraphGenerator.exponentialRandomGraph(size, 3, size-1, lambda);
			for(Node n: graph.getNodesList()) writer1.printf("%d\n", n.getDegree());
			for(Link l: graph.getLinksList()) writer2.printf("%d\t%d\n", l.getStart().getDegree(), l.getEnd().getDegree());
			writer3.printf("%.4f\n", GraphTools.averageClusteringCoefficient(graph));
		}
		writer1.close();
		writer2.close();
		writer3.close();
		/*
		writer1 = new ExpressWriter("../outputs/networksProperties/ba_degrees.txt");
		writer2 = new ExpressWriter("../outputs/networksProperties/ba_pairs.txt");
		writer3 = new ExpressWriter("../outputs/networksProperties/ba_clustering.txt");
			
		writer1.init();
		writer2.init();
		writer3.init();
		for(int r=0; r<realizations; r++){
			
			graph = GraphGenerator.generateBarabasiAlbert(m+2, m, size);
			for(Node n: graph.getNodesList()) writer1.printf("%d\n", n.getDegree());
			for(Link l: graph.getLinksList()) writer2.printf("%d\t%d\n", l.getStart().getDegree(), l.getEnd().getDegree());
			writer3.printf("%.4f\n", GraphTools.averageClusteringCoefficient(graph));
			
		}
		writer1.close();
		writer2.close();
		writer3.close();
		
		writer1 = new ExpressWriter("../outputs/networksProperties/er_degrees.txt");
		writer2 = new ExpressWriter("../outputs/networksProperties/er_pairs.txt");
		writer3 = new ExpressWriter("../outputs/networksProperties/er_clustering.txt");
		
		writer1.init();
		writer2.init();
		writer3.init();
		for(int r=0; r<realizations; r++){
			
			graph = GraphGenerator.generateErdosRenyi(size, size*m);
			for(Node n: graph.getNodesList()) writer1.printf("%d\n", n.getDegree());
			for(Link l: graph.getLinksList()) writer2.printf("%d\t%d\n", l.getStart().getDegree(), l.getEnd().getDegree());
			writer3.printf("%.4f\n", GraphTools.averageClusteringCoefficient(graph));
			
		}
		writer1.close();
		writer2.close();
		writer3.close();
		
		writer1 = new ExpressWriter("../outputs/networksProperties/erg_degrees.txt");
		writer2 = new ExpressWriter("../outputs/networksProperties/erg_pairs.txt");
		writer3 = new ExpressWriter("../outputs/networksProperties/erg_clustering.txt");
		
		writer1.init();
		writer2.init();
		writer3.init();
		for(int r=0; r<realizations; r++){
			
			graph = GraphGenerator.generateExponentialRandomGraph(m+2, m, size, 0.0);
			for(Node n: graph.getNodesList()) writer1.printf("%d\n", n.getDegree());
			for(Link l: graph.getLinksList()) writer2.printf("%d\t%d\n", l.getStart().getDegree(), l.getEnd().getDegree());
			writer3.printf("%.4f\n", GraphTools.averageClusteringCoefficient(graph));
			
		}
		writer1.close();
		writer2.close();
		writer3.close();
		*/
	}

}
