package tests;

import java.util.ArrayList;
import java.util.LinkedList;

import org.jblas.DoubleMatrix;

import dynamics.SusceptibleInfected;
import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Link;
import graphs.Node;
import graphs.PathsTools;
import io.ExpressWriter;

public class TestObserversCorrelations {

	public static void main(String[] args) {
		
		String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/observers_correlations/ba_N1000_k6_O0.2_";
		ExpressWriter writer = new ExpressWriter(dir+"obs_mean_corr4.txt");
		writer.init();
		//writer.println("N200\tN500\tN1000\tN2000\tN5000");
		
		int realizations = 1000;
		int[] size = {1000};
		int numberOfObservers;
		//int independent;
		int dim;
		int m = 3;
		//double threshold = 0.66;
		Graph g;
		Graph tree;
		SusceptibleInfected si;
		//double rate = 0.5;
		//int sourceID;
		ArrayList<Integer> observersID;
		ArrayList<LinkedList<Node>> allPaths;
		ArrayList<Integer> pathsLengths;
		//ArrayList<Node> dependentObservers;
		int commonLength;
		
		long tstart = System.currentTimeMillis();
		for(int r=0; r<realizations; r++) {
			for(int s=0; s<size.length; s++){
				numberOfObservers = 2*size[s]/10;
				//independent = numberOfObservers;
				g = GraphGenerator.barabasiAlbert(m+2, m, size[s]);
				si = new SusceptibleInfected(g);
				si.addRandomObservers(numberOfObservers);
				//si.setInfectionRate(rate);
				//si.runUntilAllObserversInfected();
				//sourceID = si.getSources().get(0).getId();
				observersID = new ArrayList<Integer>();
				for(Node obs : si.getObservers()) observersID.add(obs.getId());
				tree = GraphTools.findBFSTree(g, g.getRandomNode().getId(), observersID);
				//dependentObservers = new ArrayList<Node>();
				// The list of all paths between reference observer and other observers
				dim = numberOfObservers-1;
				allPaths = PathsTools.shortestPaths(tree, observersID.get(0), observersID);
				DoubleMatrix delCov = DoubleMatrix.zeros(dim, dim);
				pathsLengths = new ArrayList<Integer>();
				// First fill diagonal elements
				for(int i=0; i<dim; i++) pathsLengths.add(allPaths.get(i+1).size()-1);
				// Next fill the rest of matrix
				for(int i=0; i<dim-1; i++) {
					for(int j=i+1; j<dim; j++) {
						commonLength = lengthOfCommonPart(allPaths.get(i+1),allPaths.get(j+1));
						delCov.put(i,j,2.0*commonLength/(pathsLengths.get(i)+pathsLengths.get(j)));
						delCov.put(j,i,2.0*commonLength/(pathsLengths.get(i)+pathsLengths.get(j)));
					}
				}
				for(int i=0; i<dim; i++)
					writer.printf("%.3f\n", delCov.getColumn(i).sum()/(dim-1));
			}
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, r+1, realizations));
			//writer.printf("\n");
		}
		writer.close();
	}
	
	public static int lengthOfCommonPart(LinkedList<Node> path1, LinkedList<Node> path2) {
		int length = 0;
		int maxLength = Math.min(path1.size(), path2.size())-1;
		Link link1 = new Link(path1.get(0), path1.get(1));
		Link link2 = new Link(path2.get(0), path2.get(1));
		while(link1.equals(link2)) {
			length = length + 1;
			if(length+1>maxLength) break;
			link1.setStart(path1.get(length));
			link1.setEnd(path1.get(length+1));
			link2.setStart(path2.get(length));
			link2.setEnd(path2.get(length+1));
		}
		return length;
	}

	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}
}

