package tests;

import io.ExpressWriter;

import java.util.ArrayList;
import java.util.Collections;

import tracking.Observer;
import dynamics.SusceptibleInfected;
import graphs.Graph;
import graphs.GraphTools;
import graphs.Node;
import graphs.GraphGenerator;

public class TestSusceptibleInfected {

	public static void main(String[] args) {
		
		ExpressWriter writer = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/bfs_similarity/ba_N1000_k6_O0.2_beta0.5_bfs_similarity.txt");
		int realizations = 1000;
		int size = 1000;
		int m = 3;
		int numberOfObservers = 200;
		int[] numberOfNearestObservers = {3,5,7,10,15,20,30,40,50,60,70,80,90,100,120,140,160,180,200};
		String header = "no3\tno5\tno7\tno10\tno15\tno20\tno30\tno40\tno50\tno60\tno70\tno80\tno90\tno100\tno120\tno140\tno160\tno180\tno200";
		ArrayList<Integer> indices = new ArrayList<Integer>();
		for(int i=0; i<size; i++) indices.add(i);
		ArrayList<Observer> observers;
		ArrayList<Integer> nearestObserversIds;
		Node source;
		Graph limitedCascade, bfs;
		//double[] similarity = new double[numberOfNearestObservers.length];
		
		writer.init();
		writer.println(header);
		long tstart = System.currentTimeMillis();
		for(int r=0; r<realizations; r++){
			for(int k=0; k<numberOfNearestObservers.length; k++){
				
				Graph ba = GraphGenerator.barabasiAlbert(m+2, m, size);
				
				SusceptibleInfected si = new SusceptibleInfected(ba);
				si.setInfectionRate(0.5);
				si.setObservers(ba.getNodesList());
				si.runUntilAllInfected();
				source = si.getSources().get(0);
				
				Collections.shuffle(indices);
				observers = new ArrayList<Observer>();
				for(int i=0; i<numberOfObservers; i++) 
					observers.add(new Observer(si.getObservers().get(indices.get(i)), si.getInfectionTime().get(indices.get(i)).doubleValue()));
				
				Collections.sort(observers, Observer.delaysAscending);
				
				nearestObserversIds = new ArrayList<Integer>();
				for(int i=0; i<numberOfNearestObservers[k]; i++)
					nearestObserversIds.add(observers.get(i).getNode().getId());
				
				limitedCascade = GraphTools.findBFSTree(si.getCascade(), source.getId(), nearestObserversIds);
				bfs = GraphTools.findBFSTree(ba, source.getId(), nearestObserversIds);
				//similarity[k] = similarity[k] + GraphTools.graphsSimilarity(limitedCascade, bfs);
				writer.printf("%.3f\t", GraphTools.graphsSimilarity(limitedCascade, bfs));
			}
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, r+1, realizations));
			writer.printf("\n");
		}
		writer.close();
		//for(int i=0; i<similarity.length; i++) System.out.printf("%d: %.2f\n", numberOfNearestObservers[i],100*similarity[i]/realizations);	
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d", currentReal, 
																					   totalReal, 
																					   0.001*elapsed, 
																					   0.001*elapsed/3600,
																					   hour, 
																					   minute, 
																					   second);
	}

}
