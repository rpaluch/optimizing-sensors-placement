package tests;

import java.util.ArrayList;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Node;
import graphs.PathsTools;
import graphs.PathsRegister;
import io.ExpressWriter;

public class TestGraphTools3 {

	public static void main(String[] args) {
		
		String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/observers_placement3/hvobs_er_k6_O0.1";
		//ExpressWriter writer1 = new ExpressWriter(dir+"_time.txt");
		//writer1.init();
		//writer1.println("N50\tN100\tN200\tN300\tN500\tN1000");
		
		long tstart, t0;
		int[] size = {1000};
		int k = 4;
		Graph graph;
		PathsRegister register;
		//PathsRegister limitedRegister;
		//int maxLength = 3;
		ArrayList<Node> observers = new ArrayList<Node>();
		//ArrayList<Node> observers2 = new ArrayList<Node>();
		//ArrayList<Node> union;
		double budget = 0.075;
		int realizations = 1;
		//int counts;
		
		tstart = System.currentTimeMillis();
		for(int r=0; r<realizations; r++) {
			
			//counts = 0;
			
			for(int i=0; i<size.length; i++) {
				
				//tstart = System.currentTimeMillis();
				graph = GraphGenerator.contiguousErdosRenyi(size[i], 2*k, "degree");
				//graph = GraphGenerator.barabasiAlbert(k+2, k, size[i]);
				//System.out.printf("%d\t", System.currentTimeMillis()-tstart);
				System.out.printf("%d %d\n", size[i], GraphTools.diameter(graph));
				
				//tstart = System.currentTimeMillis();
				register = PathsTools.allShortestPathsInGraph(graph);
				//limitedRegister = GraphTools.allLimitedShortestPathsInGraph(graph,3);
				//System.out.printf("%d\t", System.currentTimeMillis()-tstart);
				
				//GraphTools.betweennessCentrality(graph, register);
				
				//	System.out.printf("%d\t%.3f\n", n.getId(), n.getBetweenness());
				
				//t0 = System.currentTimeMillis();
				//observers = GraphTools.greedyCollectiveBetweenness(graph, register, (int)(size[i]*budget));
				//System.out.printf("%d\n", System.currentTimeMillis()-t0);
				//System.out.printf("%d\t", System.currentTimeMillis()-t0);
				//for(Node n : observers)
					//System.out.println(x);
					//writer1.printf("%.3f ", n.getBetweenness());
				//writer1.printf("\n");
				//GraphTools.betweennessCentrality(graph, register);
				//limitedRegister = GraphTools.allLimitedShortestPathsInGraph(graph,maxLength);
				//observers2 = GraphTools.highestBetweennessNodes(graph, size[i]);
				//for(Node n : graph.getNodesList())
				//for(Node n : observers)
					//System.out.printf("%d ", observers2.indexOf(n)+1);
				//System.out.printf("\n");
				
				//for(Node n : graph.getNodesList())
				//	n.setBetweenness(0.0);
				t0 = System.currentTimeMillis();
				observers = GraphTools.greedyHVObsParallel(graph, register, (int)(size[i]*budget), 3, 1);
				//writer1.printf("%d\t", System.currentTimeMillis()-t0);
				//for(Node n : observers)
					//System.out.printf("%d ", observers2.indexOf(n)+1);
				//System.out.printf("\n");

				//tstart = System.currentTimeMillis();
				//observers2 = GraphTools.greedyCollectiveBetweennessParallel(graph, limitedRegister, (int)(size[i]*budget), 10);
				//System.out.printf("%d\n", System.currentTimeMillis()-tstart);
				/*
				union = new ArrayList<Node>();
				for(Node n : observers)
					if(observers2.contains(n))
						union.add(n);
				counts = counts + union.size();
				*/
				
			}
			//System.out.println(estimateTime(System.currentTimeMillis()-tstart, r+1, realizations));
			//System.out.printf("%.2f\n", 1.0*counts/(realizations*size[i]));
			//writer1.printf("\n");
		}
		//writer1.close();
		
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}

}
