package tests;

import java.util.ArrayList;
import java.util.Collections;

import dynamics.SusceptibleInfected;
import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Node;
import io.ExpressWriter;
import tracking.ParallelPinto2;

public class TestSeparatedNodes2 {

	public static void main(String[] args) {
		
		String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/separated_observers/pinto_sfn_N500_gamma3_O0.2_beta0.5_";
		ExpressWriter writer = new ExpressWriter(dir+"separatedByDegree_accuracy.txt");
		writer.init();
		writer.println("random\tsep.high\tsep.low\tsep.rand");
		
		int realizations = 100;
		int size = 500;	
		int m = 3;
		int degreeOfSeparation = 2;
		Graph g; 
		ArrayList<Node> separatedNodes;
		//ArrayList<Integer> observersID;
		//ArrayList<Observer> separatedObservers;
		//ArrayList<LinkedList<Node>> allPaths;
		
		SusceptibleInfected si;
		double rate = 0.5;
		int sourceID;
		int percentOfObservers = 20;
		
		ParallelPinto2 pinto = null;
		ArrayList<Integer> supposedSourcesId;
		
		long tstart = System.currentTimeMillis();
		for(int r=0; r<realizations; r++) {		
			int numberOfObservers = percentOfObservers*size/100;
			// Generate graph and find separated nodes
			//g = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
			//g = GraphGenerator.barabasiAlbert(m+2, m, size);
			g = GraphGenerator.scaleFreeNetwork(size, m, size-1, 3);
			separatedNodes = GraphTools.findSeparatedNodesByDegree(g, degreeOfSeparation);
			while(!g.isContiguous() || separatedNodes.size()<numberOfObservers){ 
				g = GraphGenerator.scaleFreeNetwork(size, m, size-1, 3);
				separatedNodes = GraphTools.findSeparatedNodesByDegree(g, degreeOfSeparation);
			}
			
			
			// Run SI with random observers
			si = new SusceptibleInfected(g);
			si.addRandomObservers(numberOfObservers);
			si.setInfectionRate(rate);
			si.runUntilAllObserversInfected();
			sourceID = si.getSources().get(0).getId();
			
			// PTVA-LI with random observers
			pinto = new ParallelPinto2(g);
			pinto.setObservers(si.getObservers());
			pinto.setDelays(si.getInfectionTimeAsDouble());
			pinto.setSupposedSources(g.getNodesList());	
			pinto.setDelayMean(1/rate);
			pinto.setDelayVariance((1-rate)/(rate*rate));
			pinto.setNumberOfThreads(6);
			pinto.estimateScoresForNodes();
			supposedSourcesId = pinto.findNodesWithHighestScores();
			if(supposedSourcesId.contains(sourceID)) writer.printf("%.3f\t", 1.0/supposedSourcesId.size());
			else writer.print("0.000\t");
			
			// Run SI with separated highest-degree observers
			si = new SusceptibleInfected(g);
			si.setObservers(new ArrayList<Node>(separatedNodes.subList(0, numberOfObservers)));
			si.setInfectionRate(rate);
			si.runUntilAllObserversInfected();
			sourceID = si.getSources().get(0).getId();
			
			// PTVA-LI with separated highest-degree observers
			pinto = new ParallelPinto2(g);
			pinto.setObservers(si.getObservers());
			pinto.setDelays(si.getInfectionTimeAsDouble());
			pinto.setSupposedSources(g.getNodesList());	
			pinto.setDelayMean(1/rate);
			pinto.setDelayVariance((1-rate)/(rate*rate));
			pinto.setNumberOfThreads(6);
			pinto.estimateScoresForNodes();
			supposedSourcesId = pinto.findNodesWithHighestScores();
			if(supposedSourcesId.contains(sourceID)) writer.printf("%.3f\t", 1.0/supposedSourcesId.size());
			else writer.print("0.000\t");
			
			// Run SI with separated lowest-degree observers
			si = new SusceptibleInfected(g);
			si.setObservers(new ArrayList<Node>(separatedNodes.subList(separatedNodes.size()-numberOfObservers, separatedNodes.size())));
			si.setInfectionRate(rate);
			si.runUntilAllObserversInfected();
			sourceID = si.getSources().get(0).getId();
			
			// PTVA-LI with separated lowest-degree observers
			pinto = new ParallelPinto2(g);
			pinto.setObservers(si.getObservers());
			pinto.setDelays(si.getInfectionTimeAsDouble());
			pinto.setSupposedSources(g.getNodesList());	
			pinto.setDelayMean(1/rate);
			pinto.setDelayVariance((1-rate)/(rate*rate));
			pinto.setNumberOfThreads(6);
			pinto.estimateScoresForNodes();
			supposedSourcesId = pinto.findNodesWithHighestScores();
			if(supposedSourcesId.contains(sourceID)) writer.printf("%.3f\t", 1.0/supposedSourcesId.size());
			else writer.print("0.000\t");
			
			// Run SI with separated random observers
			Collections.shuffle(separatedNodes);
			si = new SusceptibleInfected(g);
			si.setObservers(new ArrayList<Node>(separatedNodes.subList(0, numberOfObservers)));
			si.setInfectionRate(rate);
			si.runUntilAllObserversInfected();
			sourceID = si.getSources().get(0).getId();
			
			// PTVA-LI with separated random observers
			pinto = new ParallelPinto2(g);
			pinto.setObservers(si.getObservers());
			pinto.setDelays(si.getInfectionTimeAsDouble());
			pinto.setSupposedSources(g.getNodesList());	
			pinto.setDelayMean(1/rate);
			pinto.setDelayVariance((1-rate)/(rate*rate));
			pinto.setNumberOfThreads(6);
			pinto.estimateScoresForNodes();
			supposedSourcesId = pinto.findNodesWithHighestScores();
			if(supposedSourcesId.contains(sourceID)) writer.printf("%.3f\t", 1.0/supposedSourcesId.size());
			else writer.print("0.000\t");
			
			/*
			// Find the separated observers with the highest mean distance	
			observersID = new ArrayList<Integer>();
			separatedObservers = new ArrayList<Observer>();
			for(Node n : separatedNodes)
				observersID.add(n.getId());
			DoubleMatrix distances = DoubleMatrix.zeros(observersID.size(), observersID.size());
			for(int i=0; i<observersID.size(); i++){
				allPaths = GraphTools.shortestPaths(g, observersID.get(i), observersID);
				for(int j=0; j<observersID.size(); j++)
					distances.put(i,j,allPaths.get(j).size()-1);
				separatedObservers.add(new Observer(separatedNodes.get(i),-1.0,distances.getRow(i).sum()/(observersID.size()-1)));
			}
			Collections.sort(separatedObservers, Observer.meanCorrelationDescending);
			
			// Run SI with top separated observers
			si = new SusceptibleInfected(g);
			for(int i=0; i<numberOfObservers; i++)
				si.addObserver(separatedObservers.get(i).getNode());
			si.setInfectionRate(rate);
			si.runUntilAllObserversInfected();
			sourceID = si.getSources().get(0).getId();
			
			// PTVA-LI with top separated observers
			pinto = new ParallelPinto2(g);
			pinto.setObservers(si.getObservers());
			pinto.setDelays(si.getInfectionTimeAsDouble());
			pinto.setSupposedSources(g.getNodesList());	
			pinto.setDelayMean(1/rate);
			pinto.setDelayVariance((1-rate)/(rate*rate));
			pinto.setNumberOfThreads(4);
			pinto.estimateScoresForNodes();
			supposedSourcesId = pinto.findNodesWithHighestScores();
			if(supposedSourcesId.contains(sourceID)) writer.printf("%.3f\t", 1.0/supposedSourcesId.size());
			else writer.print("0.000\t");
			
			// Run SI with last separated observers
			si = new SusceptibleInfected(g);
			for(int i=0; i<numberOfObservers; i++)
				si.addObserver(separatedObservers.get(separatedObservers.size()-1-i).getNode());
			si.setInfectionRate(rate);
			si.runUntilAllObserversInfected();
			sourceID = si.getSources().get(0).getId();
			
			// PTVA-LI with top separated observers
			pinto = new ParallelPinto2(g);
			pinto.setObservers(si.getObservers());
			pinto.setDelays(si.getInfectionTimeAsDouble());
			pinto.setSupposedSources(g.getNodesList());	
			pinto.setDelayMean(1/rate);
			pinto.setDelayVariance((1-rate)/(rate*rate));
			pinto.setNumberOfThreads(4);
			pinto.estimateScoresForNodes();
			supposedSourcesId = pinto.findNodesWithHighestScores();
			if(supposedSourcesId.contains(sourceID)) writer.printf("%.3f\t", 1.0/supposedSourcesId.size());
			else writer.print("0.000\t");
			*/
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, r+1, realizations));
			writer.printf("\n");
		}
		writer.close();
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}

}