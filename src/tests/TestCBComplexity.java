package tests;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.NodesSelectors;
import io.ExpressWriter;
import io.MemoryUsage;

public class TestCBComplexity {

	public static void main(String[] args) {
		
		String outputdir = args[0];
		String graphType = args[1];
		int degree = Integer.parseInt(args[2]);
		int m = degree/2;
		double densityOfObservers = Double.parseDouble(args[3]);
		int maxLength = Integer.parseInt(args[4]);
		int realizations = Integer.parseInt(args[5]);
		
		String header = "";
		int[] size = new int[args.length-6];
		for(int i=0; i<size.length; i++) {
			size[i] = Integer.parseInt(args[i+6]);
			header = header + "N" + args[i+6] + "\t";
		}

		String path = outputdir+"/"+graphType+"_k"+degree+"_O"+densityOfObservers+"_L"+maxLength;
		ExpressWriter writerMemory = new ExpressWriter(path+"_memory.txt");
		ExpressWriter writerTime = new ExpressWriter(path+"_time.txt");
		writerMemory.init();
		writerMemory.println(header);
		writerTime.init();
		writerTime.println(header);
		
		Graph g;
		MemoryUsage memoryMeasurement;
		long tstartCB = 0;
		long time = 0;
		long tstart = System.currentTimeMillis();
		for(int i=0; i<realizations; i++) {
			for(int s=0; s<size.length; s++) {
				System.out.printf("N = %d, status: ", size[s]);
				time = System.currentTimeMillis();
				// Generate graph
				switch(graphType) {
					case "ba":	g = GraphGenerator.barabasiAlbert(m+2, m, size[s]);
						    	break;
					case "er":  g = GraphGenerator.contiguousErdosRenyi(size[s], degree, "degree");
								break;
					case "rrg": g = GraphGenerator.regularRandomGraph(size[s], degree);
								while(!g.isContiguous()) g = GraphGenerator.regularRandomGraph(size[s], degree);
								break;
					case "sfn": g = GraphGenerator.scaleFreeNetwork(size[s], m, size[s]-m, 3);
								while(!g.isContiguous()) g = GraphGenerator.scaleFreeNetwork(size[s], m, size[s]-m, 3);
								break;
					default:	g = GraphGenerator.contiguousErdosRenyi(size[s], degree, "degree");
								break;
							
				}
				
				// Start memory usage measurement
				memoryMeasurement = new MemoryUsage();
				memoryMeasurement.start();
				// Find observers
				tstartCB = System.currentTimeMillis();
				NodesSelectors.fastCollectiveBetweenness(g, (int)(densityOfObservers*size[s]), maxLength);
				writerTime.printf("%d\t", System.currentTimeMillis()-tstartCB);
				// Stop memory usage measurement
				memoryMeasurement.stopMeasurement();
				writerMemory.printf("%d\t", memoryMeasurement.getDiffMemoryUsage());
				System.out.printf("finished in %d ms\n",System.currentTimeMillis()-time);
			}
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, i+1, realizations));
			writerMemory.printf("\n");
			writerTime.printf("\n");
		}
		writerMemory.close();
		writerTime.close();
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}
}
