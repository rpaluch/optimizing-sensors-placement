package tests;

import java.util.ArrayList;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.Node;
import graphs.NodesSelectors;

public class TestLocalHubs {

	public static void main(String[] args) {
	
		int realizations = 10;
		int[] size = {10,20,30,50,100,200,300,500,1000,2000,3000,5000,10000};
		int k = 6;
		Graph g;
		ArrayList<Node> hubs;
		double nhubs = 0;
		
		for(int i=0; i<size.length; i++) {
			nhubs = 0.0;
			for(int j=0; j<realizations; j++) {
				//g = GraphGenerator.contiguousErdosRenyi(size[i], k, "degree");
				g = GraphGenerator.barabasiAlbert(k/2, k/2, size[i]);
				hubs = NodesSelectors.findLocalHubs(g, 8);
				nhubs = nhubs + hubs.size();
			}
			nhubs = nhubs/realizations;
			System.out.printf("size=%d, nhubs=%.1f (%.3f)\n", size[i], nhubs, nhubs/size[i]);
		}

	}

}
