package tests;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.PathsTools;
import graphs.PathsRegister;
import io.ExpressWriter;

public class TestObserversPlacementComplexityFixedNumberOfObservers {

	public static void main(String[] args) {
		
		String outputdir = args[0];
		String method = args[1];
		int degree = Integer.parseInt(args[2]);
		int numberOfObservers = Integer.parseInt(args[3]);
		int realizations = Integer.parseInt(args[4]);
		
		String header = "";
		int[] size = new int[args.length-5];
		for(int i=0; i<size.length; i++) {
			size[i] = Integer.parseInt(args[i+5]);
			header = header + "N" + args[i+5] + "\t";
		}

		String path = outputdir+"/er_k"+degree+"_O"+numberOfObservers+"_"+method+"_time.txt";
		ExpressWriter writer = new ExpressWriter(path);
		writer.init();
		writer.println(header);
		
		Graph g = GraphGenerator.contiguousErdosRenyi(size[size.length-1], degree, "degree");
		long time = 0;
		long tstart = System.currentTimeMillis();
		for(int i=0; i<realizations; i++) {
			for(int s=0; s<size.length; s++) {
				System.out.printf("N = %d, status: ", size[s]);
				g = GraphGenerator.contiguousErdosRenyi(size[s], degree, "degree");
				switch(method) {
					case "Coverage": 	time = timeOfCoverage(g, numberOfObservers);
									 	break;
					case "KMedian":   	time = timeOfKMedian(g, numberOfObservers);
									 	break;
					case "HV":		 	time = timeOfHighVariance(g, numberOfObservers);
									 	break;
					case "BC":			time = timeOfBetweennessCentrality(g, numberOfObservers);	
										break;
					case "CB":			time = timeOfCollectiveBetweenness(g, numberOfObservers);	
										break;
				}
				writer.printf("%d\t", time);
				System.out.printf("finished in %d ms\n",time);
			}
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, i+1, realizations));
			writer.printf("\n");
		}
	}
	
	public static long timeOfCoverage(Graph g, int numberOfObservers) {
		long tstart = System.currentTimeMillis();
		GraphTools.greedyCoverageObsParallel(g, numberOfObservers, 1);
		return System.currentTimeMillis()-tstart;
	}
	
	public static long timeOfKMedian(Graph g, int numberOfObservers) {
		long tstart = System.currentTimeMillis();
		PathsRegister register = PathsTools.allShortestPathsInGraph(g);
		GraphTools.greedyKMedianObsParallel(g, PathsTools.allDistancesInGraph(g, register), numberOfObservers, 1);
		return System.currentTimeMillis()-tstart;
	}
	
	public static long timeOfHighVariance(Graph g, int numberOfObservers) {
		long tstart = System.currentTimeMillis();
		PathsRegister register = PathsTools.allShortestPathsInGraph(g);
		GraphTools.greedyHVObsParallel(g, register, numberOfObservers, 3, 1);
		return System.currentTimeMillis()-tstart;
	}
	
	public static long timeOfBetweennessCentrality(Graph g, int numberOfObservers) {
		long tstart = System.currentTimeMillis();
		PathsRegister register = PathsTools.allShortestPathsInGraph(g);
		GraphTools.betweennessCentrality(g, register);
		GraphTools.highestBetweennessNodes(g, numberOfObservers);
		return System.currentTimeMillis()-tstart;
	}
	
	public static long timeOfCollectiveBetweenness(Graph g, int numberOfObservers) {
		long tstart = System.currentTimeMillis();
		PathsRegister register = PathsTools.allShortestPathsInGraph(g);
		GraphTools.fastCollectiveBetweenness(g, register, numberOfObservers);
		return System.currentTimeMillis()-tstart;
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}

}
