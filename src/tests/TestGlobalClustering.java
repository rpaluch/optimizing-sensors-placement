package tests;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import io.ExpressWriter;
import jdistlib.Poisson;
import jdistlib.evd.GeneralizedPareto;

public class TestGlobalClustering {

	public static void main(String[] args) {
		
		int realizations = 603;
		Graph graph;
		int size = 1000;
		int m = 4;
		//String[] networks = {"er","rrg","ba","sfn"};
		String[] networks = {"pois"};
		
		ExpressWriter writer = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/networks_properties/pois_apl.txt");
		writer.init();
		/*
		writer.println("ER.degree\tER.apl\tER.cluster\t"
						+ "RRG.degree\tRRG.apl\tRRG.cluster\t"
						+ "BA.degree\tBA.apl\tBA.cluster\t"
						+ "SFN.degree\tSFN.apl\tSFN.cluster");
		*/
		writer.println("apl");
		long tstart = System.currentTimeMillis();
		for(int i=0; i<realizations; i++) {
			for(String network : networks) {
				switch(network) {
				case "ba":	 graph = GraphGenerator.barabasiAlbert(m+2, m, size);
						     break;
				case "er":   graph = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
							 break;
				case "rrg":  graph = GraphGenerator.regularRandomGraph(size, 2*m);
				 			 GraphTools.findComponents(graph);
				 			 GraphTools.connectAllComponents(graph);
				 			 break;
				case "sfn":  graph = GraphGenerator.scaleFreeNetwork(size, m, size-m, 3);
							 GraphTools.findComponents(graph);
							 GraphTools.connectAllComponents(graph);
							 break;
				case "pois": graph = GraphGenerator.configurationModelWithClustering(GraphGenerator.degreeSequence(new Poisson(2*m), size), 0.75);
							 GraphTools.findComponents(graph);
							 GraphTools.connectAllComponents(graph);
							 break;
				case "par":  graph = GraphGenerator.configurationModelWithClustering(GraphGenerator.degreeSequence(new GeneralizedPareto(m,m/2.0,0.5), size), 0.75);
							 GraphTools.findComponents(graph);
							 GraphTools.connectAllComponents(graph);
							 break;
				default:	graph = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
							break;			
				}
				/*
				writer.printf("%f\t%f\t%f\t", graph.averageDegree(),
						  					  GraphTools.averagePathLength(graph),
						  					  GraphTools.globalClusteringCoefficient(graph));
				*/
				writer.printf("%f", GraphTools.averagePathLength(graph));
			}
			writer.printf("\n");
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, i+1, realizations));

		}
		writer.close();
	}

	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}
}
