package tests;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.NodesSelectors;
import io.ExpressWriter;

public class TestCoverageObservers3 {

	public static void main(String[] args) {
		int[] size = {100,150,200,250,300,400,500,600,700,800,1000,1500,2000,2500,3000,4000,5000};
		int m = 4;
		int budget = 25;
		int realizations = 20;
		Graph graph;
		long time;
		
		ExpressWriter writer = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/observers_placement_complexity2/er_k8_O0.05_CoverageFast_fixedBudget.txt");
		writer.init();
		writer.println("100\t150\t200\t250\t300\t400\t500\t600\t700\t800\t1000\t1500\t2000\t2500\t3000\t4000\t5000");
		
		long tstart = System.currentTimeMillis();
		for(int r=0; r<realizations; r++) {
			for(int i=0; i<size.length; i++) {
				graph = GraphGenerator.contiguousErdosRenyi(size[i], 2*m, "degree");
				time = System.nanoTime();
				NodesSelectors.greedyCoverageObservers(graph, budget);
				writer.printf("%.3f\t", 0.000001*(System.nanoTime()-time));
			}
			writer.printf("\n");
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, r+1, realizations));
		}
		writer.close();
		
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}

}
