package tests;

import java.util.ArrayList;

import tracking.GradientPinto2;
import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Node;
import graphs.PathsTools;
import graphs.PathsRegister;
import io.ExpressWriter;
import dynamics.SusceptibleInfected;

public class TestObserversPlacement {

	public static void main(String[] args) {
		
		int numberOfGraphs = Integer.parseInt(args[0]);
		int numberOfTestsPerNode = Integer.parseInt(args[1]);
		int numberOfThreads = Integer.parseInt(args[2]);
		
		//String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/observers_placement/pinto_ba_N100_k6_beta0.5_";
		String dir = "../outputs/observers_placement3/gradient_er_N1000_k6_O0.2";
		ExpressWriter writer1 = new ExpressWriter(dir+"_BC_accuracy.txt");
		ExpressWriter writer2 = new ExpressWriter(dir+"_CBC_accuracy.txt");
		ExpressWriter writer3 = new ExpressWriter(dir+"_rand_accuracy.txt");
		//ExpressWriter writer4 = new ExpressWriter(dir+"var_mean_accuracy.txt");
		
		String header = "beta0.3\tbeta0.4\tbeta0.5\tbeta0.6\tbeta0.7\tbeta0.8";
		
		long tstart;
		
		Graph g;
		int size = 1000;
		int m = 3;
		int realizations = numberOfGraphs*numberOfTestsPerNode*size;
		
		PathsRegister register;
		SusceptibleInfected si;
		double[] rate = {0.3,0.4,0.5,0.6,0.7,0.8};
		double densityOfObservers = 0.2;
		int numberOfObservers = (int)(densityOfObservers*size);
		Node source;
		ArrayList<Node> observersA;
		ArrayList<Node> observersB;
		//ArrayList<Node> observersC;

		GradientPinto2 gradient = null;
		ArrayList<Integer> supposedSourcesId;
		
		writer1.init();	
		writer2.init();
		writer3.init();
		//writer4.init();
		
		writer1.println(header);
		writer2.println(header);
		writer3.println(header);
		//writer4.println(header);
		
		tstart = System.currentTimeMillis();
		int counter = 0;
		for(int n=0; n<numberOfGraphs; n++) {
			
			// Generate graph
			//g = GraphGenerator.barabasiAlbert(m+2, m, size);
			g = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
			//source = g.getRandomNode();
			
			// Find all shortest paths
			register = PathsTools.allShortestPathsInGraph(g);
						
			// Compute the betweenness centrality
			GraphTools.betweennessCentrality(g, register);
			
			observersA = GraphTools.highestBetweennessNodes(g, numberOfObservers);
			observersB = GraphTools.fastCollectiveBetweenness(g, register, numberOfObservers);
			//observersC = GraphTools.greedyHVObsParallel(g, register, numberOfObservers, 4, numberOfThreads);
			
			for(int s=0; s<size; s++) {
				source = g.getNodesList().get(s);
				
				for(int t=0; t<numberOfTestsPerNode; t++) {
					
					for(int i=0; i<rate.length; i++) {
								
						// Run propagation with high betweenness observers
						si = new SusceptibleInfected(g);
						si.setObservers(observersA);
						si.setOneSource(source);
						si.setInfectionRate(rate[i]);
						si.runUntilAllObserversInfected();
						
						// GMLA with random observers
						gradient = new GradientPinto2(g);
						gradient.setObservers(si.getObservers());
						gradient.setNumberOfNearestObservers(15);
						gradient.setDelays(si.getInfectionTimeAsDouble());
						gradient.setDelayMean(1/rate[i]);
						gradient.setDelayVariance((1-rate[i])/(rate[i]*rate[i]));
						gradient.setMaxThreads(numberOfThreads);
						gradient.estimateScoresForNodes();
						supposedSourcesId = gradient.findNodesWithHighestScores();
						if(supposedSourcesId.contains(source.getId())) writer1.printf("%.3f\t", 1.0/supposedSourcesId.size());
						else writer1.print("0.000\t");
						
						// Run propagation with minimum average paths overlap observers
						si = new SusceptibleInfected(g);
						si.setObservers(observersB);
						si.setOneSource(source);
						si.setInfectionRate(rate[i]);
						si.runUntilAllObserversInfected();
						
						// GMLAwith high betweenness observers
						gradient = new GradientPinto2(g);
						gradient.setObservers(si.getObservers());
						gradient.setNumberOfNearestObservers(15);
						gradient.setDelays(si.getInfectionTimeAsDouble());
						gradient.setDelayMean(1/rate[i]);
						gradient.setDelayVariance((1-rate[i])/(rate[i]*rate[i]));
						gradient.setMaxThreads(numberOfThreads);
						gradient.estimateScoresForNodes();
						supposedSourcesId = gradient.findNodesWithHighestScores();
						if(supposedSourcesId.contains(source.getId())) writer2.printf("%.3f\t", 1.0/supposedSourcesId.size());
						else writer2.print("0.000\t");
						
						// Run propagation with random observers
						
						si = new SusceptibleInfected(g);
						si.addRandomObservers(numberOfObservers);
						si.setOneSource(source);
						si.setInfectionRate(rate[i]);
						si.runUntilAllObserversInfected();
						
						// GMLA with minimum average paths overlap observers
						gradient = new GradientPinto2(g);
						gradient.setObservers(si.getObservers());
						gradient.setNumberOfNearestObservers(15);
						gradient.setDelays(si.getInfectionTimeAsDouble());
						gradient.setDelayMean(1/rate[i]);
						gradient.setDelayVariance((1-rate[i])/(rate[i]*rate[i]));
						gradient.setMaxThreads(numberOfThreads);
						gradient.estimateScoresForNodes();
						supposedSourcesId = gradient.findNodesWithHighestScores();
						if(supposedSourcesId.contains(source.getId())) writer3.printf("%.3f\t", 1.0/supposedSourcesId.size());
						else writer3.print("0.000\t");
						/*
						// Run propagation with high variance*mean observer
						observers = g.copyOfNodesList();
						Collections.sort(observers, GraphTools.nodeValueDescending);
						si = new SusceptibleInfected(g);
						si.setObservers(new ArrayList<Node>(observers.subList(0, numberOfObservers)));
						si.setOneSource(source);
						si.setInfectionRate(rate);
						si.runUntilAllObserversInfected();
						
						// PTVA-LI with high variance*mean observer
						pinto = new ParallelPinto2(g);
						pinto.setObservers(si.getObservers());
						pinto.setDelays(si.getInfectionTimeAsDouble());
						pinto.setSupposedSources(g.getNodesList());	
						pinto.setDelayMean(1/rate);
						pinto.setDelayVariance((1-rate)/(rate*rate));
						pinto.setNumberOfThreads(numberOfThreads);
						pinto.estimateScoresForNodes();
						supposedSourcesId = pinto.findNodesWithHighestScores();
						if(supposedSourcesId.contains(source.getId())) writer4.printf("%.3f\t", 1.0/supposedSourcesId.size());
						else writer4.print("0.000\t");
						*/
					}
					counter = counter + 1;
					System.out.println(estimateTime(System.currentTimeMillis()-tstart, counter, realizations));
					writer1.printf("\n");
					writer2.printf("\n");
					writer3.printf("\n");
					//writer4.printf("\n");
				}
			}
				
		}
		writer1.close();
		writer2.close();
		writer3.close();
		//writer4.close();	
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}
}
