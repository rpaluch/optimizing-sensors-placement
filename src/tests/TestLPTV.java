package tests;

import java.util.ArrayList;
import java.util.LinkedList;

import dynamics.GenericSpreading;
import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.PathsTools;
import io.ExpressWriter;
import io.GraphWriter;
import jdistlib.Normal;
import tracking.SingleSourceDetector;

public class TestLPTV {

	public static void main(String[] args) {
		
		int size = 100;
		int m = 3;
		int numberOfObservers = 10;
		double mu = 4;
		double sd = 1;
		
		Graph graph = GraphGenerator.barabasiAlbert(m+2, m, size);
		GenericSpreading gs = new GenericSpreading(graph, new Normal(mu,sd));
		gs.addRandomObservers(numberOfObservers);
		gs.run();
		
		SingleSourceDetector detector = new SingleSourceDetector(graph);
		detector.setObserversAndDelays(gs.getObserversAndDelays(numberOfObservers));
		detector.setDelayMean(mu);
		detector.setDelayVariance(sd*sd);
		detector.setSupposedSources(gs.getSources());
		detector.computeScores("LPTV");
		
		//Print network to file
		GraphWriter gwriter = new GraphWriter(graph);
		gwriter.exportEdgeListWithWeights("/home/rpaluch/Workspace/ReverseEngineering/outputs/testLPTV/ba_N100_k6_mu4_sd1.txt");

		//Print shortest weighted paths from source to observers
		int sourceID = gs.getSources().get(0).getId();
		ArrayList<Integer> observersID = GraphTools.nodesToIDs(gs.getObservers());
		ArrayList<LinkedList<Integer>> paths = PathsTools.shortestWeightedPaths(graph, sourceID, observersID);
		ExpressWriter writer = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/testLPTV/diffusion_paths.txt");
		writer.init();
		for(LinkedList<Integer> path : paths) {
			for(Integer id : path) writer.printf("%d ",id);
			writer.printf("\n");
		}
		writer.close();

	}

}
