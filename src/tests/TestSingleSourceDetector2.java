package tests;

import java.util.ArrayList;

import dynamics.SusceptibleInfected;
import graphs.Graph;
import graphs.GraphGenerator;
import io.MultiExpressWriter;
import tracking.SingleSourceDetector;

public class TestSingleSourceDetector2 {

	public static void main(String[] args) {
		
		String graphType = args[0];
		double rate = Double.parseDouble(args[1]);
		int realizations = Integer.parseInt(args[2]);
		int numberOfThreads = Integer.parseInt(args[3]);
		int size = 1000;
		int degree = 8;
		int m = degree/2;
		double densityOfObservers = 0.1;
		int[] numberOfNearestObservers = {5,10,15,20,25,30,35,40,45,50,60,70,80,90,100};
		Graph graph;		
		SusceptibleInfected si;
		int sourceID;
		String[] methods = {"LPTV","EPP","EPL"};
		SingleSourceDetector detector;
		ArrayList<Integer> supposedSourcesId;
		
		String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/springer/"+graphType+"_N"+size+"_k"+2*m+"_rate"+rate+"_O"+densityOfObservers;
		String[] files = {dir+"_LPTV_accuracy2.txt",
						  dir+"_LPTV_rank2.txt",
						  dir+"_EPP_accuracy2.txt",
						  dir+"_EPP_rank2.txt",
						  dir+"_EPL_accuracy2.txt",
						  dir+"_EPL_rank2.txt",
						  dir+"_LPTV_gradient_accuracy2.txt",
						  dir+"_LPTV_gradient_rank2.txt",
						  dir+"_EPP_gradient_accuracy2.txt",
						  dir+"_EPP_gradient_rank2.txt",
						  dir+"_EPL_gradient_accuracy2.txt",
						  dir+"_EPL_gradient_rank2.txt"};
		String[] names = {"accuracyLPTV","rankLPTV","accuracyEPP","rankEPP","accuracyEPL","rankEPL",
						  "accuracy_gradientLPTV","rank_gradientLPTV","accuracy_gradientEPP","rank_gradientEPP","accuracy_gradientEPL","rank_gradientEPL"};
		MultiExpressWriter writer = new MultiExpressWriter(files, names);
		writer.initAll();
		writer.printlnHeaderAll(numberOfNearestObservers, "no", "\t");
		long tstart = System.currentTimeMillis();
		for(int i=0; i<realizations; i++) {
			
			// Generate graph
			switch(graphType) {
				case "ba":	graph = GraphGenerator.barabasiAlbert(m+2, m, size);
						    break;
				case "er":  graph = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
							break;
				case "rrg": graph = GraphGenerator.regularRandomGraph(size, 2*m);
							while(!graph.isContiguous()) graph = GraphGenerator.regularRandomGraph(size, 2*m);
							break;
				case "sfn": graph = GraphGenerator.scaleFreeNetwork(size, m, size-m, 3);
							while(!graph.isContiguous()) graph = GraphGenerator.scaleFreeNetwork(size, m, size-m, 3);
							break;
				default:	graph = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
							break;
							
			}
			
			for(int n=0; n<numberOfNearestObservers.length; n++) {
				
				si = new SusceptibleInfected(graph);
				si.setObservers(graph.getRandomNodes((int)(densityOfObservers*size)));
				si.setInfectionRate(rate);
				si.runUntilAllObserversInfected();
				sourceID = si.getSources().get(0).getId();
				
				for(String method: methods) {
					detector = new SingleSourceDetector(graph);
					detector.setObservers(si.getObservers());
					detector.setDelays(si.getInfectionTimeAsDouble());
					detector.setDelayMean(1/rate);
					detector.setDelayVariance((1-rate)/(rate*rate));
					detector.setMaxThreads(numberOfThreads);
					detector.setNumberOfNearestObservers(numberOfNearestObservers[n]);
					detector.setSupposedSources(graph.getNodesList());
					detector.computeScores(method);
					supposedSourcesId = detector.findNodesWithHighestScores();
					if(supposedSourcesId.contains(sourceID)) writer.printf("accuracy"+method,"%.3f\t",1.0/supposedSourcesId.size());
					else writer.printf("accuracy"+method,"0.000\t");
					writer.printf("rank"+method,"%d\t",detector.findRankOfNode(sourceID));
				}
				
				for(String method: methods) {
					detector = new SingleSourceDetector(graph);
					detector.setObservers(si.getObservers());
					detector.setDelays(si.getInfectionTimeAsDouble());
					detector.setDelayMean(1/rate);
					detector.setDelayVariance((1-rate)/(rate*rate));
					detector.setMaxThreads(numberOfThreads);
					detector.setNumberOfGradientWalks(1);
					detector.setNumberOfNearestObservers(numberOfNearestObservers[n]);
					detector.computeScores(method);
					supposedSourcesId = detector.findNodesWithHighestScores();
					if(supposedSourcesId.contains(sourceID)) writer.printf("accuracy_gradient"+method,"%.3f\t",1.0/supposedSourcesId.size());
					else writer.printf("accuracy_gradient"+method,"0.000\t");
					writer.printf("rank_gradient"+method,"%d\t",detector.findRankOfNode(sourceID));
				}
			}
			writer.newLineAll();
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, i+1, realizations));
		}
		writer.closeAll();
	}

	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}
}
