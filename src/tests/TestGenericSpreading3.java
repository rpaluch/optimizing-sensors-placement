package tests;

import java.util.ArrayList;
import java.util.LinkedList;

import dynamics.GenericSpreading;
import dynamics.SusceptibleInfected;
import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Link;
import graphs.Node;
import graphs.PathsTools;
import io.ExpressWriter;
import io.GraphWriter;
import jdistlib.Geometric;
import jdistlib.Normal;

public class TestGenericSpreading3 {

	public static void main(String[] args) {
		
		int realizations = 1000;
		int size = 1000;
		int m = 1;
		double mu = 2.0;
		double ratio = 1.41;
		double mean;
		
		Graph graph = GraphGenerator.barabasiAlbert(m+2, m, size);
		//Graph graph = GraphGenerator.randomTree(size);
		graph.printStats();
		GenericSpreading gs;
		ArrayList<Double> times;
		Node source = graph.getRandomNode();
		ArrayList<Node> observers = graph.getRandomNodes(20);
		ArrayList<Double> lengths = PathsTools.shortestWeightedPathsLengths(graph, source.getId(), GraphTools.nodesToIDs(observers));
		
		ExpressWriter writer = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/testGenericSpreading/ba_N1000_k2_mu2.0_ratio1.41.txt");
		writer.init();
		
		for(int i=0; i<realizations; i++) {
			gs = new GenericSpreading(graph, new Normal(mu,mu/ratio));
			//gs = new GenericSpreading(graph, new Geometric(1.0/mu));
			gs.setOneSource(source);
			gs.setObservers(observers);
			gs.run();
			times = gs.getInfectionTimes();
			mean = gs.computeMeanDelay();
			for(int j=0; j<times.size(); j++) writer.printf("%f\t", mean*lengths.get(j)-times.get(j));
			writer.printf("\n");
		}
		writer.close();
		
	}

}
