package tests;

import io.ExpressWriter;
import io.MultiExpressWriter;
import jdistlib.Poisson;
import jdistlib.evd.GeneralizedPareto;
import tracking.SingleSourceDetector;

import java.util.ArrayList;

import dynamics.SusceptibleInfected;
import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Node;
import graphs.NodesSelectors;

public class TestObserversPlacementSyntheticNetwork2 {

	public static void main(String[] args) {
		
		// Read the main parameters
		String graphType = args[0];
		int size = Integer.parseInt(args[1]);
		int m = Integer.parseInt(args[2]);
		double densityOfObservers = Double.parseDouble(args[3]);
		int numberOfGraphs = Integer.parseInt(args[4]);
		int numberOfTestsPerGraph = Integer.parseInt(args[5]);
		int realizations = numberOfGraphs*numberOfTestsPerGraph;
		int numberOfThreads = Integer.parseInt(args[6]);
		String part = args[7];
				
		// Initialize variables
		long time = 0;
		int numberOfObservers = (int)(densityOfObservers*size);
		int numberOfNearestObservers = 20;
		double rate = 0;
		double[] sigma = {0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95};
		
		// Spreading and detection
		Graph graph;
		SusceptibleInfected si;
		ArrayList<Node> sources;
		SingleSourceDetector detector;
		ArrayList<Integer> supposedSourcesId;
		
		// Initialize observers sets
		String[] sets = {"GLSP1","GLSP2","GLSP3"};
		ArrayList<ArrayList<Node>> observers;
				
		// Prepare output files
		String[] files = new String[12];
		String[] names = new String[12];
		String dir1 = "../outputs/observers_placement_ptva_"+graphType+"/ptva_"+graphType+"_N"+size+"_k"+2*m+"_O"+densityOfObservers+"_part"+part;
		String dir2 = "../outputs/observers_placement_gmla_"+graphType+"/gmla_"+graphType+"_N"+size+"_k"+2*m+"_O"+densityOfObservers+"_part"+part;
		int counter = 0;
		for(int op=0; op<sets.length; op++) {
			files[counter] = dir1+"_"+sets[op]+"_accuracy.txt";
			names[counter] = "precision_ptva_"+sets[op];
			counter++;
			files[counter] = dir1+"_"+sets[op]+"_rank.txt";
			names[counter] = "rank_ptva_"+sets[op];
			counter++;
		}
		for(int op=0; op<sets.length; op++) {
			files[counter] = dir2+"_"+sets[op]+"_accuracy.txt";
			names[counter] = "precision_gmla_"+sets[op];
			counter++;
			files[counter] = dir2+"_"+sets[op]+"_rank.txt";
			names[counter] = "rank_gmla_"+sets[op];
			counter++;
		}			
		MultiExpressWriter writer = new MultiExpressWriter(files, names);
		writer.initAll();
		writer.printlnHeaderAll(sigma, "sigma", "\t");
		ExpressWriter graphInfo = new ExpressWriter(dir1+"_graphInfo.txt");
		graphInfo.init();
		graphInfo.println("av.degree\tclustering");
		
		// Main loop
		counter = 0;
		long tstart = System.currentTimeMillis();
		for(int g=0; g<numberOfGraphs; g++) {
			// Generate graph
			switch(graphType) {
				case "ba":	 graph = GraphGenerator.barabasiAlbert(m+2, m, size);
						     break;
				case "er":   graph = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
							 break;
				case "rrg":  graph = GraphGenerator.regularRandomGraph(size, 2*m);
							 GraphTools.findComponents(graph);
							 GraphTools.connectAllComponents(graph);
							 break;
				case "sfn":  graph = GraphGenerator.scaleFreeNetwork(size, m, size-m, 3);
							 GraphTools.findComponents(graph);
							 GraphTools.connectAllComponents(graph);
							 break;
				case "pois": graph = GraphGenerator.configurationModelWithClustering(GraphGenerator.degreeSequence(new Poisson(2*m), size), 0.75);
							 GraphTools.findComponents(graph);
							 GraphTools.connectAllComponents(graph);
							 break;
				case "par":  graph = GraphGenerator.configurationModelWithClustering(GraphGenerator.degreeSequence(new GeneralizedPareto(m,m/2.0,0.5), size), 0.75);
							 GraphTools.findComponents(graph);
							 GraphTools.connectAllComponents(graph);
							 break;
				default:	 graph = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
							 break;				
			}
			// Write info about graph
			graphInfo.printf("%f\t%f\n", graph.averageDegree(), GraphTools.globalClusteringCoefficient(graph));
			// Choose randomly source
			sources = graph.getRandomNodes(numberOfTestsPerGraph);
			// Prepare observers sets
			observers = new ArrayList<ArrayList<Node>>();		
			// (0) Least Shortest Paths with minLength=1
			time = System.currentTimeMillis();
			observers.add(NodesSelectors.greedyLSPParallel(graph, numberOfObservers, 1, numberOfThreads));
			System.out.printf("GLSP1 observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
			// (1) KMedian observers
			time = System.currentTimeMillis();
			observers.add(NodesSelectors.greedyLSPParallel(graph, numberOfObservers, 2, numberOfThreads));
			System.out.printf("GLSP2 observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
			// (2) HVObs observers
			time = System.currentTimeMillis();
			observers.add(NodesSelectors.greedyLSPParallel(graph, numberOfObservers, 3, numberOfThreads));
			System.out.printf("GLSP3 observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
			
			for(Node source : sources) {
				
				for(int r=0; r<sigma.length; r++) {
					
					rate = 1-sigma[r]*sigma[r];
					
					si = new SusceptibleInfected(graph);
					si.setObservers(graph.getNodesList());
					si.setOneSource(source);
					si.setInfectionRate(rate);
					si.runUntilAllObserversInfected();
					
					for(int s=0; s<sets.length; s++) {
						//PTVA-LI
						detector = new SingleSourceDetector(graph);
						detector.setObservers(observers.get(s));
						detector.setDelays(si.getInfectionTimesForGivenObservers(observers.get(s)));
						detector.setDelayMean(1/rate);
						detector.setDelayVariance((1-rate)/(rate*rate));
						detector.setMaxThreads(numberOfThreads);
						detector.setSupposedSources(graph.getNodesList());
						detector.computeScores("LPTV");
						supposedSourcesId = detector.findNodesWithHighestScores();
						if(supposedSourcesId.contains(source.getId())) 
							writer.printf("precision_ptva_"+sets[s],"%.3f\t",1.0/supposedSourcesId.size());
						else writer.printf("precision_ptva_"+sets[s],"0.000\t");
						writer.printf("rank_ptva_"+sets[s],"%d\t",detector.findRankOfNode(source.getId()));
	
						//GMLA
						detector = new SingleSourceDetector(graph);
						detector.setObservers(observers.get(s));
						detector.setDelays(si.getInfectionTimesForGivenObservers(observers.get(s)));
						detector.setDelayMean(1/rate);
						detector.setDelayVariance((1-rate)/(rate*rate));
						detector.setMaxThreads(numberOfThreads);
						detector.setNumberOfGradientWalks(1);
						detector.setNumberOfNearestObservers(numberOfNearestObservers);
						detector.computeScores("LPTV");
						supposedSourcesId = detector.findNodesWithHighestScores();
						if(supposedSourcesId.contains(source.getId())) 
							writer.printf("precision_gmla_"+sets[s],"%.3f\t",1.0/supposedSourcesId.size());
						else writer.printf("precision_gmla_"+sets[s],"0.000\t");
						writer.printf("rank_gmla_"+sets[s],"%d\t",detector.findRankOfNode(source.getId()));
					}
				}
				writer.newLineAll();
				counter = counter + 1;
				System.out.println(estimateTime(System.currentTimeMillis()-tstart, counter, realizations));
			}
		}
		writer.closeAll();
		graphInfo.close();
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}

}
