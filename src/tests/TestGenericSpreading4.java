package tests;

import java.util.ArrayList;

import dynamics.GenericSpreading;
import graphs.Graph;
import io.ExpressWriter;
import io.GraphReader;
import jdistlib.Geometric;
import tracking.SingleSourceDetector;

public class TestGenericSpreading4 {

	public static void main(String[] args) {
		
		int realizations = 1000;
		double rate = 15.0/16.0;
		
		GraphReader reader = new GraphReader("/home/rpaluch/Workspace/ReverseEngineering/networks/ba1000-4.edge");
		Graph graph = reader.readGraphFromEdgeList();
		graph.printStats();
		GenericSpreading gs;
		int sourceID;
		ArrayList<Integer> supposedSourcesId;
		int numberOfObservers = 100;
		SingleSourceDetector detector;
		
		
		ExpressWriter writer = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/testGenericSpreading/ba_N1000_k8_rate0.9375_precision3.txt");
		writer.init();
		long tstart = System.currentTimeMillis();
		for(int i=0; i<realizations; i++) {
			gs = new GenericSpreading(graph, new Geometric(rate));
			//gs = new GenericSpreading(graph, new Geometric(1.0/mu));
			gs.addRandomObservers(numberOfObservers);
			gs.run();
			sourceID = gs.getSources().get(0).getId();
			
			detector = new SingleSourceDetector(graph);
			detector.setObservers(gs.getObservers());
			detector.setDelays(gs.getInfectionTimes());
			detector.setDelayMean(1/rate);
			detector.setDelayVariance((1-rate)/(rate*rate));
			detector.setMaxThreads(4);
			detector.setSupposedSources(graph.getNodesList());
			detector.computeScores("LPTV");
			supposedSourcesId = detector.findNodesWithHighestScores();
			if(supposedSourcesId.contains(sourceID)) writer.printf("%.3f\n",1.0/supposedSourcesId.size());
			else writer.printf("0.000\n");	
			System.out.println(estimateTime(System.currentTimeMillis()-tstart, i+1, realizations));
		}
		writer.close();
		
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}

}
