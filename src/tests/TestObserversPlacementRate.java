package tests;

import java.util.ArrayList;

import tracking.ParallelPinto2;
import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Node;
import graphs.PathsTools;
import graphs.PathsRegister;
import io.ExpressWriter;
import dynamics.SusceptibleInfected;

public class TestObserversPlacementRate {

	public static void main(String[] args) {
		
		String graphType = args[0];
		double densityOfObservers = Double.parseDouble(args[1]);
		int numberOfGraphs = Integer.parseInt(args[2]);
		int numberOfTestsPerGraph = Integer.parseInt(args[3]);
		int numberOfThreads = Integer.parseInt(args[4]);
		int part = Integer.parseInt(args[5]);
		
		//String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/observers_placement/pinto_ba_N100_k6_beta0.5_";
		String dir = "../outputs/observers_placement_ptva_"+graphType+"/ptva_"+graphType+"_N1000_k8_O"+densityOfObservers+"_part"+part;
		ExpressWriter accuracy_random = new ExpressWriter(dir+"_Random_accuracy.txt");
		ExpressWriter rank_random = new ExpressWriter(dir+"_Random_rank.txt");
		ExpressWriter distance_random = new ExpressWriter(dir+"_Random_distance.txt");
		ExpressWriter accuracy_coverage = new ExpressWriter(dir+"_Coverage_accuracy.txt");
		ExpressWriter rank_coverage = new ExpressWriter(dir+"_Coverage_rank.txt");
		ExpressWriter distance_coverage = new ExpressWriter(dir+"_Coverage_distance.txt");
		ExpressWriter accuracy_kmedian = new ExpressWriter(dir+"_KMedian_accuracy.txt");
		ExpressWriter rank_kmedian = new ExpressWriter(dir+"_KMedian_rank.txt");
		ExpressWriter distance_kmedian = new ExpressWriter(dir+"_KMedian_distance.txt");
		ExpressWriter accuracy_hv = new ExpressWriter(dir+"_HV_accuracy.txt");
		ExpressWriter rank_hv = new ExpressWriter(dir+"_HV_rank.txt");
		ExpressWriter distance_hv = new ExpressWriter(dir+"_HV_distance.txt");
		ExpressWriter accuracy_bc = new ExpressWriter(dir+"_BC_accuracy.txt");
		ExpressWriter rank_bc = new ExpressWriter(dir+"_BC_rank.txt");
		ExpressWriter distance_bc = new ExpressWriter(dir+"_BC_distance.txt");
		ExpressWriter accuracy_cb = new ExpressWriter(dir+"_CB_accuracy.txt");
		ExpressWriter rank_cb = new ExpressWriter(dir+"_CB_rank.txt");
		ExpressWriter distance_cb = new ExpressWriter(dir+"_CB_distance.txt");
		
		
		String header = "sigma0.2\tsigma0.25\tsigma0.3\tsigma0.35\tsigma0.4\tsigma0.45\tsigma0.5\tsigma0.55\tsigma0.6\tsigma0.65\tsigma0.7\tsigma0.75\tsigma0.8\tsigma0.85\tsigma0.9\tsigma0.95";
		
		long tstart;
		
		Graph g;
		int size = 1000;
		int m = 4;
		int realizations = numberOfGraphs*numberOfTestsPerGraph;
		
		PathsRegister register;
		SusceptibleInfected si;
		double[] sigma = {0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95};
		double rate = 0;
		int numberOfObservers = (int)(densityOfObservers*size);
		int maxLength = 3;
		if(densityOfObservers<0.077) {
			maxLength=5;
			if(densityOfObservers>0.074) maxLength=4;
		}
		
		ArrayList<Node> sources;
		ArrayList<Node> observersRandom;
		ArrayList<Node> observersCoverage;
		ArrayList<Node> observersKMedian;
		ArrayList<Node> observersBC;
		ArrayList<Node> observersCB;
		ArrayList<Node> observersHV;

		ParallelPinto2 ptva = null;
		ArrayList<Integer> supposedSourcesId;
		
		accuracy_random.init();	
		rank_random.init();
		distance_random.init();
		accuracy_random.println(header);
		rank_random.println(header);
		distance_random.println(header);
		
		accuracy_coverage.init();	
		rank_coverage.init();
		distance_coverage.init();
		accuracy_coverage.println(header);
		rank_coverage.println(header);
		distance_coverage.println(header);
		
		accuracy_kmedian.init();	
		rank_kmedian.init();
		distance_kmedian.init();
		accuracy_kmedian.println(header);
		rank_kmedian.println(header);
		distance_kmedian.println(header);
		
		accuracy_hv.init();	
		rank_hv.init();
		distance_hv.init();
		accuracy_hv.println(header);
		rank_hv.println(header);
		distance_hv.println(header);
		
		accuracy_bc.init();	
		rank_bc.init();
		distance_bc.init();
		accuracy_bc.println(header);
		rank_bc.println(header);
		distance_bc.println(header);
		
		accuracy_cb.init();	
		rank_cb.init();
		distance_cb.init();
		accuracy_cb.println(header);
		rank_cb.println(header);
		distance_cb.println(header);
		
		tstart = System.currentTimeMillis();
		int counter = 0;
		for(int n=0; n<numberOfGraphs; n++) {
			
			// Generate graph
			switch(graphType) {
				case "ba":	g = GraphGenerator.barabasiAlbert(m+2, m, size);
						    break;
				case "er":  g = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
							break;
				case "rrg": g = GraphGenerator.regularRandomGraph(size, 2*m);
							while(!g.isContiguous()) g = GraphGenerator.regularRandomGraph(size, 2*m);
							break;
				case "sfn": g = GraphGenerator.scaleFreeNetwork(size, m, size-m, 3);
							while(!g.isContiguous()) g = GraphGenerator.scaleFreeNetwork(size, m, size-m, 3);
							break;
				default:	g = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
							break;
							
			}
			
			sources = g.getRandomNodes(numberOfTestsPerGraph);
			
			// Find all shortest paths
			register = PathsTools.allShortestPathsInGraph(g);
			System.out.println("All shortest paths founded.");
						
			// Prepare observers
			observersRandom = g.getRandomNodes(numberOfObservers);
			observersCoverage = GraphTools.greedyCoverageObsParallel(g, numberOfObservers, numberOfThreads);
			System.out.println("observersCoverage prepared.");
			observersKMedian = GraphTools.greedyKMedianObsParallel(g, PathsTools.allDistancesInGraph(g, register), numberOfObservers, numberOfThreads);
			System.out.println("observersKMedian prepared.");
			observersCB = GraphTools.fastCollectiveBetweenness(g, register, numberOfObservers);
			System.out.println("observersCB prepared.");
			observersHV = GraphTools.greedyHVObsParallel(g, register, numberOfObservers, maxLength, numberOfThreads);
			System.out.println("observersHV prepared.");
			
			// Compute the betweenness centrality
			GraphTools.betweennessCentrality(g, register);
			observersBC = GraphTools.highestBetweennessNodes(g, numberOfObservers);
			System.out.println("observersBC prepared.");
			
			//System.out.printf("Finding optimal observers took %.0f s.\n", 0.001*(System.currentTimeMillis()-tstart));
			
			for(Node source : sources) {
				
						for(int i=0; i<sigma.length; i++) {
							
							rate = 1-sigma[i]*sigma[i];
									
							// Run propagation with all nodes as observers
							si = new SusceptibleInfected(g);
							si.setObservers(g.getNodesList());
							si.setOneSource(source);
							si.setInfectionRate(rate);
							si.runUntilAllObserversInfected();
							
							// GPTVA with random observers
							ptva = new ParallelPinto2(g);
							ptva.setSupposedSources(g.getNodesList());
							ptva.setObservers(observersRandom);
							ptva.setDelays(si.getInfectionTimesForGivenObservers(observersRandom));
							ptva.setDelayMean(1/rate);
							ptva.setDelayVariance((1-rate)/(rate*rate));
							ptva.setNumberOfThreads(numberOfThreads);
							ptva.estimateScoresForNodes();
							supposedSourcesId = ptva.findNodesWithHighestScores();
							if(supposedSourcesId.contains(source.getId())) accuracy_random.printf("%.3f\t", 1.0/supposedSourcesId.size());
							else accuracy_random.print("0.000\t");
							rank_random.printf("%d\t", ptva.findRankOfNode(source.getId()));
							distance_random.printf("%.3f\t", register.getAveragePathLength(source.getId(), supposedSourcesId));
							
							// GPTVA with high coverage observers
							ptva = new ParallelPinto2(g);
							ptva.setSupposedSources(g.getNodesList());
							ptva.setObservers(observersCoverage);
							ptva.setDelays(si.getInfectionTimesForGivenObservers(observersCoverage));
							ptva.setDelayMean(1/rate);
							ptva.setDelayVariance((1-rate)/(rate*rate));
							ptva.setNumberOfThreads(numberOfThreads);
							ptva.estimateScoresForNodes();
							supposedSourcesId = ptva.findNodesWithHighestScores();
							if(supposedSourcesId.contains(source.getId())) accuracy_coverage.printf("%.3f\t", 1.0/supposedSourcesId.size());
							else accuracy_coverage.print("0.000\t");
							rank_coverage.printf("%d\t", ptva.findRankOfNode(source.getId()));
							distance_coverage.printf("%.3f\t", register.getAveragePathLength(source.getId(), supposedSourcesId));
							
							// GPTVA with high KMedian observers
							ptva = new ParallelPinto2(g);
							ptva.setSupposedSources(g.getNodesList());
							ptva.setObservers(observersKMedian);
							ptva.setDelays(si.getInfectionTimesForGivenObservers(observersKMedian));
							ptva.setDelayMean(1/rate);
							ptva.setDelayVariance((1-rate)/(rate*rate));
							ptva.setNumberOfThreads(numberOfThreads);
							ptva.estimateScoresForNodes();
							supposedSourcesId = ptva.findNodesWithHighestScores();
							if(supposedSourcesId.contains(source.getId())) accuracy_kmedian.printf("%.3f\t", 1.0/supposedSourcesId.size());
							else accuracy_kmedian.print("0.000\t");
							rank_kmedian.printf("%d\t", ptva.findRankOfNode(source.getId()));
							distance_kmedian.printf("%.3f\t", register.getAveragePathLength(source.getId(), supposedSourcesId));
							
							// GPTVA with high HV observers
							ptva = new ParallelPinto2(g);
							ptva.setSupposedSources(g.getNodesList());
							ptva.setObservers(observersHV);
							ptva.setDelays(si.getInfectionTimesForGivenObservers(observersHV));
							ptva.setDelayMean(1/rate);
							ptva.setDelayVariance((1-rate)/(rate*rate));
							ptva.setNumberOfThreads(numberOfThreads);
							ptva.estimateScoresForNodes();
							supposedSourcesId = ptva.findNodesWithHighestScores();
							if(supposedSourcesId.contains(source.getId())) accuracy_hv.printf("%.3f\t", 1.0/supposedSourcesId.size());
							else accuracy_hv.print("0.000\t");
							rank_hv.printf("%d\t", ptva.findRankOfNode(source.getId()));
							distance_hv.printf("%.3f\t", register.getAveragePathLength(source.getId(), supposedSourcesId));
							
							// GPTVA with high BC observers
							ptva = new ParallelPinto2(g);
							ptva.setSupposedSources(g.getNodesList());
							ptva.setObservers(observersBC);
							ptva.setDelays(si.getInfectionTimesForGivenObservers(observersBC));
							ptva.setDelayMean(1/rate);
							ptva.setDelayVariance((1-rate)/(rate*rate));
							ptva.setNumberOfThreads(numberOfThreads);
							ptva.estimateScoresForNodes();
							supposedSourcesId = ptva.findNodesWithHighestScores();
							if(supposedSourcesId.contains(source.getId())) accuracy_bc.printf("%.3f\t", 1.0/supposedSourcesId.size());
							else accuracy_bc.print("0.000\t");
							rank_bc.printf("%d\t", ptva.findRankOfNode(source.getId()));
							distance_bc.printf("%.3f\t", register.getAveragePathLength(source.getId(), supposedSourcesId));
							
							// GPTVA with high BC observers
							ptva = new ParallelPinto2(g);
							ptva.setSupposedSources(g.getNodesList());
							ptva.setObservers(observersCB);
							ptva.setDelays(si.getInfectionTimesForGivenObservers(observersCB));
							ptva.setDelayMean(1/rate);
							ptva.setDelayVariance((1-rate)/(rate*rate));
							ptva.setNumberOfThreads(numberOfThreads);
							ptva.estimateScoresForNodes();
							supposedSourcesId = ptva.findNodesWithHighestScores();
							if(supposedSourcesId.contains(source.getId())) accuracy_cb.printf("%.3f\t", 1.0/supposedSourcesId.size());
							else accuracy_cb.print("0.000\t");
							rank_cb.printf("%d\t", ptva.findRankOfNode(source.getId()));
							distance_cb.printf("%.3f\t", register.getAveragePathLength(source.getId(), supposedSourcesId));
						}
						counter = counter + 1;
						System.out.println(estimateTime(System.currentTimeMillis()-tstart, counter, realizations));
						accuracy_random.printf("\n");
						rank_random.printf("\n");
						distance_random.printf("\n");
						accuracy_coverage.printf("\n");
						rank_coverage.printf("\n");
						distance_coverage.printf("\n");
						accuracy_kmedian.printf("\n");
						rank_kmedian.printf("\n");
						distance_kmedian.printf("\n");
						accuracy_hv.printf("\n");
						rank_hv.printf("\n");
						distance_hv.printf("\n");
						accuracy_bc.printf("\n");
						rank_bc.printf("\n");
						distance_bc.printf("\n");
						accuracy_cb.printf("\n");
						rank_cb.printf("\n");
						distance_cb.printf("\n");
				}
				
		}
		accuracy_random.close();
		rank_random.close();
		distance_random.close();
		accuracy_coverage.close();
		rank_coverage.close();
		distance_coverage.close();
		accuracy_kmedian.close();
		rank_kmedian.close();
		distance_kmedian.close();
		accuracy_hv.close();
		rank_hv.close();
		distance_hv.close();
		accuracy_bc.close();
		rank_bc.close();
		distance_bc.close();
		accuracy_cb.close();
		rank_cb.close();
		distance_cb.close();
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}
}
