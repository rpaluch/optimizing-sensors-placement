package tests;

import io.GraphReader;
import io.MultiExpressWriter;
import tracking.SingleSourceDetector;

import java.util.ArrayList;

import dynamics.SusceptibleInfected;
import graphs.Graph;
import graphs.GraphTools;
import graphs.Node;
import graphs.NodesSelectors;
import graphs.PathsTools;

public class TestUCIrving2 {

	public static void main(String[] args) {
		
		// Read the main parameters
		double densityOfObservers = Double.parseDouble(args[0]);
		int numberOfTestsPerNode = Integer.parseInt(args[1]);
		int numberOfThreads = Integer.parseInt(args[2]);
		String part = args[3];
		
		// Read the network from the file
		GraphReader reader = new GraphReader("../data/UC_Irving_messages/UCIrving.net");
		Graph ucirving = reader.readGraphFromPajek();
		ucirving.printStats();
		System.out.println("Graph ready.");
		
		// Initialize variables
		long time = 0;
		int numberOfObservers = (int)(densityOfObservers*ucirving.getNumberOfNodes());
		int numberOfNearestObservers = 20;
		int maxLengthCB = 3;
		int maxLengthHV = 3;
		if(densityOfObservers <= 0.1) {
			maxLengthHV = 4;
			if(densityOfObservers <= 0.05) maxLengthHV = 5;
		}
		double rate = 0;
		double[] sigma = {0.2,0.25,0.3,0.35,0.4};
		String header = "sigma0.2\tsigma0.25\tsigma0.3\tsigma0.35\tsigma0.4";
		
		// Spreading and detection
		SusceptibleInfected si;
		SingleSourceDetector detector;
		ArrayList<Integer> supposedSourcesId;
		
		// Prepare observers sets
		String[] sets = {"Random","Coverage","KMedian","HVObs","CB3","BC"};
		ArrayList<ArrayList<Node>> observers = new ArrayList<ArrayList<Node>>();		
		// (0) Random observers
		observers.add(ucirving.getRandomNodes(numberOfObservers));
		// (1) Coverage observers
		time = System.currentTimeMillis();
		observers.add(NodesSelectors.greedyCoverageObsParallel(ucirving, numberOfObservers, numberOfThreads));
		System.out.printf("Coverage observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
		// (2) KMedian observers
		time = System.currentTimeMillis();
		observers.add(NodesSelectors.greedyKMedianObsParallel(ucirving, PathsTools.allDistancesInGraph(ucirving), numberOfObservers, numberOfThreads));
		System.out.printf("KMedian observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
		// (3) HVObs observers
		time = System.currentTimeMillis();
		observers.add(NodesSelectors.greedyHVObsParallel(ucirving, numberOfObservers, maxLengthHV, numberOfThreads));
		System.out.printf("HVObs observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
		// (4) CB3 observers
		time = System.currentTimeMillis();
		observers.add(NodesSelectors.fastCollectiveBetweenness(ucirving, numberOfObservers, maxLengthCB));
		System.out.printf("CB3 observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
		// (5) BC observers
		time = System.currentTimeMillis();
		GraphTools.computeBetweennessCentrality(ucirving);
		observers.add(GraphTools.highestBetweennessNodes(ucirving, numberOfObservers));
		System.out.printf("BC observers ready in %.2f sec.\n",0.001*(System.currentTimeMillis()-time));
		
		// Prepare output files
		String[] files = new String[36];
		String[] names = new String[36];
		String dir1 = "../outputs/ucirving_tests/ptva_ucirving_O"+densityOfObservers+"_part"+part;
		String dir2 = "../outputs/ucirving_tests/gmla_ucirving_O"+densityOfObservers+"_part"+part;
		int counter = 0;
		for(int op=0; op<sets.length; op++) {
			files[counter] = dir1+"_"+sets[op]+"_accuracy.txt";
			names[counter] = "accuracy_ptva_"+sets[op];
			counter++;
			files[counter] = dir1+"_"+sets[op]+"_rank.txt";
			names[counter] = "rank_ptva_"+sets[op];
			counter++;
			files[counter] = dir1+"_"+sets[op]+"_distance.txt";
			names[counter] = "distance_ptva_"+sets[op];
			counter++;
		}
		for(int op=0; op<sets.length; op++) {
			files[counter] = dir2+"_"+sets[op]+"_accuracy.txt";
			names[counter] = "accuracy_gmla_"+sets[op];
			counter++;
			files[counter] = dir2+"_"+sets[op]+"_rank.txt";
			names[counter] = "rank_gmla_"+sets[op];
			counter++;
			files[counter] = dir2+"_"+sets[op]+"_distance.txt";
			names[counter] = "distance_gmla_"+sets[op];
			counter++;
		}			
		MultiExpressWriter writer = new MultiExpressWriter(files, names);
		writer.initAll();
		writer.printlnAll(header);
		
		// Main loop
		long tstart = System.currentTimeMillis();
		counter = 0;
		int realizations = numberOfTestsPerNode*ucirving.getNumberOfNodes();
		for(int i=0; i<numberOfTestsPerNode; i++) {
			for(Node source : ucirving.getNodesList()) {
				for(int r=0; r<sigma.length; r++) {
					
					rate = 1-sigma[r]*sigma[r];
					
					si = new SusceptibleInfected(ucirving);
					si.setOneSource(source);
					si.setObservers(ucirving.getNodesList());
					si.setInfectionRate(rate);
					si.runUntilAllObserversInfected();
					
					observers.set(0,ucirving.getRandomNodes(numberOfObservers));
					
					for(int s=0; s<sets.length; s++) {
						//PTVA-LI
						detector = new SingleSourceDetector(ucirving);
						detector.setObservers(observers.get(s));
						detector.setDelays(si.getInfectionTimesForGivenObservers(observers.get(s)));
						detector.setDelayMean(1/rate);
						detector.setDelayVariance((1-rate)/(rate*rate));
						detector.setMaxThreads(numberOfThreads);
						detector.setSupposedSources(ucirving.getNodesList());
						detector.computeScores("LPTV");
						supposedSourcesId = detector.findNodesWithHighestScores();
						if(supposedSourcesId.contains(source.getId())) 
							writer.printf("accuracy_ptva_"+sets[s],"%.3f\t",1.0/supposedSourcesId.size());
						else writer.printf("accuracy_ptva_"+sets[s],"0.000\t");
						writer.printf("rank_ptva_"+sets[s],"%d\t",detector.findRankOfNode(source.getId()));
						writer.printf("distance_ptva_"+sets[s],"%.3f\t", GraphTools.shortestPathLength(ucirving, source.getId(), supposedSourcesId));

						//GMLA
						detector = new SingleSourceDetector(ucirving);
						detector.setObservers(observers.get(s));
						detector.setDelays(si.getInfectionTimesForGivenObservers(observers.get(s)));
						detector.setDelayMean(1/rate);
						detector.setDelayVariance((1-rate)/(rate*rate));
						detector.setMaxThreads(numberOfThreads);
						detector.setNumberOfGradientWalks(1);
						detector.setNumberOfNearestObservers(numberOfNearestObservers);
						detector.computeScores("LPTV");
						supposedSourcesId = detector.findNodesWithHighestScores();
						if(supposedSourcesId.contains(source.getId())) 
							writer.printf("accuracy_gmla_"+sets[s],"%.3f\t",1.0/supposedSourcesId.size());
						else writer.printf("accuracy_gmla_"+sets[s],"0.000\t");
						writer.printf("rank_gmla_"+sets[s],"%d\t",detector.findRankOfNode(source.getId()));
						writer.printf("distance_gmla_"+sets[s],"%.3f\t", GraphTools.shortestPathLength(ucirving, source.getId(), supposedSourcesId));
					}
				}
				writer.newLineAll();
				counter = counter + 1;
				System.out.println(estimateTime(System.currentTimeMillis()-tstart, counter, realizations));
			}	
		}
		writer.closeAll();
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}

}
