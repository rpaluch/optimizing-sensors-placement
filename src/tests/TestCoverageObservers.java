package tests;

import java.util.ArrayList;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Node;
import graphs.NodesSelectors;
import io.ExpressWriter;
import io.GraphReader;

public class TestCoverageObservers {

	public static void main(String[] args) {
		//String networkPath = "/home/rpaluch/Workspace/ReverseEngineering/data/Rovira/arenas-email/rovira.txt";
		int size = 500;
		int m = 4;
		int budget = 25;
		
		Graph graph = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
		//Graph graph = GraphGenerator.barabasiAlbert(m+2, m, size);

				
		/* Read the network from the file
		GraphReader reader = new GraphReader(networkPath);
		Graph graph = reader.readGraphFromEdgeList();
		graph.printStats();
		System.out.println("Graph ready.");
		*/
		
		ArrayList<Node> observersNew = NodesSelectors.greedyCoverageObservers(graph, budget);
		ArrayList<Node> observersOld = NodesSelectors.greedyCoverageObsParallel(graph, budget, 4);
		
		ExpressWriter writer = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/testCoverageObservers/test1.txt");
		writer.init();
		for(int i=0; i<budget; i++) writer.printf("%d\t%d\n", observersNew.get(i).getId(), observersOld.get(i).getId());
		writer.close();
		
		Node node = GraphTools.highestDegreeNodes(graph, 1).get(0);
		ArrayList<Node> dominatedNew = GraphTools.findDominatedNodes(graph, observersNew);
		ArrayList<Node> dominatedOld = GraphTools.findDominatedNodes(graph, observersOld);
		System.out.printf("%d\t%d\n", dominatedNew.size(), dominatedOld.size());
		System.out.printf("%d\t%d\n", score2(graph, observersNew), score2(graph, observersOld));
		System.out.println(observersOld.indexOf(node));
	}
	
	public static int score(Graph graph, ArrayList<Node> observers) {
		int score = 0;
		for(Node n : graph.getNodesList()) {
			for(Node obs : observers)
				if(n.getNeighbours().contains(obs))
					score = score + 1;
		}
		return score;
	}
	
	public static int score2(Graph graph, ArrayList<Node> observers) {
		int score = 0;
		int counter = 0;
		for(Node n : graph.getNodesList()) {
			counter = 0;
			for(Node obs : observers)
				if(n.getNeighbours().contains(obs))
					counter = counter + 1;
			if(counter>=1)
				score = score + 1;
		}
		return score;
	}

}
