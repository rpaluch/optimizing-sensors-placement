package tests;

import io.ExpressWriter;
import java.util.ArrayList;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Node;

public class TestGraph {

	public static void main(String[] args) {
		
		ExpressWriter degree_common = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/compare_sets_er_N2000/mds_degree_common.txt");
		ExpressWriter random_common = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/compare_sets_er_N2000/mds_random_common.txt");
		ExpressWriter dominated_by_mds = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/compare_sets_er_N2000/dominated_by_mds.txt");
		ExpressWriter dominated_by_degree = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/compare_sets_er_N2000/dominated_by_degree.txt");
		ExpressWriter dominated_by_random = new ExpressWriter("/home/rpaluch/Workspace/ReverseEngineering/outputs/compare_sets_er_N2000/dominated_by_random.txt");

		int real = 500;
		long tstart;
		
		Graph graph;
		int size = 2000;
		int[] k={4,6,8,10,12};
		ArrayList<Node> mds;
		ArrayList<Node> degree;
		ArrayList<Node> random;
		ArrayList<Node> dominated;
		ArrayList<Node> commonPart;
		
		degree_common.init();
		random_common.init();
		dominated_by_mds.init();
		dominated_by_degree.init();
		dominated_by_random.init();
		degree_common.println("k4\tk6\tk8\tk10\tk12");
		random_common.println("k4\tk6\tk8\tk10\tk12");
		dominated_by_mds.println("k4\tk6\tk8\tk10\tk12");
		dominated_by_degree.println("k4\tk6\tk8\tk10\tk12");
		dominated_by_random.println("k4\tk6\tk8\tk10\tk12");
		tstart = System.currentTimeMillis();
		for(int r=0; r<real; r++) {
			for(int i=0; i<k.length; i++){
				graph = GraphGenerator.contiguousErdosRenyi(size, k[i]*size/2);
				mds = GraphTools.greedyMDS(graph);
				degree = GraphTools.highestDegreeNodes(graph, mds.size());
				random = graph.getRandomNodes(mds.size());
				commonPart = new ArrayList<Node>(mds);
				commonPart.retainAll(degree);
				degree_common.printf("%.3f\t", 1.0*commonPart.size()/mds.size());
				commonPart = new ArrayList<Node>(mds);
				commonPart.retainAll(random);
				random_common.printf("%.3f\t", 1.0*commonPart.size()/mds.size());
				dominated = GraphTools.findDominatedNodes(graph, mds);
				dominated_by_mds.printf("%.3f\t", 1.0*dominated.size()/size);
				dominated = GraphTools.findDominatedNodes(graph, degree);
				dominated_by_degree.printf("%.3f\t", 1.0*dominated.size()/size);
				dominated = GraphTools.findDominatedNodes(graph, random);
				dominated_by_random.printf("%.3f\t", 1.0*dominated.size()/size);
			}
			degree_common.printf("\n");
			random_common.printf("\n");
			dominated_by_mds.printf("\n");
			dominated_by_degree.printf("\n");
			dominated_by_random.printf("\n");
			System.out.printf("%d %.0f s\n", r+1, 0.001*(System.currentTimeMillis()-tstart));
		}
		degree_common.close();
		random_common.close();
		dominated_by_mds.close();
		dominated_by_degree.close();
		dominated_by_random.close();
	}
}
