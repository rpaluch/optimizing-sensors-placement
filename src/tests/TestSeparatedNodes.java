package tests;

import java.util.ArrayList;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.Node;

public class TestSeparatedNodes {

	public static void main(String[] args) {
		
		int realizations = 10;
		int size = 1000;	
		int m = 3;
		int degreeOfSeparation = 3;
		ArrayList<Node> separatedNodes;
		//ArrayList<Integer> observersID;
		//ArrayList<LinkedList<Node>> allPaths;
		Graph g;
		//Graph g = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
		//Graph g = GraphGenerator.barabasiAlbert(m+2, m, size);
		
		for(int r=0; r<realizations; r++) {
			g = GraphGenerator.contiguousErdosRenyi(size, 2*m, "degree");
			//g = GraphGenerator.barabasiAlbert(m+2, m, size);
			separatedNodes = GraphTools.findSeparatedNodesByDegree(g, degreeOfSeparation);
			System.out.printf("%d\n", separatedNodes.size());
			/*
			observersID = new ArrayList<Integer>();
			for(Node n : separatedNodes)
				observersID.add(n.getId());
			// The matrix with distances between any two observers, except reference observer
			DoubleMatrix distances = DoubleMatrix.zeros(observersID.size(), observersID.size());
			for(int i=0; i<observersID.size(); i++){
				allPaths = GraphTools.shortestPaths(g, observersID.get(i), observersID);
				for(int j=0; j<observersID.size(); j++)
					distances.put(i,j,allPaths.get(j).size()-1);
			}
			for(int i=0; i<observersID.size()-1; i++) {
				for(int j=i+1; j<observersID.size(); j++) {
					System.out.printf("%.1f\n", distances.get(i,j));
				}
			}
			*/
		}
	}

}