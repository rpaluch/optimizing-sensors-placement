package tests;

import graphs.Graph;
import graphs.GraphGenerator;
import graphs.GraphTools;
import graphs.PathsTools;
import graphs.PathsRegister;

public class TestPathsRegister2 {

	public static void main(String[] args) {
		
		//long tstart;
			int[] size = {50,100,200,300,500,1000,2000};
			int k = 3;
			Graph graph;
			//PathsRegister register;
			PathsRegister limitedRegister;
			int realizations = 5;
			//int noPaths;
			int noLimitedPaths;
			
			for(int i=0; i<size.length; i++) {
								
				//noPaths = 0;
				noLimitedPaths = 0;
				
				for(int r=0; r<realizations; r++) {
					
					//tstart = System.currentTimeMillis();
					graph = GraphGenerator.barabasiAlbert(k+3, k, size[i]);
					//System.out.printf("%d\t", System.currentTimeMillis()-tstart);
					
					//tstart = System.currentTimeMillis();
					//register = GraphTools.allShortestPathsInGraph(graph);
					//System.out.printf("%d\t", System.currentTimeMillis()-tstart);
					//noPaths = noPaths + register.getTotalNumberOfSinglePaths();

					//tstart = System.currentTimeMillis();
					limitedRegister = PathsTools.allLimitedShortestPathsInGraph(graph, 5);
					//System.out.printf("%d\n", System.currentTimeMillis()-tstart);
					noLimitedPaths = noLimitedPaths + limitedRegister.getTotalNumberOfSinglePaths();
				}
				System.out.printf("%d\n", noLimitedPaths/realizations);

			}
	}
}
