package tests;

import java.util.ArrayList;

import graphs.Graph;
import graphs.GraphTools;
import graphs.Node;
import graphs.PathsTools;
import graphs.PathsRegister;
import io.ExpressWriter;
import io.GraphReader;

public class TestObserversPlacementGnutella {

	public static void main(String[] args) {
		
		double densityOfObservers = Double.parseDouble(args[0]);
		int numberOfThreads = Integer.parseInt(args[1]);
		String betweennessON = args[2];
		long tStartGlobal = System.currentTimeMillis();
		int maxLength = 3;
		if(densityOfObservers<0.077) {
			maxLength=5;
			if(densityOfObservers>0.074) maxLength=4;
		}
		ArrayList<Node> observersCoverage;
		ArrayList<Node> observersKMedian;
		ArrayList<Node> observersHV;
		ArrayList<Node> observersCB;
		ArrayList<Node> observersBC;
		
		long tStartLocal = System.currentTimeMillis();
		String dir = "../outputs/observers_placement_gnutella/gnutella08_O"+densityOfObservers;
		GraphReader reader = new GraphReader("../data/Gnutella/p2p-Gnutella08.txt");
		Graph gnutella = GraphTools.extractLargestComponent(reader.readGraphFromEdgeList());
		gnutella.printStats();
		int size = gnutella.getNumberOfNodes();
		int numberOfObservers = (int)(densityOfObservers*size);
		System.out.printf("Graph loaded in %.2f sec | Total time: %.2f sec\n", 0.001*(System.currentTimeMillis()-tStartLocal),
																			   0.001*(System.currentTimeMillis()-tStartGlobal));
		
		// Find all shortest paths
		tStartLocal = System.currentTimeMillis();
		PathsRegister register = PathsTools.allShortestPathsInGraph(gnutella);
		System.out.printf("All shortest paths founded in %.2f sec | Total time: %.2f sec\n", 0.001*(System.currentTimeMillis()-tStartLocal),
																							 0.001*(System.currentTimeMillis()-tStartGlobal));
		System.out.printf("%d shortest paths founded | %.2f GiB of memory used.\n", register.getTotalNumberOfSinglePaths(),
																					1.0*Runtime.getRuntime().totalMemory()/(1024*1024*1024));
					
		// Coverage
		tStartLocal = System.currentTimeMillis();
		observersCoverage = GraphTools.greedyCoverageObsParallel(gnutella, numberOfObservers, numberOfThreads);
		System.out.printf("Coverage computed in %.2f sec | Total time: %.2f sec\n", 0.001*(System.currentTimeMillis()-tStartLocal),
				 																	0.001*(System.currentTimeMillis()-tStartGlobal));
		ExpressWriter writerCoverage = new ExpressWriter(dir+"_Coverage.txt");
		writerCoverage.init();
		for(int i=0; i<numberOfObservers;i++) writerCoverage.printf("%d\n", observersCoverage.get(i).getId());
		writerCoverage.close();
		
		// KMedian
		tStartLocal = System.currentTimeMillis();
		observersKMedian = GraphTools.greedyKMedianObsParallel(gnutella, PathsTools.allDistancesInGraph(gnutella, register), numberOfObservers, numberOfThreads);
		System.out.printf("K-Median computed in %.2f sec | Total time: %.2f sec\n", 0.001*(System.currentTimeMillis()-tStartLocal),
																					0.001*(System.currentTimeMillis()-tStartGlobal));
		ExpressWriter writerKMedian = new ExpressWriter(dir+"_KMedian.txt");
		writerKMedian.init();
		for(int i=0; i<numberOfObservers;i++) writerKMedian.printf("%d\n", observersKMedian.get(i).getId());
		writerKMedian.close();
		
		if(betweennessON.equals("true")) {
			// Collective Betweenness
			tStartLocal = System.currentTimeMillis();
			observersCB = GraphTools.fastCollectiveBetweenness(gnutella, register, numberOfObservers);
			System.out.printf("CB computed in %.2f sec | Total time: %.2f sec\n", 0.001*(System.currentTimeMillis()-tStartLocal),
					  															  0.001*(System.currentTimeMillis()-tStartGlobal));
			ExpressWriter writerCB = new ExpressWriter(dir+"_CB.txt");
			writerCB.init();
			for(int i=0; i<numberOfObservers;i++) writerCB.printf("%d\n", observersCB.get(i).getId());
			writerCB.close();
			
			// Betweenness Centrality
			tStartLocal = System.currentTimeMillis();
			GraphTools.betweennessCentrality(gnutella, register);
			observersBC = GraphTools.highestBetweennessNodes(gnutella, numberOfObservers);
			System.out.printf("BC computed in %.2f sec | Total time: %.2f sec\n", 0.001*(System.currentTimeMillis()-tStartLocal),
																				  0.001*(System.currentTimeMillis()-tStartGlobal));
			ExpressWriter writerBC = new ExpressWriter(dir+"_BC.txt");	
			writerBC.init();
			for(int i=0; i<numberOfObservers;i++) writerBC.printf("%d\n", observersBC.get(i).getId());
			writerBC.close();
		}
		
		// High-Variance
		tStartLocal = System.currentTimeMillis();
		observersHV = GraphTools.greedyHVObsParallel(gnutella, register, numberOfObservers, maxLength, numberOfThreads);
		System.out.printf("HV computed in %.2f sec | Total time: %.2f sec\n", 0.001*(System.currentTimeMillis()-tStartLocal),
																			  0.001*(System.currentTimeMillis()-tStartGlobal));
		ExpressWriter writerHV = new ExpressWriter(dir+"_HV.txt");
		writerHV.init();
		for(int i=0; i<numberOfObservers;i++) writerHV.printf("%d\n", observersHV.get(i).getId());
		writerHV.close();
	}
}