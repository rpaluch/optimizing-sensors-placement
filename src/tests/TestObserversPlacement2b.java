package tests;

import java.util.ArrayList;

import tracking.ParallelPinto2;
import graphs.Graph;
import graphs.GraphTools;
import graphs.Node;
import graphs.PathsTools;
import io.ExpressWriter;
import io.GraphReader;
import graphs.PathsRegister;
import dynamics.SusceptibleInfected;

public class TestObserversPlacement2b {

	public static void main(String[] args) {
		
		int sourceID = 9;
		String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/observers_placement/pinto_er_N10_k4_beta0.5_";
		ExpressWriter writer = new ExpressWriter(dir+"source"+sourceID+"_accuracy.txt");
		writer.init();
		writer.println("id1\tid2\tid3\taccur\tdegree\tbetween\thvobs\tkmedian\tcover");
		
		GraphReader reader = new GraphReader("/home/rpaluch/Workspace/ReverseEngineering/data/er_N10_k4.net");
		Graph g = reader.readGraphFromEdgeList();
		//g.printAll();
		int size = g.getNumberOfNodes();
		PathsRegister register = PathsTools.allShortestPathsInGraph(g);
		/*
		for(int i=0; i<size-1; i++)
			for(int j=i+1; j<size; j++)
				System.out.println(register.getPathsAsString(i, j));
		*/
		GraphTools.betweennessCentrality(g, register);
		GraphTools.closenessCentrality(g, register);
		
		ArrayList<ArrayList<Node>> observersSets = new ArrayList<ArrayList<Node>>();
		for(int i=0; i<size-2; i++)
			for(int j=i+1; j<size-1; j++)
				for(int k=j+1; k<size; k++){
					ArrayList<Node> newSet = new ArrayList<Node>();
					newSet.add(g.getNode(i));
					newSet.add(g.getNode(j));
					newSet.add(g.getNode(k));
					observersSets.add(newSet);
				}
		int observersSetsSize = observersSets.size();
		double[] observersAccuracy = new double[observersSetsSize];
		double acc;
		
		SusceptibleInfected si;
		double rate = 0.5;
		ArrayList<Node> observers;
		
		ParallelPinto2 pinto = null;
		ArrayList<Integer> supposedSourcesId;
		int numberOfThreads = 10;
		
		int testPerSource = 100;
		int realizations = observersSetsSize*testPerSource;
		int r=0;
		
		long tstart = System.currentTimeMillis();
		for(int i=0; i<observersSetsSize; i++) {
			observers = observersSets.get(i);
			for(int t=0; t<testPerSource; t++) {
				
				si = new SusceptibleInfected(g);
				si.setObservers(observers);
				si.setOneSource(g.getNode(sourceID));
				si.setInfectionRate(rate);
				si.runUntilAllObserversInfected();
				
				pinto = new ParallelPinto2(g);
				pinto.setObservers(si.getObservers());
				pinto.setDelays(si.getInfectionTimeAsDouble());
				pinto.setSupposedSources(g.getNodesList());	
				pinto.setDelayMean(1/rate);
				pinto.setDelayVariance((1-rate)/(rate*rate));
				pinto.setNumberOfThreads(numberOfThreads);
				pinto.estimateScoresForNodes();
				supposedSourcesId = pinto.findNodesWithHighestScores();
				if(supposedSourcesId.contains(sourceID)) acc = 1.0/supposedSourcesId.size();
				else acc = 0.0;
				observersAccuracy[i] = observersAccuracy[i] + acc;
				
				r = r + 1;
				System.out.println(estimateTime(System.currentTimeMillis()-tstart, r+1, realizations));
			}
			
			observersAccuracy[i] = observersAccuracy[i]/testPerSource;
			Node obs1 = observers.get(0);
			Node obs2 = observers.get(1);
			Node obs3 = observers.get(2);
			writer.printf("%d\t%d\t%d\t%.3f\t%d\t%.3f\t%d\t%d\t%d\n", obs1.getId(),obs2.getId(),obs3.getId(),
																		   		  observersAccuracy[i],
																		   		  obs1.getDegree()+obs2.getDegree()+obs3.getDegree(),
																		   		  obs1.getBetweenness()+obs2.getBetweenness()+obs3.getBetweenness(),
																		   		  GraphTools.examinePathCovering(g, register, observers, 3),
																		   		  GraphTools.examineKMedianSet(g, register, observers),
																		   		  GraphTools.findDominatedNodes(g, observers).size());
		}
		writer.close();
	}
	
	public static String estimateTime(long elapsed, int currentReal, int totalReal) {
		long meanTime = elapsed/currentReal;
		long remainingTime = meanTime*(totalReal-currentReal);
		long second = (remainingTime / 1000) % 60;
		long minute = (remainingTime / (1000 * 60)) % 60;
		long hour = (remainingTime / (1000 * 60 * 60)) % 24;
		long day = (remainingTime / (1000 * 60 * 60 * 24));
		return String.format("%d/%d: elapsed %.0fs (%.1fh), remaining %02d:%02d:%02d:%02d", currentReal, 
																					   		totalReal, 
																					   		0.001*elapsed, 
																					   		0.001*elapsed/3600,
																					   		day,
																					   		hour, 
																					   		minute, 
																					   		second);
	}
}
