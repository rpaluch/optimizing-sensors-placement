package tests;

import java.util.ArrayList;

import graphs.Graph;
import graphs.GraphTools;
import graphs.Node;
import graphs.NodesSelectors;
import graphs.PathsTools;
import io.ExpressWriter;
import io.GraphReader;
import io.MemoryUsage;

public class TestObserversPlacementGnutella2 {

	public static void main(String[] args) {
		
		double densityOfObservers = 0.05;
		int numberOfThreads = 16;
		long tStartGlobal = System.currentTimeMillis();
		int maxLength = 3;
		if(densityOfObservers<0.077) {
			maxLength=5;
			if(densityOfObservers>0.074) maxLength=4;
		}
		ArrayList<Node> observersCoverage;
		ArrayList<Node> observersKMedian;
		ArrayList<Node> observersHV;
		ArrayList<Node> observersCB;
		ArrayList<Node> observersBC;
		
		long tStartLocal = System.currentTimeMillis();
		String dir = "/home/rpaluch/Workspace/ReverseEngineering/outputs/observers_placement_gnutella/gnutella08_O"+densityOfObservers;
		GraphReader reader = new GraphReader("/home/rpaluch/Workspace/ReverseEngineering/data/Gnutella/p2p-Gnutella08.txt");
		Graph gnutella = GraphTools.extractLargestComponent(reader.readGraphFromEdgeList());
		gnutella.printStats();
		System.out.printf("Diameter = %d\n", GraphTools.diameter(gnutella));
		int size = gnutella.getNumberOfNodes();
		int numberOfObservers = (int)(densityOfObservers*size);
		System.out.printf("Graph loaded in %.2f sec | Total time: %.2f sec\n", 0.001*(System.currentTimeMillis()-tStartLocal),
																			   0.001*(System.currentTimeMillis()-tStartGlobal));
		/*			
		// Coverage
		tStartLocal = System.currentTimeMillis();
		observersCoverage = NodesSelectors.greedyCoverageObsParallel(gnutella, numberOfObservers, numberOfThreads);
		System.out.printf("Coverage computed in %.2f sec | Total time: %.2f sec\n", 0.001*(System.currentTimeMillis()-tStartLocal),
				 																	0.001*(System.currentTimeMillis()-tStartGlobal));
		ExpressWriter writerCoverage = new ExpressWriter(dir+"_Coverage.txt");
		writerCoverage.init();
		for(int i=0; i<numberOfObservers;i++) writerCoverage.printf("%d\n", observersCoverage.get(i).getId());
		writerCoverage.close();
		
		// KMedian
		tStartLocal = System.currentTimeMillis();
		observersKMedian = NodesSelectors.greedyKMedianObsParallel(gnutella, PathsTools.allDistancesInGraph(gnutella), numberOfObservers, numberOfThreads);
		System.out.printf("K-Median computed in %.2f sec | Total time: %.2f sec\n", 0.001*(System.currentTimeMillis()-tStartLocal),
																					0.001*(System.currentTimeMillis()-tStartGlobal));
		ExpressWriter writerKMedian = new ExpressWriter(dir+"_KMedian.txt");
		writerKMedian.init();
		for(int i=0; i<numberOfObservers;i++) writerKMedian.printf("%d\n", observersKMedian.get(i).getId());
		writerKMedian.close();
		
		
		// Collective Betweenness
		MemoryUsage mem = new MemoryUsage();
		mem.start();
		tStartLocal = System.currentTimeMillis();
		observersCB = NodesSelectors.fastCollectiveBetweenness(gnutella, numberOfObservers,maxLength);
		mem.stopMeasurement();
		System.out.printf("CB computed in %.2f sec | Total time: %.2f sec | Memory: %d MB\n", 0.001*(System.currentTimeMillis()-tStartLocal),
				  															  			   	  0.001*(System.currentTimeMillis()-tStartGlobal),
				  															  			      mem.getDiffMemoryUsage());
		ExpressWriter writerCB = new ExpressWriter(dir+"_CB.txt");
		writerCB.init();
		for(int i=0; i<numberOfObservers;i++) writerCB.printf("%d\n", observersCB.get(i).getId());
		writerCB.close();
		*/
		// Betweenness Centrality
		tStartLocal = System.currentTimeMillis();
		GraphTools.computeBetweennessCentrality(gnutella);
		observersBC = GraphTools.highestBetweennessNodes(gnutella, numberOfObservers);
		System.out.printf("BC computed in %.2f sec | Total time: %.2f sec\n", 0.001*(System.currentTimeMillis()-tStartLocal),
																			  0.001*(System.currentTimeMillis()-tStartGlobal));
		ExpressWriter writerBC = new ExpressWriter(dir+"_BC.txt");	
		writerBC.init();
		for(int i=0; i<numberOfObservers;i++) writerBC.printf("%d\n", observersBC.get(i).getId());
		writerBC.close();
		
		/*
		// High-Variance
		tStartLocal = System.currentTimeMillis();
		observersHV = NodesSelectors.greedyHVObsParallel(gnutella, numberOfObservers, maxLength, numberOfThreads);
		System.out.printf("HV computed in %.2f sec | Total time: %.2f sec\n", 0.001*(System.currentTimeMillis()-tStartLocal),
																			  0.001*(System.currentTimeMillis()-tStartGlobal));
		ExpressWriter writerHV = new ExpressWriter(dir+"_HV.txt");
		writerHV.init();
		for(int i=0; i<numberOfObservers;i++) writerHV.printf("%d\n", observersHV.get(i).getId());
		writerHV.close();
		*/
	}
}